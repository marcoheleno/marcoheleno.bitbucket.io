

window.onload = function() 
{
  set_sections_to_window_innerHeight();
  get_colors_from_CSS_file();
  indexScrollTo (0, 300);
}


window.addEventListener ("resize", function() 
{
  set_sections_to_window_innerHeight();
});


function set_sections_to_window_innerHeight() 
{
  document.body.style.height = window.innerHeight + "px";
  document.getElementsByTagName("section")[0].style.height = window.innerHeight-50 + "px";
  document.getElementsByTagName("section")[1].style.height = window.innerHeight-50 + "px";
}


function get_colors_from_CSS_file()
{
  // See style.css
  color_background = getComputedStyle(document.documentElement).getPropertyValue("--color_background");
  color_foreground = getComputedStyle(document.documentElement).getPropertyValue("--color_foreground");
}


function indexScrollTo (x, y) 
{
  if (window.location.href === "https://marcoheleno.bitbucket.io/p5js/index.html") 
  {
    window.scrollTo (x, y);
  }
}


document.addEventListener("DOMContentLoaded", function() 
{
  fetch_code_location();
});


function fetch_code_location() 
{
  const url_query = window.location.search;

  if (url_query != "") 
  {
    const url_param = new URLSearchParams (url_query);
    const url_param_val = url_param.get('code');

    let url_script_element = document.createElement("script");
    url_script_element.setAttribute("src", "codes/"+url_param_val+"/"+url_param_val+".js");
    document.head.appendChild (url_script_element);

    url_script_element = document.createElement("script");
    url_script_element.setAttribute("src", "codes/"+url_param_val+"/explainer.js");
    document.head.appendChild (url_script_element);

    load_and_parse_code ("codes/"+url_param_val+"/"+url_param_val+".js");
  }
}


function load_and_parse_code (url) 
{
  fetch(url)

    .then(function (response) 
    {
      response.text().then(function (text_string) 
      {
        //console.log (text_string);
        
        for (let i=0; i<text_string.length; i++) 
        {
          if (text_string[i] === "/" && text_string[i+1] === "*") 
          {
            text_string = text_string.replace("/*", "");
            i-=2;
          }
          if (text_string[i] === "*" && text_string[i+1] === "/") 
          {
            text_string = text_string.replace("*/", "");
            i-=2;
          }
        }
        
        //console.log (text_string);

        let text_array = text_string.split(/\r?\n/);
        //console.log (text_array);

        document.getElementsByTagName("section")[0].appendChild (document.createElement("code"));

        for (let i=0; i<text_array.length; i++) 
        {
          document.getElementsByTagName("code")[0].appendChild (document.createElement("p"));
          document.getElementsByTagName("p")[i].innerHTML = text_array[i];
        }
      })
    })
    
    .catch(function (err) 
    {
      console.log("Something went wrong!", err);
    });
}


function set_p5_canvas_to (w, h) 
{
  if (w===undefined || h===undefined) 
  {
    let p5_containerSize = 
    {
      w: document.getElementsByTagName("section")[1].offsetWidth-1,
      h: document.getElementsByTagName("section")[1].offsetHeight-1
    };

    resizeCanvas (p5_containerSize.w, p5_containerSize.h);
  }

  document.getElementsByTagName("section")[1].appendChild(document.getElementById("defaultCanvas0"));
}

