/*<span>Code based on:</span>*/
/*<span><a target="_blank" href="https://marcoheleno.bitbucket.io/p5js/index.html?code=array_random_names">https://marcoheleno.bitbucket.io/p5js/index.html?code=array_random_names</a></span>*/
/*<br>*/
let names, verbs, nouns, locations, random_phrase;
let text_size, num_lines, line_num, name_index, font;
/*<br>*/
function preload()
{
/*&nbsp;&nbsp;*/font = loadFont (/*<span class=hide>*/"codes/array_random_phrases/" + /*</span>*/"IBMPlexMono-Regular.ttf");
}
/*<br>*/
function setup()
{
/*&nbsp;&nbsp;*/createCanvas (windowWidth, windowHeight);
/*&nbsp;&nbsp;*/frameRate (1);
/*<br>*/
/*&nbsp;&nbsp;*/names = [];
/*&nbsp;&nbsp;*/names[0] = "John";
/*&nbsp;&nbsp;*/names[1] = "Mike";
/*&nbsp;&nbsp;*/names[2] = "Susy";
/*&nbsp;&nbsp;*/names[3] = "Kate";
/*<br>*/
/*&nbsp;&nbsp;*/verbs = [];
/*&nbsp;&nbsp;*/verbs[0] = "went";
/*&nbsp;&nbsp;*/verbs[1] = "fetched";
/*&nbsp;&nbsp;*/verbs[2] = "sang";
/*&nbsp;&nbsp;*/verbs[3] = "tossed";
/*<br>*/
/*&nbsp;&nbsp;*/nouns = [];
/*&nbsp;&nbsp;*/nouns[0] = "to the car";
/*&nbsp;&nbsp;*/nouns[1] = "the pen";
/*&nbsp;&nbsp;*/nouns[2] = "a melody";
/*&nbsp;&nbsp;*/nouns[3] = "a stick";
/*<br>*/
/*&nbsp;&nbsp;*/locations = [];
/*&nbsp;&nbsp;*/locations[0] = "in the garage.";
/*&nbsp;&nbsp;*/locations[1] = "in the office.";
/*&nbsp;&nbsp;*/locations[2] = "by the pool.";
/*&nbsp;&nbsp;*/locations[3] = "to the backyard.";
/*<br>*/
/*&nbsp;&nbsp;*/line_num = 1;
/*&nbsp;&nbsp;*/background (255);
}
/*<br>*/
function draw()
{
/*<span class=hide>*/if(frameCount===1)/*</span>*/
/*<span class=hide>*/{/*</span>*/
/*<span class=hide>*/set_p5_canvas_to();/*</span>*/
/*<span class=hide>*/background (255);/*</span>*/
/*<span class=hide>*/}/*</span>*/
/*&nbsp;&nbsp;*/text_size = 20;
/*&nbsp;&nbsp;*/num_lines = floor( height/text_size );
/*<br>*/
/*&nbsp;&nbsp;*/if (frameCount%num_lines === 0) 
/*&nbsp;&nbsp;*/{
/*&nbsp;&nbsp;&nbsp;&nbsp;*/background (255);
/*&nbsp;&nbsp;&nbsp;&nbsp;*/line_num = 1;
/*&nbsp;&nbsp;*/}
/*<br>*/
/*&nbsp;&nbsp;*/name_index = int( random(0, names.length) );
/*&nbsp;&nbsp;*/verb_index = int( random(0, verbs.length) );
/*&nbsp;&nbsp;*/noun_index = int( random(0, nouns.length) );
/*&nbsp;&nbsp;*/location_index = int( random(0, locations.length) );
/*<br>*/
/*&nbsp;&nbsp;*/random_phrase = names[name_index] +" "+ verbs[verb_index] +" "+ nouns[noun_index] +" "+ locations[location_index];
/*<br>*/
/*&nbsp;&nbsp;*/textFont (font);
/*&nbsp;&nbsp;*/textSize (text_size);
/*&nbsp;&nbsp;*/text (random_phrase, 10, line_num*text_size);
/*&nbsp;&nbsp;*/line_num++;
}
/*<br>*/
/*<span class>function windowResized()</span>*/
/*<span class>{</span>*/
/*&nbsp;&nbsp;*//*<span class>resizeCanvas (windowWidth, windowHeight);</span>*/
/*<span class>}</span>*/