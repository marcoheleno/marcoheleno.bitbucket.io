let x1, x2, y, size, RGB1, RGB2, distance;
/*<br>*/
function setup()
{
/*&nbsp;&nbsp;*/createCanvas (windowWidth, windowHeight);
}
/*<br>*/
function draw()
{
/*<span class=hide>*/if (frameCount===1) set_p5_canvas_to();/*</span>*/
/*<span class=hide>*/clear();/*</span>*/
/*&nbsp;&nbsp;*/x1 = (width/4)*1;
/*&nbsp;&nbsp;*/x2 = (width/4)*3;
/*&nbsp;&nbsp;*/y = height/2;
/*&nbsp;&nbsp;*/size = 200;
/*&nbsp;&nbsp;*/noStroke();
/*<br>*/
/*&nbsp;&nbsp;*/if (mouseX > x1-size/2 && 
/*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;*/mouseX < x1+size/2 && 
/*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;*/mouseY > y-size/2 && 
/*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;*/mouseY < y+size/2 && 
/*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;*/mouseIsPressed === true) 
/*&nbsp;&nbsp;*/{
/*&nbsp;&nbsp;&nbsp;&nbsp;*/RGB1 = color (0, 255, 0);
/*&nbsp;&nbsp;*/}
/*&nbsp;&nbsp;*/else 
/*&nbsp;&nbsp;*/{
/*&nbsp;&nbsp;&nbsp;&nbsp;*/RGB1 = color (255, 0, 0);
/*&nbsp;&nbsp;*/}
/*&nbsp;&nbsp;*/fill (RGB1);
/*&nbsp;&nbsp;*/rectMode (CENTER);
/*&nbsp;&nbsp;*/rect (x1, y, size, size);
/*<br>*/
/*&nbsp;&nbsp;*/distance = dist (mouseX, mouseY, x2, y);
/*&nbsp;&nbsp;*/if (distance<=size/2 && mouseIsPressed===true) 
/*&nbsp;&nbsp;*/{
/*&nbsp;&nbsp;&nbsp;&nbsp;*/RGB2 = color (0, 255, 0);
/*&nbsp;&nbsp;*/}
/*&nbsp;&nbsp;*/else 
/*&nbsp;&nbsp;*/{
/*&nbsp;&nbsp;&nbsp;&nbsp;*/RGB2 = color (255, 0, 0);
/*&nbsp;&nbsp;*/}
/*&nbsp;&nbsp;*/fill (RGB2);
/*&nbsp;&nbsp;*/ellipse (x2, y, size, size);
}
/*<br>*/
/*<span class>function windowResized()</span>*/
/*<span class>{</span>*/
/*&nbsp;&nbsp;*//*<span class>resizeCanvas (windowWidth, windowHeight);</span>*/
/*<span class>}</span>*/