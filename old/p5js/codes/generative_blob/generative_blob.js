/*<span>Code based on:</span>*/
/*<span><a target="_blank" href="https://marcoheleno.bitbucket.io/p5js/index.html?code=array_color_snake">https://marcoheleno.bitbucket.io/p5js/index.html?code=array_color_snake</a></span>*/
/*<br>*/
let x, y, num_ellipses, color_ellipses, speed;
/*<br>*/
function setup()
{
/*&nbsp;&nbsp;*/createCanvas (windowWidth, windowHeight);
/*&nbsp;&nbsp;*/colorMode (HSB, 360, 100, 100, 100);
/*<br>*/
/*&nbsp;&nbsp;*/x = [];
/*&nbsp;&nbsp;*/y = [];
/*&nbsp;&nbsp;*/num_ellipses = 50;
/*&nbsp;&nbsp;*/speed = 20;
/*<br>*/
/*&nbsp;&nbsp;*/for (let i=0; i<num_ellipses; i++) /*<span>&lt;num_ellipses; i++) </span>*/
/*&nbsp;&nbsp;*/{
/*&nbsp;&nbsp;&nbsp;&nbsp;*/x[i] = width/2;
/*&nbsp;&nbsp;&nbsp;&nbsp;*/y[i] = height/2;
/*&nbsp;&nbsp;*/}
/*<br>*/
/*&nbsp;&nbsp;*/noStroke();
/*&nbsp;&nbsp;*/background (0, 0, 0);
}
/*<br>*/
function draw()
{
/*<span class=hide>*/if(frameCount===1) set_p5_canvas_to();/*</span>*/
/*&nbsp;&nbsp;*/x[x.length-1] += random (-speed, speed);
/*&nbsp;&nbsp;*/y[y.length-1] += random (-speed, speed);
/*<br>*/
/*&nbsp;&nbsp;*/if (x[x.length-1] < 0) x[x.length-1] += num_ellipses;
/*&nbsp;&nbsp;*/if (x[x.length-1] > width) x[x.length-1] -= num_ellipses;
/*&nbsp;&nbsp;*/if (y[y.length-1] < 0) y[y.length-1] += num_ellipses;
/*&nbsp;&nbsp;*/if (y[y.length-1] > height) y[y.length-1] -= num_ellipses;
/*<br>*/
/*&nbsp;&nbsp;*/for (let i=0; i<num_ellipses-1; i++) /*<span>&lt;num_ellipses-1; i++) </span>*/
/*&nbsp;&nbsp;*/{
/*&nbsp;&nbsp;&nbsp;&nbsp;*/x[i] = x[i+1];
/*&nbsp;&nbsp;&nbsp;&nbsp;*/y[i] = y[i+1];
/*&nbsp;&nbsp;*/}
/*<br>*/
/*&nbsp;&nbsp;*/color_ellipses = color (random(360), 100, 100);
/*&nbsp;&nbsp;*/for (let i=0; i<num_ellipses; i++) /*<span>&lt;num_ellipses; i++) </span>*/
/*&nbsp;&nbsp;*/{
/*&nbsp;&nbsp;&nbsp;&nbsp;*/fill  (color_ellipses);
/*&nbsp;&nbsp;&nbsp;&nbsp;*/ellipse (x[i], y[i], i, i);
/*&nbsp;&nbsp;*/}
}
/*<br>*/
/*<span class>function windowResized()</span>*/
/*<span class>{</span>*/
/*&nbsp;&nbsp;*//*<span class>resizeCanvas (windowWidth, windowHeight);</span>*/
/*<span class>}</span>*/