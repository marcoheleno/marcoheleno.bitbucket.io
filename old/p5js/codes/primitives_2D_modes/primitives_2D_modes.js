function setup()
{
/*&nbsp;&nbsp;*/createCanvas (windowWidth, windowHeight);
}
/*<br>*/
function draw()
{
/*<span class=hide>*/if(frameCount===1) set_p5_canvas_to();/*</span>*/
/*<span class=hide>*/clear();/*</span>*/
/*<span class=hide>*/stroke (200);/*</span>*/
/*<span class=hide>*/line( (width/5)*0, 0, (width/5)*0, height);/*</span>*/
/*<span class=hide>*/line( (width/5)*1, 0, (width/5)*1, height);/*</span>*/
/*<span class=hide>*/line( (width/5)*2, 0, (width/5)*2, height);/*</span>*/
/*<span class=hide>*/line( (width/5)*3, 0, (width/5)*3, height);/*</span>*/
/*<span class=hide>*/line( (width/5)*4, 0, (width/5)*4, height);/*</span>*/
/*<span class=hide>*/line(0, (height/4)*0, width, (height/4)*0);/*</span>*/
/*<span class=hide>*/line(0, (height/4)*1, width, (height/4)*1);/*</span>*/
/*<span class=hide>*/line(0, (height/4)*2, width, (height/4)*2);/*</span>*/
/*<span class=hide>*/line(0, (height/4)*3, width, (height/4)*3);/*</span>*/
/*<span class=hide>*/stroke (0);/*</span>*/

/*<span class=hide>*/textAlign(CENTER, CENTER);/*</span>*/
/*<span class=hide>*/textFont("sans-serif");/*</span>*/

/*&nbsp;&nbsp;*/ellipseMode (CORNERS);
/*&nbsp;&nbsp;*/ellipse( (width/5)*0, (height/4)*0, (width/5)*1, (height/4)*1); // 1
/*<span class=hide>*/text("1", (width/5)*0.5, (height/4)*0.5);/*</span>*/

/*<br>*/
/*&nbsp;&nbsp;*/ellipseMode (CENTER);
/*&nbsp;&nbsp;*/ellipse( (width/5)*2, (height/4)*1, 100, 100); // 2
/*<span class=hide>*/text("2", (width/5)*2, (height/4)*1);/*</span>*/

/*<br>*/
/*&nbsp;&nbsp;*/ellipseMode (CORNER);
/*&nbsp;&nbsp;*/ellipse( (width/5)*3, (height/4)*1, 100, 100); // 3
/*<span class=hide>*/text("3", (width/5)*3+50, (height/4)*1+50);/*</span>*/

/*<br>*/
/*&nbsp;&nbsp;*/ellipseMode (RADIUS);
/*&nbsp;&nbsp;*/ellipse( (width/5)*4, (height/4)*1, 50, 50); // 4
/*<span class=hide>*/text("4", (width/5)*4, (height/4)*1);/*</span>*/

/*<br>*/
/*&nbsp;&nbsp;*/rectMode (CORNERS);
/*&nbsp;&nbsp;*/rect( (width/5)*0, (height/4)*2, (width/5)*1, (height/4)*3); // 5
/*<span class=hide>*/text("5", (width/5)*0.5, (height/4)*2.5);/*</span>*/

/*<br>*/
/*&nbsp;&nbsp;*/rectMode (CENTER);
/*&nbsp;&nbsp;*/rect( (width/5)*2, (height/4)*3, 100, 100); // 6
/*<span class=hide>*/text("6", (width/5)*2, (height/4)*3);/*</span>*/

/*<br>*/
/*&nbsp;&nbsp;*/rectMode (CORNER);
/*&nbsp;&nbsp;*/rect( (width/5)*3, (height/4)*3, 100, 100); // 7
/*<span class=hide>*/text("7", (width/5)*3+50, (height/4)*3+50);/*</span>*/

/*<br>*/
/*&nbsp;&nbsp;*/rectMode (RADIUS);
/*&nbsp;&nbsp;*/rect( (width/5)*4, (height/4)*3, 50, 50); // 8
/*<span class=hide>*/text("8", (width/5)*4, (height/4)*3);/*</span>*/
}
/*<br>*/
/*<span class>function windowResized()</span>*/
/*<span class>{</span>*/
/*&nbsp;&nbsp;*//*<span class>resizeCanvas (windowWidth, windowHeight);</span>*/
/*<span class>}</span>*/