let number_lines, number_columns;
let space_between_columns, space_between_lines;
let line_height;
/*<br>*/
function setup()
{
/*&nbsp;&nbsp;*/createCanvas (windowWidth, windowHeight);
/*<br>*/
/*&nbsp;&nbsp;*/number_lines = 20;
/*&nbsp;&nbsp;*/number_columns = number_lines;
/*&nbsp;&nbsp;*/line_height = 10;
/*&nbsp;&nbsp;*/strokeWeight (2);
}
/*<br>*/
function draw()
{
/*<span class=hide>*/if(frameCount===1) set_p5_canvas_to();/*</span>*/
/*&nbsp;&nbsp;*/background (255);
/*<br>*/
/*&nbsp;&nbsp;*/space_between_columns = width/number_columns;
/*&nbsp;&nbsp;*/space_between_lines = height/number_lines;
/*<br>*/
/*&nbsp;&nbsp;*/for (let x=1; x<number_columns; x++)/*<span>&lt;number_columns; x++) </span>*/
/*&nbsp;&nbsp;*/{
/*&nbsp;&nbsp;&nbsp;&nbsp;*/for (let y=1; y<number_lines; y++)/*<span>&lt;number_lines; y++) </span>*/
/*&nbsp;&nbsp;&nbsp;&nbsp;*/{
/*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;*/if (x===y || x+y===number_lines) 
/*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;*/{
/*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;*/stroke (0);
/*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;*/}
/*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;*/else 
/*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;*/{
/*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;*/stroke (200);
/*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;*/}
/*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;*/line(
/*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;*/space_between_columns*x, space_between_lines*y-line_height/2, 
/*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;*/space_between_columns*x, space_between_lines*y+line_height/2
/*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;*/);
/*&nbsp;&nbsp;&nbsp;&nbsp;*/}
/*&nbsp;&nbsp;*/}
}
/*<br>*/
/*<span class>function windowResized()</span>*/
/*<span class>{</span>*/
/*&nbsp;&nbsp;*//*<span class>resizeCanvas (windowWidth, windowHeight);</span>*/
/*<span class>}</span>*/