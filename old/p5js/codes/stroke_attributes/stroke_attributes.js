function setup()
{
/*&nbsp;&nbsp;*/createCanvas (windowWidth, windowHeight);
}
/*<br>*/
function draw()
{
/*<span class=hide>*/if(frameCount===1) set_p5_canvas_to();/*</span>*/
/*<span class=hide>*/clear();/*</span>*/
/*&nbsp;&nbsp;*/strokeWeight (1);
/*&nbsp;&nbsp;*/line (0, 0, width, height);
/*&nbsp;&nbsp;*/circle (51, 51, 100);
/*<br>*/
/*&nbsp;&nbsp;*/strokeWeight (10);
/*&nbsp;&nbsp;*/point (51, 51);
/*<br>*/
/*&nbsp;&nbsp;*/strokeCap (ROUND);
/*&nbsp;&nbsp;*/arc( (width/4)*3, (height/4), width/3, width/3, 0, HALF_PI);
/*&nbsp;&nbsp;*/arc( (width/4)*3, (height/4), width/3, width/3, PI + HALF_PI, TWO_PI);
}
/*<br>*/
/*<span class>function windowResized()</span>*/
/*<span class>{</span>*/
/*&nbsp;&nbsp;*//*<span class>resizeCanvas (windowWidth, windowHeight);</span>*/
/*<span class>}</span>*/