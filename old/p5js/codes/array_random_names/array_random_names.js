let names, text_size, num_lines, line_num, name_index, font;
/*<br>*/
function preload()
{
/*&nbsp;&nbsp;*/font = loadFont (/*<span class=hide>*/"codes/array_random_names/" + /*</span>*/"IBMPlexMono-Regular.ttf");
}
/*<br>*/
function setup()
{
/*&nbsp;&nbsp;*/createCanvas (windowWidth, windowHeight);
/*&nbsp;&nbsp;*/frameRate (2);
/*<br>*/
/*&nbsp;&nbsp;*/names = [];
/*&nbsp;&nbsp;*/names[0] = "John";
/*&nbsp;&nbsp;*/names[1] = "Mike";
/*&nbsp;&nbsp;*/names[2] = "Susy";
/*&nbsp;&nbsp;*/names[3] = "Kate";
/*<br>*/
/*&nbsp;&nbsp;*/line_num = 1;
/*&nbsp;&nbsp;*/background (255);
}
/*<br>*/
function draw()
{
/*<span class=hide>*/if(frameCount===1)/*</span>*/
/*<span class=hide>*/{/*</span>*/
/*<span class=hide>*/set_p5_canvas_to();/*</span>*/
/*<span class=hide>*/background (255);/*</span>*/
/*<span class=hide>*/}/*</span>*/
/*&nbsp;&nbsp;*/text_size = 20;
/*&nbsp;&nbsp;*/num_lines = floor( height/text_size );
/*<br>*/
/*&nbsp;&nbsp;*/if (frameCount%num_lines === 0) 
/*&nbsp;&nbsp;*/{
/*&nbsp;&nbsp;&nbsp;&nbsp;*/background (255);
/*&nbsp;&nbsp;&nbsp;&nbsp;*/line_num = 1;
/*&nbsp;&nbsp;*/}
/*<br>*/
/*&nbsp;&nbsp;*/name_index = int( random(0, names.length) );
/*&nbsp;&nbsp;*/textFont (font);
/*&nbsp;&nbsp;*/textSize (text_size);
/*&nbsp;&nbsp;*/text (names[name_index], random(10, width-70), line_num*text_size);
/*&nbsp;&nbsp;*/line_num++;
}
/*<br>*/
/*<span class>function windowResized()</span>*/
/*<span class>{</span>*/
/*&nbsp;&nbsp;*//*<span class>resizeCanvas (windowWidth, windowHeight);</span>*/
/*<span class>}</span>*/