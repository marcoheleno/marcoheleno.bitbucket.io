/*<span class=hide>*/var variable_1 = 100;/*</span>*/
/*<span class=hide>*/var variable_2 = variable_1 * 5;/*</span>*/
function setup()
{
/*&nbsp;&nbsp;*/createCanvas (windowWidth, windowHeight);
/*<span class=hide>*/colorMode (RGB, 255, 255, 255, 100);/*</span>*/
/*<span class=hide>*/background (255);/*</span>*/
/*<span class=hide>*/noLoop();/*</span>*/
/*<br>*/
/*
&nbsp;&nbsp;// Function scoped
&nbsp;&nbsp;var variable_1 = 100;
*/
/*<br>*/
/*&nbsp;&nbsp;*/if (variable_1 > 50) 
/*&nbsp;&nbsp;*/{
/*&nbsp;&nbsp;&nbsp;&nbsp;*//*var variable_2 = variable_1 * 5;*/
/*&nbsp;&nbsp;&nbsp;&nbsp;*/text ("variable 2 = " + variable_2, 20, 20);
/*&nbsp;&nbsp;*/}
/*<br>*/
/*&nbsp;&nbsp;*/variable_1 = variable_2;
/*&nbsp;&nbsp;*/text ("variable 1 = " + variable_1, 20, 60);
}
/*<br>*/
function draw()
{
/*<span class=hide>*/if(frameCount===1) set_p5_canvas_to();/*</span>*/
/*<span class=hide>*/text ("variable 2 = " + variable_2, 20, 20);/*</span>*/
/*<span class=hide>*/text ("variable 1 = " + variable_1, 20, 60);/*</span>*/
/*&nbsp;&nbsp;*/// Both variables are not accessible outside the setup() function.
}
/*<br>*/
/*<span class>function windowResized()</span>*/
/*<span class>{</span>*/
/*&nbsp;&nbsp;*//*<span class>resizeCanvas (windowWidth, windowHeight);</span>*/
/*<span class>}</span>*/