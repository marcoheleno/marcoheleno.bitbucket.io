let font, phrase;
/*<br>*/
function preload()
{
/*&nbsp;&nbsp;*/font = loadFont (/*<span class=hide>*/"codes/typography/" + /*</span>*/"IBMPlexMono-Thin.ttf");
}
/*<br>*/
function setup()
{
/*&nbsp;&nbsp;*/createCanvas (windowWidth, windowHeight);
}
/*<br>*/
function draw()
{
/*<span class=hide>*/if(frameCount===1) set_p5_canvas_to();/*</span>*/
/*&nbsp;&nbsp;*/background (255, 20);
/*<br>*/
/*&nbsp;&nbsp;*/phrase = "p5.js is a JavaScript library for creative coding";
/*&nbsp;&nbsp;*/textSize (50);
/*&nbsp;&nbsp;*/textStyle (NORMAL);
/*&nbsp;&nbsp;*/textAlign (LEFT);
/*&nbsp;&nbsp;*/textFont (font);
/*&nbsp;&nbsp;*/textLeading (55);
/*&nbsp;&nbsp;*/text (phrase, mouseX, mouseY, 400, 400);
}
/*<br>*/
/*<span class>function windowResized()</span>*/
/*<span class>{</span>*/
/*&nbsp;&nbsp;*//*<span class>resizeCanvas (windowWidth, windowHeight);</span>*/
/*<span class>}</span>*/