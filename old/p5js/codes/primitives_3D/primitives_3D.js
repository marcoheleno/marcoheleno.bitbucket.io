let t, a;
/*<br>*/
function setup()
{
/*&nbsp;&nbsp;*/createCanvas (windowWidth, windowHeight, WEBGL);
/*&nbsp;&nbsp;*/t = 100;
/*&nbsp;&nbsp;*/a = 0;
}
/*<br>*/
function draw()
{
/*<span class=hide>*/if(frameCount===1) set_p5_canvas_to();/*</span>*/
/*&nbsp;&nbsp;*/background (60);
/*&nbsp;&nbsp;*/fill (255, 200, 0);
/*&nbsp;&nbsp;*/stroke (0, 0, 0);
/*<br>*/
/*&nbsp;&nbsp;*/push();
/*&nbsp;&nbsp;&nbsp;&nbsp;*/translate ((width/8)*-2, (height/8)*-2, (t/3)*-1);
/*&nbsp;&nbsp;&nbsp;&nbsp;*/rotateX (radians(a));
/*&nbsp;&nbsp;&nbsp;&nbsp;*/rotateY (radians(a));
/*&nbsp;&nbsp;&nbsp;&nbsp;*/rotateZ (radians(a));
/*&nbsp;&nbsp;&nbsp;&nbsp;*/box (t, t, t);
/*&nbsp;&nbsp;*/pop();
/*<br>*/
/*&nbsp;&nbsp;*/push();
/*&nbsp;&nbsp;&nbsp;&nbsp;*/translate ((width/8)*2, (height/8)*-2, (t/3)*-1);
/*&nbsp;&nbsp;&nbsp;&nbsp;*/rotateX (radians(a));
/*&nbsp;&nbsp;&nbsp;&nbsp;*/rotateY (radians(a));
/*&nbsp;&nbsp;&nbsp;&nbsp;*/rotateZ (radians(a));
/*&nbsp;&nbsp;&nbsp;&nbsp;*/sphere (t/1.5, t/5, t/5);
/*&nbsp;&nbsp;*/pop();
/*<br>*/
/*&nbsp;&nbsp;*/push();
/*&nbsp;&nbsp;&nbsp;&nbsp;*/translate ((width/8)*2, (height/8)*2, (t/5)*-1);
/*&nbsp;&nbsp;&nbsp;&nbsp;*/rotateX (radians(a));
/*&nbsp;&nbsp;&nbsp;&nbsp;*/rotateY (radians(a));
/*&nbsp;&nbsp;&nbsp;&nbsp;*/rotateZ (radians(a));
/*&nbsp;&nbsp;&nbsp;&nbsp;*/cone (t/2, t);
/*&nbsp;&nbsp;*/pop();
/*<br>*/
/*&nbsp;&nbsp;*/push();
/*&nbsp;&nbsp;&nbsp;&nbsp;*/translate ((width/8)*-2, (height/8)*2, (t/6)*-1);
/*&nbsp;&nbsp;&nbsp;&nbsp;*/rotateX (radians(a));
/*&nbsp;&nbsp;&nbsp;&nbsp;*/rotateY (radians(a));
/*&nbsp;&nbsp;&nbsp;&nbsp;*/rotateZ (radians(a));
/*&nbsp;&nbsp;&nbsp;&nbsp;*/torus (t/2, t/4);
/*&nbsp;&nbsp;*/pop();
/*<br>*/
/*&nbsp;&nbsp;*/push();
/*&nbsp;&nbsp;&nbsp;&nbsp;*/translate (0, 0, (t/6)*-1);
/*&nbsp;&nbsp;&nbsp;&nbsp;*/rotateX (radians(a));
/*&nbsp;&nbsp;&nbsp;&nbsp;*/rotateY (radians(a));
/*&nbsp;&nbsp;&nbsp;&nbsp;*/rotateZ (radians(a));
/*&nbsp;&nbsp;&nbsp;&nbsp;*/cylinder (t/2, t);
/*&nbsp;&nbsp;*/pop();
/*<br>*/
/*&nbsp;&nbsp;*/if (a < 360) 
/*&nbsp;&nbsp;*/{
/*&nbsp;&nbsp;&nbsp;&nbsp;*/a += 0.9;
/*&nbsp;&nbsp;*/}
/*&nbsp;&nbsp;*/else 
/*&nbsp;&nbsp;*/{
/*&nbsp;&nbsp;&nbsp;&nbsp;*/a = 0;
/*&nbsp;&nbsp;*/}
/*<br>*/
/*&nbsp;&nbsp;*/push();
/*&nbsp;&nbsp;&nbsp;&nbsp;*/rectMode (CENTER);
/*&nbsp;&nbsp;&nbsp;&nbsp;*/noStroke();
/*&nbsp;&nbsp;&nbsp;&nbsp;*/fill(0, 200);
/*&nbsp;&nbsp;&nbsp;&nbsp;*/rect (0, 0, width, height);
/*&nbsp;&nbsp;*/pop();
}
/*<br>*/
/*<span class>function windowResized()</span>*/
/*<span class>{</span>*/
/*&nbsp;&nbsp;*//*<span class>resizeCanvas (windowWidth, windowHeight);</span>*/
/*<span class>}</span>*/