let x, y, num_ellipses, color_ellipses;
/*<br>*/
function setup()
{
/*&nbsp;&nbsp;*/createCanvas (windowWidth, windowHeight);
/*&nbsp;&nbsp;*/colorMode (HSB, 360, 100, 100, 100);
/*<br>*/
/*&nbsp;&nbsp;*/x = [];
/*&nbsp;&nbsp;*/y = [];
/*&nbsp;&nbsp;*/num_ellipses = 50;
/*<br>*/
/*&nbsp;&nbsp;*/for (let i=0; i<num_ellipses; i++) /*<span>&lt;num_ellipses; i++) </span>*/
/*&nbsp;&nbsp;*/{
/*&nbsp;&nbsp;&nbsp;&nbsp;*/x[i] = mouseX;
/*&nbsp;&nbsp;&nbsp;&nbsp;*/y[i] = mouseY; 
/*&nbsp;&nbsp;*/}
/*<br>*/
/*&nbsp;&nbsp;*/noStroke();
}
/*<br>*/
function draw()
{
/*<span class=hide>*/if(frameCount===1) set_p5_canvas_to();/*</span>*/
/*&nbsp;&nbsp;*/background (0, 0, 100);
/*<br>*/
/*&nbsp;&nbsp;*/x[x.length-1] = mouseX;
/*&nbsp;&nbsp;*/y[y.length-1] = mouseY;
/*<br>*/
/*&nbsp;&nbsp;*/for (let i=0; i<num_ellipses-1; i++) /*<span>&lt;num_ellipses-1; i++) </span>*/
/*&nbsp;&nbsp;*/{
/*&nbsp;&nbsp;&nbsp;&nbsp;*/x[i] = x[i+1];
/*&nbsp;&nbsp;&nbsp;&nbsp;*/y[i] = y[i+1];
/*&nbsp;&nbsp;*/}
/*<br>*/
/*&nbsp;&nbsp;*/if (pmouseX === mouseX && x[0] === x[x.length-1] || 
/*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;*/pmouseY === mouseY && y[0] === y[y.length-1]) 
/*&nbsp;&nbsp;*/{
/*&nbsp;&nbsp;&nbsp;&nbsp;*/color_ellipses = random (0, 360);
/*&nbsp;&nbsp;*/}
/*<br>*/
/*&nbsp;&nbsp;*/for (let i=0; i<num_ellipses; i++) /*<span>&lt;num_ellipses; i++) </span>*/
/*&nbsp;&nbsp;*/{
/*&nbsp;&nbsp;&nbsp;&nbsp;*/fill (color_ellipses, 100, map(i, 0, x.length, 100, 50) );
/*&nbsp;&nbsp;&nbsp;&nbsp;*/ellipse (x[i], y[i], i, i);
/*&nbsp;&nbsp;*/}
}
/*<br>*/
/*<span class>function windowResized()</span>*/
/*<span class>{</span>*/
/*&nbsp;&nbsp;*//*<span class>resizeCanvas (windowWidth, windowHeight);</span>*/
/*<span class>}</span>*/