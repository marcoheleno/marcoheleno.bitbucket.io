let a, obj3D;
/*<br>*/
function preload() 
{
/*&nbsp;&nbsp;<span class>obj3D = loadModel ("deer.obj", true);</span>*/
/*<span class=hide>*/obj3D = loadModel ("codes/model_import/deer.obj", true);/*</span>*/
}
/*<br>*/
function setup()
{
/*&nbsp;&nbsp;*/createCanvas (windowWidth, windowHeight, WEBGL);
/*&nbsp;&nbsp;*/a = 0;
}
/*<br>*/
function draw()
{
/*<span class=hide>*/if(frameCount===1) set_p5_canvas_to();/*</span>*/
/*&nbsp;&nbsp;*/background (30);
/*&nbsp;&nbsp;*/push();
/*&nbsp;&nbsp;&nbsp;&nbsp;*/translate (0, 0, 0);
/*&nbsp;&nbsp;&nbsp;&nbsp;*/scale (2);
/*<br>*/
/*&nbsp;&nbsp;&nbsp;&nbsp;*/rotateX (radians(a));
/*&nbsp;&nbsp;&nbsp;&nbsp;*/rotateY (radians(a));
/*&nbsp;&nbsp;&nbsp;&nbsp;*/rotateZ (radians(a));
/*<br>*/
/*&nbsp;&nbsp;&nbsp;&nbsp;*/directionalLight (255, 255, 0, 1, 0, 0);
/*&nbsp;&nbsp;&nbsp;&nbsp;*/directionalLight (0, 255, 255, -1, 0, 0);
/*<br>*/
/*&nbsp;&nbsp;&nbsp;&nbsp;*/noStroke();
/*&nbsp;&nbsp;&nbsp;&nbsp;*/specularMaterial (255);
/*<br>*/
/*&nbsp;&nbsp;&nbsp;&nbsp;*/model (obj3D);
/*&nbsp;&nbsp;*/pop();
/*<br>*/
/*&nbsp;&nbsp;*/if (a < 360) 
/*&nbsp;&nbsp;*/{
/*&nbsp;&nbsp;&nbsp;&nbsp;*/a += 0.9;
/*&nbsp;&nbsp;*/}
/*&nbsp;&nbsp;*/else 
/*&nbsp;&nbsp;*/{
/*&nbsp;&nbsp;&nbsp;&nbsp;*/a = 0;
/*&nbsp;&nbsp;*/}
}
/*<br>*/
/*<span class>function windowResized()</span>*/
/*<span class>{</span>*/
/*&nbsp;&nbsp;*//*<span class>resizeCanvas (windowWidth, windowHeight);</span>*/
/*<span class>}</span>*/