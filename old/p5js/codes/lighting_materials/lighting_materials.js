let t, a, l, m;
/*<br>*/
function setup()
{
/*&nbsp;&nbsp;*/createCanvas (windowWidth, windowHeight, WEBGL);
/*&nbsp;&nbsp;*/t = 300/2;
/*&nbsp;&nbsp;*/a = 0;
/*&nbsp;&nbsp;*/l = 1;
/*&nbsp;&nbsp;*/m = "q";
}
/*<br>*/
function draw()
{
/*<span class=hide>*/if(frameCount===1) set_p5_canvas_to();/*</span>*/
/*&nbsp;&nbsp;*/background (60);
/*&nbsp;&nbsp;*/noStroke();
/*<br>*/
/*&nbsp;&nbsp;*/push();
/*&nbsp;&nbsp;&nbsp;&nbsp;*/translate (0, 0, (t/3)*1);
/*&nbsp;&nbsp;&nbsp;&nbsp;*/rotateX (radians(a));
/*&nbsp;&nbsp;&nbsp;&nbsp;*/rotateY (radians(a));
/*&nbsp;&nbsp;&nbsp;&nbsp;*/rotateZ (radians(a));
/*<br>*/
/*&nbsp;&nbsp;&nbsp;&nbsp;*/if (l === 1) noLights();
/*&nbsp;&nbsp;&nbsp;&nbsp;*/if (l === 2) ambientLight (255, 0, 0);
/*&nbsp;&nbsp;&nbsp;&nbsp;*/if (l === 3) pointLight (255, 0, 0, (width/2)*-1, 0, t*10);
/*&nbsp;&nbsp;&nbsp;&nbsp;*/if (l === 4) pointLight (0, 255, 0, width/2, 0, t*10);
/*&nbsp;&nbsp;&nbsp;&nbsp;*/if (l === 5) directionalLight (255, 255, 0, 0, 0, -1);
/*<br>*/
/*&nbsp;&nbsp;&nbsp;&nbsp;*/if (m === "q") fill (255);
/*&nbsp;&nbsp;&nbsp;&nbsp;*/if (m === "w") normalMaterial();
/*&nbsp;&nbsp;&nbsp;&nbsp;*/if (m === "e") ambientMaterial (255);
/*&nbsp;&nbsp;&nbsp;&nbsp;*/if (m === "r") specularMaterial (255);
/*<br>*/
/*&nbsp;&nbsp;&nbsp;&nbsp;*/sphere (t);
/*&nbsp;&nbsp;*/pop();
/*<br>*/
/*&nbsp;&nbsp;*/if (a < 360) 
/*&nbsp;&nbsp;*/{
/*&nbsp;&nbsp;&nbsp;&nbsp;*/a += 0.9;
/*&nbsp;&nbsp;*/}
/*&nbsp;&nbsp;*/else 
/*&nbsp;&nbsp;*/{
/*&nbsp;&nbsp;&nbsp;&nbsp;*/a = 0;
/*&nbsp;&nbsp;*/}
}
/*<br>*/
function keyPressed()
{
/*&nbsp;&nbsp;*/if (key === "1") l = 1;
/*&nbsp;&nbsp;*/if (key === "2") l = 2;
/*&nbsp;&nbsp;*/if (key === "3") l = 3;
/*&nbsp;&nbsp;*/if (key === "4") l = 4;
/*&nbsp;&nbsp;*/if (key === "5") l = 5;
/*&nbsp;&nbsp;*/if (key === "q") m = "q";
/*&nbsp;&nbsp;*/if (key === "w") m = "w";
/*&nbsp;&nbsp;*/if (key === "e") m = "e";
/*&nbsp;&nbsp;*/if (key === "r") m = "r";
}
/*<br>*/
/*<span class>function windowResized()</span>*/
/*<span class>{</span>*/
/*&nbsp;&nbsp;*//*<span class>resizeCanvas (windowWidth, windowHeight);</span>*/
/*<span class>}</span>*/