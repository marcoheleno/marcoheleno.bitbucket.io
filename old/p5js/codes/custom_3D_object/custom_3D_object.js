let t, a;
/*<br>*/
function setup()
{
/*&nbsp;&nbsp;*/createCanvas (windowWidth, windowHeight, WEBGL);
/*&nbsp;&nbsp;*/t = 300;
/*&nbsp;&nbsp;*/a = 0;
}
/*<br>*/
function draw()
{
/*<span class=hide>*/if(frameCount===1) set_p5_canvas_to();/*</span>*/
/*&nbsp;&nbsp;*/background (60);
/*&nbsp;&nbsp;*/noStroke();
/*<br>*/
/*&nbsp;&nbsp;*/push();
/*&nbsp;&nbsp;&nbsp;&nbsp;*/translate (0, 0, (t/3)*-1);
/*&nbsp;&nbsp;&nbsp;&nbsp;*/rotateX (radians(a));
/*&nbsp;&nbsp;&nbsp;&nbsp;*/rotateY (radians(a));
/*&nbsp;&nbsp;&nbsp;&nbsp;*/rotateZ (radians(a));
/*<br>*/
/*&nbsp;&nbsp;&nbsp;&nbsp;*/fill (255, 0, 0);
/*&nbsp;&nbsp;&nbsp;&nbsp;*/beginShape();
/*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;*/vertex (-t/2, -t/2, t/2);
/*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;*/vertex ( t/2, -t/2, t/2);
/*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;*/vertex ( t/2, /*&nbsp;*/t/2, t/2);
/*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;*/vertex (-t/2, /*&nbsp;*/t/2, t/2);
/*&nbsp;&nbsp;&nbsp;&nbsp;*/endShape(CLOSE);
/*<br>*/
/*&nbsp;&nbsp;&nbsp;&nbsp;*/fill (0, 255, 0);
/*&nbsp;&nbsp;&nbsp;&nbsp;*/beginShape();
/*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;*/vertex (-t/2, -t/2, -t/2);
/*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;*/vertex ( t/2, -t/2, -t/2);
/*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;*/vertex ( t/2, /*&nbsp;*/t/2, -t/2);
/*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;*/vertex (-t/2, /*&nbsp;*/t/2, -t/2);
/*&nbsp;&nbsp;&nbsp;&nbsp;*/endShape(CLOSE);
/*<br>*/
/*&nbsp;&nbsp;&nbsp;&nbsp;*/fill (0, 0, 255);
/*&nbsp;&nbsp;&nbsp;&nbsp;*/beginShape();
/*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;*/vertex (-t/2, -t/2, /*&nbsp;*/t/2);
/*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;*/vertex (-t/2, -t/2, -t/2);
/*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;*/vertex (-t/2, /*&nbsp;*/t/2, -t/2);
/*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;*/vertex (-t/2, /*&nbsp;*/t/2, /*&nbsp;*/t/2);
/*&nbsp;&nbsp;&nbsp;&nbsp;*/endShape(CLOSE);
/*<br>*/
/*&nbsp;&nbsp;&nbsp;&nbsp;*/fill (255, 200, 0);
/*&nbsp;&nbsp;&nbsp;&nbsp;*/beginShape();
/*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;*/vertex ( t/2, -t/2, /*&nbsp;*/t/2);
/*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;*/vertex ( t/2, -t/2, -t/2);
/*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;*/vertex ( t/2, /*&nbsp;*/t/2, -t/2);
/*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;*/vertex ( t/2, /*&nbsp;*/t/2, /*&nbsp;*/t/2);
/*&nbsp;&nbsp;&nbsp;&nbsp;*/endShape(CLOSE);
/*<br>*/
/*&nbsp;&nbsp;&nbsp;&nbsp;*/fill (0, 255, 200);
/*&nbsp;&nbsp;&nbsp;&nbsp;*/beginShape();
/*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;*/vertex (-t/2, -t/2, /*&nbsp;*/t/2);
/*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;*/vertex (-t/2, -t/2, -t/2);
/*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;*/vertex ( t/2, -t/2, -t/2);
/*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;*/vertex ( t/2, -t/2, /*&nbsp;*/t/2);
/*&nbsp;&nbsp;&nbsp;&nbsp;*/endShape(CLOSE);
/*<br>*/
/*&nbsp;&nbsp;&nbsp;&nbsp;*/fill (255, 200, 200);
/*&nbsp;&nbsp;&nbsp;&nbsp;*/beginShape();
/*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;*/vertex (-t/2, /*&nbsp;*/t/2, /*&nbsp;*/t/2);
/*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;*/vertex (-t/2, /*&nbsp;*/t/2, -t/2);
/*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;*/vertex ( t/2, /*&nbsp;*/t/2, -t/2);
/*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;*/vertex ( t/2, /*&nbsp;*/t/2, /*&nbsp;*/t/2);
/*&nbsp;&nbsp;&nbsp;&nbsp;*/endShape(CLOSE);
/*&nbsp;&nbsp;*/pop();
/*<br>*/
/*&nbsp;&nbsp;*/if (a < 360) 
/*&nbsp;&nbsp;*/{
/*&nbsp;&nbsp;&nbsp;&nbsp;*/a += 0.9;
/*&nbsp;&nbsp;*/}
/*&nbsp;&nbsp;*/else 
/*&nbsp;&nbsp;*/{
/*&nbsp;&nbsp;&nbsp;&nbsp;*/a = 0;
/*&nbsp;&nbsp;*/}
}
/*<br>*/
/*<span class>function windowResized()</span>*/
/*<span class>{</span>*/
/*&nbsp;&nbsp;*//*<span class>resizeCanvas (windowWidth, windowHeight);</span>*/
/*<span class>}</span>*/