let t, a, img;
/*<br>*/
function preload() 
{
/*&nbsp;&nbsp;<span class>img = loadImage ("rock.jpg");</span>*/
/*<span class=hide>*/img = loadImage ("codes/textures/rock.jpg");/*</span>*/
}
/*<br>*/
function setup()
{
/*&nbsp;&nbsp;*/createCanvas (windowWidth, windowHeight, WEBGL);
/*&nbsp;&nbsp;*/t = 150;
/*&nbsp;&nbsp;*/a = 0;
}
/*<br>*/
function draw()
{
/*<span class=hide>*/if(frameCount===1) set_p5_canvas_to();/*</span>*/
/*&nbsp;&nbsp;*/background (60);
/*&nbsp;&nbsp;*/noStroke();
/*<br>*/
/*&nbsp;&nbsp;*/push();
/*&nbsp;&nbsp;&nbsp;&nbsp;*/translate ((width/8)*-2, 0, 0);
/*&nbsp;&nbsp;&nbsp;&nbsp;*/rotateX (radians(a));
/*&nbsp;&nbsp;&nbsp;&nbsp;*/rotateY (radians(a));
/*&nbsp;&nbsp;&nbsp;&nbsp;*/rotateZ (radians(a));
/*&nbsp;&nbsp;&nbsp;&nbsp;*/texture (img);
/*&nbsp;&nbsp;&nbsp;&nbsp;*/box (t, t*2, t);
/*&nbsp;&nbsp;*/pop();
/*<br>*/
/*&nbsp;&nbsp;*/push();
/*&nbsp;&nbsp;&nbsp;&nbsp;*/translate ((width/8)*2, 0, 0);
/*&nbsp;&nbsp;&nbsp;&nbsp;*/rotateX (radians(a));
/*&nbsp;&nbsp;&nbsp;&nbsp;*/rotateY (radians(a));
/*&nbsp;&nbsp;&nbsp;&nbsp;*/rotateZ (radians(a));
/*&nbsp;&nbsp;&nbsp;&nbsp;*/texture (img);
/*&nbsp;&nbsp;&nbsp;&nbsp;*/textureMode(NORMAL);
/*&nbsp;&nbsp;&nbsp;&nbsp;*/beginShape();
/*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;*/vertex (-t/2, -t, 0, 0);
/*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;*/vertex ( t/2, -t, 1, 0);
/*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;*/vertex ( t/2, /*&nbsp;*/t, 1, 1);
/*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;*/vertex (-t/2, /*&nbsp;*/t, 0, 1);
/*&nbsp;&nbsp;&nbsp;&nbsp;*/endShape();
/*&nbsp;&nbsp;*/pop();
/*<br>*/
/*&nbsp;&nbsp;*/if (a < 360) 
/*&nbsp;&nbsp;*/{
/*&nbsp;&nbsp;&nbsp;&nbsp;*/a += 0.9;
/*&nbsp;&nbsp;*/}
/*&nbsp;&nbsp;*/else 
/*&nbsp;&nbsp;*/{
/*&nbsp;&nbsp;&nbsp;&nbsp;*/a = 0;
/*&nbsp;&nbsp;*/}
}
/*<br>*/
/*<span class>function windowResized()</span>*/
/*<span class>{</span>*/
/*&nbsp;&nbsp;*//*<span class>resizeCanvas (windowWidth, windowHeight);</span>*/
/*<span class>}</span>*/