let x1, x2;
/*<br>*/
function setup()
{
/*&nbsp;&nbsp;*/createCanvas (windowWidth, windowHeight);
/*<br>*/
/*&nbsp;&nbsp;*/x1 = 0;
/*&nbsp;&nbsp;*/x2 = 0;
/*<br>*/
/*&nbsp;&nbsp;*/strokeWeight(2);
}
/*<br>*/
function draw()
{
/*<span class=hide>*/if(frameCount===1) set_p5_canvas_to();/*</span>*/
/*&nbsp;&nbsp;*/background (255);
/*<br>*/
/*&nbsp;&nbsp;*/x1++;
/*&nbsp;&nbsp;*/stroke (100);
/*&nbsp;&nbsp;*/line(x1, 0, x1, height);
/*&nbsp;&nbsp;*/if (x1>width) x1 = 0;
/*<br>*/
/*&nbsp;&nbsp;*/if (frameCount%2 === 0) 
/*&nbsp;&nbsp;*/{
/*&nbsp;&nbsp;&nbsp;&nbsp;*/x2++;
/*&nbsp;&nbsp;&nbsp;&nbsp;*/stroke(0);
/*&nbsp;&nbsp;&nbsp;&nbsp;*/if (x2>width) x2 = 0;
/*&nbsp;&nbsp;*/}
/*<br>*/
/*&nbsp;&nbsp;*/line(x2, 0, x2, height);
}
/*<br>*/
/*<span class>function windowResized()</span>*/
/*<span class>{</span>*/
/*&nbsp;&nbsp;*//*<span class>resizeCanvas (windowWidth, windowHeight);</span>*/
/*<span class>}</span>*/