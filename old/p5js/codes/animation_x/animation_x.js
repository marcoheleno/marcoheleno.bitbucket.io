let x, y, x_speed;
let ball_radius, ball_color, canvas_color;
/*<br>*/
function setup()
{
/*&nbsp;&nbsp;*/createCanvas (windowWidth, windowHeight);
/*<br>*/
/*&nbsp;&nbsp;*/x = width/2;
/*&nbsp;&nbsp;*/y = height/2;
/*<br>*/
/*&nbsp;&nbsp;*/x_speed = 3;
/*<br>*/
/*&nbsp;&nbsp;*/canvas_color = color (255);
/*&nbsp;&nbsp;*/ball_color = color (100);
/*<br>*/
/*&nbsp;&nbsp;*/ellipseMode (RADIUS);
/*&nbsp;&nbsp;*/ball_radius = 100;
/*&nbsp;&nbsp;*/noStroke();
/*&nbsp;&nbsp;*/fill (ball_color);
}
/*<br>*/
function draw()
{
/*<span class=hide>*/if(frameCount===1) set_p5_canvas_to();/*</span>*/
/*<span class=hide>*/if(frameCount===1) x = width/2;/*</span>*/
/*&nbsp;&nbsp;*/background (canvas_color);
/*<br>*/
/*&nbsp;&nbsp;*/x += x_speed;
/*<br>*/
/*&nbsp;&nbsp;*/if (x-ball_radius<=0 || x+ball_radius>=width) 
/*&nbsp;&nbsp;*/{
/*&nbsp;&nbsp;&nbsp;&nbsp;*/x_speed *= -1;
/*&nbsp;&nbsp;*/}
/*<br>*/
/*&nbsp;&nbsp;*/ellipse(x, y, ball_radius, ball_radius);
}
/*<br>*/
/*<span class>function windowResized()</span>*/
/*<span class>{</span>*/
/*&nbsp;&nbsp;*//*<span class>resizeCanvas (windowWidth, windowHeight);</span>*/
/*<span class>}</span>*/