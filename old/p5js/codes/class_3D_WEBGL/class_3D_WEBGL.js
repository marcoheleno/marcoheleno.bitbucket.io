let cubos;
/*<br>*/
function setup() 
{
/*&nbsp;&nbsp;*/createCanvas (windowWidth, windowHeight, WEBGL);
/*&nbsp;&nbsp;*/cubos = [];
}
/*<br>*/
function draw()
{
/*<span class=hide>*/if(frameCount===1) set_p5_canvas_to();/*</span>*/
/*&nbsp;&nbsp;*/background (255);
/*<br>*/
/*&nbsp;&nbsp;*/directionalLight (255, 0, 0, /*&nbsp;*/1, 1, 0);
/*&nbsp;&nbsp;*/directionalLight (0, 255, 0, -1, 1, 0);
/*&nbsp;&nbsp;*/directionalLight (0, 0, 255, 0, -1, 0);
/*<br>*/
/*&nbsp;&nbsp;*/for (let i=0; i<cubos.length; i++)/*<span>&lt;cubos.length; i++) </span>*/
/*&nbsp;&nbsp;*/{
/*&nbsp;&nbsp;&nbsp;&nbsp;*/cubos[i].desenha();
/*&nbsp;&nbsp;*/}
}
/*<br>*/
function mousePressed() 
{
/*&nbsp;&nbsp;*/append (cubos, new CuboRotativo (mouseX, mouseY) );
}
/*<br>*/
class CuboRotativo
{
/*&nbsp;&nbsp;*/constructor (x_, y_) 
/*&nbsp;&nbsp;*/{
/*&nbsp;&nbsp;&nbsp;&nbsp;*/this.x = x_;
/*&nbsp;&nbsp;&nbsp;&nbsp;*/this.y = y_;
/*&nbsp;&nbsp;&nbsp;&nbsp;*/this.z = int(random (-300, 300));
/*&nbsp;&nbsp;&nbsp;&nbsp;*/this.t = random (10, 20);
/*&nbsp;&nbsp;*/}
/*<br>*/
/*&nbsp;&nbsp;*/desenha() 
/*&nbsp;&nbsp;*/{
/*&nbsp;&nbsp;&nbsp;&nbsp;*/push();
/*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;*/translate (this.x-width/2, this.y-height/2, this.z);
/*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;*/noStroke();
/*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;*/specularMaterial (255);
/*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;*/box (this.t);
/*&nbsp;&nbsp;&nbsp;&nbsp;*/pop();
/*&nbsp;&nbsp;*/}
}
/*<br>*/
/*<span class>function windowResized()</span>*/
/*<span class>{</span>*/
/*&nbsp;&nbsp;*//*<span class>resizeCanvas (windowWidth, windowHeight);</span>*/
/*<span class>}</span>*/