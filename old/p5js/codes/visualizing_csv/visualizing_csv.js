/*<span>See CVS file:</span>*/
/*<span><a target="_blank" href="https://marcoheleno.bitbucket.io/p5js/codes/visualizing_csv/data.csv">https://marcoheleno.bitbucket.io/p5js/codes/visualizing_csv/data.csv</a></span>*/
/*<br>*/
let data, years;
/*<br>*/
function preload() 
{
/*&nbsp;&nbsp;*/data = loadTable("https://marcoheleno.bitbucket.io/p5js/codes/visualizing_csv/data.csv", "csv", "header");
}
/*<br>*/
function setup()
{
/*&nbsp;&nbsp;*/createCanvas (windowWidth, windowHeight);
/*&nbsp;&nbsp;*/years = [];
/*&nbsp;&nbsp;*/importData();
}
/*<br>*/
function importData()
{
/*&nbsp;&nbsp;*/let value_scale = 20;
/*&nbsp;&nbsp;*/let x = 100;
/*&nbsp;&nbsp;*/let y = ( (data.getString(0, 2)/value_scale) / 2) * -1;
/*<br>*/
/*&nbsp;&nbsp;*/for (let lines=0; lines<data.getRowCount(); lines++)/*<span>&lt;data.getRowCount(); lines++) </span>*/
/*&nbsp;&nbsp;*/{
/*&nbsp;&nbsp;&nbsp;&nbsp;*/let year = data.getNum (lines, 0);
/*&nbsp;&nbsp;&nbsp;&nbsp;*/let men = data.getString (lines, 1);
/*&nbsp;&nbsp;&nbsp;&nbsp;*/let women = data.getString (lines, 2);
/*<br>*/
/*&nbsp;&nbsp;&nbsp;&nbsp;*/y += women/value_scale + value_scale/2;
/*<br>*/
/*&nbsp;&nbsp;&nbsp;&nbsp;*/years[lines] = new Years (x, y, year, men, women, value_scale);
/*&nbsp;&nbsp;*/}
}
/*<br>*/
function draw()
{
/*<span class=hide>*/if(frameCount===1) set_p5_canvas_to();/*</span>*/
/*&nbsp;&nbsp;*/background (255);
/*<br>*/
/*&nbsp;&nbsp;*/for (let i=0; i<years.length; i++)/*<span>&lt;years.length; i++) </span>*/
/*&nbsp;&nbsp;*/{
/*&nbsp;&nbsp;&nbsp;&nbsp;*/years[i].drawYear();
/*&nbsp;&nbsp;*/}
}
/*<br>*/
class Years  
{
/*&nbsp;&nbsp;*/constructor (x, y, year, men, women, value_scale) 
/*&nbsp;&nbsp;*/{
/*&nbsp;&nbsp;&nbsp;&nbsp;*/this.x = x;
/*&nbsp;&nbsp;&nbsp;&nbsp;*/this.y = y;
/*&nbsp;&nbsp;&nbsp;&nbsp;*/this.year = year;
/*&nbsp;&nbsp;&nbsp;&nbsp;*/this.men = men;
/*&nbsp;&nbsp;&nbsp;&nbsp;*/this.women = women;
/*&nbsp;&nbsp;&nbsp;&nbsp;*/this.value_scale = value_scale;
/*&nbsp;&nbsp;*/}
/*<br>*/
/*&nbsp;&nbsp;*/drawYear () 
/*&nbsp;&nbsp;*/{
/*&nbsp;&nbsp;&nbsp;&nbsp;*/noFill();
/*<br>*/
/*&nbsp;&nbsp;&nbsp;&nbsp;*/stroke (0, 180, 0);
/*&nbsp;&nbsp;&nbsp;&nbsp;*/ellipse (this.x, this.y, this.men/this.value_scale);
/*<br>*/
/*&nbsp;&nbsp;&nbsp;&nbsp;*/stroke (200, 0, 0);
/*&nbsp;&nbsp;&nbsp;&nbsp;*/ellipse (this.x, this.y, this.women/this.value_scale);
/*<br>*/
/*&nbsp;&nbsp;&nbsp;&nbsp;*/noStroke();
/*&nbsp;&nbsp;&nbsp;&nbsp;*/fill (0);
/*&nbsp;&nbsp;&nbsp;&nbsp;*/textAlign (CENTER, CENTER);
/*&nbsp;&nbsp;&nbsp;&nbsp;*/textSize (14);
/*&nbsp;&nbsp;&nbsp;&nbsp;*/text (this.year, this.x - 1000/this.value_scale, this.y);
/*&nbsp;&nbsp;*/}
}
/*<br>*/
/*<span class>function windowResized()</span>*/
/*<span class>{</span>*/
/*&nbsp;&nbsp;*//*<span class>resizeCanvas (windowWidth, windowHeight);</span>*/
/*<span class>}</span>*/
