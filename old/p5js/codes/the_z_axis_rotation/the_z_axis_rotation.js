let z1, z2, a;
/*<br>*/
function setup()
{
/*&nbsp;&nbsp;*/createCanvas (windowWidth, windowHeight, WEBGL);
/*&nbsp;&nbsp;*/z1 = 1;
/*&nbsp;&nbsp;*/z2 = -1;
/*&nbsp;&nbsp;*/a = 0;
}
/*<br>*/
function draw()
{
/*<span class=hide>*/if(frameCount===1) set_p5_canvas_to();/*</span>*/
/*&nbsp;&nbsp;*/background (60);
/*&nbsp;&nbsp;*/rectMode (CENTER);
/*&nbsp;&nbsp;*/noStroke();
/*<br>*/
/*&nbsp;&nbsp;*/push();
/*&nbsp;&nbsp;&nbsp;&nbsp;*/translate (0, 0, z1);
/*&nbsp;&nbsp;&nbsp;&nbsp;*/rotateZ (radians(a));
/*&nbsp;&nbsp;&nbsp;&nbsp;*/fill (255, 200, 0);
/*&nbsp;&nbsp;&nbsp;&nbsp;*/rect (0, 0, 200, 200);
/*&nbsp;&nbsp;*/pop();
/*<br>*/
/*&nbsp;&nbsp;*/push();
/*&nbsp;&nbsp;&nbsp;&nbsp;*/translate (0, 0, z2);
/*&nbsp;&nbsp;&nbsp;&nbsp;*/rotateZ (radians(a)*-1);
/*&nbsp;&nbsp;&nbsp;&nbsp;*/fill (0, 255, 200);
/*&nbsp;&nbsp;&nbsp;&nbsp;*/rect (0, 0, 170, 170);
/*&nbsp;&nbsp;*/pop();
/*<br>*/
/*&nbsp;&nbsp;*/if (a < 360) 
/*&nbsp;&nbsp;*/{
/*&nbsp;&nbsp;&nbsp;&nbsp;*/a += 0.9;
/*&nbsp;&nbsp;*/}
/*&nbsp;&nbsp;*/else 
/*&nbsp;&nbsp;*/{
/*&nbsp;&nbsp;&nbsp;&nbsp;*/a = 0;
/*&nbsp;&nbsp;*/}
}
/*<br>*/
function keyPressed()
{
/*&nbsp;&nbsp;*/z1 *= -1;
/*&nbsp;&nbsp;*/z2 *= -1;
}
/*<br>*/
/*<span class>function windowResized()</span>*/
/*<span class>{</span>*/
/*&nbsp;&nbsp;*//*<span class>resizeCanvas (windowWidth, windowHeight);</span>*/
/*<span class>}</span>*/