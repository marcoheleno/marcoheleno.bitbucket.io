function setup()
{
/*&nbsp;&nbsp;*/createCanvas (windowWidth, windowHeight);
}
/*<br>*/
function draw()
{
/*<span class=hide>*/if(frameCount===1) set_p5_canvas_to();/*</span>*/
/*&nbsp;&nbsp;*/background (255);
/*&nbsp;&nbsp;*/noFill();
/*<br>*/
/*&nbsp;&nbsp;*/rect (10, 10, 80, 80);
/*<br>*/
/*&nbsp;&nbsp;*/myRectangle (20, 20, 80, 80);
}
/*<br>*/
function myRectangle (x, y, w, h) 
{
/*&nbsp;&nbsp;*/line (x, y, x+w, y);
/*&nbsp;&nbsp;*/line (x+w, y, x+w, y+h);
/*&nbsp;&nbsp;*/line (x, y+h, x+w, y+h);
/*&nbsp;&nbsp;*/line (x, y, x, y+h);
}
/*<br>*/
/*<span class>function windowResized()</span>*/
/*<span class>{</span>*/
/*&nbsp;&nbsp;*//*<span class>resizeCanvas (windowWidth, windowHeight);</span>*/
/*<span class>}</span>*/