function setup()
{
/*&nbsp;&nbsp;*/createCanvas (windowWidth, windowHeight);
}
/*<br>*/
function draw()
{
/*<span class=hide>*/if(frameCount===1) set_p5_canvas_to();/*</span>*/
/*<span class=hide>*/clear();/*</span>*/

/*<span class=hide>*/strokeWeight(1);/*</span>*/
/*<span class=hide>*/stroke(255, 0, 0);/*</span>*/
/*&nbsp;&nbsp;*/line (0, 0, width, height); // red line

/*<span class=hide>*/stroke(0, 255, 0);/*</span>*/
/*&nbsp;&nbsp;*/circle (51, 51, 100); // green circle

/*<span class=hide>*/strokeWeight(2);/*</span>*/
/*<span class=hide>*/stroke(0, 0, 0);/*</span>*/
/*&nbsp;&nbsp;*/point (51, 51); // black point in green circle
/*<br>*/
/*<span class=hide>*/strokeWeight(1);/*</span>*/
/*<span class=hide>*/stroke(0, 0, 255);/*</span>*/
/*&nbsp;&nbsp;*/ellipse (width/2, height/2, width/3, width/2); // blue ellipse

/*<span class=hide>*/stroke(0, 255, 255);/*</span>*/
/*&nbsp;&nbsp;*/arc( (width/4)*3, (height/4), width/3, width/3, PI + HALF_PI, TWO_PI);//cyan arc
/*<span class=hide>*/stroke(230, 230, 0);/*</span>*/
/*&nbsp;&nbsp;*/arc( (width/4)*3, (height/4), width/3, width/3, 0, HALF_PI); // yellow arc
/*<br>*/
/*<span class=hide>*/stroke(255, 0, 255);/*</span>*/
/*&nbsp;&nbsp;*/rect (width/2, height/2, width/3, width/2); // purple rectangle
/*<span class=hide>*/stroke(75, 150, 255);/*</span>*/
/*&nbsp;&nbsp;*/square (width/2, height/2, width/4); // light blue square
/*<span class=hide>*/stroke(255, 150, 75);/*</span>*/
/*&nbsp;&nbsp;*/quad (1, height/2, (width/4)*1, height/2, (width/4)*2, height-1, (width/4)*1, height-1); // orange quad
/*<br>*/
/*<span class=hide>*/stroke(150, 75, 150);/*</span>*/
/*&nbsp;&nbsp;*/triangle (1, height/2, (width/4)*1, height-1, 1, height-1); // brown triangle
}
/*<br>*/
/*<span class>function windowResized()</span>*/
/*<span class>{</span>*/
/*&nbsp;&nbsp;*//*<span class>resizeCanvas (windowWidth, windowHeight);</span>*/
/*<span class>}</span>*/