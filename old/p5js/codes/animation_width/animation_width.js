let x, y;
let rect_width, rect_color;
let width_speed, canvas_color;
/*<br>*/
function setup()
{
/*&nbsp;&nbsp;*/createCanvas (windowWidth, windowHeight);
/*<br>*/
/*&nbsp;&nbsp;*/x = 0;
/*&nbsp;&nbsp;*/y = 0;
/*<br>*/
/*&nbsp;&nbsp;*/rect_width = 0;
/*&nbsp;&nbsp;*/width_speed = 1;
/*<br>*/
/*&nbsp;&nbsp;*/rect_color = color (100);
/*&nbsp;&nbsp;*/canvas_color = color (255);
/*<br>*/
/*&nbsp;&nbsp;*/fill (rect_color);
/*&nbsp;&nbsp;*/noStroke();
}
/*<br>*/
function draw()
{
/*<span class=hide>*/if(frameCount===1) set_p5_canvas_to();/*</span>*/
/*&nbsp;&nbsp;*/background (canvas_color);
/*<br>*/
/*&nbsp;&nbsp;*/rect_width += width_speed;
/*<br>*/
/*&nbsp;&nbsp;*/if (rect_width >= width) 
/*&nbsp;&nbsp;*/{
/*&nbsp;&nbsp;&nbsp;&nbsp;*/rect_width = random(width);
/*&nbsp;&nbsp;*/}
/*<br>*/
/*&nbsp;&nbsp;*/rect(x, y, rect_width, height);
}
/*<br>*/
/*<span class>function windowResized()</span>*/
/*<span class>{</span>*/
/*&nbsp;&nbsp;*//*<span class>resizeCanvas (windowWidth, windowHeight);</span>*/
/*<span class>}</span>*/