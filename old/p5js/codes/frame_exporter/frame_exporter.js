let x, speed, ball_radius;
/*<br>*/
let desired_export_frame_rate, desired_export_duration;
let frame_counter, max_num_frames, run_frame_exporter;
/*<br>*/
function setup()
{
/*&nbsp;&nbsp;*/createCanvas (1920, 1080);
/*<br>*/
/*&nbsp;&nbsp;*/x = width/2;
/*&nbsp;&nbsp;*/speed = 30;
/*&nbsp;&nbsp;*/ball_radius = 30;
/*&nbsp;&nbsp;*/ellipseMode (RADIUS);
/*<br>*/
/*&nbsp;&nbsp;*/frame_counter = 1;
/*&nbsp;&nbsp;*/desired_export_frame_rate = 10; //fps
/*&nbsp;&nbsp;*/desired_export_duration = 1; //seconds
/*&nbsp;&nbsp;*/max_num_frames = desired_export_frame_rate * desired_export_duration;
/*&nbsp;&nbsp;*/run_frame_exporter = false;
}
/*<br>*/
function draw()
{
/*<span class=hide>*/if(frameCount===1) set_p5_canvas_to(width, height);/*</span>*/
/*<span class=hide>*/if(frameCount===1) x = width/2;/*</span>*/
/*&nbsp;&nbsp;*/background (255);
/*<br>*/
/*&nbsp;&nbsp;*/x += speed;
/*&nbsp;&nbsp;*/if (x-ball_radius<=0 || x+ball_radius>=width) 
/*&nbsp;&nbsp;*/{
/*&nbsp;&nbsp;&nbsp;&nbsp;*/speed *= -1;
/*&nbsp;&nbsp;*/}
/*&nbsp;&nbsp;*/ellipse (x, height/2, ball_radius, ball_radius);
/*<br>*/
/*&nbsp;&nbsp;*/if (run_frame_exporter===true && frame_counter<=max_num_frames) 
/*&nbsp;&nbsp;*/{
/*&nbsp;&nbsp;&nbsp;&nbsp;*/frameRate (1);
/*&nbsp;&nbsp;&nbsp;&nbsp;*/saveCanvas ("frame_"+frame_counter, "png");
/*<br>*/
/*&nbsp;&nbsp;&nbsp;&nbsp;*/// Do not forget to check the download folder
/*&nbsp;&nbsp;&nbsp;&nbsp;*/// and switch off the "Ask before downloading" 
/*&nbsp;&nbsp;&nbsp;&nbsp;*/// option in your browser
/*<br>*/
/*&nbsp;&nbsp;&nbsp;&nbsp;*/if (frame_counter <= max_num_frames) frame_counter++;
/*&nbsp;&nbsp;&nbsp;&nbsp;*/else console.log ("Export completed!");
/*&nbsp;&nbsp;*/}
/*&nbsp;&nbsp;*/else frameRate (60);
}
/*<br>*/
function keyPressed() 
{
/*&nbsp;&nbsp;*/if (key === "e") 
/*&nbsp;&nbsp;*/{
/*&nbsp;&nbsp;&nbsp;&nbsp;*/run_frame_exporter = !run_frame_exporter;
/*&nbsp;&nbsp;*/}
}
