let y, number_lines, line_height, line_spacing;
/*<br>*/
function setup()
{
/*&nbsp;&nbsp;*/createCanvas (windowWidth, windowHeight);
/*<br>*/
/*&nbsp;&nbsp;*/number_lines = 5;
/*&nbsp;&nbsp;*/line_height = 40;
/*&nbsp;&nbsp;*/strokeWeight (2);
}
/*<br>*/
function draw()
{
/*<span class=hide>*/if(frameCount===1) set_p5_canvas_to();/*</span>*/
/*&nbsp;&nbsp;*/background (255);
/*&nbsp;&nbsp;*/line_spacing = width/number_lines;
/*<br>*/
/*&nbsp;&nbsp;*/y = (height/4)*1;
/*&nbsp;&nbsp;*/for (let x=line_spacing; x<width; x+=line_spacing)/*<span>&lt;width; x+=line_spacing) </span>*/
/*&nbsp;&nbsp;*/{
/*&nbsp;&nbsp;&nbsp;&nbsp;*/line(
/*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;*/x, y-line_height/2, 
/*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;*/x, y+line_height/2
/*&nbsp;&nbsp;&nbsp;&nbsp;*/);
/*&nbsp;&nbsp;*/}
/*<br>*/
/*&nbsp;&nbsp;*/y = (height/4)*3;
/*&nbsp;&nbsp;*/for (let line_number=1; line_number<number_lines; line_number++)/*<span>&lt;number_lines; line_number++) </span>*/
/*&nbsp;&nbsp;*/{
/*&nbsp;&nbsp;&nbsp;&nbsp;*/line(
/*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;*/line_number*line_spacing, y-line_height/2, 
/*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;*/line_number*line_spacing, y+line_height/2
/*&nbsp;&nbsp;&nbsp;&nbsp;*/);
/*&nbsp;&nbsp;*/}
}
/*<br>*/
/*<span class>function windowResized()</span>*/
/*<span class>{</span>*/
/*&nbsp;&nbsp;*//*<span class>resizeCanvas (windowWidth, windowHeight);</span>*/
/*<span class>}</span>*/