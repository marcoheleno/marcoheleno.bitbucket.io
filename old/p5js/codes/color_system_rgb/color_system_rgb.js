function setup()
{
/*&nbsp;&nbsp;*/createCanvas (windowWidth, windowHeight);
/*&nbsp;&nbsp;*/colorMode (RGB, 255, 255, 255, 100);
}
/*<br>*/
function draw()
{
/*<span class=hide>*/if (frameCount===1) set_p5_canvas_to();/*</span>*/
/*&nbsp;&nbsp;*/background (255);
/*<br>*/
/*&nbsp;&nbsp;*/stroke (255, 0, 0);
/*&nbsp;&nbsp;*/line (0, 0, width, height);
/*&nbsp;&nbsp;*/noFill();
/*&nbsp;&nbsp;*/circle (51, 51, 100);
/*<br>*/
/*&nbsp;&nbsp;*/stroke (0, 255, 0);
/*&nbsp;&nbsp;*/fill (255);
/*&nbsp;&nbsp;*/ellipse (width/2, height/2, width/3, width/3);
/*&nbsp;&nbsp;*/stroke (0, 255, 0, 50);
/*&nbsp;&nbsp;*/arc( (width/4)*3, (height/4), width/3, width/3, 0, HALF_PI);
/*&nbsp;&nbsp;*/arc( (width/4)*3, (height/4), width/3, width/3, PI + HALF_PI, TWO_PI);
/*<br>*/
/*&nbsp;&nbsp;*/fill (0, 0, 255, 50);
/*&nbsp;&nbsp;*/rect (width/2, height/2, width/3, width/3);
/*&nbsp;&nbsp;*/fill (0, 0, 255, 50);
/*&nbsp;&nbsp;*/square (width/2, height/2, width/4);
/*<br>*/
/*&nbsp;&nbsp;*/noStroke();
/*&nbsp;&nbsp;*/quad (1, height/2, (width/4)*1, height/2, (width/4)*2, height-1, (width/4)*1, height-1);
/*&nbsp;&nbsp;*/stroke (0, 0, 255);
/*&nbsp;&nbsp;*/triangle (1, height/2, (width/4)*1, height-1, 1, height-1);
}
/*<br>*/
/*<span class>function windowResized()</span>*/
/*<span class>{</span>*/
/*&nbsp;&nbsp;*//*<span class>resizeCanvas (windowWidth, windowHeight);</span>*/
/*<span class>}</span>*/