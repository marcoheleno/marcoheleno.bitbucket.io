let x, y, number_lines, line_height, line_spacing;
/*<br>*/
function setup()
{
/*&nbsp;&nbsp;*/createCanvas (windowWidth, windowHeight);
/*<br>*/
/*&nbsp;&nbsp;*/number_lines = 5;
/*&nbsp;&nbsp;*/line_height = 40;
/*&nbsp;&nbsp;*/strokeWeight (2);
}
/*<br>*/
function draw()
{
/*<span class=hide>*/if(frameCount===1) set_p5_canvas_to();/*</span>*/
/*<span class=hide>*/clear();/*</span>*/
/*&nbsp;&nbsp;*/line_spacing = width/number_lines;
/*&nbsp;&nbsp;*/x = line_spacing;
/*&nbsp;&nbsp;*/y = height/2;
/*<br>*/
/*&nbsp;&nbsp;*/while (x < width) 
/*&nbsp;&nbsp;*/{
/*&nbsp;&nbsp;&nbsp;&nbsp;*/line(x, y-line_height/2, x, y+line_height/2);
/*&nbsp;&nbsp;&nbsp;&nbsp;*/x += line_spacing;
/*&nbsp;&nbsp;*/}
}
/*<br>*/
/*<span class>function windowResized()</span>*/
/*<span class>{</span>*/
/*&nbsp;&nbsp;*//*<span class>resizeCanvas (windowWidth, windowHeight);</span>*/
/*<span class>}</span>*/