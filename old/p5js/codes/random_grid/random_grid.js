let size, num_lines, num_columns, pick_form, grey_color;
/*<br>*/
function setup()
{
/*&nbsp;&nbsp;*/createCanvas (windowWidth, windowHeight);
/*&nbsp;&nbsp;*/frameRate (1);
}
/*<br>*/
function draw()
{
/*<span class=hide>*/if(frameCount===1) set_p5_canvas_to();/*</span>*/
/*&nbsp;&nbsp;*/clear();
/*<br>*/
/*&nbsp;&nbsp;*/size = width/18;
/*&nbsp;&nbsp;*/num_lines = height/size;
/*&nbsp;&nbsp;*/num_columns = width/size;
/*<br>*/
/*&nbsp;&nbsp;*/for (let x=0; x<num_columns; x++)/*<span>&lt;num_columns; x++) </span>*/
/*&nbsp;&nbsp;*/{
/*&nbsp;&nbsp;&nbsp;&nbsp;*/for (let y=0; y<num_lines; y++)/*<span>&lt;num_lines; y++) </span>*/
/*&nbsp;&nbsp;&nbsp;&nbsp;*/{
/*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;*/grey_color = color( random(100, 150) );
/*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;*/fill (grey_color);
/*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;*/stroke (grey_color);
/*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;*/pick_form = int( random(1,3) );
/*<br>*/
/*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;*/if (pick_form === 1) 
/*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;*/{
/*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;*/ellipse (x*size+size/2, y*size+size/2, size, size);
/*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;*/}
/*<br>*/
/*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;*/else if (pick_form === 2) 
/*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;*/{
/*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;*/rect (x*size, y*size, size, size);
/*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;*/}
/*&nbsp;&nbsp;&nbsp;&nbsp;*/}
/*&nbsp;&nbsp;*/}
}
/*<br>*/
/*<span class>function windowResized()</span>*/
/*<span class>{</span>*/
/*&nbsp;&nbsp;*//*<span class>resizeCanvas (windowWidth, windowHeight);</span>*/
/*<span class>}</span>*/