/*<span>Code based on:</span>*/
/*<span><a target="_blank" href="https://marcoheleno.bitbucket.io/p5js/index.html?code=generative_ellipse">https://marcoheleno.bitbucket.io/p5js/index.html?code=generative_ellipse</a></span>*/
/*<br>*/
let x, y, px, py, speed;
/*<br>*/
function setup()
{
/*&nbsp;&nbsp;*/createCanvas (windowWidth, windowHeight);
/*<br>*/
/*&nbsp;&nbsp;*/x = width/2;
/*&nbsp;&nbsp;*/y = height/2;
/*&nbsp;&nbsp;*/px = width/2;
/*&nbsp;&nbsp;*/py = height/2;
/*<br>*/
/*&nbsp;&nbsp;*/speed = random (10, 50);
/*<br>*/
/*&nbsp;&nbsp;*/background (255);
}
/*<br>*/
function draw()
{
/*<span class=hide>*/if(frameCount===1) set_p5_canvas_to();/*</span>*/
/*<span class=hide>*/if(frameCount===1) { x = width/2; px = width/2; }/*</span>*/
/*&nbsp;&nbsp;*/if (frameCount%3 === 0) 
/*&nbsp;&nbsp;*/{
/*&nbsp;&nbsp;&nbsp;&nbsp;*/background (255, 10);
/*&nbsp;&nbsp;&nbsp;&nbsp;*/speed = random (10, 50);
/*&nbsp;&nbsp;*/}
/*<br>*/
/*&nbsp;&nbsp;*/x += random (-speed, speed);
/*&nbsp;&nbsp;*/y += random (-speed, speed);
/*<br>*/
/*&nbsp;&nbsp;*/if (x <= 0) x += 2;
/*&nbsp;&nbsp;*/if (x >= width) x -= 2;
/*&nbsp;&nbsp;*/if (y <= 0) y += 2;
/*&nbsp;&nbsp;*/if (y >= height) y -= 2;
/*<br>*/
/*&nbsp;&nbsp;*/stroke( random(220) );
/*&nbsp;&nbsp;*/line (x, y, px, py);
/*<br>*/
/*&nbsp;&nbsp;*/px = x;
/*&nbsp;&nbsp;*/py = y;
}
/*<br>*/
/*<span class>function windowResized()</span>*/
/*<span class>{</span>*/
/*&nbsp;&nbsp;*//*<span class>resizeCanvas (windowWidth, windowHeight);</span>*/
/*<span class>}</span>*/