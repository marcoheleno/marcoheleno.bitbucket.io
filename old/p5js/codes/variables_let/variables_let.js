/*<span class=hide>*/let variable_1 = 100;/*</span>*/
/*<span class=hide>*/let variable_2 = variable_1 * 5;/*</span>*/
function setup()
{
/*&nbsp;&nbsp;*/createCanvas (windowWidth, windowHeight);
/*<span class=hide>*/colorMode (RGB, 255, 255, 255, 100);/*</span>*/
/*<span class=hide>*/background (255);/*</span>*/
/*<span class=hide>*/noLoop();/*</span>*/
/*<br>*/
/*
&nbsp;&nbsp;// Block scoped
&nbsp;&nbsp;let variable_1 = 100;
*/
/*<br>*/
/*&nbsp;&nbsp;*/if (variable_1 > 50) 
/*&nbsp;&nbsp;*/{
/*&nbsp;&nbsp;&nbsp;&nbsp;*//*let variable_2 = variable_1 * 5;*/
/*&nbsp;&nbsp;&nbsp;&nbsp;*/text ("variable 2 = " + variable_2, 20, 20);
/*&nbsp;&nbsp;*/}
/*<br>*/
/*&nbsp;&nbsp;variable_1 = variable_2;
/*&nbsp;&nbsp;*/text ("variable 1 = " + variable_1, 20, 80);
}
/*<br>*/
function draw()
{
/*<span class=hide>*/if(frameCount===1) set_p5_canvas_to();/*</span>*/
/*<span class=hide>*/text ("variable 2 = " + variable_2, 20, 20);/*</span>*/
/*<span class=hide>*/text ("variable_1 = variable_2; // ERROR: variable_2 is not defined", 20, 60);/*</span>*/
/*<span class=hide>*/text ("variable 1 = " + variable_1, 20, 80);/*</span>*/
}
/*<br>*/
/*<span class>function windowResized()</span>*/
/*<span class>{</span>*/
/*&nbsp;&nbsp;*//*<span class>resizeCanvas (windowWidth, windowHeight);</span>*/
/*<span class>}</span>*/