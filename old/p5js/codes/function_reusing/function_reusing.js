/*<span>Code based on:</span>*/
/*<span><a target="_blank" href="https://marcoheleno.bitbucket.io/p5js/index.html?code=function_input_output">https://marcoheleno.bitbucket.io/p5js/index.html?code=function_input_output</a></span>*/
/*<br>*/
function setup()
{
/*&nbsp;&nbsp;*/createCanvas (windowWidth, windowHeight);
/*&nbsp;&nbsp;*/colorMode (RGB, 255, 255, 255, 100);
/*&nbsp;&nbsp;*/frameRate (1);
/*&nbsp;&nbsp;*/noStroke();
}
/*<br>*/
function draw()
{
/*<span class=hide>*/if(frameCount===1) set_p5_canvas_to();/*</span>*/
/*&nbsp;&nbsp;*/let x = random (0, width);
/*&nbsp;&nbsp;*/let y = random (0, height);
/*&nbsp;&nbsp;*/let max_size = random (1, width);
/*&nbsp;&nbsp;*/let num_ellipses = random (1, max_size);
/*&nbsp;&nbsp;*/let gradient_beginning = color (0);
/*&nbsp;&nbsp;*/let gradient_ending = randomColor();
/*<br>*/
/*&nbsp;&nbsp;*/gradientEllipse (x, y, max_size, num_ellipses, gradient_beginning, gradient_ending);
}
/*<br>*/
function gradientEllipse (x, y, max_size, num_ellipses, color_inner, color_outer) 
{
/*&nbsp;&nbsp;*/let gap = max_size/num_ellipses;
/*<br>*/
/*&nbsp;&nbsp;*/if (gap < 1) 
/*&nbsp;&nbsp;*/{
/*&nbsp;&nbsp;&nbsp;&nbsp;*/console.log ("Error: Number of circles cannot be greater than the maximum size");
/*&nbsp;&nbsp;&nbsp;&nbsp;*/noLoop();
/*&nbsp;&nbsp;*/}
/*<br>*/
/*&nbsp;&nbsp;*/else
/*&nbsp;&nbsp;*/{
/*&nbsp;&nbsp;&nbsp;&nbsp;*/for (let i=num_ellipses; i>0; i--) 
/*&nbsp;&nbsp;&nbsp;&nbsp;*/{
/*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;*/fill( lerpColor(color_inner, color_outer, i*(1/num_ellipses))  );
/*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;*/ellipse (x, y, i*gap, i*gap);
/*&nbsp;&nbsp;&nbsp;&nbsp;*/}
/*&nbsp;&nbsp;*/}
}
/*<br>*/
function randomColor() 
{
/*&nbsp;&nbsp;*/let random_color = color (random(0, 255), random(0, 255), random(0, 255), 0);
/*&nbsp;&nbsp;*/return random_color;
}
/*<br>*/
/*<span class>function windowResized()</span>*/
/*<span class>{</span>*/
/*&nbsp;&nbsp;*//*<span class>resizeCanvas (windowWidth, windowHeight);</span>*/
/*<span class>}</span>*/