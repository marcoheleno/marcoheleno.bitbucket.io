let x, y, speed;
let ball_radius, ball_radius_max, ball_radius_factor;
/*<br>*/
function setup()
{
/*&nbsp;&nbsp;*/createCanvas (windowWidth, windowHeight);
/*<br>*/
/*&nbsp;&nbsp;*/x = width/2;
/*&nbsp;&nbsp;*/y = height/2;
/*<br>*/
/*&nbsp;&nbsp;*/speed = 30;
/*<br>*/
/*&nbsp;&nbsp;*/ellipseMode (RADIUS);
/*&nbsp;&nbsp;*/ball_radius = 30;
/*&nbsp;&nbsp;*/ball_radius_max = 30;
/*&nbsp;&nbsp;*/radius_change_factor = 1;
/*<br>*/
/*&nbsp;&nbsp;*/noFill();
/*<br>*/
/*&nbsp;&nbsp;*/background (255);
}
/*<br>*/
function draw()
{
/*<span class=hide>*/if(frameCount===1) set_p5_canvas_to();/*</span>*/
/*<span class=hide>*/if(frameCount===1) x = width/2;/*</span>*/
/*&nbsp;&nbsp;*/if (frameCount%3 === 0) background (255, 10);
/*<br>*/
/*&nbsp;&nbsp;*/x += random (-speed, speed);
/*&nbsp;&nbsp;*/y += random (-speed, speed);
/*<br>*/
/*&nbsp;&nbsp;*/if (x-ball_radius <= 0) x += ball_radius_max*2;
/*&nbsp;&nbsp;*/if (x-ball_radius >= width) x -= ball_radius_max*2;
/*&nbsp;&nbsp;*/if (y-ball_radius <= 0) y += ball_radius_max*2;
/*&nbsp;&nbsp;*/if (y-ball_radius >= height) y -= ball_radius_max*2;
/*<br>*/
/*&nbsp;&nbsp;*/if (ball_radius<1 || ball_radius>ball_radius_max) 
/*&nbsp;&nbsp;*/{
/*&nbsp;&nbsp;&nbsp;&nbsp;*/radius_change_factor *= -1;
/*&nbsp;&nbsp;*/}
/*&nbsp;&nbsp;*/ball_radius += radius_change_factor;
/*<br>*/
/*&nbsp;&nbsp;*/stroke( random(255) );
/*&nbsp;&nbsp;*/ellipse (x, y, ball_radius, ball_radius);
}
/*<br>*/
/*<span class>function windowResized()</span>*/
/*<span class>{</span>*/
/*&nbsp;&nbsp;*//*<span class>resizeCanvas (windowWidth, windowHeight);</span>*/
/*<span class>}</span>*/