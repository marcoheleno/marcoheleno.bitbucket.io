/*<span>Code based on:</span>*/
/*<span><a target="_blank" href="https://marcoheleno.bitbucket.io/p5js/index.html?code=for_loop_chained">https://marcoheleno.bitbucket.io/p5js/index.html?code=for_loop_chained</a></span>*/
/*<br>*/
let pixel_size, pixels_array, num_columns, num_lines;
/*<br>*/
function setup()
{
/*&nbsp;&nbsp;*/createCanvas (windowWidth, windowHeight);
/*<br>*/
/*&nbsp;&nbsp;*/pixel_size = 15;
/*&nbsp;&nbsp;*/pixels_array = [];
/*&nbsp;&nbsp;*/noStroke();
}
/*<br>*/
function draw()
{
/*<span class=hide>*/if(frameCount===1) set_p5_canvas_to();/*</span>*/
/*&nbsp;&nbsp;*/num_columns = ceil (width/pixel_size);
/*&nbsp;&nbsp;*/num_lines = ceil (height/pixel_size);
/*<br>*/
/*&nbsp;&nbsp;*/for (let column=0; column<num_columns; column++) /*<span>&lt;num_columns; column++) </span>*/
/*&nbsp;&nbsp;*/{
/*&nbsp;&nbsp;&nbsp;&nbsp;*/pixels_array[column] = [];
/*&nbsp;&nbsp;&nbsp;&nbsp;*/for (let line=0; line<num_lines; line++) /*<span>&lt;num_lines; line++) </span>*/
/*&nbsp;&nbsp;&nbsp;&nbsp;*/{
/*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;*/pixels_array[column][line] = random (0, 255);
/*&nbsp;&nbsp;&nbsp;&nbsp;*/}
/*&nbsp;&nbsp;*/}
/*<br>*/
/*&nbsp;&nbsp;*/for (let column=0; column<num_columns; column++) /*<span>&lt;num_columns; column++) </span>*/
/*&nbsp;&nbsp;*/{
/*&nbsp;&nbsp;&nbsp;&nbsp;*/for (let line=0; line<num_lines; line++) /*<span>&lt;num_lines; line++) </span>*/
/*&nbsp;&nbsp;&nbsp;&nbsp;*/{
/*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;*/fill( pixels_array[column][line] );
/*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;*/rect( column*pixel_size, line*pixel_size, pixel_size, pixel_size );
/*&nbsp;&nbsp;&nbsp;&nbsp;*/}
/*&nbsp;&nbsp;*/}
}
/*<br>*/
/*<span class>function windowResized()</span>*/
/*<span class>{</span>*/
/*&nbsp;&nbsp;*//*<span class>resizeCanvas (windowWidth, windowHeight);</span>*/
/*<span class>}</span>*/