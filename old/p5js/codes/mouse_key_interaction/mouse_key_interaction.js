function setup()
{
/*&nbsp;&nbsp;*/createCanvas (windowWidth, windowHeight);
/*&nbsp;&nbsp;*/colorMode (HSB, 360, 100, 100, 100);
}
/*<br>*/
function draw()
{
/*<span class=hide>*/if(frameCount===1) set_p5_canvas_to();/*</span>*/
/*&nbsp;&nbsp;*/if (mouseIsPressed === true) 
/*&nbsp;&nbsp;*/{
/*&nbsp;&nbsp;&nbsp;&nbsp;*/stroke( map(mouseX, 0, width, 0, 360), 100, 100);
/*&nbsp;&nbsp;&nbsp;&nbsp;*/strokeWeight( abs(mouseX-pmouseX) + abs(mouseY-pmouseY) );
/*&nbsp;&nbsp;&nbsp;&nbsp;*/line (pmouseX, pmouseY, mouseX, mouseY);
/*&nbsp;&nbsp;*/}
}
/*<br>*/
function mousePressed() 
{
/*&nbsp;&nbsp;*/if (mouseButton === LEFT) clear();
}
/*<br>*/
function keyPressed() 
{
/*&nbsp;&nbsp;*/if (keyCode === ENTER || key === 'c' || key === 'C') 
/*&nbsp;&nbsp;*/{
/*&nbsp;&nbsp;&nbsp;*/clear();
/*&nbsp;&nbsp;*/}
}
/*<br>*/
/*<span class>function windowResized()</span>*/
/*<span class>{</span>*/
/*&nbsp;&nbsp;*//*<span class>resizeCanvas (windowWidth, windowHeight);</span>*/
/*<span class>}</span>*/

