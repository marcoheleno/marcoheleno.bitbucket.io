let object_01;
/*<br>*/
function setup()
{
/*&nbsp;&nbsp;*/createCanvas (windowWidth, windowHeight);
/*&nbsp;&nbsp;*/object_01 = new MyCircle();
}
/*<br>*/
function draw()
{
/*<span class=hide>*/if(frameCount===1) set_p5_canvas_to();/*</span>*/
/*&nbsp;&nbsp;*/background (255);
/*&nbsp;&nbsp;*/object_01.animate();
/*&nbsp;&nbsp;*/object_01.collision();
/*&nbsp;&nbsp;*/object_01.draw();
}
/*<br>*/
class MyCircle
{
/*&nbsp;&nbsp;*/constructor() 
/*&nbsp;&nbsp;*/{
/*&nbsp;&nbsp;&nbsp;&nbsp;*/this.x = random (0, width);
/*&nbsp;&nbsp;&nbsp;&nbsp;*/this.y = random (0, height);
/*&nbsp;&nbsp;&nbsp;&nbsp;*/this.speed_x = 1;
/*&nbsp;&nbsp;&nbsp;&nbsp;*/this.speed_y = 1;
/*&nbsp;&nbsp;&nbsp;&nbsp;*/this.size = random (10, 30);
/*&nbsp;&nbsp;*/}
/*<br>*/
/*&nbsp;&nbsp;*/animate() 
/*&nbsp;&nbsp;*/{
/*&nbsp;&nbsp;&nbsp;&nbsp;*/this.direction_x = int( random(-2, 2) );
/*&nbsp;&nbsp;&nbsp;&nbsp;*/this.x += this.speed_x * this.direction_x;
/*<br>*/
/*&nbsp;&nbsp;&nbsp;&nbsp;*/this.direction_y = int( random(-2, 2) );
/*&nbsp;&nbsp;&nbsp;&nbsp;*/this.y += this.speed_y * this.direction_y;
/*&nbsp;&nbsp;*/}
/*<br>*/
/*&nbsp;&nbsp;*/collision (x, y, size)
/*&nbsp;&nbsp;*/{
/*&nbsp;&nbsp;&nbsp;&nbsp;*/if (this.x < 0) this.x = 0;
/*&nbsp;&nbsp;&nbsp;&nbsp;*/if (this.x > width) this.x = width;
/*&nbsp;&nbsp;&nbsp;&nbsp;*/if (this.y < 0) this.y = 0;
/*&nbsp;&nbsp;&nbsp;&nbsp;*/if (this.y > height) this.y = height;
/*<br>*/
/*&nbsp;&nbsp;&nbsp;&nbsp;*/this.distance = dist (this.x, this.y, x, y);
/*&nbsp;&nbsp;&nbsp;&nbsp;*/if (this.distance <= this.size/2+size/2) 
/*&nbsp;&nbsp;&nbsp;&nbsp;*/{
/*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;*/this.speed_x = 3;
/*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;*/this.speed_y = 3;
/*<br>*/
/*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;*/this.direction_x *= -1;
/*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;*/this.direction_y *= -1;
/*<br>*/
/*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;*/this.x += this.speed_x * this.direction_x;
/*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;*/this.y += this.speed_y * this.direction_y;
/*<br>*/
/*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;*/this.speed_x = 1;
/*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;*/this.speed_y = 1;
/*&nbsp;&nbsp;&nbsp;&nbsp;*/}
/*&nbsp;&nbsp;*/}
/*<br>*/
/*&nbsp;&nbsp;*/draw() 
/*&nbsp;&nbsp;*/{
/*&nbsp;&nbsp;&nbsp;&nbsp;*/noFill();
/*&nbsp;&nbsp;&nbsp;&nbsp;*/ellipse(this.x, this.y, this.size, this.size);
/*&nbsp;&nbsp;*/}
}
/*<br>*/
/*<span class>function windowResized()</span>*/
/*<span class>{</span>*/
/*&nbsp;&nbsp;*//*<span class>resizeCanvas (windowWidth, windowHeight);</span>*/
/*<span class>}</span>*/