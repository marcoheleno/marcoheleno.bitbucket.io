const int photoresistorPin = A0;

const int pushButton1Pin = 4;

const int ledpin = 6; //PWM

int pushButton1State = 0;

int photoresistorMax = 0;
int photoresistorValue = 0;
int photoresistorMap = 0;

void setup() 
{
  Serial.begin (9600);
  
  pinMode (pushButton1Pin, INPUT);
  
  pinMode (ledpin, OUTPUT);
  
  delay(1000);
  
  photoresistorMax = analogRead (photoresistorPin);
  
  Serial.println (photoresistorPin);
  
  delay(1000);
}

void loop() 
{
  pushButton1State = digitalRead (pushButton1Pin);
  
  photoresistorValue = analogRead (photoresistorPin);
  
  photoresistorMap = constrain(map(photoresistorValue, 0, photoresistorMax, 0, 255), 0, 255);
  
  Serial.print (pushButton1State);
  Serial.print (" / ");
  Serial.println (photoresistorMap);
  
  if (pushButton1State == HIGH)
  {
  analogWrite (ledpin, photoresistorMap);
  }
  else
  {
  analogWrite (ledpin, 0);
  }
}
//saco 23