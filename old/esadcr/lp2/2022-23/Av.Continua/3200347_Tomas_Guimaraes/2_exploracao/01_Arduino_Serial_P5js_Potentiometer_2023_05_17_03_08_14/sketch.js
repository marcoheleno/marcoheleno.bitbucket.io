function setup() 
{
  createCanvas(windowWidth, windowHeight);
  startSerial ("COM3");
}


function draw()
{
  background(255);
  console.log (serial_data);
  
  const resultado_separador = split(serial_data, '/');
  
  const size = int(resultado_separador[2]);
  noFill();
  
  if ( int(resultado_separador[0]) === 1)
  {
    rect(width/2, height/2, size, size);
  }
  
  if ( int(resultado_separador[1]) === 1)
  {
    ellipse(width/2, height/2, size, size);
  }
  
}
