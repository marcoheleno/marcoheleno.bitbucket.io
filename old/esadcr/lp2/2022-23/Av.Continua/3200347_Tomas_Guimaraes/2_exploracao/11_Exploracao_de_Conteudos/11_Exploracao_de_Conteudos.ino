const int potentiometerPin = A0;
const int pushButton1Pin = 4;
const int pushButton2Pin = 7;

int pushButton1State = 0;
int pushButton2State = 0;

int potentiometerValue = 0;
int potentiometerMap = 0;

void setup() 
{
  Serial.begin (9600);
  
  pinMode (pushButton1Pin, INPUT);
  pinMode (pushButton2Pin, INPUT);
  
}

void loop() 
{
  potentiometerValue = analogRead (potentiometerPin);
  
  potentiometerMap = constrain(map(potentiometerValue, 0, 1023, 0, 255), 0, 255);
  
  pushButton1State = digitalRead (pushButton1Pin);
  pushButton2State = digitalRead (pushButton2Pin);
  
  Serial.print (pushButton1State);
  Serial.print ("/");
  Serial.print (pushButton2State);
  Serial.print ("/");
  Serial.println (potentiometerValue);
  
  }
//saco 23