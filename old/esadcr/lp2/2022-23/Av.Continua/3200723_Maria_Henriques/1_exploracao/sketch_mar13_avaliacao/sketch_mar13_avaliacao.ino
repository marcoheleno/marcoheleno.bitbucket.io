const int pushButton1Pin = 4;
const int pushButton2Pin = 2;

const int ledPin1 = 12;
const int ledPin2 = 8;
const int ledPin3 = 7;

int pushButton1State = 0;
int pushButton2State = 0;


void setup() {
    Serial.begin (9600);
    
    pinMode (pushButton1Pin, INPUT);
    pinMode (pushButton2Pin, INPUT);
    
    pinMode (ledPin1, OUTPUT);
    pinMode (ledPin2, OUTPUT);
    pinMode (ledPin3, OUTPUT);
}

void loop() {
    pushButton1State = digitalRead (pushButton1Pin);
    pushButton2State = digitalRead (pushButton2Pin);
    
   Serial.print (pushButton1State);
   Serial.print (" / ");
   Serial.println (pushButton2State);
   
   if (pushButton1State == HIGH)
   { 
    digitalWrite(ledPin1, HIGH);
   }
   else
   {
     digitalWrite(ledPin1, LOW);
   }
   
   if (pushButton2State == HIGH)
   { 
    digitalWrite(ledPin3, HIGH);
   }
   else
   {
     digitalWrite(ledPin3, LOW);
   }
   
   if (pushButton1State == HIGH && pushButton2State == HIGH)   // o || é um butão qualquer, && é os dois pressionados
   { 
    digitalWrite(ledPin2, HIGH);
    digitalWrite(ledPin1, LOW);
    digitalWrite(ledPin3, LOW);
   }
   else
   {
     digitalWrite(ledPin2, LOW);
   }
}

// meu kit é o 22
