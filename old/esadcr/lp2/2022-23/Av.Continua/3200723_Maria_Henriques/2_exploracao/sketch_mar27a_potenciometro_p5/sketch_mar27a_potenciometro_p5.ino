// Additional Software
// https://marcoheleno.bitbucket.io/lp2/repo_exercises/additional_software/index.html


const int potentiometerPin = A0;

int potentiometerValue = 0;
int potentiometerMap = 0;


void setup() 
{
  Serial.begin (9600);
}


void loop() 
{
  potentiometerValue = analogRead (potentiometerPin);

  potentiometerMap = constrain(map(potentiometerValue, 0, 1023, 0, 255), 0, 255);

  Serial.println (potentiometerMap);
}
