
const int pushButton1Pin = 4;
const int pushButton2Pin = 2;

const int LEDpin = 7;

int pushButton1State = 0;
int pushButton2State = 0;
int radomdelay = 0;


void setup() 
{
  Serial.begin (9600);
  
  pinMode (pushButton1Pin, INPUT);
  pinMode (pushButton2Pin, INPUT);
  
  pinMode (LEDpin, OUTPUT);
}


void loop() 
{
  pushButton1State = digitalRead (pushButton1Pin);
  pushButton2State = digitalRead (pushButton2Pin);
  radomdelay = int(random(0,3000));
  
  Serial.print (pushButton1State);
  Serial.print ("/");
  Serial.print (pushButton2State);
  Serial.print (":");
  Serial.println (radomdelay);
  
  if (pushButton1State == HIGH)
  {
    delay(1000);
    digitalWrite(LEDpin, HIGH);
    delay(1000);
    digitalWrite(LEDpin, LOW);
  }
  else 
  {
    digitalWrite(LEDpin, LOW);
  }
  
  if (pushButton2State == HIGH)
  {
    delay(radomdelay);
    digitalWrite(LEDpin, HIGH);
    delay(radomdelay);
    digitalWrite(LEDpin, LOW);
  }
  else 
  {
    digitalWrite(LEDpin, LOW);
  }
  
}
