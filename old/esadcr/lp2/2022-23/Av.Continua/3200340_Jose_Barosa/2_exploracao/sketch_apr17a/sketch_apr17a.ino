const int potentiometerPin = A0;
const int pushButton1Pin = 7;


int pushButton1State = 0;
int potentiometerValue = 0;
float potentiometerMap = 0;


void setup() 
{
  
  Serial.begin(9600);
  
  pinMode (pushButton1Pin, INPUT);
}


void loop() 
{
  potentiometerValue = analogRead (potentiometerPin);

  //potentiometerMap = float(constrain(map(potentiometerValue, 0, 1023, 0.0, 1.0), 0.0, 1.0));

    pushButton1State = digitalRead (pushButton1Pin);

  Serial.print (potentiometerValue);
  Serial.print ("/");
  Serial.println(pushButton1State);
}


