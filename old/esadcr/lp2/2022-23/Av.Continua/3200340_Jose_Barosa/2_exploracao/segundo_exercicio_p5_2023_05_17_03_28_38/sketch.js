let ele;
let esta_carregado;

function preload()
{
  ele = createAudio('lucky_dragons.mp3');
}

function setup() 
{
  createCanvas(windowWidth, windowHeight);
  startSerial ("COM5");
  
  esta_carregado = false;
}


function draw()
{
  //console.log (serial_data);
  background(255);
  
  const resultado_separador = split(serial_data, '/');
  
  if ( int(resultado_separador[1]) === 1 && esta_carregado === false)
  {
    esta_carregado = true;
    ele.play();
  }
  else 
  if ( int(resultado_separador[1]) === 0 && esta_carregado === true)
  {
    esta_carregado = false;
    ele.pause();
  }
  //const vol = map(resultado_separador[0], 0, 1023, 0, 1).toFixed(1);
  ele.volume( 0.5);

}
