let serial;
let serial_data = "waiting for data";


function startSerial (arduino_port) 
{
  serial = new p5.SerialPort();
  serial.list();
  serial.open (arduino_port);
  serial.on ('connected', serverConnected);
  serial.on ('list', gotList);
  serial.on ('data', gotData);
  serial.on ('error', gotError);
  serial.on ('open', gotOpen);
  serial.on ('close', gotClose);
}


function serverConnected() 
{
  console.log ("Connected to Server");
}


function gotList(thelist) 
{
  console.log ("List of Serial Ports:");

  for (let i = 0; i < thelist.length; i++) 
  {
    console.log (i + " " + thelist[i]);
  }
}


function gotOpen() 
{
  console.log ("Serial Port is Open");
}


function gotClose() 
{
  serial_data = "Serial Port is Closed";
  console.log (serial_data);
}


function gotError(theerror) 
{
  console.log (theerror);
}


function gotData() 
{
  const current_string = serial.readLine();
  trim (current_string);
  if (!current_string) return;
  
  serial_data = current_string;
  //console.log (serial_data);
}

