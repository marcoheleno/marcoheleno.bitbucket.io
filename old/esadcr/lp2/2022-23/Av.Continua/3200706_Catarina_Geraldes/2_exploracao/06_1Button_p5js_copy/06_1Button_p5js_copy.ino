// Additional Software
// https://marcoheleno.bitbucket.io/lp2/repo_exercises/additional_software/index.html


const int pushButton1Pin = 7;

int pushButton1State = 0;


void setup() 
{
  Serial.begin(9600);
  
  pinMode (pushButton1Pin, INPUT);
}


void loop() 
{  
  pushButton1State = digitalRead (pushButton1Pin);
  
  Serial.println(pushButton1State);
}
