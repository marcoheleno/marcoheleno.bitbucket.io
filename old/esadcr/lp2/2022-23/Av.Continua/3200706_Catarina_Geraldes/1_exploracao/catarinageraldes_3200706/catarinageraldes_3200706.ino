const int photoresistorPin = A0;

int photoresistorMax = 0;
int photoresistorValue = 0;
int photoresistorMap = 0;

const int pushButton1Pin = 4;

const int LEDpin = 6;

int pushbutton1State = 0;


void setup() 
{
 Serial.begin (9600);
 
 pinMode (LEDpin, OUTPUT);
  
  pinMode (pushButton1Pin, INPUT);
 
 delay(1000);
  
  photoresistorMax = analogRead (photoresistorPin);
  
  Serial.println (photoresistorMax);
  
  delay(1000);
}


void loop() 
{
  pushbutton1State = digitalRead (pushButton1Pin);
  photoresistorValue = analogRead (photoresistorPin);
  photoresistorMap = constrain(map(photoresistorValue, 0, photoresistorMax, 0, 255), 0, 255);
  
  if (pushbutton1State == HIGH)
  {
    analogWrite (LEDpin, photoresistorMap);
  }
  else 
  {
    analogWrite (LEDpin, 0);
  }
  
}

