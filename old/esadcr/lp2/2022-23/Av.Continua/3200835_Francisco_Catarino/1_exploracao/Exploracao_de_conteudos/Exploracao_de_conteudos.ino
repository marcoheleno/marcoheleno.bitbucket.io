//arduino num 26

const int photoresistorPin = A0;

const int LEDpin = 6; //PWM

int photoresistorMax = 0;
int photoresistorValue = 0;
int photoresistorMap = 0;

void setup() 
{
    Serial.begin (9600);
  
  pinMode (LEDpin, OUTPUT);
  
  delay(1000);
  
  photoresistorMax = analogRead (photoresistorPin);
  
  Serial.println (photoresistorMax);
  
  delay(1000);
  
}

void loop() 
{
  photoresistorValue = analogRead (photoresistorPin);
  
  //Serial.println (photoresistorValue);
  
  photoresistorMap = constrain(map(photoresistorValue, 0, photoresistorMax, 0, 255), 0, 255);
  
  Serial.println (photoresistorMap);
  
  analogWrite (LEDpin, photoresistorMap);
  delay (1000);
  analogWrite (LEDpin, 0);
  delay (1000);
}
