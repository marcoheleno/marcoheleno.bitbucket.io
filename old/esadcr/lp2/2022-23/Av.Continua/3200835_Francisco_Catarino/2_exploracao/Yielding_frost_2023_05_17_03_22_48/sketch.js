let serial;
let font;
let fontSize = 32;

function setup() {
  createCanvas(400, 400);
  font = loadFont("EBGaramond-SemiBold.ttf");
  textFont(font);
  textSize(fontSize);
  serial = new p5.SerialPort();
  serial.open('COM4');
  serial.on('data', serialEvent);
}

function draw() {
  background(220);
  text('Modular Type', 150, 150);
}

function serialEvent() {
  let data = serial.readStringUntil('\n');
  if (data !== null) {
    data = trim(data);
    let value = map(data, 0, 1023, 10, 100);
    fontSize = value;
    textSize(fontSize);
  }
}