/*
      One Step Away
Trabalho realizado por
-Catarina Geraldes – 3200706
-Francisco Catarino – 3200835
-José Barosa – 3200340
-Maria Henriques – 3200723
-Tomás Guimarães - 3200347
Docente - Marco Heleno
Enunciado - Emergência Ecológica
Design Gráfico e Multimédia 3ºano (2022/2023) 2ºsemestre
Laboratório de Projeto II
ESAD.CR
*/
// capacitive sensing includes
#include <CapacitiveSensor.h>

// capacitive sensing constant
CapacitiveSensor sensor = CapacitiveSensor(4,2);  // 1M resistor between pins 4 & 2, pin 2 is sensor pin
int limite_activacao = 100;

void setup() 
{
  Serial.begin(9600);
}

void loop() 
{
  long measurement =  sensor.capacitiveSensor(30);
  
  if (measurement > limite_activacao)
  {
    Serial.println("1");
    analogWrite (11, 255);
  }
  else
  {
    Serial.println("0");
    analogWrite (11, 40);
  }
  
  delay(10);
}
