/*
      One Step Away
Trabalho realizado por
-Catarina Geraldes – 3200706
-Francisco Catarino – 3200835
-José Barosa – 3200340
-Maria Henriques – 3200723
-Tomás Guimarães - 3200347
Docente - Marco Heleno
Enunciado - Emergência Ecológica
Design Gráfico e Multimédia 3ºano (2022/2023) 2ºsemestre
Laboratório de Projeto II
ESAD.CR
*/

let audio;

function preload()
{
  //audio = loadSound("sons/Foca Monge do Mediterrâneo.wav");
  //audio = loadSound("sons/Baleia Franca do Atlântico Norte.wav");
  audio = loadSound("sons/Urso Polar.wav");
  //audio = loadSound("sons/Bebe (1).wav");
  //audio = loadSound("sons/Raposa do Ártico.wav"); //O sou é preloaded da pasta sons podendo ser mudado entre os outros sons da pasta
}

function setup() 
{
  createCanvas(windowWidth, windowHeight);
  startSerial ("COM7");
}


function draw()
{
  console.log (serial_data);
  
  if (audio.isPlaying()===false)
  {
    background (255, 0, 0); //utilizamos o background para termos um input visual do inpput do button estar primido
    
    if ( int(serial_data)===1) 
    {
      audio.play(); //ao premir o sensor de capacitancia o seu valor muda para um mostrando que está ativo e é ativado o audio
    }
  }
  
  else 
    
  if (audio.isPlaying()===true)
  {
    background (0, 255, 0);
  }
  
}

