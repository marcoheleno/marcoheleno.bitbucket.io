/*
 Trabalho do Semibreve
Catarina Geraldes – 3200706
Francisco Catarino – 3200835
José Barosa – 3200340
Maria Henriques – 3200723
Tomás Guimarães - 3200347
*/
let som

function preload()
{
  soundFormats('wav');
  som = loadSound('assets/bengal_tiger.wav');
}

function setup() 
{
  createCanvas(windowWidth, windowHeight);
  startSerial ("COM7");
}


function draw()
{
  if ( int(serial_data) < 1) 
  {
    background (255);
    textSize(16);
    textAlign(CENTER);
    text('tigre', 30, 30)
  }
  
  else
  
  if ( int(serial_data) < 2) 
  {
    som.play();
  }
    
  else 
  {
    background (int(serial_data));
  }
  
  //serial.write( int(random(0, 2)) );
}

 /* console.log (serial_data);
  
 const resultado_separador = split(serial_data, '/');
  
  if ( int(resultado_separador[1]) === 1 && esta_carregado === false)
  {
    esta_carregado = true;
    som.play();
  }
  else 
  if ( int(resultado_separador[1]) === 0 && esta_carregado === true)
  {
    esta_carregado = false;
    som.pause();
  }
  //const vol = map(resultado_separador[0], 0, 1023, 0, 1).toFixed(1);
  //som.volume( 0.5);
  
  //serial.write( int(random(0, 2)) );
}
*/
