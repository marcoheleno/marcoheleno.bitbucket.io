

let timer;


window.onload = function() 
{
  set_student_project_section_to_window_innerHeight();
}


window.addEventListener ("resize", function() 
{
  set_student_project_section_to_window_innerHeight();
});


function set_student_project_section_to_window_innerHeight() 
{
  document.body.style.height = window.innerHeight + "px";
  document.getElementById("student_project").style.height = window.innerHeight-100 + "px";
  
  if (document.getElementById("defaultCanvas0") != null) prepare_p5_student_project();
}


document.addEventListener("DOMContentLoaded", function() 
{
  fetch_code_location();
});


function fetch_code_location() 
{
  const url_query = window.location.search;

  if (url_query != "") 
  {
    const url_param = new URLSearchParams (url_query);
    const url_param_val = url_param.get('student_project'); // section tag id
    const url_param_split = url_param_val.split("/");
    const url_param_briefing = url_param_split[0];
    const url_param_project = url_param_split[1];

    let url_script_element = document.createElement("script");
    url_script_element.setAttribute("src", "student_projects/"+url_param_briefing+"/"+url_param_project+"/"+url_param_project+".js");
    document.head.appendChild (url_script_element);

    document.getElementById("more_info").setAttribute("href", "https://bitbucket.org/marcoheleno/marcoheleno.bitbucket.io/src/master/esadcr/lp2/2020-21/student_projects/"+url_param_briefing+"/"+url_param_project+"/");
    document.getElementById("more_info").setAttribute("style", "visibility: visible;");
    
    if (url_param_briefing === "Semibreve_2021")
    {
      document.getElementById("help").setAttribute("style", "visibility: visible;");
    }

    timer = setInterval (prepare_p5_student_project, 1000);
  }
}

function prepare_p5_student_project() 
{
  let p5_containerSize = 
  {
    w: document.getElementsByTagName("section")[0].offsetWidth-1,
    h: document.getElementsByTagName("section")[0].offsetHeight-1
  };

  resizeCanvas (p5_containerSize.w, p5_containerSize.h);

  document.getElementById("student_project").appendChild(document.getElementById("defaultCanvas0"));

  clearInterval(timer);
}