Artificial System
=================

Relatório Final
---------------

#### Conceito

A inteligência artificial tem apresentado um grande avanço na forma de
como os sistemas simulam uma inteligência similar à humana, baseada em
padrões de enormes bancos de dados. O nosso objetivo foca-se na criação
de um ambiente que represente algoritmos, e que idealize a sua criação
pela parte do ser humano e a influência que tem atualmente sob o mesmo.
Deste modo, iremos criar um elemento central que simbolize a humanidade,
rodeado por componentes que representem estes algoritmos.

O resultado final do projeto será projetado numa parede de uma sala
escura, onde o utilizador terá a possibilidade de interagir com a obra,
permitindo girar o busto e observar assim a dependência que este cria
com as esferas (algoritmos), que eventualmente ficam viciados, de forma
a criar uma espécie de caos.

#### Moodboard

![](images/rId21.jpg)

#### Funcionamento do Projeto

Para o funcionamento do projeto optámos por criar uma experiência
sensorial através do *Arduíno* e do *p5js*, exposta numa sala escura,
onde o utilizador irá encontrar à sua frente um móvel iluminado, com uma
*breadboard* e os respetivos elementos para o seu funcionamento; um
computador e um projetor (que estará a projetar o busto). Quando o
utilizador se aproxima do móvel, interage com a fotorresistência da
*breadboard*, de modo a que a instalação se aperceba que há a presença
de um utilizador. Deste modo, as esferas (que representam os algoritmos)
aparecem e começam a movimentar-se a uma velocidade cada vez maior
criando um certo caos visual; o utilizador tem também a possibilidade de
movimentar o busto através de 3 potenciômetros também presentes na
*breadboard*.

Assim que o utilizador se afasta da fotorresistência, a experiência
visual volta ao início.

#### Ambiente / Estrutura da Instalação

![](images/rId24.png)

![](images/rId25.png)

![](images/rId26.png)

![](images/rId27.png)

#### Sensores utilizados

-   3 Potenciômetros (sensores analógicos);

-   1 Fotorresistência (sensor analógico).

#### Lista completa de requisitos técnicos para a montagem do Projeto

-   1 Arduíno;

-   1 *Breadboard*;

-   3 Potenciômetros;

-   1 Fotorresistência;

-   1 Resistência (10k ohms);

-   14 Cabos;

-   1 Computador Portátil;

-   1 Projetor;

-   1 Móvel (40x28x106cm).

#### Formas e comportamentos da parte visual em *P5JS*

Para a instalação funcionar, o utilizador terá de interagir com a
fotorresistência presente na *breadboard*, que irá fazer com que a
instalação se aperceba que temos um utilizador presente/próximo do
projeto, fazendo com que através do *p5js* apareçam esferas com cores
que representam redes sociais, à volta do busto, que vão aumentando cada
vez mais a sua velocidade conforme o tempo de interação do utilizador.
Este tem ainda disponível 3 potenciômentros que pode girar de forma a
controlar a posição do busto, cada um num eixo diferente - X, Y, e Z -
de 0 a 360º.

#### Tecnologias usadas

-   Computador Portátil - todo o processo de desenvolvimento do projeto
    (código no editor online do *p5js* + no editor online do *Arduíno*);

-   *Arduíno*;

-   Programa 3D (Blender) - para a elaboração do ambiente/estrutura da
    instalação;

-   iPhone 11 - gravação 4K a 60fps da demonstração do funcionamento do
    projeto;

-   Programa de edição de video (Adobe Premiere Pro) - elaboração do
    video final.

##### Exercício de Avaliação: Semibreve\_2021

Anabela Vicente, nº3180183

Daniela Martins, nº3180758

Mariana Soares, nº3180483

##### UC Laboratório de Projeto II (2020/2021)

##### Docente: Marco Heleno
