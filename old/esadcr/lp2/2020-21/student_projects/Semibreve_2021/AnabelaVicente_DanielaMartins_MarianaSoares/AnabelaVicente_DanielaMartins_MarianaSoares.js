// Anabela Vicente, aluna nº 3180183
// Daniela Martins, aluna nº 3180758
// Mariana Soares,  aluna nº 3180483
//
// Exercício de Avaliação: Semibreve_2021
//
// Ano Letivo: 3º Ano, 2020-2021
// 2º Semestre
// Unidade Curricular: Laboratório de Projeto II
// Licenciatura em Design Gráfico e Multimédia
// Escola Superior de Artes e Design - ESAD.CR
// Docente: Marco Heleno
// Data de avaliação: 16 de Junho 2021

//-----------------------------------------------------------------------------------

// Instruções das interações dos elementos físicos com o espaço:

// Interação com o Potenciômetro 1
  // rotação do busto que vai dos 0 aos 360 graus no eixo de X

// Interação com o Potenciômetro 2
  // rotação do busto que vai dos 0 aos 360 graus no eixo de Y

// Interação com o Potenciômetro 3
  // rotação do busto que vai dos 0 aos 360 graus no eixo de Z
  
// Interação com a Fotorresistência
  // reação da fotorresistência devido à presença de um utilizador = esferas aparecem

//-----------------------------------------------------------------------------------

// Arduino
let serial;
let latestData = "waiting for data";
let frameCountGuardado = 0;
// Variável de interação do utilizador
let user = false; // Ausência de interação do utilizador = ausência de esferas

// Variável da Velocidade das esferas
let v = 0.01;

// Objeto central (busto)
function preload() {
  obj3D = loadModel("student_projects/Semibreve_2021/AnabelaVicente_DanielaMartins_MarianaSoares/head.obj", true); // Carregar o objeto
}

// Definição do material
var matWhite = {
  diff: [0.6, 0.3, 0.3], // Reflexão difusa (produzida por superfícies ásperas que tendem a refletir luz em todas as direções)
  spec: [255, 200, 200], // Reflexão especular (produzida por superfícies lisas que refletem luz num ângulo definido)
  spec_exp: 500.0 // Exposição da reflexão especular
};

// Material
var materials = [matWhite];

// Definição dos cálculos das cores das pointlights (luz emitida pelas esferas 0, 1, 2, 3, 4)
var pointlights = [
  // Cor branca (0)
  {
    pos: [0, 0, 0, 1], // Posição
    col: [1.00, 1.00, 1.00], // Cor (RGB)
    rad: 400 // Quantidade de luz transmitida
  },
  // Cor rosa (1)
  {
    pos: [0, 0, 0, 1], // Posição
    col: [2.00, 0.00, 0.50], // Cor (RGB)
    rad: 200 // Quantidade de luz transmitida
  },
  // Cor azul (2)
  {
    pos: [0, 0, 0, 1], // Posição
    col: [0.00, 0.40, 1.00], // Cor (RGB)
    rad: 200 // Quantidade de luz transmitida
  },
  // Cor amarela (3)
  {
    pos: [0, 0, 0, 1], // Posição
    col: [1.00, 0.40, 0.00], // Cor (RGB)
    rad: 300 // Quantidade de luz transmitida
  },
  // Cor verde (4)
  {
    pos: [0, 0, 0, 1], // Posição
    col: [0.00, 1.00, 0.00], // Cor (RGB)
    rad: 300 // Quantidade de luz transmitida
  },
];

// Geometria
var busto_def = {
  r1: 100, // Raio
};


function setup() {
  createCanvas(windowWidth, windowHeight, WEBGL);

// Arduino
serial = new p5.SerialPort();

 serial.list();
 serial.open('/dev/tty.usbmodem14601');

 serial.on('connected', serverConnected);

 serial.on('list', gotList);

 serial.on('data', gotData);

 serial.on('error', gotError);

 serial.on('open', gotOpen);

 serial.on('close', gotClose);
  
  
  var phong_vert = document.getElementById("phong.vert").textContent;
  var phong_frag = document.getElementById("phong.frag").textContent;

  phongshader = new p5.Shader(this._renderer, phong_vert, phong_frag);

  // Câmera 1
  var state1 = {
    distance: 700, // Distância da câmera aos objetos
    // center: [0, 0, 0], // Centro da câmera aos objetos
    // rotation: [1, 1, 0, 0], // Rotação da câmera aos objetos
  };
  
  easycam = new Dw.EasyCam(this._renderer, state1);
}


// Arduino
function reStart () // Para recomeçar as Esferas
{
  phong_vert = document.getElementById("phong.vert").textContent;
  phong_frag = document.getElementById("phong.frag").textContent;
  phongshader = new p5.Shader(this._renderer, phong_vert, phong_frag);
}

function serverConnected() {
 print("Connected to Server");
}

function gotList(thelist) {
 print("List of Serial Ports:");

 for (let i = 0; i < thelist.length; i++) {
  print(i + " " + thelist[i]);
 }
}

function gotOpen() {
 print("Serial Port is Open");
}

function gotClose(){
 print("Serial Port is Closed");
 latestData = "Serial Port is Closed";
}

function gotError(theerror) {
 print(theerror);
}

function gotData() {
 let currentString = serial.readLine();
  trim(currentString);
 if (!currentString) return;
 console.log(currentString);
 latestData = currentString;
}


function draw() {
  
  // Arduino
  let myStrArr = splitTokens(latestData, '/');
  
  let pot1 = int (myStrArr[0]); // Potenciômetro 1
  let pot2 = int (myStrArr[1]); // Potenciômetro 2
  let pot3 = int (myStrArr[2]); // Potenciômetro 3
  let fot = int (myStrArr[3]); // Fotorresistência (sensor)
  
  push(); // Guarda a posição atual da origem de desenho
  translate(0, 0, 0); // Faz a alteração à origem de desenho
  rotateX (radians(pot1)); // Potenciômetro 1 - roda o busto no eixo de X
  rotateY (radians(pot2)); // Potenciômetro 2 - roda o busto no eixo de Y
  rotateZ (radians(pot3)); // Potenciômetro 3 - roda o busto no eixo de Z
  
  background(0); // Definição da cor de fundo
  
  push(); // Guarda a posição atual da origem de desenho 
  translate(0, 0, 0); // Faz a alteração à origem de desenho
  rotateX(radians(0)); // Rotação do objeto conforme o valor do ângulo inserido, no eixo X
  rotateY(radians(0)); // Rotação do objeto conforme o valor do ângulo inserido, no eixo Y
  rotateZ(radians(180)); // Rotação do objeto conforme o valor do ângulo inserido, no eixo Z
  scale(2.5); // Definir o tamanho do busto
  noStroke(); // Definir o contorno
  model(obj3D); // Definição do objeto 3D
  pop(); // Repõe as configurações guardadas no push() anterior 
  
  
  // Animação das esferas - 2 pointlights movem-se livremente no espaço e 3 ao redor do busto
  
  // push() guarda a posição atual da origem de desenho
  push(); {
    var ang = sin( (frameCount-frameCountGuardado) * v) * 0.6; // Velocidade de Rotação + Ângulo
    var ty = busto_def.r1 * 2 + (1 - abs(ang)) * 100; // Movimento/raio

    // Pointlight em frente ao busto
    push(); // Guarda a posição atual da origem de desenho 
    rotateX((ang + 1) * PI / 2); // Rotação da pointlight no eixo x, com valor de "1" para que esta se movimente na parte frontal do busto + PI (3,14) a dividir por 2 de forma a percorrer apenas meia volta ao busto
    translate(0, ty, 0); // Alteração à origem de desenho (em "ty" = busto)
    addPointLight(phongshader, pointlights,0); // Adição da pointlight com a cor branca (0)
    pop(); // Repõe as configurações guardadas no push() anterior 

    // Interação através da Fotorresistência (sensor) 
    // Se a luminosidade captada pela fotorresistência for menor que 90, significa que o utilizador está a interagir com a mesma
    if (fot < 90) 
    {
      user = true; // Interação do utilizador = esferas aparecem

    // Pointlight que faz uma volta ao busto perfeita e infinita
    push(); // Guarda a posição atual da origem de desenho 
    rotateZ((frameCount-frameCountGuardado) * v); // Rotação da pointlight no eixo Z + Velocidade de Rotação
    translate(240, 0, 0); // Alteração à origem de desenho (em X)
    addPointLight(phongshader, pointlights, 1); // Adição da pointlight com a cor rosa (1)
    pop(); // Repõe as configurações guardadas no push() anterior
    
    // Pointlight que não segue uma linha contínua no espaço
    push(); // Guarda a posição atual da origem de desenho 
    rotateZ(0 * TWO_PI / 3 + (frameCount-frameCountGuardado) * v); // Rotação da pointlight no eixo Z; TWO_PI (6.28) - duas vezes o radio da circunferência de um círculo ao seu diâmetro, a dividir por 3 + Velocidade de Rotação
    translate(10, 90, 0); // Alteração à origem de desenho (em X e Y)
    rotateY(sin((frameCount-frameCountGuardado) * v) * TWO_PI); // Rotação da pointlight no eixo Y + Velocidade de Rotação
    translate(50, 100, 20); // Alteração à origem de desenho (em X, Y, Z)
    addPointLight(phongshader, pointlights, 2); // Adição da pointlight com a cor azul (2)
    pop(); // Repõe as configurações guardadas no push() anterior 

    // Pointlight que emite luz amarela
    push(); // Guarda a posição atual da origem de desenho 
    rotateZ(2 * TWO_PI / 3 + (frameCount-frameCountGuardado) * v); // Rotação da pointlight no eixo Z; TWO_PI (6.28) - duas vezes o radio da circunferência de um círculo ao seu diâmetro, a dividir por 3 + Velocidade de Rotação
    translate(180, 80, 100); // Alteração à origem de desenho (em X, Y, Z)
    rotateY((frameCount-frameCountGuardado) * v); // Rotação da pointlight no eixo Y + Velocidade de Rotação
    translate(180, 60, 20); // Alteração à origem de desenho (em X, Y, Z)
    addPointLight(phongshader, pointlights, 3); // Adição da pointlight com a cor amarela (3)
    pop(); // Repõe as configurações guardadas no push() anterior 
    
    // Pointlight que não chega a dar uma volta completa horizontalmente
    push(); // Guarda a posição atual da origem de desenho 
    rotateZ(+PI / 2 + sin((frameCount-frameCountGuardado) * v) * 2 * PI / 3); // Rotação da pointlight no eixo Z; PI (3,14) a dividir por 2 de forma a percorrer apenas meia volta + Velocidade de Rotação
    translate(180, 80, 0); // Alteração à origem de desenho (em X e Y)
    rotateY((frameCount-frameCountGuardado) * v); // // Rotação da pointlight no eixo Y + Velocidade de Rotação
    // translate(0, 0, 0);
    addPointLight(phongshader, pointlights, 1); // Adição da pointlight com a cor rosa (1)
    pop(); // Repõe as configurações guardadas no push() anterior 
    
    // Pointlight que não chega a dar uma volta completa horizontalmente a nível dos olhos
    push(); // Guarda a posição atual da origem de desenho 
    rotateZ(+PI / 2 + sin((frameCount-frameCountGuardado) * v) * 2 * PI / 3); // Rotação da pointlight no eixo Z; PI (3,14) a dividir por 2 de forma a percorrer apenas meia volta + Velocidade de Rotação (ida)
    translate(120, 0, 80); // Alteração à origem de desenho (em X e Z)
    rotateY((frameCount-frameCountGuardado) * v); // // Rotação da pointlight no eixo Y + Velocidade de Rotação (volta)
    // translate(0, 0, 0);
    addPointLight(phongshader, pointlights, 4); // Adição da pointlight com a cor verde (4)
    pop(); // Repõe as configurações guardadas no push() anterior
  }
  
  // Quando o código verifica que não há nenhum utilizador a interagir com a fotorresistência
  else if (user === true)
  {
    user = false; // Ausência de interação do utilizador = Esferas desaparecem
    // Na ausência de interação do utilizador, o desenho apresenta os seus valores iniciais:
    frameCountGuardado = frameCount; // 0
    v = 0.01; // Velocidade das Esferas
    
    reStart (); // Quando o utilizador pára de interagir com a fotorresistência (esta capta uma luminosidade maior que 90), o desenho recomeça = as esferas desaparecem 
  }
  
  // Velocidade de rotação das esferas
  if (user === true) // Quando existe interação do utilizador
  {
    // Se o valor da velocidade das esferas for menor que o valor de (0.25)
    if (v < 0.25) {
      // Vamos adicionando o valor de (0.00005), de forma a que as esferas comecem a aumentar a velocidade gradualmente, até que cheguem ao valor de (0.25)
      v = v + 0.00005;
      }
    // Quando atingem o valor de (0.25), retornam o seu valor inicial de (0.01)
    else {
      v = 0.01;
      background (255); // Para simular um ligeiro flash quando as esferas retornam o seu valor inicial
      }
    }
    
  }

  pop();
  
// Definição do phongshader como o shader
  // setShader ativa sombras e luzes; resetShader retira sombras e luzes
  setShader(phongshader);
}


// Definição do shader
function setShader(shader) {
  this.shader(shader);
}


// Definição do shader no material
function setMaterial(shader, material) {
  shader.setUniform('material.diff', material.diff);
  shader.setUniform('material.spec', material.spec);
  shader.setUniform('material.spec_exp', material.spec_exp);
}


// Definição do shader nas pointlights
function addPointLight(shader, pointlights, idx) {
  var light = pointlights[idx];

  light.pos_cam = easycam.renderer.uMVMatrix.multVec(light.pos);

  shader.setUniform('pointlights[' + idx + '].pos', light.pos_cam);
  shader.setUniform('pointlights[' + idx + '].col', light.col);
  shader.setUniform('pointlights[' + idx + '].rad', light.rad);

  // Atribuição do material dos objetos (e cor)
  setMaterial(phongshader, matWhite);
  noStroke(); // Definir o contorno
  
  // Tamanho das esferas
  sphere(8);
}

// Matrix
// Multiplica: vdst = mat * vsrc
p5.Matrix.prototype.multVec = function(vsrc, vdst) {

  vdst = (vdst instanceof Array) ? vdst : [];

  var x = 0,
    y = 0,
    z = 0,
    w = 1;

  var mat = this.mat4 || this.mat3;
  if (mat.length === 16) {
    vdst[0] = mat[0] * x + mat[4] * y + mat[8] * z + mat[12] * w;
    vdst[1] = mat[1] * x + mat[5] * y + mat[9] * z + mat[13] * w;
    vdst[2] = mat[2] * x + mat[6] * y + mat[10] * z + mat[14] * w;
    vdst[3] = mat[3] * x + mat[7] * y + mat[11] * z + mat[15] * w;
  } else {
    vdst[0] = mat[0] * x + mat[3] * y + mat[6] * z;
    vdst[1] = mat[1] * x + mat[4] * y + mat[7] * z;
    vdst[2] = mat[2] * x + mat[5] * y + mat[8] * z;
  }
  return vdst;
}

/*
function windowResized()
{
  resizeCanvas (windowWidth, windowHeight);
}
*/

function keyPressed() 
{
  latestData = "0/0/0/89";
}

//-----------------------------------------------------------------------------------

// The p5.EasyCam library - Easy 3D CameraControl for p5.js and WEBGL.

// Copyright 2018 by Thomas Diewald (https://www.thomasdiewald.com)

// Source: https://github.com/diwi/p5.EasyCam

// MIT License: https://opensource.org/licenses/MIT