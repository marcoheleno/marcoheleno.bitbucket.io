// Anabela Vicente, aluna nº 3180183
// Daniela Martins, aluna nº 3180758
// Mariana Soares,  aluna nº 3180483
//
// Exercício de Avaliação: Semibreve_2021
//
// Ano Letivo: 3º Ano, 2020-2021
// 2º Semestre
// Unidade Curricular: Laboratório de Projeto II
// Licenciatura em Design Gráfico e Multimédia
// Escola Superior de Artes e Design - ESAD.CR
// Docente: Marco Heleno
// Data de avaliação: 16 de Junho 2021

//-----------------------------------------------------------------------------------

// Instruções das interações dos elementos físicos com o espaço:

// Interação com o Potenciômetro 1
  // rotação do busto (p5js) que vai dos 0 aos 360 graus no eixo de X

// Interação com o Potenciômetro 2
  // rotação do busto (p5js) que vai dos 0 aos 360 graus no eixo de Y

// Interação com o Potenciômetro 3
  // rotação do busto (p5js) que vai dos 0 aos 360 graus no eixo de Z
  
// Interação com a Fotorresistência
  // reação da fotorresistência devido à presença de um utilizador = esferas aparecem (p5js)

//-----------------------------------------------------------------------------------

// "const" = constante: qualificador que modifica o comportamento de uma variável, e faz com que esta seja de "apenas-leitura"
const int pot1 = A0; // variável (apenas de leitura) do potenciômetro 1 que está ligado ao pino A0
const int pot2 = A1; // variável (apenas de leitura) do potenciômetro 2 que está ligado ao pino A1
const int pot3 = A2; // variável (apenas de leitura) do potenciômetro 3 que está ligado ao pino A2
const int photoresistorPin = A3; // variável (apenas de leitura) da fotorresistência que está ligada ao pino A3

// "int" = inteiros: variável que guarda dados de valores inteiros
int pot1Value = 90; // variável que guarda a leitura analógica transmitida pelo potenciômetro 1, cujo valor inicial é 90
int pot1Map = 0; // variável que guarda os valores de leitura definidos para o potenciômetro 1 (re-mapeamento)

int pot2Value = 180; // variável que guarda a leitura analógica transmitida pelo potenciômetro 2, cujo valor inicial é 180
int pot2Map = 0; // variável que guarda os valores de leitura definidos para o potenciômetro 2 (re-mapeamento)

int pot3Value = 360; // variável que guarda a leitura analógica transmitida pelo potenciômetro 3, cujo valor inicial é 360
int pot3Map = 0; // variável que guarda os valores de leitura definidos para o potenciômetro 3 (re-mapeamento)

int photoresistorMax = 0; // variável que guarda os valores definidos para a fotorresistência, relativamente à ausência/presença de um utilizador (de 0 = ausência de um utilizador; a 100 = presença de um utilizador) *
int photoresistorValue = 0; // variável que guarda a leitura analógica transmitida pela fotorresistência, cujo valor inicial é 0
int photoresistorMap = 0; // variável que guarda os valores de leitura definidos para a fotorresistência (re-mapeamento)


void setup() // código que é executado apenas uma vez, logo de início:
{
  // comunicação entre uma placa Arduino e um computador (Serial):
  Serial.begin (9600); // iniciar velocidade de comunicação serial a 9600bps (bits por segundo)

  delay(1000); // 1000 milissegundos (= 1 segundo) de espera antes da próxima leitura
  
  photoresistorMax = analogRead (photoresistorPin); // lê o pino de entrada da fotorresistência, que inicia com o valor 0 (= sem interação de um utilizador)
  
  Serial.println (photoresistorMax); // envia para a porta serial os dados que lê da fotorresistência, com o valor 0 (= sem interação de um utilizador)
  // "ln": newline = nova linha (= fim da linha atual)
}


void loop() // código que fica em constante execução e atualização:
{
  pot1Value = analogRead (pot1); // lê o valor analógico do potenciômetro 1 (no eixo X)
  pot2Value = analogRead (pot2); // lê o valor analógico do potenciômetro 2 (no eixo Y)
  pot3Value = analogRead (pot3); // lê o valor analógico do potenciômetro 3 (no eixo Z)
  photoresistorValue = analogRead (photoresistorPin); // lê o valor analógico da fotorresistência
  
  // "constrain": restringe um número a ficar dentro de um intervalo = "constrain(x[número a restringir], a[extremo menor do intervalo], b[extremo maior do intervalo])"
  // "map(valor[número/qualquer tipo de dado a ser mapeado, neste caso - três potenciômetros e uma fotorresistência], deMenor[limite menor do intervalo atual do valor], deMAior[limite maior do intervalo atual do valor], paraMenor[o menor limite alvo do intervalo], paraMaior[o maior limite alvo do intervalo])"
  pot1Map = constrain( map(pot1Value, 0, 1023, 0, 360), 0, 360 ); // os valores de leitura do potenciômetro 1 são limitados entre 0 a 360 (graus)
  pot2Map = constrain( map(pot2Value, 0, 1023, 0, 360), 0, 360 ); // os valores de leitura do potenciômetro 2 são limitados entre 0 a 360 (graus)
  pot3Map = constrain( map(pot3Value, 0, 1023, 0, 360), 0, 360 ); // os valores de leitura do potenciômetro 3 são limitados entre 0 a 360 (graus)
  photoresistorMap = constrain( map(photoresistorValue, 200,  photoresistorMax, 0, 100), 0, 100 ); // os valores de leitura da fotorresistência são limitados entre 0 a 100 (intensidade de luz)

  Serial.print (pot1Map); // envia para a porta serial os dados que lê do potenciômetro 1
  Serial.print (" / "); // "fecha" - separa o potenciômetro 1 do potenciômetro 2 (elementos físicos)
  Serial.print (pot2Map); // envia para a porta serial os dados que lê do potenciômetro 2
  Serial.print (" / "); // "fecha" - separa o potenciômetro 2 do potenciômetro 3 (elementos físicos)
  Serial.print (pot3Map); // envia para a porta serial os dados que lê do potenciômetro 3
  Serial.print (" / "); // "fecha" - separa o potenciômetro 3 da fotorresistência (elementos físicos)
  Serial.println (photoresistorMap); // envia para a porta serial os dados que lê da fotorresistência
  // "ln": newline = nova linha (= fim da linha atual)
  
  delay(200); // 200 milissegundos (= 0.2 segundos) de espera antes da próxima leitura
}