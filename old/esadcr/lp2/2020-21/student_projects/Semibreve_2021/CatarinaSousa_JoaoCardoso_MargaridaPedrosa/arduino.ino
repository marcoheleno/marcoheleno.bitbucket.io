// Unidade Curricular de Laboratório de Projecto II
// Professor Marco Heleno
// Licenciatura de Design Gráfico e Multimédia 2º Semestre / 3º Ano
// Ano Letivo 2020/2021
// Trabalho realizado por:
// Catarina Sousa    - 3170233
// João Cardoso      - 3180756
// Margarida Pedrosa - 3180452
// SEMIBREVE

const int pushButton1Pin = 2; //definir a porta pela qual o botão 1 está ligado no circuito do arduino
const int pushButton2Pin = 4; //definir a porta pela qual o botão 2 está ligado no circuito do arduino
const int pushButton3Pin = 6; //definir a porta pela qual o botão 3 está ligado no circuito do arduino


int pushButton1State = 0; // dado definido como inteiro, que conecta o sinal que recebe do botão. Se não for pressionado, mantém-se no 0 não enviando qualquer sinal para o programa
int pushButton2State = 0; // dado definido como inteiro, que conecta o sinal que recebe do botão. Se não for pressionado, mantém-se no 0 não enviando qualquer sinal para o programa
int pushButton3State = 0; // dado definido como inteiro, que conecta o sinal que recebe do botão. Se não for pressionado, mantém-se no 0 não enviando qualquer sinal para o programa

boolean carregou = false; //o boolean oferece à variável carregou dois valores, aqui está negativo
int pushButton1StateCarregou = 0; //dado inteiro, 0 significa que não recebe o sinal, que se mantém inativo
int pushButton2StateCarregou = 0; //dado inteiro, 0 significa que não recebe o sinal, que se mantém inativo
int pushButton3StateCarregou = 0; //dado inteiro, 0 significa que não recebe o sinal, que se mantém inativo

void setup()
{
  Serial.begin (9600); //define o ritmo de bits por segundo

  pinMode (pushButton1Pin, INPUT); //configuração do pino do arduino para INPUT, preparando-o para receber data do botão 1
  pinMode (pushButton2Pin, INPUT); //configuração do pino do arduino para INPUT, preparando-o para receber data do botão 2
  pinMode (pushButton3Pin, INPUT); //configuração do pino do arduino para INPUT, preparando-o para receber data do botão 3
  
 }


void loop()
{
    
  pushButton1State = digitalRead (pushButton1Pin); //lê o sinal do pin digital que foi conectado ao botão 1 anteriormente
  pushButton2State = digitalRead (pushButton2Pin); //lê o sinal do pin digital que foi conectado ao botão 3 anteriormente
  pushButton3State = digitalRead (pushButton3Pin); //lê o sinal do pin digital que foi conectado ao botão 3 anteriormente
  
  if (pushButton1State == 1 || pushButton2State == 1 || pushButton3State == 1) //se receber o sinal do botão
  {
    carregou = true; // o valor da variável torna-se positivo
    pushButton1StateCarregou = pushButton1State; // de 0 passa a 1
    pushButton2StateCarregou = pushButton2State; // de 0 passa a 1
    pushButton3StateCarregou = pushButton3State; // de 0 passa a 1
  }
  
  if (carregou == true && pushButton1State == 0 && pushButton2State == 0 && pushButton3State == 0) //se não receber o sinal do botão
    {
      Serial.print   (pushButton1StateCarregou); //imprime na consola o sinal recebido
      Serial.print   (pushButton2StateCarregou);  //imprime na consola o sinal recebido
      Serial.println   (pushButton3StateCarregou);  //imprime na consola o sinal recebido, com o valor
      carregou = false; //volta à condição anterior e mantém-se inativo
    }
    
  
}
