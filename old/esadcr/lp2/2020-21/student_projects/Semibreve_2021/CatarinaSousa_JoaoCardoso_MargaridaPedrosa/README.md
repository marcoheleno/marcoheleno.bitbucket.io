Projeto "Automação Contemporânea" de Laboratório de Projeto II
================================================================

### por Catarina Sousa 3170233, João Cardoso 3180756 e Margarida Pedrosa 3180452

Quem és? - Semibreve\_2021
--------------------------

Foi nos desafiado, enquanto estudantes da Esad.cr no curso de Design
Multimédia, a criar uma proposta de instalação para o evento
Semibreve\_2021 que visa estimular a criação artística no domínio da
intersecção entre arte e tecnologia. Explorando técnicas de computação
física e p5js, o tema deste projeto será "Automação Contemporânea".

Esta é a nossa proposta.

**4º momento de avaliação - Conceito:**

De forma a valorizar a presença humana no festival Semibreve, iríamos
apresentar ao utilizador um Quiz/Formulário que iria determinar se este
era humano ou máquina. Partimos da premissa que o público seria um
algoritmo, cabendo a si próprio provar a sua humanidade.

O utilizador iria ter 3 possíveis respostas para cada uma de 5 perguntas
e o nosso objetivo é criar uma reflexão sobre a sua própria essência, e
questionar-se o que o torna humano quando confrontado com estas
questões, tendo conseguido ultrapassar este desafio ao acertar a última
pergunta.

Servirá então como um jogo interativo, cujo contexto se reflete numa
sociedade cada vez mais tecnológica, mais sedenta de inovação que poderá
esquecer-se que alguns dos seus próprios comportamentos se assemelham
com automatização. Este projeto então serve como uma pausa na sua rotina
e um apelo à consciencialização da sua humanidade.

**Funcionamento do projeto**

O utilizador, após entrar no edifício em que decorre o evento Semibreve,
vai se encontrar numa sala comum, que entre musica e artes, irá se
deparar com uma instalação na sala onde se poderá ler "QUEM ÉS?". Após
o público ter de facto se aproximado, terá a oportunidade de responder
entre 3 opções com um comando à sua frente.

Do resultado do quiz, terá dois feedbacks diferentes "ÉS HUMANO.
PARABÉNS" ou "ÉS UM ROBOT, TENTA OUTRA VEZ".

**Sketch/mockup do espaço envolvente**

![](images/rId21.png)

**Estrutura da instalação**

![](images/rId22.jpg)

**Requisitos técnicos para a montagem do projeto**

-   1 Arduino e consequente cabo de ligação ao pc

-   3 Botões - Sensores

-   1 Breadboard

-   3 Resistências 220 Ohms

-   8 Cabos

-   1 computador

-   1 projetor

-   Ligação à internet e eletricidade

**Moodboard**

![](images/rId23.png)

**Comportamento P5.js**

É a partir do p5.js que conseguimos controlar o feedback que o
utilizador recebe ao interagir com esta instalação. Ao ligar os botões
presentes no circuito com respostas predefinidas em formato png em que
serão dispostas as perguntas e as respostas do formulário, temos a
possibilidade de felicitar o público com a resposta certa ( e passagem
para a próxima pergunta) e a de apresentar uma mensagem de erro e
motivar o utilizador a pensar na resposta correta.

**Método de trabalho/pesquisa:**

Após uma ideia inicial que se explorava pela interpretação de estímulos
mentais dos espectadores através de biofeedbacks físicos, como ligação
ao projeto anterior de Automação Contemporânea, foi nos dado a entender
que não conseguiríamos resultados fidedignos. Tentamos então explorar
conceitos mais acessíveis, tendo encontrado o tema Man vs Machine.
Seguiu-se uma analise e algumas pesquisas intendendo encontrar agentes
diferenciadores do Homem e da máquina, tendo sido, entre outros, os
seguintes que o Homem possuí: consciência, pensamento, livre arbítrio,
necessidade de se relacionar com o próximo, empatia, emoção, sonhos,
capacidade de adaptação, calor, batimento cardíaco, sangue, impressão
digital. Após indicação do docente, foi decido explorar um CAPTCHA, que
segundo o google, significa \'\'Completely Automated Public Turing test
to tell Computers and Humans Apart\'\', ou seja uma simulação capaz de
distinguir humanos de computadores. Com base nesta ideia, construímos
uma proposta de trabalho, que assume automaticamente que o seu público
seja um algoritmo gerado a computador, e lhe dá ao próprio público a
oportunidade de o provar errado.

**Tecnologias usadas**

3 computadores, Arduino e componentes físicos (cabos, botões,
resistências, breadboards, leds, fotoresistor), P5.JS, Arduino editor,
Serial protocol, Photoshop, Illustrator, Typora, Microsoft Teams,
Tinkercad, Internet.

**Fotografias/ esquemas explicativos do circuito eletrónico**

Circuito feito no tinkercad que serviu de guia às constantes montagens
do projeto.

![](images/rId24.png)

![](images/rId25.jpg)

**Link do código arduino e P5.js**

<https://editor.p5js.org/CatSousaEsad/sketches/vecqwFEgD> // Versão
Final

<https://editor.p5js.org/CatSousaEsad/sketches/vF3GeHnps> // Versão c/
erros para comparação

<https://create.arduino.cc/editor/catsousaesad/7b013b87-c557-47ef-904b-02f7481ef12a/preview>

**Links de referência**

<https://medium.com/predict/what-makes-humans-different-from-machines-ff791f45187e>

<https://willrobotstakemyjob.com/>

<https://www.bayometric.com/how-accurate-are-todays-fingerprint-scanners/>

<https://www.cooking-hacks.com/ehealth-sensor-shield-biometric-medical-arduino-raspberry-pi.html>

<https://www.electrofun.pt/sensores-arduino/sensor-otico-leitor-impressao-digital-fpm10a-arduino?utm_campaign=efshopping&utm_source=google&utm_medium=cpc&gclid=CjwKCAjw1uiEBhBzEiwAO9B_HSKJcH-TmIYHGdLPO58GdmMj0tq_08eIBy8RUrwcTxEg5v-dnLN6rxoCecUQAvD_BwE>

<https://learn.adafruit.com/pir-passive-infrared-proximity-motion-sensor/using-a-pir-w-arduino>

<https://create.arduino.cc/projecthub/MissionCritical/how-to-set-up-fingerprint-sensor-with-arduino-ebd543>

<http://www.willpenman.com/teaching/robotic-musicians.html>

.

.

.

.

Obrigada.

###### Professor Marco Jorge Heleno

###### 3º Ano Licenciatura Design Multimédia - 2020-21

###### Politécnico de Leiria
