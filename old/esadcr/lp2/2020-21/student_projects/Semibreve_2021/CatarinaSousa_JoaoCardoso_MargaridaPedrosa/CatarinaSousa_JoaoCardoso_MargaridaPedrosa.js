// Unidade Curricular de Laboratório de Projecto II
// Professor Marco Heleno
// Licenciatura de Design Gráfico e Multimédia 2º Semestre / 3º Ano
// Ano Letivo 2020/2021
// Trabalho realizado por:
// Catarina Sousa    - 3170233
// João Cardoso      - 3180756
// Margarida Pedrosa - 3180452  
// SEMIBREVE  

let serial;                             //definição de variáveis
let latestData = "waiting for data"; 
let pergunta = 1;  //todas as perguntas são definidas através desta variável
                                     
let quiz1;         //variáveis ligadas às imagens
let quiz1_errado;
let quiz2;
let quiz2_errado;
let quiz3;
let quiz3_errado;
let quiz4;
let quiz4_errado;
let quiz5;
let quiz5_errado;
let quiz6;
let quiz6_errado;
let final;


//Função que carrega as imagens presentes no sketch files
function preload() {
  quiz1 = loadImage("student_projects/Semibreve_2021/CatarinaSousa_JoaoCardoso_MargaridaPedrosa/images/q1.png");
  quiz1_errado = loadImage("student_projects/Semibreve_2021/CatarinaSousa_JoaoCardoso_MargaridaPedrosa/images/q1_e.png");
  quiz2 = loadImage("student_projects/Semibreve_2021/CatarinaSousa_JoaoCardoso_MargaridaPedrosa/images/q2.png");
  quiz2_errado = loadImage("student_projects/Semibreve_2021/CatarinaSousa_JoaoCardoso_MargaridaPedrosa/images/q2_e.png");
  quiz3 = loadImage("student_projects/Semibreve_2021/CatarinaSousa_JoaoCardoso_MargaridaPedrosa/images/q3.png");
  quiz3_errado = loadImage("student_projects/Semibreve_2021/CatarinaSousa_JoaoCardoso_MargaridaPedrosa/images/q3_e.png");
  quiz4 = loadImage("student_projects/Semibreve_2021/CatarinaSousa_JoaoCardoso_MargaridaPedrosa/images/q4.png");
  quiz4_errado = loadImage("student_projects/Semibreve_2021/CatarinaSousa_JoaoCardoso_MargaridaPedrosa/images/q4_e.png");
  quiz5 = loadImage("student_projects/Semibreve_2021/CatarinaSousa_JoaoCardoso_MargaridaPedrosa/images/q5.png");
  quiz5_errado = loadImage("student_projects/Semibreve_2021/CatarinaSousa_JoaoCardoso_MargaridaPedrosa/images/q5_e.png");
  quiz6 = loadImage("student_projects/Semibreve_2021/CatarinaSousa_JoaoCardoso_MargaridaPedrosa/images/q6.png");
  quiz6_errado = loadImage("student_projects/Semibreve_2021/CatarinaSousa_JoaoCardoso_MargaridaPedrosa/images/q6_e.png");
  final = loadImage("student_projects/Semibreve_2021/CatarinaSousa_JoaoCardoso_MargaridaPedrosa/images/parabens.png");
}



function setup() {
 createCanvas(windowWidth, windowHeight); //O conteúdo adapta-se à largura e altura do ecrã, de forma a ser responsive
  
 

  serial = new p5.SerialPort();  //Protocolo Serial

 serial.list();
 serial.open('COM8');

 serial.on('connected', serverConnected);

 serial.on('list', gotList);

 serial.on('data', gotData);

 serial.on('error', gotError);

 serial.on('open', gotOpen);

 serial.on('close', gotClose); 
  
  background(255);       //fundo branco
}

function serverConnected() {
 print("Connected to Server");
}

function gotList(thelist) {
 print("List of Serial Ports:");

 for (let i = 0; i < thelist.length; i++) {
  print(i + " " + thelist[i]);
 }
}

function gotOpen() {
 print("Serial Port is Open");
}

function gotClose(){
 print("Serial Port is Closed");
 latestData = "Serial Port is Closed";
}

function gotError(theerror) {
 print(theerror);
}

function gotData() {
 let currentString = serial.readLine();
  trim(currentString);
 if (!currentString) return;
 //console.log(currentString);
 
 latestData = currentString;
}

function resetLatestData() {  // Limpar o estado dos botões, organizar o fluxo de informação que o botão transmite
  latestData = "000"          //cada 0 para cada botão
}

function draw() {
 
  
  let btn1 = int (latestData[0]);  //definição da variável do primeiro botão, marcando-o como recebedor de um sinal de número inteiro 
  let btn2 = int (latestData[1]);  //definição da variável do segundo botão, marcando-o como recebedor de um sinal de número inteiro 
  let btn3 = int (latestData[2]);  //definição da variável do terceiro botão, marcando-o como recebedor de um sinal de número inteiro 
  
  
      

 
     //                   P E R G U N T A       1
  
  
  
  if (pergunta == 1) {   //variável que controla as perguntas
    image (quiz1, 0,0, windowWidth, windowHeight); //imagem 1 e a sua localização que se expande de acordo com a largura e altura da janela
    
    //Se carregar no 1ºbotão, a 1ºpergunta marca certo, imprime na consola, soma 1 à variavél pergunta e limpa o sinal dos botões  
    
    if (btn1 === 1 && btn2 === 0 && btn3 === 0) {
        console.log ("pergunta 1" + pergunta);  
        pergunta = pergunta + 1; 
        resetLatestData();
      
      //Se carregar no 2º ou 3º botão, aparece a imagem da pergunta que contém a mensagem de erro
      
    } else if (  
      (btn1 === 0 && btn2 === 1 && btn3 === 0) ||  //2ºbotão
      (btn1 === 0 && btn2 === 0 && btn3 === 1)     //3ºbotão
    ) {      
        image (quiz1_errado, 0,0, windowWidth, windowHeight); //imagem 1 e a sua localização que se expande de acordo com a largura e altura da janela  
    }
      
    //Se a soma da variavél for 2, passa para a pergunta seguinte.
    if (pergunta == 2) {
        image (quiz2, 0,0, windowWidth, windowHeight);  //imagem 2 e a sua localização que se expande de acordo com a largura e altura da janela 
        timeoutPergunta2 = millis(); // Cronometra a partir do momento que o utilizador entra na pergunta 2
    }
   
    
    
     //SÓ se a soma da variavél da pergunta for 2, é que carrega as seguintes condições
    
    
    //                 P E R G U N T A       2
    
    
   } else if (pergunta == 2) {   // se a variável que controla as perguntas for 2 
     
    // se o segundo botão for pressionado, a 2ºpergunta marca certo, imprime na consola, soma 1 à variavél pergunta e limpa o sinal dos botões 
      if (btn1 === 0 && btn2 === 1 && btn3 === 0) 
      {
      console.log ("pergunta 2" + pergunta);
      pergunta = pergunta + 1;
      resetLatestData(); }
     
     
     //Se carregar no 2º ou 3º botão, aparece a imagem da pergunta que contém a mensagem de erro
      else if ( (btn1 === 1 && btn2 === 0 && btn3 === 0) || 
               (btn1 === 0 && btn2 === 0 && btn3 === 1) 
      ) {
        image (quiz2_errado, 0,0, windowWidth, windowHeight); //imagem 2 com erro e a sua localização que se expande de acordo com a largura e altura da janela  
    }
  
    //Se a soma da variavél chegar a 3, passa para a pergunta seguinte
    if (pergunta == 3)
    {
        image (quiz3,0,0, windowWidth, windowHeight); //imagem 3 e a sua localização que se expande de acordo com a largura e altura da janela  
        timeoutPergunta3 = millis(); // Cronometra a partir do momento que o utilizador entra na pergunta 3
    }
   
      // Verificação se o utilizador desistiu do questionário - se passarem 20 segundos desde que entrou na pergunta 3 sem que o utilizador tenha acertado, a variavél volta a 1, o que faz reiniciar o quizz
    if (millis() - timeoutPergunta2 > 20000) {
      pergunta = 1;
    }
    
    
    
     //                 P E R G U N T A       3

   } else if (pergunta == 3) {  // se a variável que controla as perguntas for 3
     
     
   // se o ultimo botão for pressionado, a 3ºpergunta marca certo, imprime na consola, soma 1 à variavél pergunta e limpa o sinal dos botões 
    if (btn1 === 0 && btn2 === 0 && btn3 === 1) {  
    
      console.log ("pergunta 2 " + pergunta);
      pergunta = pergunta + 1;
      resetLatestData();
      
     //Se carregar no 1º ou 2º botão, aparece a imagem da pergunta que contém a mensagem de erro 
    }  else if ( (btn1 === 1 && btn2 === 0 && btn3 === 0) ||
                (btn1 === 0 && btn2 === 1 && btn3 === 0) 
               ){
               image (quiz3_errado, 0,0, windowWidth, windowHeight); //imagem 3 com erro e a sua localização que se expande de acordo com a largura e altura da janela  
    }
    
    if (pergunta == 4)   //Se a soma da variavél chegar a 4  
    {
        image (quiz4, 0,0, windowWidth, windowHeight); //imagem 4 e a sua localização que se expande de acordo com a largura e altura da janela  
       timeoutPergunta4 = millis(); // Cronometra a partir do momento que o utilizador entra na pergunta 4
    }
   
      // Verificação se o utilizador desistiu do questionário - se passarem 20 segundos desde que entrou na pergunta 3 sem que o utilizador tenha acertado, a variavél volta a 1, o que faz reiniciar o quizz
    if (millis() - timeoutPergunta3 > 20000) {
      // 20 segundos
      pergunta = 1;
    }
    
    
    //                 P E R G U N T A       4
    

  } else if (pergunta == 4) { // se a variável que controla as perguntas for 4
   
     // se o segundo botão for pressionado, a 4ºpergunta marca certo, imprime na consola, soma 1 à variavél pergunta e limpa o sinal dos botões 
    
    if (btn1 === 0 && btn2 === 1 && btn3 === 0) 
    {
      console.log ("pergunta 4 " + pergunta);
      pergunta = pergunta + 1;
      resetLatestData();
      
      //Se carregar no 1º ou 3º botão, aparece a imagem da pergunta que contém a mensagem de erro 
      
    }  else if ( (btn1 === 1 && btn2 === 0 && btn3 === 0) ||
                (btn1 === 0 && btn2 === 0 && btn3 === 1) ) 
    {
      
        image (quiz4_errado, 0,0, windowWidth, windowHeight);    //imagem 4 com erro e a sua localização que se expande de acordo com a largura e altura da janela  
    }
    
    if (pergunta == 5) //se a variavel chegar a 5
    {
        image (quiz5, 0,0, windowWidth, windowHeight); //imagem 5 e a sua localização que se expande de acordo com a largura e altura da janela  
      timeoutPergunta5 = millis(); // Cronometra a partir do momento que o utilizador entra na pergunta 5
    }
    

      // Verificação se o utilizador desistiu do questionário - se passarem 20 segundos desde que entrou na pergunta 3 sem que o utilizador tenha acertado, a variavél volta a 1, o que faz reiniciar o quizz
    if (millis() - timeoutPergunta4 > 20000) {
      // 20 segundos
      pergunta = 1;
    }
    
    
    
    //                 P E R G U N T A       5
    

   } else if (pergunta == 5) { // se a variável que controla as perguntas for 5
    
    // se o primeiro botão for pressionado, a 5ºpergunta marca certo, imprime na consola, soma 1 à variavél pergunta e limpa o sinal dos botões 
     
     if (btn1 === 1 && btn2 === 0 && btn3 === 0) 
    {
      console.log ("pergunta 2 " + pergunta);
      pergunta = pergunta + 1;
      resetLatestData();
      
 //Se carregar no 2º ou 3º botão, aparece a imagem da pergunta que contém a mensagem de erro 
      
    }  else if ( 
      (btn1 === 0 && btn2 === 1 && btn3 === 0) ||
      (btn1 === 0 && btn2 === 0 && btn3 === 1) ) 
    {        
        image (quiz5_errado, 0,0, windowWidth, windowHeight);  //imagem 5 com erro e a sua localização que se expande de acordo com a largura e altura da janela  
  
    }   
    if (pergunta == 6) //se a variavél a chegar a 6
    {
        image (quiz6, 0,0, windowWidth, windowHeight); //imagem 6 e a sua localização que se expande de acordo com a largura e altura da janela 
        timeoutPergunta6 = millis(); // Cronometra a partir do momento que o utilizador entra na pergunta 6
    }
   

      // Verificação se o utilizador desistiu do questionário - se passarem 20 segundos desde que entrou na pergunta 3 sem que o utilizador tenha acertado, a variavél volta a 1, o que faz reiniciar o quizz
    if (millis() - timeoutPergunta5 > 20000) {
      // 20 segundos
      pergunta = 1;
    }
     
    
    
      //                 P E R G U N T A       6
    

  } else if (pergunta == 6) {  // se a variável que controla as perguntas for 6
    
   // se o ultimo botão for pressionado, a 6ºpergunta marca certo, imprime na consola, soma 1 à variavél pergunta e limpa o sinal dos botões 
    
    if (btn1 === 0 && btn2 === 0 && btn3 === 1) 
    {
      console.log ("pergunta 2 " + pergunta);
      pergunta = pergunta + 1;
      resetLatestData();
     
      //Se carregar no 1º ou 2º botão, aparece a imagem da pergunta que contém a mensagem de erro      
      
    }  else if ( (btn1 === 1 && btn2 === 0 && btn3 === 0) ||
                (btn1 === 0 && btn2 === 1 && btn3 === 0) )  {           
         image (quiz6_errado, 0,0, windowWidth, windowHeight);  //imagem 6 com erro e a sua localização que se expande de acordo com a largura e altura da janela 
    }
    
    
    if (pergunta == 7) //se a variavél chegar a 7
    {      
      image (final,  0,0, windowWidth, windowHeight);  //aparece a imagem final, que felicita o utilizador por ter completado. Dentro dos parenteses temos a sua localização que se expande de acordo com a largura e altura da janela 
      timeoutFinal = millis(); // Cronometra a partir do momento que o utilizador entra na imagem final - o resultado do quizz
    }
    
    
    
    // reiniciar o quizz, após a sua completação!
    
  } else if (pergunta == 7) { // se a variável que controla as perguntas for 7
    if (millis() - timeoutFinal > 10000)  // e se tiver passado 10 segundos, começados a contar a partir do momento que carrega a imagem final
      pergunta = 1;  //a variavél volta a 1, o que faz reiniciar o quizz
    }
  
    /*
  function windowResized()
{
resizeCanvas (windowWidth, windowHeight) ; //O conteúdo reorganiza-se de acordo com a largura e altura do ecrã, de forma a ser responsive
}*/
  }
  