// NOME DO PROJETO - Unnoticed Algorithm
// Semibreve_2021 - 4º Momento de Avaliação
// ESAD.CR
// Ano letivo de 2020/2021
// Carolina Parente | 3180377
// Inês Piçarra | 3180372
// Laboratório de Projeto II
// 3º ano
// 2º semestre
// Licenciatura em Design Gráfico e Multimédia



const int pushButton1Pin = 4;  // definição da porta 4 do arduino para o botão
int pushButton1State = 0;  // definição de dados inteiros


void setup() 
{
  Serial.begin (9600);  // velocidade da transmissão de dados
  pinMode (pushButton1Pin, INPUT); // definição do pin como INPUT
}


void loop() 
{
  pushButton1State = digitalRead (pushButton1Pin);  // leitura do botão / porta 4 do arduino
  Serial.println   (pushButton1State);  // impressão dos dados recolhidos
}


