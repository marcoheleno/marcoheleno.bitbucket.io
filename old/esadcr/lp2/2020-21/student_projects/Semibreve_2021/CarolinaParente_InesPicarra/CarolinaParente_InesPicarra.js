// NOME DO PROJETO - Unnoticed Algorithm
// Semibreve_2021 - 4º Momento de Avaliação
// ESAD.CR
// Ano letivo de 2020/2021
// Carolina Parente | 3180377
// Inês Piçarra | 3180372
// Laboratório de Projeto II
// 3º ano
// 2º semestre
// Licenciatura em Design Gráfico e Multimédia



let serial;
let latestData = "waiting for data";

let my_video, my_rasterTranscoding, transcoding_points, rgb, brilho; // definição de variáveis

let pg1, pg2; // definição de variável para as texturas


function setup() {

 serial = new p5.SerialPort();

 serial.list();
 serial.open('COM7'); // VER VIDEO

 serial.on('connected', serverConnected);

 serial.on('list', gotList);

 serial.on('data', gotData);

 serial.on('error', gotError);

 serial.on('open', gotOpen);

 serial.on('close', gotClose);


 setAttributes('antialias', true);                   // suavizar
 createCanvas (windowWidth, windowHeight, WEBGL);    // definição do canvas responsivo e do espaço 3D
  
  colorMode(HSB, 255); // modo de cor
  
  //criação da textura '0'
  pg1 = createGraphics (100, 100);
  pg1.textSize(90);                    // definição do tamanho do texto
  pg1.textAlign (CENTER, CENTER);      // alinhar texto
  pg1.background (0, 0);               // definição de fundo
  pg1.fill(255);                       // preenchimento/cor do texto
  pg1.text("0", 50, 54);               // definição de texto a apresentar
  
  //criação da textura '1'
  pg2 = createGraphics (100, 100);
  pg2.textSize(90);                    // definição do tamanho do texto
  pg2.textAlign (CENTER, CENTER);      // alinhar texto
  pg2.background (0, 0);               // definição de fundo
  pg2.fill(255);                       // preenchimento/cor do texto
  pg2.text("1", 50, 54);               // definição de texto a apresentar
  
  my_video = createCapture (VIDEO);    // definição da 'source' da captura
  my_video.hide();                     // instrução que 'esconde' o vídeo 'original'
  my_video.play();                     // instrução que permite o vídeo ser reproduzido
  my_video.volume(0);                  // retira o som do video
}



function serverConnected() {
 print("Connected to Server");
}

function gotList(thelist) {
 print("List of Serial Ports:");

 for (let i = 0; i < thelist.length; i++) {
  print(i + " " + thelist[i]);
 }
}

function gotOpen() {
 print("Serial Port is Open");
}

function gotClose(){
 print("Serial Port is Closed");
 latestData = "Serial Port is Closed";
}

function gotError(theerror) {
 print(theerror);
}

function gotData() {
 let currentString = serial.readLine();
 trim(currentString);
 if (!currentString) return;
 console.log(currentString);
 latestData = currentString;
}


function draw() {
  
  background(0,0,0);  // definição do fundo

// se o botão estiver pressionado...
  if ( int(latestData) === 1) {
   
    scale(-1, 1);   // inverter imagem captada
    
    my_rasterTranscoding = new RasterTranscoding (my_video); //tracking do video
  transcoding_points = my_rasterTranscoding.transcode (0, 0, 10);
  
  for  (let i=0; i<transcoding_points.length; i++) // criação da variável 'i' e definição do seu valor
  {
    rgb = transcoding_points[i].c; 
    brilho = brightness(transcoding_points[i].c);
    
    push();  //salva as definições anteriores
      translate (transcoding_points[i].x, transcoding_points[i].y, map(brilho, 0, 255, 0, 200));
      rotateY(radians(180));   // inverte o plano no eixo Y
    
      let numeroAleatorio = int( random(0, 2) );       // textura aleatória, altera entre '0' e '1'
    
      if (numeroAleatorio === 0) texture(pg1);         // textura que permite ver os '1'
      else texture(pg2);                               // textura que permite ver os '0'
    
      noStroke();     // sem contorno , evita um quadrado á volta dos 'zeros'
    
      if (frameCount > 125)   // contagem até que o projeto ganhe cor
      {
        tint( red(transcoding_points[i].c), green(transcoding_points[i].c), blue(transcoding_points[i].c) );   //cor do projeto
      }
      
      plane(10);           // tamanho dos planos onde aparece o texto
    
    pop();  //restaura as definições anteriores
  }
  
  }
}

/*
function windowResized() {
  resizeCanvas (windowWidth, windowHeight, WEBGL);  // redimensiona o projeto, de acordo com a tela em que o mesmo está a ser visualizado
}
*/

function keyPressed() 
{
  latestData = "1";
}