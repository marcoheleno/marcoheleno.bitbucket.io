// NOME DO PROJETO - Unnoticed Algorithm

// Semibreve\_2021 - 4º Momento de Avaliação

// ESAD.CR

// Ano letivo de 2020/2021

// Carolina Parente \| 3180377

// Inês Piçarra \| 3180372

// Laboratório de Projeto II

// 3º ano

// 2º semestre

// Licenciatura em Design Gráfico e Multimédia

**CONCEITO**

Século XXI, um século onde as inovações tecnológicas têm vindo a marcar
uma forte e aguerrida presença.

Mesmo não sendo percecionado por muitos, não deixa de ser um facto
relevante a permanência dos algoritmos nas sociedades, nas economias e
na geopolítica. Como exemplo do anteriormente referido pode mencionar-se
a intensificação/massificação da utilização das redes sociais.

Plataformas como o Instagram, Behance, Facebook, Pinterest, entre
outras, são utilizadas diariamente por grande parte da população
mundial, contudo, a maioria dos utilizadores das mesmas não se apercebe
de que o conteúdo apresentado tem por base algoritmos. Utilizando um
post no Instagram, como exemplo, ao publicar uma fotografia no feed, a
plataforma recebe dados sobre a mesma, e transforma esses mesmos dados,
em informação (fotografia que os restantes utilizadores vão ver), sem
que seja necessário terem acesso à origem da mesma.

Os resultados apresentados a cada utilizador, as \'publicidades\'
apresentadas na lateral de um website, ou até mesmo as publicações
apresentadas no feed de uma rede social; tudo isto são exemplos do modo
como os algoritmos recolhem dados e \'aprendem\' o que o utilizador
gosta ou pesquisa maioritariamente. Esta \'aprendizagem\' leva a que
sejam apresentados conteúdos relacionados com os gostos de cada
utilizador, podendo ser algo positivo, na medida em que o utilizador
apenas vê o que lhe interessa, ou negativo, pois o utilizador deixa de
receber novos conteúdos e informações, ficando limitado ao que conhece e
gosta.

Assim surge o termo 'Era do Algoritmo'.

**PROJETO**

O presente projeto consiste na interação realizada no arduino através de
um botão iluminado por um LED, cujo resultado é visível na projeção.

Ao entrar no espaço da instalação, a projeção será apenas uma tela
escura, sem conteúdo, simulando a inexistência de projeção. Ao
pressionar o botão, a imagem do utilizador é captada em tempo real
através de uma webcam, e esta captação é \'transformada\' em números.

**CIRCUITO ARDUINO**

![](images/image1.jpg)

**VÍDEO**

**LINK EDITOR P5.JS:**

<https://editor.p5js.org/Grupo_1/sketches/pjFWad0-B>

**LINK VISUALIZADOR P5.JS:**

<https://editor.p5js.org/Grupo_1/present/pjFWad0-B>

**LINK ARDUINO EDITOR:**

<https://create.arduino.cc/editor/2020_2021_grupoi/821dc9d2-3d30-4e47-8fd2-dc01b9b8ae96/preview>

**INSTALAÇÃO**
==============

A instalação do projeto em questão consiste na projeção de elementos
visuais interativos, sobre uma parede preta. Esta projeção é realizada
num ambiente escuro de modo a aumentar o constraste, para que a projeção
possa ser visualizada sem quaisquer problemas.

Neste espaço existe um LED a iluminar o botão, que permite a interação
do utilizador com a instalação em questão. Assim, ao entrar no espaço, o
utilizador é \'levado\' para o botão.

Para abrigar os restantes elmentos necessários ao projeto (computador,
arduino, projetor, entre outros), será utilizado uma estrutura vertical
com 2 prateleiras (uma para suportar o projetor, e outra para suportar o
computador e respetivo arduino. Esta estrutura será aberta apenas na
parte da frente e \'costas\' fechadas, de modo a que o utilizador não
consiga visualizar todos estes elementos.

**SENSORES A UTILIZAR:**

**Digital:**

-   Botão

> ​

**ATUADORES A UTILIZAR:**

-   1 Estrutura vertical (para suportar e \'esconder\' o projetor, o
    computador, e o arduino) (medidas aproximadas: 80cm de largura,
    120cm de altura, 45cm de comprimento)

-   1 Projetor

-   1 Parede Escura (para a projeção)

-   1 LED (para iluminar o botão)

-   2 Resistências 220 Ohms

-   1 Computador

-   1 Arduino

-   1 Webcam (para captar imagem em tempo real)

-   1 candeeiro para ficar pendurado no teto (para ser possivel captar
    imagem com pontos de luz e sombra)

> ​

**MOCKUP DO ESPAÇO E AMBIENTE**

![](images/image2.jpg)

**TECNOLOGIAS UTILIZADAS**

-   Cinema 4D (criar mockup da instalação)

-   Arduino Create (criar e enviar código para o arduino)

-   p5.js Editor (criar conteúdo visual, resultado da interação do
    utilizador)

-   p5 Serial Controler (criar comunicação entre o Arduino Create e o
    p5.js)

-   Typora (criar presente relatório)

> ​

**REQUISITOS TÉCNICOS PARA MONTAGEM DA INSTALAÇÃO**

-   1 Parede escura/preta (no caso de não existir, deve ser colocado uma
    superfície plana de cor preta, sem quaisquer irregularidades, com a
    dimensão da parede onde será realizada a projeção)

-   1 Estrutura vertical

-   1 Webcam ajustável (permitindo regular a inclinação a que capta a
    imagem)

-   1 Cabo 2 metros, entradas USB A - USB B

-   1 Cabo USB - HDMI / VGA (para ligar o computador ao projetor) (caso
    seja necessário, pode ser utilizado um adaptador)

-   1 Ficha tripla (para ligar o computador e o projetor)

-   Botão

**EXEMPLO DE MATERIAL FÍSICO A UTILIZAR**

Tecido a utilizar para projeção, caso não exista parede preta:

![](images/image3.jpg)

Candeeiro de mesa, que deverá ser \'colado\' ao teto. O cabo que fornece
energia a este candeeiro, deve descer por uma das paredes laterais,
sempre junto/colado à parede:

![](images/image4.jpg)

Estrutura vertical, para suportar e \'esconder\' os elementos
necessários:

![](images/image5.jpg)

**MOODBOARD**

![](images/image6.png)

![](images/image7.jpg)

![](images/image8.jpg)
