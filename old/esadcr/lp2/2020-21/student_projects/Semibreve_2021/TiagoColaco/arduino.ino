const int photoresistorPin = A0;
//entrada do elemento fotoresistor ligado á porta analógica 0 (A0)

int photoresistorValue = 0;
//entrada do valor de fotoresistor

int photoresistorMax = 0;
//entrada do valor de leitura do ambiente do fotoresistor

boolean photoresistorState = false;
// estabelece o padrão de lógica boleana do estado do fotoresistor como falso


void setup() 
{
  Serial.begin (9600);
  // estabelece a frequência a 9600 bits por segundo
  delay(1000);
  // leitura ao fim de ciclos de 1 segundo
  photoresistorMax =analogRead(photoresistorPin);
  //leitura do valor de Luz do ambiente
  Serial.println (photoresistorMax);
  //apresentação do valor máximo de luz ambiente
  delay(1000);
  // leitura ao fim de ciclos de 1 segundo
}


void loop()
{
  photoresistorValue = analogRead (photoresistorPin);
  //o valor do fotoresistor é igual ao valor da leitura analógica do fotoresistor

  if (photoresistorValue <= photoresistorMax/9){
      photoresistorState = true;
      //se o valor fotoresistor for menor ou igual a uma nona parte do valor máximo do fotoresistor
      //então o estado do fotoresistor é de valor VERDADEIRO
  }
  
  if(photoresistorState == true && photoresistorValue > photoresistorMax/9){
      Serial.println (1);
      photoresistorState = false;
      //se o valor do estado do fotoresistor for VERDADEIRO e o valor do fotoresistor for maior
      //que uma nona parte da leitura máxima do fotoresistor, o Serial introduzirá no monitor o valor 1, em vez do padrão 0,
      //o estado do fotoresistor é de valor FALSO
      }
  
  delay (500);
  //leitura ao fim de ciclos de meio segundo
  Serial.println (0);
  //leitura máxima do fotoresistor no Serial será introduzida no monitor como padrão o valor 0.
}