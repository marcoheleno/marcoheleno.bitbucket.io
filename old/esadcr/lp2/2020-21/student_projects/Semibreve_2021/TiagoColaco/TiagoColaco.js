// Unidade Curricular de Laboratório de Projecto II
// Professor Marco Heleno
// Licenciatura de Design Gráfico e Multimédia - 6º Semestre / 3º Ano
// Ano Lectivo 2020-2021
// Trabalho de: 
// Tiago Colaço - 3190743
// PULSAR


let serial;
let latestData = "waiting for data";
let num_boxs, x_box, y_box, z_box, largura_box, altura_box, profundidade_box;
let espaco_maximo_composicao, espacamento_entre_boxs, ajuste, t;
let frameRateContador, r,g,b,a;
//definição de variáveis

function setup() {
 createCanvas(windowWidth, windowHeight);
  
 setAttributes('antialas', true); //melhora a qualidade final do render
 createCanvas (windowWidth, windowHeight, WEBGL); 
  
 t = 10; // tamanho
 num_boxs = t; // numero de caixas
 frameRateContador = 1;
 frameRate (frameRateContador); // velocidade dos frames
  r = 255;
  g = 255;
  b = 255;
  a = 0;

 serial = new p5.SerialPort();

 serial.list();
 serial.open('COM3');

 serial.on('connected', serverConnected);

 serial.on('list', gotList);

 serial.on('data', gotData);

 serial.on('error', gotError);

 serial.on('open', gotOpen);

 serial.on('close', gotClose);
}

function serverConnected() {
 print("Connected to Server");
}

function gotList(thelist) {
 print("List of Serial Ports:");

 for (let i = 0; i < thelist.length; i++) {
  print(i + " " + thelist[i]);
 }
}

function gotOpen() {
 print("Serial Port is Open");
}

function gotClose(){
 print("Serial Port is Closed");
 latestData = "Serial Port is Closed";
}

function gotError(theerror) {
 print(theerror);
}

function gotData() {
 let currentString = serial.readLine();
  trim(currentString);
 if (!currentString) return;
 console.log(currentString);
 latestData = currentString;
}

function draw() 
{
  //console.log (frameRate());
  // consola lê a frequência de frames
  
  
  //orbitControl(); 
  // rotação da visualização
  background (0);
  // cor do fundo em preto
  shininess(20); // brilho do objeto
  ambientLight(r,g,b); // luz ambiente no objeto
  ambientMaterial(r,g,b); // cor do material
  
  // criação da grelha de cubos, lendo a altura e largura da tela preview
  if (width > height) 
  {
    ajuste = height/2.5;
    espaco_maximo_composicao = height - ajuste;
  }
  else 
  {
    ajuste = width/2.5;
    espaco_maximo_composicao = width - ajuste;
  }
  espacamento_entre_boxs = espaco_maximo_composicao/t;
  //definição do espaçamento entre cubos
  
  push();
    translate(0, 0, 0);
    // posição da composição
  
    rotateX (radians(0));
    rotateY (radians(0));
    rotateZ (radians(45)); 
    //rotação da composição nos eixos X,Y,Z
  
    for (let h=0; h<t; h++)
    // horizontal
    {
    for (let v=0; v<t; v++)
    //vertical
    {
    push();
      x_box = h * espacamento_entre_boxs;
      y_box = v * espacamento_entre_boxs; 
      z_box = 0;
      // posicao de cada box
    translate (
      -espaco_maximo_composicao/2 + espacamento_entre_boxs/2 + x_box, 
      -espaco_maximo_composicao/2 + espacamento_entre_boxs/2 + y_box, z_box);
          
          noStroke();
          //sem linha de contorno
          a = 35;
          specularMaterial (r,g,b,a); // cor especular(relativo à refração da luz) do objeto
          largura_box = t*2;
          //largura, tamanho vezes 2
          altura_box  = t*2;
          //altura, tamanho vezes 2
          profundidade_box = random(0,espaco_maximo_composicao);
          //randomização do tamanho em altura dos cubos
          box (largura_box, altura_box, profundidade_box);
          //primitive shape box
        pop();
      }
    }
  pop();
    
    if (int(latestData) === 1){
    frameRateContador+=1; 
    // incrementa 2 ao Frame Rate
    frameRate (frameRateContador); 
    r = random(255);
    g = random(255);
    b = random(255);
    }

}

/*function () //comando que interage com o público, quanto mais o user clica com o rato, maior será a velocidade da animação
{
  if (int(latestData) === 1)
  //if (frameRate() <= 30)
  // se a Frame Rate for inferior a 30 frames por segundo
  {
    frameRateContador+=2;
    // incrementa 2 ao Frame Rate
    frameRate (frameRateContador);
    // velocidade dos Frames
    // random no valor
    r = random(255);
    g = random(255);
    b = random(255);
  }
}*/

function keyPressed(){ 
// se a tecla Enter for pressionada, limpa e recomeça o código

  if (keyCode === ENTER){
  //tecla enter
    clear();
    //limpa
    setup();
    //recomeça código
    prepare_p5_student_project();
  }

  else latestData = "1";
}
/*
function windowResized() {
  // redimensiona o ecrã em largura e altura
  resizeCanvas(windowWidth, windowHeight);
}
*/