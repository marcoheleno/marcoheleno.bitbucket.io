Projeto "Pulsar" de Laboratório de Projeto II
===============================================

### por Tiago Colaço 3190743

Leitor de Intenção - PULSAR - Semibreve\_2021
---------------------------------------------

Foi nos desafiado, enquanto estudantes da Esad.cr no curso de Design
Multimédia, a criar uma proposta para o evento do Semibreve\_2021, o
desenvolvimento da instalação com arduíno.

Esta é a nossa proposta.

**Conceito:**

Com base no trabalho desenvolvido anteriormente "Pulsar" para o evento
PCD\@Lisboa\_2021, juntamente com a aluna Margarida Pedrosa, apresento o
conceito para este evento "Semibreve" existindo o desafio da criação
de uma interação eletrónica por intermédio do Arduino.

O leitor de Intenção - Pulsar, informa o investigador criminal de qual a
intenção do suspeito relativamente a determinada questão, construindo um
perfil no sistema Pulsar que se vai tornando mais intenso com o acumular
da informação conseguida ao longo do questionário feito pelo
Investigador.

O utilizador/visitante da instalação colocará o o seu polegar sobre um
leitor (sensor de Luz) que irá ler a sua impressão digital, ritmo
cardiaco e outros sinais vitais, alimentando o "castelo de informação"
do sistema "Pulsar", aumentando o seu banco de informação e dados
recolhidos da utilização do visitante.

Na prática, o Arduíno irá ler através de um sensor de luz a cada
utilização a opacidade de luz e criando uma randomização da visualização
do código em P5, presente no LCD, dando a ideia de que existe uma
alteração vital, entre as várias questões, no espaço sobre a mesa
existirá uma lista de perguntas que o utilizador deve ler e intercalar
com o colocar o dedo sobre o sensor, disposto na parede junto ao LCD,
imagens de mugshots, impressoes digitais, perfis sociais, perfis
psicológicos, etc.

Todo este conceito se baseia no desenvolvimento da interação
homem/máquina que leva a automação de um sistema informático passível de
desenvolvimento visual que chamará à atenção do utilizador sobre o
impacto do aumento de pegada informática que deixamos disponível no
sistema.

#### Moodboard:

<https://www.pinterest.pt/tiagocolacoart/processing/>

**Código em Arduino:**

`const int photoresistorPin = A0;`\
`//entrada do elemento fotoresistor ligado á porta analógica 0 (A0)`

`int photoresistorValue = 0;`\
`//entrada do valor de fotoresistor`

`int photoresistorMax = 0;`\
`//entrada do valor de leitura do ambiente do fotoresistor`

`boolean photoresistorState = false;`\
`// es``tabelece o padrão de lógica boleana do estado do fotoresistor como falso`

`void setup()`\
`{`\
`Serial.begin (9600);`\
`// estabelece a frequência a 9600 bits por segundo`\
`delay(1000);`\
`// leitura ao fim de ciclos de 1 segundo`\
`photoresistorMax =analogRead(photore``sistorPin);`\
`//leitura do valor de Luz do ambiente`\
`Serial.println (photoresistorMax);`\
`//apresentação do valor máximo de luz ambiente`\
`delay(1000);`\
`// leitura ao fim de ciclos de 1 segundo`\
`}`

`void loop()`\
`{`\
`photoresistorValue = analogRead (photoresistorPi``n);`\
`//o valor do fotoresistor é igual ao valor da leitura analógica do fotoresistor`

`if (photoresistorValue <= photoresistorMax/9){`\
`photoresistorState = true;`\
`//se o valor fotoresistor for menor ou igual a uma nona parte do valor máximo do fotoresistor`\
`//então o estado do fotoresistor é de valor VERDADEIRO`\
`}`

`if(photoresistorState == true && photoresistorValue > photoresistorMax/9){`\
`Serial.println (1);`\
`photoresistorState = false;`\
`//se o valor do estado do fotoresistor for VERDADEIRO e o valor do fotoresistor for maior`\
`//que uma nona parte da leitura máxima do fotoresistor, o Serial introduz no monitor o valor 1, em vez do padrão 0,`\
`//o estado do fotoresistor é de valor FALSO`\
`}`

`delay (500);`\
`//leitura ao fim de ciclos de meio segundo`\
`Serial.println (0);`\
`//leitura máxima do fotoresistor no Serial será introduzida no monitor como padrão o valor 0.`\
`}`

**Código em P5.js:**

`// Unidade Curricular de Laboratório de Projecto II`\
`// Professor`` Marco Heleno`\
`// Licenciatura de Design Gráfico e Multimédia - 6º Semestre / 3º Ano`\
`// Ano Lectivo 2020-2021`\
`// Trabalho de:`\
`// Tiago Colaço - 3190743`\
`// PULSAR`

`let serial;`\
`let latestData = "waiting for data";`\
`let num_boxs, x_box, y_box, z_box, largura_bo``x, altura_box, profundidade_box;`\
`let espaco_maximo_composicao, espacamento_entre_boxs, ajuste, t;`\
`let frameRateContador, r,g,b,a;`\
`//definição de variáveis`

`function setup() {`\
`createCanvas(windowWidth, windowHeight);`

`setAttributes('antialas', true); //melhora a qualidade final do render`\
`createCanvas (windowWidth, windowHeight, WEBGL);`

`t = 10; // tamanho`\
`num_boxs = t; // numero de caixas`\
`frameRateContador = 1;`\
`frameRate (frameRateContador); // velocidade dos frame``s`\
`r = 255;`\
`g = 255;`\
`b = 255;`\
`a = 0;`

`serial = new p5.SerialPort();`

`serial.list();`\
`serial.open('COM3');`

`serial.on('connected', serverConnected);`

`serial.on('list', gotList);`

`serial.on('data', gotData);`

`serial.on('error', gotError);`

`serial.on('open', gotOpen);`

`serial.on('close', gotClose);`\
`}`

`function serverConnected() {`\
`print("Connected to Server");`\
`}`

`function gotList(thelist) {`\
`print("List of Serial Ports:");`

`for (let i = 0; i < thelist.length; i++) {`\
`print(i + " " + thelist[i])``;`\
`}`\
`}`

`function gotOpen() {`\
`print("Serial Port is Open");`\
`}`

`function gotClose(){`\
`print("Serial Port is Closed");`\
`latestData = "Serial Port is Closed";`\
`}`

`function gotError(theerror) {`\
`print(theerror);`\
`}`

`function gotData() {`\
`let currentString = serial.r``eadLine();`\
`trim(currentString);`\
`if (!currentString) return;`\
`console.log(currentString);`\
`latestData = currentString;`\
`}`

`function draw()`\
`{`\
`console.log (frameRate());`\
`// consola lê a frequência de frames`

`//orbitControl();`\
`// rotação da visualização`\
`background (0);`\
`// cor do fundo em preto`\
`shininess(20); // brilho do objeto`\
`ambientLight(r,g,b); // luz ambiente no objeto`\
`ambientMaterial(r,g,b); // cor do material`

`// criação da grelha de cubos, lendo a altura e largura da tela preview`\
`if (width > ``height)`\
`{`\
`ajuste = height/2.5;`\
`espaco_maximo_composicao = height - ajuste;`\
`}`\
`else`\
`{`\
`ajuste = width/2.5;`\
`espaco_maximo_composicao = width - ajuste;`\
`}`\
`espacamento_entre_boxs = espaco_maximo_composicao/t;`\
`//definição do espaçamento entre cubos`

`push();`\
`translate(0, 0, 0);`\
`// posição da composição`

    rotateX (radians(0));
    rotateY (radians(0));
    rotateZ (radians(45)); 
    //rotação da composição nos eixos X,Y,Z
      
    for (let h=0; h<t; h++)
    // horizontal
    {
    for (let v=0; v<t; v++)
    //vertical
    {
    push();
      x_box = h * espacamento_entre_boxs;
      y_box = v * espacamento_entre_boxs; 
      z_box = 0;
      // posicao de cada box
    translate (
      -espaco_maximo_composicao/2 + espacamento_entre_boxs/2 + x_box, 
      -espaco_maximo_composicao/2 + espacamento_entre_boxs/2 + y_box, z_box);
          
          noStroke();
          //sem linha de contorno
          a = 35;
          specularMaterial (r,g,b,a); // cor especular(relativo à refração da luz) do objeto
          largura_box = t*2;
          //largura, tamanho vezes 2
          altura_box  = t*2;
          //altura, tamanho vezes 2
          profundidade_box = random(0,espaco_maximo_composicao);
          //randomização do tamanho em altura dos cubos
          box (largura_box, altura_box, profundidade_box);
          //primitive shape box
        pop();
      }
    }

`pop();`\
\`\`\
if (int(latestData) === 1){\
frameRateContador+=1;\
// incrementa 2 ao Frame Rate\
frameRate (frameRateContador);\
r = random(255);\
g = random(255);\
b = random(255);\
}

`}`

`/*function () //comando que interage com o público, quanto mais o user clica com o rato, maior será a velocidade da animação`\
`{`\
`  if (int(latestData) === 1)`\
`  //if (frameRate() <= 30)`\
`  // se a Frame Rate for inferior a 30 frames por segundo`\
`  {`\
`    frameRateContador+=2;`\
`    // incrementa 2 ao Frame Rate`\
`    frameRate (frameRateContador);`\
`    // velocidade dos Frames`\
`    // random no valor`\
`    r = random(255);`\
`    g = random(255);`\
`    b`` = random(255);`\
`  }`\
`}*/`

`function keyPressed(){`\
`// se a tecla Enter for pressionada, limpa e recomeça o código`

`if (keyCode === ENTER){`\
`//tecla enter`\
`clear();`\
`//limpa`\
`setup();`\
`//recomeça código`\
`}`\
`}`

`function windowResized() {`\
`// redimensiona o ecrã e``m largura e altura`\
`resizeCanvas(windowWidth, windowHeight);`\
`}`

**Elementos:**

-   LCD 32"

-   Computador com Ligação à Internet

-   Cabo USB

-   Cabo HDMI

-   Arduíno

-   1 Breadboard

-   5 fios conectores

-   2 resistências

-   1LedPin

-   1 Sensor de Luz

-   Mesa castanha escura +- 165x70cm

-   Cartazes Perfis Psicológicos (decoração do espaço)

-   Código de cores do Teste Pulsar

-   Luz Ambiente amarelada, meia-luz

```{=html}
<!-- -->
```
-   

**Comportamento Visual:**

###### https://editor.p5js.org/TiagoColaco/sketches/F2Fe\_xibI

**Exemplo:**

![](images/image1.jpg)

**Circuito Breadboard:**

![](images/image2.png)

**Vídeo Exemplo:**

<https://drive.google.com/file/d/16U75PQ9XNlbRCoZUf1Hub8Nws0HnzJWy/view?usp=sharing>

**Desenhos Técnicos:**

![](images/image3.jpg)

![](images/image4.jpg)

**Elementos Gráficos:**

![](images/image5.jpg)

![](images/image6.jpg)

Obrigado

![](images/image7.jpg)

###### Professor Marco Jorge Heleno

###### 3º Ano Licenciatura Design Multimédia - 2020-21

###### Politécnico de Leiria
