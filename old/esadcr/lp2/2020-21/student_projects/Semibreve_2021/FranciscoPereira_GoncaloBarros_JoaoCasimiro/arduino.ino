// Francisco Pereira, aluno nº 3180576
// João Casimiro, aluno nº 3180477
// Gonçalo Barros,  aluno nº 3180743
//
// Exercício de Avaliação: Festival Semibreve
//
// Ano Letivo: 3º Ano, 2020-2021
// 2º Semestre
// Unidade Curricular: Laboratório de Projeto I
// Licenciatura em Design Gráfico e Multimédia
// Escola Superior de Artes e Design - ESAD.CR
// Docente: Marco Heleno
// Data de avaliação: 16 de Junho
//
// ------------------------------------------------

// Ligação dos potenciometros ao arduino
const int potentiometerPin1 = A0;
const int potentiometerPin2 = A1;
const int potentiometerPin3 = A2;
const int potentiometerPin4 = A3;

//Ligação do Botão ao arduino
const int pushButton1Pin = 7; 
// Estado do botão, neste caso 0=desigado/sem ação
int pushButton1State = 0;


//Valores dos vários potenciometros
int potentiometerValue1 = 0;
int potentiometer2FadeValue1 = 0;

int potentiometerValue2 = 0;
int potentiometer2FadeValue2 = 0;

int potentiometerValue3 = 0;
int potentiometer2FadeValue3 = 0;

int potentiometerValue4 = 0;
int potentiometer2FadeValue4 = 0;

void setup()
{
  Serial.begin (9600);
  
  // definição do button como INPUT
  pinMode(pushButton1Pin, INPUT); 

}

void loop()
{
  //mapeamento de valores dos potenciometros, neste caso min valor= 0 max valor= 360
  //potenciometro Eixo do X
  potentiometerValue1 = analogRead (potentiometerPin1);
  potentiometer2FadeValue1 = constrain( map(potentiometerValue1, 0, 1023, 0, 360), 0, 360 );
  
  //potenciometro Eixo do Y
    potentiometerValue2 = analogRead (potentiometerPin2);
  potentiometer2FadeValue2 = constrain( map(potentiometerValue2, 0, 1023, 0, 360), 0, 360 );
 
  //potenciometro Eixo do Z
    potentiometerValue3 = analogRead (potentiometerPin3);
  potentiometer2FadeValue3 = constrain( map(potentiometerValue3, 0, 1023, 0, 360), 0, 360 );
  
  //potenciometro do "zoom"
  potentiometerValue4 = analogRead (potentiometerPin4);
  potentiometer2FadeValue4 = constrain( map(potentiometerValue4, 0, 1023, 0, 300), 0, 300 );
  
  // leitura do botão
  pushButton1State = digitalRead (pushButton1Pin);  
  // impressão dos dados recolhidos
  Serial.println   (pushButton1State);  
 
 //leitura dos potenciometros na consola
 Serial.print (potentiometer2FadeValue1);
  Serial.print ("/");
  Serial.print (potentiometer2FadeValue2);
  
  Serial.print ("/");
  Serial.print (potentiometer2FadeValue3);
  
  Serial.print ("/");
  Serial.println (potentiometer2FadeValue4);
  
  delay (500);
}