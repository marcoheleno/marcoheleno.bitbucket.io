// Francisco Pereira, aluno nº 3180576
// João Casimiro, aluno nº 3180477
// Gonçalo Barros,  aluno nº 3180743
//
// Exercício de Avaliação: Festival Semibreve
//
// Ano Letivo: 3º Ano, 2020-2021
// 2º Semestre
// Unidade Curricular: Laboratório de Projeto I
// Licenciatura em Design Gráfico e Multimédia
// Escola Superior de Artes e Design - ESAD.CR
// Docente: Marco Heleno
// Data de avaliação: 16 de Junho
//
// ------------------------------------------------


let obj3D=[], a, limiteActualizacao;
let pot1, pot2, pot3, pot4;
let but;
let terminado;
let serial;
let latestData;
let obj3D_index;
let novo_obj = 0;


function preload()
{
  // esta função faz o load dos vários objetos 3D

  obj3D[0] = loadModel ("student_projects/Semibreve_2021/FranciscoPereira_GoncaloBarros_JoaoCasimiro/falo.obj", true); 
  obj3D[1] = loadModel ("student_projects/Semibreve_2021/FranciscoPereira_GoncaloBarros_JoaoCasimiro/couve.obj", true); 
  obj3D[2] = loadModel ("student_projects/Semibreve_2021/FranciscoPereira_GoncaloBarros_JoaoCasimiro/sapo.obj", true); 
  obj3D[3] = loadModel ("student_projects/Semibreve_2021/FranciscoPereira_GoncaloBarros_JoaoCasimiro/andorinha.obj", true);
}


function setup()
{
  createCanvas (windowWidth, windowHeight, WEBGL);
  
  serial = new p5.SerialPort();
 serial.list();
 serial.open('COM6');

 serial.on('connected', serverConnected);

 serial.on('list', gotList);

 serial.on('data', gotData);

 serial.on('error', gotError);

 serial.on('open', gotOpen);

 serial.on('close', gotClose);
 
  
  //Um intervalo de valores entre 300 e um numero random de 500 a 1500de maneira aleatoria faz parar a atualização do objeto 3D
  limiteActualizacao = random (300, random (500, 1500));
  
  //angulo inicial da rotação do objeto 3D
  a=0;
  
  //os potenciometros e o botao começam com valores = 0
  pot1 = 0;
  pot2 = 0;
  pot3 = 0;
  pot4 = 0;
  but = 0;
  novo_obj = 0;
  latestData = "0/0/0/0/0";
  terminado = false;
  // escolhe um dos objetos 3d aleatoriamente entre 0 a 3 (sendo 0=o primeiro objeto 3d, neste caso o falo e 3=o ultimo objeto, a andorinha)
  obj3D_index = int(random(0, obj3D.length));
  
  
}
  function serverConnected() {
 print("Connected to Server");
}

function gotList(thelist) {
 print("List of Serial Ports:");

 for (let i = 0; i < thelist.length; i++) {
  print(i + " " + thelist[i]);
 }
}

function gotOpen() {
 print("Serial Port is Open");
}

function gotClose(){
 print("Serial Port is Closed");
 //latestData = "Serial Port is Closed";
}

function gotError(theerror) {
 print(theerror);
}

function gotData() {
 let currentString = serial.readLine();
  trim(currentString);
 if (!currentString) return;
 console.log(currentString);
 latestData = currentString;
}


function draw()
{
  //console.log(latestData);
  let myStrArr = splitTokens (latestData, '/');
  //faz a ligação entre os dados dos potenciometros do arduino e o p5js
  pot1 = int (myStrArr[0]);
  pot2 = int (myStrArr[1]);
  pot3 = int (myStrArr[2]);
  pot4 = int (myStrArr[3]);
  but = int (myStrArr[4]);
    
  //condição que ao clicar no botão este dá reset à animação
   if (but === 1) 
   { 
     //torna a "linha 144" sempre verdadeira
     novo_obj = frameCount;
     //escolhe aleatoriamente um novo objeto
     obj3D_index = int(random(0, obj3D.length));
     latestData = "0/0/0/0/0";
     a=0;
     terminado = false;
     limiteActualizacao = random (300, random (500, 1500));
     //console.log("botao");
     //console.log(obj3D_index);
    }
  
  // 1º define a velocidade de actualização da malha
  // 2º define o intervalo de tempo que actualiza malha
  if ( (frameCount-novo_obj)%2==0 && (frameCount-novo_obj)<= 500 )
  {
    //console.log("renderizacao");
    for (let v=0; v<obj3D[obj3D_index].vertices.length; v++) 
    {
      //faz distorcer/mover os vertices do objecto 3D nos eixos X,Y e Z
      obj3D[obj3D_index].vertices[v].y += random (-1,1);
      obj3D[obj3D_index].vertices[v].x += random (-1,1);
      obj3D[obj3D_index].vertices[v].z += random (-1,1);
    }

    // "Hack" que descobrimos, ao fazer chamar o CreateCanvas ele faz atualizar o preview frame a frame
    createCanvas (windowWidth, windowHeight, WEBGL);
    prepare_p5_student_project();
    
    
    if (frameCount-novo_obj === 500) terminado = true;
   
  background (0);
    
  // o push() guarda as definições e personalização do objeto 3D enquanto o pop recupera essas definições.
  push();
    
  //podemos alterar a visão do objeto nos eixos X,Y,Z e fazer zoom in com os potenciometros
    rotateX (radians(pot1));
    rotateY (radians(pot2));
    rotateZ (radians(pot3));
    translate (0, 0, pot4);
    scale (1.5);
  
  //faz a rotação no eixo do Y
    rotateY (radians(a));
  //enquanto o objeto se deforma o stroke aparece a vermelho, depois de deformado este passa a branco   
    if (terminado == true) stroke (255,175);
    else stroke (255,0,0);
     
    noFill();
    model (obj3D[obj3D_index]);
  
  //Condição que dá inicio à rotação do objeto 3D
  if ( terminado == true) a =0.3;
  else a += 2.5;
  
  pop();
   
  }
}
function keyPressed() 
{
  latestData = "0/0/0/0/1";
}
/*
function windowResized()
{
  resizeCanvas (windowWidth, windowHeight);
}
*/