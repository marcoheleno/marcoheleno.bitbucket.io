Culture Chaos

Francisco Pereira, aluno nº 3180576

João Casimiro, aluno nº 3180477

Gonçalo Barros, aluno nº 3180743

Conceito

Cultura pode se definir em características e conhecimentos de um
particular grupo de pessoas, que compreende entre linguagem, arte,
religião, culinária, hábitos sociais, música, entre outras. É a
totalidade dos costumes, tradições, crenças, padrões morais e
manifestações artísticas, o que a distingue de uma sociedade.

A partir do século 21 a tecnologia integra uma peça fundamental do nosso
dia a dia, além de usarmos tecnologia nos computadores, telemóveis
também está nos ajudou a contribuir para o nosso bem estar a nível de
saúde a partir de vacinas e medicamentos. Por isso, a tecnologia faz
parte da sociedade e esta ajuda a modificar aspectos essenciais da
cultura, incluindo a arte, culinária, religião e até comportamentos
sociais.

A tecnologia, e por consequência dela os algoritmos, modificam a cultura
e evidenciam as diferenças entre elas, permitindo assim a junção de
várias culturas. Através dos computadores e da internet, é possível
aprender culturas dos mais variados países sem sair do conforto das
nossas casas. Possibilitando assim a qualquer pessoa, qual seja o seu
estatuto social a oportunidade de ganhar novos conhecimentos sobre
outras culturas.

Contudo, será que conseguimos afirmar que o uso desta tecnologia para a
aprendizagem de novas noções de cultura é benéfico para a mesma?
Enquanto que por razões acima referidas podemos afirmar que sim, há no
entanto, outras questões que surgem. Com esta interligação de culturas
será que alguns aspetos das mesmas não se vão perder durante o processo
de partilha e aprendizagem?

Com o cruzamento de vários costumes, vai se perder a origem dos mesmos?
Se isto ocorrer durante longos anos, irá se criar uma cultura universal
ou teremos culturas totalmente distintas delas próprias e das de hoje em
dia?

Com a nossa instalação pretendemos abordar este tema, atacando estas
questões com a distorção de vários elementos culturais da zona das
Caldas da Rainha, nomeadamente cerâmica e faianças instauradas na
cultura da cidade já há bastante tempo. Este projeto cria um contraste
com uma particularidade cultural da zona e a visão futurista que os
algoritmos proporcionam. Criando assim uma distorção não destrutiva, mas
sim, um conjunto de objetos que passam a fazer parte de uma família
digital. A criação dos mesmos relaciona duas épocas temporais e
consequentemente a junção de culturas. Podemos dizer que é uma adaptação
de um produção manual com a linha tecnológica dos algoritmos.

Todavia, isto é apenas uma representação da criação de um possível
algoritmo, não é o algoritmo que desenha o objeto mas sim é ele que faz
variar a sua forma segundo diversas regras implementadas por nós.

Funcionamento

Para enriquecer a estética do projeto decidimos criar uma interação
entre o utilizador e a instalação. A partir daqui pensamos em criar um
ambiente escuro, visto que o "background" é de cor preta para dar mais
imersão e destaque à projeção que apresenta duas cores: vermelha e
branco.

Enquanto a projeção apresenta a cor vermelha, o utilizador já interdita
de interagir com o painel de controlo. Painel de controle que é
constituído por 4 potenciômetros, um botão e um led. Os potenciómetros
têm a função de controlar os eixos X, Y, Z e a ampliação visual ao
objeto (zoom). Como a animação na instalação tem um fim, o botão tem a
função de recomeçar a projeção, limpando a distorção do objeto. O led
apenas serve para sinalizar o painel de controle.

A instalação será visualizada através de um projetor para atingir uma
maior escala, este estará situado junto ao painel de controlo. Na
projeção iremos ver a deformação do objeto em questão. Este
acontecimento ocorre durante um tempo limitado aleatório representado
pela cor vermelha, onde o utilizador não consegue interagir. A
deformação do objeto após estar concluída apresenta uma cor branca,
assim sendo o utilizador poderá interagir com o painel de controlo como
acima referido.

Sensores e Atuadores Analogicos e Digitais

Para esta instalação são necessários estes atuadores:

\- Energia elétrica

\- Extensão elétrica

\- Computador, com acesso à web

\- Carregador de computador

\- Projetor

\- Led

Para esta instalação são necessários estes sensores:

\- Arduino

\- Breadboard

\- 4 potenciometros

\- 1 botão

\- 2 resistências

\- 15 cabos

Ambiente

![](images/image3.png)

![](images/image9.png)

![](images/image5.png)

Tecnologias usadas

\- Cinema 4d (modelação das peças)

\- P5js ( formas e comportamentos visuais das peças)

\- Arduino ( interação do utilizador com a instalação)

Circuito Eletrónico

![](images/image7.jpg)

![](images/image8.jpg)

Links do Códigos

P5js:

[[https://editor.p5js.org/miguelbarros999/sketches/bvOYO4FiE]{.underline}](https://editor.p5js.org/miguelbarros999/sketches/bvOYO4FiE)

Arduino:

https://create.arduino.cc/editor/senhorcodigo/889a5330-4d53-4831-87fe-f547974d829a/preview

Reiterações dos Códigos

1º Reiteração com logo do Semibreve:

[[https://editor.p5js.org/miguelbarros999/sketches/RMZFMonuM]{.underline}](https://editor.p5js.org/miguelbarros999/sketches/RMZFMonuM)

2º Reiteração com a planta da Esad

[[https://editor.p5js.org/miguelbarros999/sketches/9oZ-SPhBO]{.underline}](https://editor.p5js.org/miguelbarros999/sketches/9oZ-SPhBO)

MoodBoard

![](images/image2.png)

![](images/image1.png)

![](images/image4.png)
