// Anabela Vicente, aluna nº 3180183
// Daniela Martins, aluna nº 3180758
// Mariana Soares,  aluna nº 3180483
//
// Exercício de Avaliação: PCD@Lisboa_2021
//
// Ano Letivo: 3º Ano, 2020-2021
// 2º Semestre
// Unidade Curricular: Laboratório de Projeto II
// Licenciatura em Design Gráfico e Multimédia
// Escola Superior de Artes e Design - ESAD.CR
// Docente: Marco Heleno
// Data de avaliação: 21 de Abril 2021

//-----------------------------------------------------------------------------------

// Instruções da interação com a câmera e o espaço

// Interação com a câmera através do rato do computador
  // botão do lado esquerdo permite explorar a 360º os objetos
  // botão do lado direito permite fazer zoom in e zoom out
  // botão scroll permite fazer zoom in e zoom out e mover no espaço à volta
  // clicar 2 vezes no botão do lado esquerdo para voltar ao início
  
// Interação com a câmera através dos dedos em dispositivos touch
  // 1 dedo pressionado e em movimento permite explorar a 360º os objetos
  // 2 dedos a afastarem-se e a juntarem-se permite fazer zoom in e zoom out
  // 2 dedos juntos a moverem-se no ecrã permite mover no espaço à volta
  // clicar 1 vez com um dedo para voltar ao início

//-----------------------------------------------------------------------------------

// Objeto central (busto)
function preload() {
  obj3D = loadModel("student_projects/PCDLisboa_2021/AnabelaVicente_DanielaMartins_MarianaSoares/head.obj", true); // Carregar o objeto
}

// Definição do material
var matWhite = {
  diff: [0.6, 0.3, 0.3], // Reflexão difusa (produzida por superfícies ásperas que tendem a refletir luz em todas as direções)
  spec: [255, 200, 200], // Reflexão especular (produzida por superfícies lisas que refletem luz num ângulo definido)
  spec_exp: 500.0 // Exposição da reflexão especular
};

// Material
var materials = [matWhite];

// Definição dos cálculos das cores das pointlights (luz emitida pelas esferas 0, 1, 2, 3, 4)
var pointlights = [
  // Cor branca (0)
  {
    pos: [0, 0, 0, 1], // Posição
    col: [1.00, 1.00, 1.00], // Cor (RGB)
    rad: 400 // Quantidade de luz transmitida
  },
  // Cor rosa (1)
  {
    pos: [0, 0, 0, 1], // Posição
    col: [2.00, 0.00, 0.50], // Cor (RGB)
    rad: 200 // Quantidade de luz transmitida
  },
  // Cor azul (2)
  {
    pos: [0, 0, 0, 1], // Posição
    col: [0.00, 0.40, 1.00], // Cor (RGB)
    rad: 200 // Quantidade de luz transmitida
  },
  // Cor amarela (3)
  {
    pos: [0, 0, 0, 1], // Posição
    col: [1.00, 0.40, 0.00], // Cor (RGB)
    rad: 300 // Quantidade de luz transmitida
  },
  // Cor verde (4)
  {
    pos: [0, 0, 0, 1], // Posição
    col: [0.00, 1.00, 0.00], // Cor (RGB)
    rad: 300 // Quantidade de luz transmitida
  },
];

// Geometria
var busto_def = {
  r1: 100, // Raio
};


function setup() {
  createCanvas(windowWidth, windowHeight, WEBGL);

  var phong_vert = document.getElementById("phong.vert").textContent;
  var phong_frag = document.getElementById("phong.frag").textContent;

  phongshader = new p5.Shader(this._renderer, phong_vert, phong_frag);

  // Câmera 1
  var state1 = {
    distance: 700, // Distância da câmera aos objetos
    // center: [0, 0, 0], // Centro da câmera aos objetos
    // rotation: [1, 1, 0, 0], // Rotação da câmera aos objetos
  };
  
  easycam = new Dw.EasyCam(this._renderer, state1);
}


function draw() {
  
  background(0); // Definição da cor de fundo
  
  push(); // Guarda a posição atual da origem de desenho 
  translate(0, 0, 0); // Faz a alteração à origem de desenho
  rotateX(radians(0)); // Rotação do objeto conforme o valor do ângulo inserido, no eixo X
  rotateY(radians(0)); // Rotação do objeto conforme o valor do ângulo inserido, no eixo Y
  rotateZ(radians(180)); // Rotação do objeto conforme o valor do ângulo inserido, no eixo Z
  scale(2); // Definir o tamanho do busto
  noStroke(); // Definir o contorno
  model(obj3D); // Definição do objeto 3D
  pop(); // Repõe as configurações guardadas no push() anterior 
  
  // Animação das esferas - 2 pointlights movem-se livremente no espaço e 3 ao redor do busto
  // push() guarda a posição atual da origem de desenho
  push(); {
    var ang = sin(frameCount * 0.03) * 0.6; // Velocidade da rotação
    var ty = busto_def.r1 * 2 + (1 - abs(ang)) * 100; // Movimento/raio

    // Pointlight em frente ao busto
    push(); // Guarda a posição atual da origem de desenho 
    rotateX((ang + 1) * PI / 2); // Rotação da pointlight no eixo x, com valor de "1" para que esta se movimente na parte frontal do busto + PI (3,14) a dividir por 2 de forma a percorrer apenas meia volta ao busto
    translate(0, ty, 0); // Alteração à origem de desenho (em "ty" = busto)
    addPointLight(phongshader, pointlights,0); // Adição da pointlight com a cor branca (0)
    pop(); // Repõe as configurações guardadas no push() anterior 

    // Pointlight que faz uma volta ao busto perfeita e infinita
    push(); // Guarda a posição atual da origem de desenho 
    rotateZ(frameCount * 0.02); // Rotação da pointlight no eixo Z + Velocidade da rotação
    translate(240, 0, 0); // Alteração à origem de desenho (em X)
    addPointLight(phongshader, pointlights, 1); // Adição da pointlight com a cor rosa (1)
    pop(); // Repõe as configurações guardadas no push() anterior
    
    // Pointlight que não segue uma linha contínua no espaço
    push(); // Guarda a posição atual da origem de desenho 
    rotateZ(0 * TWO_PI / 3 + frameCount * 0.01); // Rotação da pointlight no eixo Z; TWO_PI (6.28) - duas vezes o radio da circunferência de um círculo ao seu diâmetro, a dividir por 3 + Velocidade da rotação
    translate(10, 90, 0); // Alteração à origem de desenho (em X e Y)
    rotateY(sin(frameCount * 0.01) * TWO_PI); // Rotação da pointlight no eixo Y + Velocidade da rotação
    translate(50, 100, 20); // Alteração à origem de desenho (em X, Y, Z)
    addPointLight(phongshader, pointlights, 2); // Adição da pointlight com a cor azul (2)
    pop(); // Repõe as configurações guardadas no push() anterior 

    // Pointlight que emite luz amarela
    push(); // Guarda a posição atual da origem de desenho 
    rotateZ(2 * TWO_PI / 3 + frameCount * 0.01); // Rotação da pointlight no eixo Z; TWO_PI (6.28) - duas vezes o radio da circunferência de um círculo ao seu diâmetro, a dividir por 3 + Velocidade da rotação
    translate(180, 80, 100); // Alteração à origem de desenho (em X, Y, Z)
    rotateY(frameCount * 0.01); // Rotação da pointlight no eixo Y + Velocidade da rotação
    translate(180, 60, 20); // Alteração à origem de desenho (em X, Y, Z)
    addPointLight(phongshader, pointlights, 3); // Adição da pointlight com a cor amarela (3)
    pop(); // Repõe as configurações guardadas no push() anterior 
    
    // Pointlight que não chega a dar uma volta completa horizontalmente
    push(); // Guarda a posição atual da origem de desenho 
    rotateZ(+PI / 2 + sin(frameCount * 0.02) * 2 * PI / 3); // Rotação da pointlight no eixo Z; PI (3,14) a dividir por 2 de forma a percorrer apenas meia volta + Velocidade da rotação
    translate(180, 80, 0); // Alteração à origem de desenho (em X e Y)
    rotateY(frameCount * 0.1); // // Rotação da pointlight no eixo Y + Velocidade da rotação
    // translate(0, 0, 0);
    addPointLight(phongshader, pointlights, 1); // Adição da pointlight com a cor rosa (1)
    pop(); // Repõe as configurações guardadas no push() anterior 
    
    // Pointlight que não chega a dar uma volta completa horizontalmente a nível dos olhos
    push(); // Guarda a posição atual da origem de desenho 
    rotateZ(+PI / 2 + sin(frameCount * 0.007) * 2 * PI / 3); // Rotação da pointlight no eixo Z; PI (3,14) a dividir por 2 de forma a percorrer apenas meia volta + Velocidade da rotação
    translate(120, 0, 80); // Alteração à origem de desenho (em X e Z)
    rotateY(frameCount * 0.00); // // Rotação da pointlight no eixo Y + Velocidade da rotação
    // translate(0, 0, 0);
    addPointLight(phongshader, pointlights, 4); // Adição da pointlight com a cor verde (4)
    pop(); // Repõe as configurações guardadas no push() anterior 
    
    noStroke(); // Esferas sem contorno 
  }

// Definição do phongshader como o shader
  // setShader ativa sombras e luzes; resetShader retira sombras e luzes
  setShader(phongshader); 
}


// Definição do shader
function setShader(shader) {
  this.shader(shader);
}


// Definição do shader no material
function setMaterial(shader, material) {
  shader.setUniform('material.diff', material.diff);
  shader.setUniform('material.spec', material.spec);
  shader.setUniform('material.spec_exp', material.spec_exp);
}


// Definição do shader nas pointlights
function addPointLight(shader, pointlights, idx) {
  var light = pointlights[idx];

  light.pos_cam = easycam.renderer.uMVMatrix.multVec(light.pos);

  shader.setUniform('pointlights[' + idx + '].pos', light.pos_cam);
  shader.setUniform('pointlights[' + idx + '].col', light.col);
  shader.setUniform('pointlights[' + idx + '].rad', light.rad);

  // Atribuição do material dos objetos (e cor)
  setMaterial(phongshader, matWhite);
  
  // Tamanho das esferas
  sphere(5);
}

// Matrix
// Multiplica: vdst = mat * vsrc
p5.Matrix.prototype.multVec = function(vsrc, vdst) {

  vdst = (vdst instanceof Array) ? vdst : [];

  var x = 0,
    y = 0,
    z = 0,
    w = 1;

  var mat = this.mat4 || this.mat3;
  if (mat.length === 16) {
    vdst[0] = mat[0] * x + mat[4] * y + mat[8] * z + mat[12] * w;
    vdst[1] = mat[1] * x + mat[5] * y + mat[9] * z + mat[13] * w;
    vdst[2] = mat[2] * x + mat[6] * y + mat[10] * z + mat[14] * w;
    vdst[3] = mat[3] * x + mat[7] * y + mat[11] * z + mat[15] * w;
  } else {
    vdst[0] = mat[0] * x + mat[3] * y + mat[6] * z;
    vdst[1] = mat[1] * x + mat[4] * y + mat[7] * z;
    vdst[2] = mat[2] * x + mat[5] * y + mat[8] * z;
  }
  return vdst;
}


//-----------------------------------------------------------------------------------

// The p5.EasyCam library - Easy 3D CameraControl for p5.js and WEBGL.

// Copyright 2018 by Thomas Diewald (https://www.thomasdiewald.com)

// Source: https://github.com/diwi/p5.EasyCam

// MIT License: https://opensource.org/licenses/MIT