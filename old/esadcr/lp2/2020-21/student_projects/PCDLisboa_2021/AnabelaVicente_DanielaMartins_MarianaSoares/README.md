**Artificial System**

**FASE 01**

**Processo de Pesquisa**

A nossa pesquisa começou a partir da visualização dos exercícios em
aula, e da revisão dos mesmos através do repositório. Consultámos os
links fornecidos pelo docente e procurámos alguns sites que nos pudessem
fornecer mais informação sobre os algoritmos e inteligência artificial.
Utilizámos como referência e inspiração a série *Westworld*, o Youtube e
o site [https://openprocessing.org](https://openprocessing.org/).

**Referências Principais:**

[https://www.khanacademy.org/computing/computer-science/algorithms](https://www.khanacademy.org/computing/computer-science/algorithms)

[https://rockcontent.com/br/blog/algoritmo/](https://rockcontent.com/br/blog/algoritmo/)

[https://westworld.fandom.com/wiki/Rehoboam](https://westworld.fandom.com/wiki/Rehoboam)

[https://openprocessing.org/sketch/494388](https://openprocessing.org/sketch/494388)

[https://www.youtube.com/watch?v=6PU74AObMfE](https://www.youtube.com/watch?v=6PU74AObMfE)

[https://www.youtube.com/watch?v=bh5tuoHmVL8](https://www.youtube.com/watch?v=bh5tuoHmVL8)

**Conceito**

A inteligência artificial tem apresentado um grande avanço na forma de
como os sistemas simulam uma inteligência similar à humana, baseada em
padrões de enormes bancos de dados. O nosso objetivo foca-se na criação
de um ambiente que represente algoritmos, e que idealize a a sua criação
pela parte do ser humano e a influência que tem atualmente sob o mesmo.
Deste modo, iremos criar um elemento central que simbolize a humanidade,
rodeado por componentes que representem estes algoritmos, de forma a que
seja possível explorar diferentes formas e posições no espaço
tridimensional do *p5js*, através de processos computacionais.

**Moodboard**

![](images/image1.png)

**FASE 02**

**Desenvolvimento**

**Histórico de Versões:**

[https://editor.p5js.org/MarianaSoares97/sketches/ICRF1J6m0](https://editor.p5js.org/MarianaSoares97/sketches/ICRF1J6m0)

Teste com um cubo no centro e exploração do mesmo e do espaço utilizando
o rato.

[https://editor.p5js.org/MarianaSoares97/sketches/nksmm1\_Rj](https://editor.p5js.org/MarianaSoares97/sketches/nksmm1_Rj)

Troca do cubo pelo busto.

[https://editor.p5js.org/MarianaSoares97/sketches/xjkWtQ1jN](https://editor.p5js.org/MarianaSoares97/sketches/xjkWtQ1jN)

Experiência com directional light.

[https://editor.p5js.org/danielamartins/sketches/tdiVhSIsZ](https://editor.p5js.org/danielamartins/sketches/tdiVhSIsZ)

Código de inspiração com alguma limpeza e início de comentários.

[https://editor.p5js.org/danielamartins/sketches/Wk1UNYtic](https://editor.p5js.org/danielamartins/sketches/Wk1UNYtic)

Continuação da limpeza e comentários, e exploração do código e da
biblioteca easycam.

[https://editor.p5js.org/danielamartins/sketches/2uvBmRPfh](https://editor.p5js.org/danielamartins/sketches/2uvBmRPfh)

Troca do cubo pelo busto.

[https://editor.p5js.org/danielamartins/sketches/E3BtWj-1G](https://editor.p5js.org/danielamartins/sketches/E3BtWj-1G)

Experiência com a easycam e cor de fundo.

[https://editor.p5js.org/MarianaSoares97/sketches/REJ-G0hVN](https://editor.p5js.org/MarianaSoares97/sketches/REJ-G0hVN)

Código mais limpo e com mais comentários. Experiência mais próxima do
protótipo final, onde foi explorado o fundo, o material dos objetos,
cores das esferas e a animação e posição das mesmas.

[https://editor.p5js.org/MarianaSoares97/sketches/ZJ8EwVkhw](https://editor.p5js.org/MarianaSoares97/sketches/ZJ8EwVkhw)

Foi retirado o shader para se perceber a cor das próprias esferas.

[https://editor.p5js.org/MarianaSoares97/sketches/YVtZ3Kbot](https://editor.p5js.org/MarianaSoares97/sketches/YVtZ3Kbot)

Protótipo final com as cores, animação e material escolhidos. Foi
adicionado mais uma esfera para complementar e uma melhoria nos
comentários com a adição de instruções para se utilizar a câmera e o
espaço onde os objetos estão inseridos.

**Protótipo:**

[https://editor.p5js.org/MarianaSoares97/sketches/YVtZ3Kbot](https://editor.p5js.org/MarianaSoares97/sketches/YVtZ3Kbot)

**FASE 03**

**Final**

**Memória Descritiva:**

No âmbito da disciplina de Laboratório de Projeto II foi-nos proposta a
elaboração de um projeto com o intuito de participar no futuro evento
"PCD@Lisboa_2021", cujo tema se insere na "Automação Contemporânea".
Sendo um tema de interesse do grupo, as nossas referências visuais
surgiram naturalmente, assim como a ideia e o conceito para o trabalho.
Idealizámos assim, a criação de um espaço que representasse a relação
estabelecida entre humanidade e inteligência artificial: um sistema em
que o ser humano é constantemente influenciado pelos algoritmos,
mostrando até dependência. Deste modo, criámos um ambiente infinito no
espaço tridimensional, em que a figura central é um busto humano que
representa toda a humanidade, recebendo luz e cores de várias esferas,
que por sua vez representam os algoritmos que temos contato no dia-a-dia
(ex: verde - WhatsApp). Com esta representação podemos sentir a
dependência que existe nesta relação de humano e inteligência
artificial: ao interagirmos com o espaço, se clicarmos e arrastarmos o
rato para qualquer lado, vemos que o busto acaba por seguir sempre as
esferas, contrastando com as mesmas que giram em direções diferentes. É
possível ainda verificarmos que estas esferas emitem luzes diferentes e
atraentes sobre busto, remetendo novamente para esta atração e
dependência que sofremos com a inteligência artificial.

Anabela Vicente | Daniela Martins | Mariana Soares

"Artificial System"

ESAD.CR - Escola Superior Artes e Design

Licenciatura em Design Gráfico e Multimédia

**Artefato Final:**

[https://editor.p5js.org/danielamartins/sketches/HpWYu1AHF](https://editor.p5js.org/danielamartins/sketches/HpWYu1AHF)

**Exercício de avaliação: PCD@Lisboa_2021**

Anabela Vicente, nº3180183

Daniela Martins, nº3180758

Mariana Soares, nº3180483

**UC Laboratório de Projeto II (2020/2021)**

**Docente: Marco Heleno**
