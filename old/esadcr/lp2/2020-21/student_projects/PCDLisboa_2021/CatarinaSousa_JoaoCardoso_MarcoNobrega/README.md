PCD@Lisboa 2021 // Briefing 1
==============================

#### Laboratório de Projeto II 

###### Catarina Sousa 3170233

###### João Cardoso 3180756

###### Marco Nóbrega 3180217

### 1ª Fase

O tema da edição de 2021 do evento Processing Community Day que será
realizado em Lisboa é a **automação contemporânea.**

### Pesquisa - O que é a automação contemporânea?

A automação descreve uma ampla gama de tecnologias que reduzem a
intervenção humana nos processos. A intervenção humana é reduzida por
critérios de decisão predeterminados, relacionamentos de subprocesso e
ações relacionadas - e incorporando essas predeterminações em máquinas.
- in [Wikipedia](https://en.wikipedia.org/wiki/Automation)

Um dos exemplos de automação contemporânea pode ser a **Inteligência
Artificial.**

Enquanto os filmes de Hollywood e os romances de ficção científica
retratam a IA como robôs semelhantes aos humanos que dominam o mundo, a
evolução atual das tecnologias de IA não é tão assustadora - ou tão
inteligente.

Em vez disso, a IA evoluiu para fornecer muitos benefícios específicos
em todos os setores.

A IA adiciona inteligência aos produtos existentes. Na maioria dos
casos, AI não será vendido como um aplicativo individual. Tal como a
Siri foi adicionada como um recurso a uma nova geração de produtos da
Apple. (Ou a Google Assistent) . Automação, plataformas de conversação,
bots e máquinas inteligentes podem ser combinados com grandes
quantidades de dados para melhorar muitas tecnologias em casa e no local
de trabalho, da inteligência de segurança à análise de investimento. -
in
[SAS](https://www.sas.com/en_us/insights/analytics/what-is-artificial-intelligence.html)

### Conceito

O conceito que queremos apresentar como proposta gráfica para a próxima
edição do PCD@Lisboa 2021 é o seguinte:

-   Um website que consiste na demonstração de sensações / emoções
    baseada na cor no objeto 3D que está a ser projetado no ecrã. **A
    tomada de decisão do utilizador influencia o layout do site.**

-   **!!Human Input and Computer Feedback!!**

### Moodboard // Pinterest Link

[Pinterest Colaborative MoodBoard Link](https://pin.it/1ssbWL7)

### 2ªFase

Nesta fase inicial do protótipo, decidimos explorar como a forma 3D
(cubo) se relaciona com a interface escolhida pelo utilizador.

No momento em que o utilizador entra na página terá que fazer uma
escolha cromática que influenciará o layout final da página do evento.

Queremos também adicionar mais à frente no desenvolvimento, uma música
ambiente associada a cada tema que seja refletivo das emoções sentidas
pelo utilizador.

#### P5.js Links:

[Representação da Futura Interface Interativa com Color Selection //
p5.js link](https://editor.p5js.org/marcospyder/sketches/VId4-9prz)

[Exploração Cromática do Cubo \\ NEON STYLE
ILLUMINATION](https://editor.p5js.org/CatSousaEsad/sketches/-29SsQ0aE)

###### How might we?

-   criar um layout semelhante a um AI ? (ex; Siri) ;

-   não perder a informação do evento no layout colorido? ;

-   reforçar a temática da automação contemporânea? ;

### 3ªFase

**"Como podemos explorar a emoção num AI?"**

Associando a **EMOÇÃO** a uma **COR,** e fazendo **display da COR
(emoção) para o utilizador no objeto 3D** que é a **materialização** de
um AI fícticio.
O giver do input será o user e o receiver do input será o AI (CUBO).

O input do utilizador será a escolha consoante a emoção que sente no
momento em que a máquina AI põe a questão" What color are you feeling
today?" e o output da máquina será a mudança cromática.

Considerando que o tema do PCD@2021 será a automação contemporânea
decidimos pegar em questões, que apesar do progresso da inteligência
artificial permanecem:

-Terá a AI capacidade de sentir emoção?
-O que nos separa da máquina?
-Podemos utilizar a cor como forma de expressar emoção? Se sim, que
cores são associadas às emoções?-Em que outros meios é a cor utilizada
para expressar determinadas emoções? (ex: PIXAR ANIMATION, HOLLYWOOD).

**Conceitos-Chave: Human-Machine Interface , Artificial Inteligence, Cor
, Emoção, Display, Interface**

Este protótipo está pensado para funcionar como uma landing page
introdutória do website do evento.

### Podemos utilizar a cor como forma de expressar emoção? Se sim, que cores são associadas às emoções?-Em que outros meios é a cor utilizada para expressar determinadas emoções? (ex: PIXAR ANIMATION, HOLLYWOOD). 

Utilizámos o exemplo da Pixar Animation Studios como exemplo de como a
cor pode ser utilizada num cenário de forma a manipular as emoções do
viewer.

![](media/rId37.jpg)

@ Lou Romano's Color Script for UP

O filme começa com cores pouco saturadas que vão ficando mais iluminadas
à medida que a personagem principal se sente mais feliz.

Os vilões da Disney estão sempre acompanhados de detalhes verdes e os
objetos que são codificados de verde também são perigosos.

![](media/rId38.jpg)

Segue em baixo as emoções escolhidas e cores que as representam, assim
como os exemplos onde estas estão a ser usadas:

SADNESS - DESATURATED BLUE (Up! ,Finding Nemo , Inside Out, Soul )

JOY - ORANGE (Up! , Inside Out , Soul, Hercules)

PASSION - BRIGHT PINK (Hercules, Soul, Inside Out, Pocahontas)

GOOD - YELLOW (Beauty and the Beast, Pocahontas, Up)

EVIL - LIME GREEN (The Lion King ,Cinderela ,Snow White and the 7 Dwarfs
)

#### P5.js Links:

[Protótipo
Final](https://editor.p5js.org/marcospyder/sketches/s6UscltJd)

Versões anteriores:

[Representação da Futura Interface Interativa com Color Selection //
p5.js link](https://editor.p5js.org/marcospyder/sketches/VId4-9prz)

[Exploração Cromática do Cubo \\ NEON STYLE
ILLUMINATION](https://editor.p5js.org/CatSousaEsad/sketches/-29SsQ0aE)

<https://editor.p5js.org/marcospyder/sketches/GPT9hbSpB> (3D Button)

### Referências:

[Machines That Learn Are Invading, and Utterly Transforming, Our
Lives](https://futurism.com/1-evergreen-machines-that-learn-are-invading-and-utterly-transforming-our-lives)

[Artificial Intelligence : What it is and why it
matters](https://www.sas.com/en_us/insights/analytics/what-is-artificial-intelligence.html)

[Inteligência artificial "não passa ao lado" de Portugal. "Há qualidade,
mas falta
quantidade"](https://www.publico.pt/2021/02/11/tecnologia/noticia/inteligencia-artificial-nao-passa-lado-portugal-ha-qualidade-falta-quantidade-1950152)

[BioFeedBack](https://en.wikipedia.org/wiki/Biofeedback)

[Amazon's Alexa Commercial](https://www.youtube.com/watch?v=xxNxqveseyI)

<https://www.konnectagency.com/2015/10/27/color-psychology-and-content-marketing/>

<https://archive.nerdist.com/pixars-use-of-color-is-incredible/>

<https://www.businessinsider.com/how-pixar-uses-lighting-to-tell-breathtaking-stories-2015-12>

<https://www.theverge.com/2014/9/6/6114171/watch-pixars-gorgeous-use-of-color-over-the-years>

<https://renzlca.wordpress.com/2017/02/03/cj011-pixar-and-color-symbolism-in-up/>

<https://ohmy.disney.com/movies/2015/11/12/disney-movies-taught-us-that-lime-green-is-a-harbinger-of-evil/>

<https://venngage.com/blog/disney-villains/>

<https://processing.org/tutorials/p3d/>

<https://processing.org/reference/box_.html>

<https://p5js.org/reference/#/p5/pointLight>

<https://blog.prototypr.io/how-to-design-for-ai-enabled-ui-77e144e99126>

<https://www.artofthetitle.com/title/the-queens-gambit/>
