//Projeto de Laboratório II - Ano Letivo 2020/2021 , DGM 3ºAno 
//Docente: Marco Heleno
//Catarina Sousa 3170233 , João Cardoso 3180756, Marco Nóbrega 3180217

//Variaveis Globais
let colors, pink, blue, lime, yellow, orange

//Variaveis de cor
pink = '#ff7994';
blue = '#58759a';
lime = '#a5f112';
yellow = '#f2d160';
orange = '#fc7427';

colors = ['#ff7994', '#58759a', '#a5f112', '#f2d160', '#fc7427'];

//Definição do ecrã responsive e 3D
function setup() {
  createCanvas(windowWidth, windowHeight, WEBGL);
}

function draw() {
  background(50);
  
  //Inicio das definições das ellipses que servem de paleta de cores, sem contornos e cores rgb
  push();
  ellipseMode(CENTER);
  noStroke();
  
  fill(pink);
  ellipse(0, 125, 40, 40);
  
  fill(blue);
  ellipse(-60, 125, 40, 40);

  fill(lime);
  ellipse(-120, 125, 40, 40);
  
  fill(yellow);
  ellipse(60, 125, 40, 40);
  
  fill(orange);
  ellipse(120, 125, 40, 40);  
  pop();
  //Final das definições
  
  //Inicio do código do cubo
  //Rotação no eixo do x e y com referência ao rato
  rotateY(mouseX * 0.01);
  rotateX(mouseY * 0.01);
  //Contorno
  stroke(50);
  //Definição das cores do cubo juntamente com um temporizador de frames para abrandar a transição das mesmas
  if(frameCount % 70 == 1){
  fill(random(colors));
  }
  //Espessura do contorno
  strokeWeight(1);
  //Cubo 3D
  box(100);
}