// NOME DO PROJETO - NumDeep
// PCD@Lisboa_2021 - 3º Momento de Avaliação
// ESAD.CR
// Ano letivo de 2020/2021
// Carolina Parente | 3180377
// Inês Piçarra | 3180372
// Laboratório de Projeto II
// 3º ano
// 2º semestre
// Licenciatura em Design Gráfico e Multimédia









let num_elementos, x_elemento, y_elemento, z_elemento, largura_box, altura_box, profundidade_box;     //criação de variáveis
let comp, espacamento;                                                                                //criação de variáveis
let desenha_circulo, verifica_se_pertence_bolha, forca_limite;                                        //criação de variáveis
let random_h, random_v;                                                                               //criação de variáveis
let pg1, pg2;                                                                                         //criação de variáveis
//let botao;                                                                                          //criação de variáveis





function setup() 
{
  createCanvas (windowWidth, windowHeight, WEBGL);                    // criação do canvas, e definição de espaço 3D
  
  num_elementos = 45;    // número de elementos desenhados
  
  pg1 = createGraphics (100, 100);
  pg1.textSize(90);                    // definição do tamanho do texto
  pg1.textAlign (CENTER, CENTER);      // alinhar texto
  pg1.background (0, 0);               // definição de fundo
  pg1.fill(255);                       // preenchimento/cor do texto
  pg1.text("0", 50, 54);               // definição de texto a apresentar
  
  pg2 = createGraphics (100, 100);
  pg2.textSize(90);                    // definição do tamanho do texto
  pg2.textAlign (CENTER, CENTER);      // alinhar texto
  pg2.background (0, 0);               // definição de fundo
  pg2.fill(255);                       // preenchimento/cor do texto
  pg2.text("1", 50, 54);               // definição de texto a apresentar
  
  /*
  //criação de botão para levar a página com conceito do projeto
  
  botao = createButton('click me');
  botao.position(width/42, height/45); // posicao botao canto superior esquerdo
  botao.mouseClicked(outrapagina); // chamar funcao*/
  
  frameRate (1);      // 'velocidade'
}









function draw() 
{
  
  random_h = int( random(10, num_elementos-10) ); // temporário para prova de ideia
  random_v = int( random(10, num_elementos-10) ); // temporário para prova de ideia
  
  //console.log(frameRate());
  background (0);                   //definição da cor 'preto' para fundo
  
  let frame_contador = frameCount; // contador
  
  if (width > height) comp = height - height/10;
  else comp = width - width/10;
  
  espacamento = comp/num_elementos;
  
  //console.log(frame_contador) // contador ver
  
  
  push(); //salva as definições anteriores
  
    translate (0, 0, 0);   //composicao
  
    for (let h=0; h<num_elementos; h++)           //horizontal
    {
      for (let v=0; v<num_elementos; v++)         //vertical
      {
        desenha_circulo = dist (num_elementos/2, num_elementos/2, h, v); //desenhar 'máscara' circular, para contenção dos elementos 'desenhados'
        
        if (desenha_circulo <= num_elementos/2) 
        {
          push(); //salva as definições anteriores
            x_elemento = -comp/2 + espacamento/2 + h * espacamento;
            y_elemento = -comp/2 + espacamento/2 + v * espacamento;

            verifica_se_pertence_bolha = dist (h, v, random_h, random_v); 
            forca_limite = constrain (verifica_se_pertence_bolha, 0, 8);
          
            z_elemento = map (forca_limite, 0, 8, 300, 0);
          
            translate (x_elemento, y_elemento, z_elemento);  //posicionar elementos no canvas
          
          if (z_elemento>0){
            texture(pg2);         // textura que permite ver os '1'
          }
          else{
             texture(pg1);        // textura que permite ver os '0'
          }
            
            noStroke();           // sem contorno
            plane(espacamento, espacamento); // plano para 'desenhar' texto
          pop();   //restaura as definições anteriores
        }
      }
    }
  pop();           //restaura as definições anteriores
}





/*
function mouseClicked() {} 

function outrapagina() {
  if (mouseClicked){
    window.open("https://editor.p5js.org/Grupo_1/present/W_thHS3VU");
  } // se clicar o rato dentro do espaço do botao abre nova pagina da explicacao
}*/