// NOME DO PROJETO -- NumDeep

// PCD@Lisboa_2021 - 3º Momento de Avaliação

// ESAD.CR

// Ano letivo de 2020/2021

// Carolina Parente | 3180377

// Inês Piçarra | 3180372

// Laboratório de Projeto II

// 3º ano

// 2º semestre

// Licenciatura em Design Gráfico e Multimédia

**INVESTIGAÇÃO**

Processing Community Day (PCD), é um evento de arte algorítmica que
mistura linguagem de programação, arte, design e ensino, que se destina
a reunir pessoas interessadas em linguagem de programação e
utilizadores/ criadores de arte em processing e plataformas
relacionadas. O evento centra-se na criatividade e criação visual.

**"Futurist Thomas Frey, as an example, predicted in a TEDx talk that 2
billion jobs will have disappeared by 2030"**

Apesar de na maioria das vezes não nos apercebermos, os algoritmos estão
presentes no nosso dia-a-dia, facilitando diversas tarefas, porém
torna-nos dependentes. Segundo um livro de Hannah Fry, não devemos
pensar nos algoritmos como algo 'bom' ou 'mau', deveríamos sim, dar
atenção aqueles que programam, pois são esses que dão 'vida' aos
'pedaços de código' que se inserem na nossa vida. Isto é importante
pois são esses mesmos 'pedaços de código' que tomam decisões, uma vez
se presentes em hospitais, GPS, motores de busca, entre muitos outros.

Associado aos algoritmos, surgem as 'máquinas'. Estas 'máquinas' são
as responsáveis por nos permitirem ver os algoritmos a funcionar. Estas
máquinas também estão presentes diariamente no nosso dia-a-dia, como por
exemplo um telemóvel que apresenta diferentes resultados de pesquisa
para a mesma questão, consoante o historial de cada utilizador. Este
tipo de situação acontece devido ao algoritmo, que guarda informações de
pesquisas do utilizador, e ao telemóvel que as apresenta ao utilizador.
Pode ser positivo, pois apenas é apresentada informação que o utilizador
pesquisa, mas também pode ser negativo, pois não é apresentada
informação diferente daquela que costuma pesquisar.

Isto faz com que nem 'pedaços de código', nem as 'máquinas' sejam
perfeitos. Não devem ser considerados como algo 'maravilhoso' mas
também não devem ser considerados 'horríveis'. Esta evolução pode ser
bastante positiva, apenas tem de ser feita de modo cauteloso.

![](images/image1.jpeg)

Durante a investigação sobre o evento @PCD, foi possível ver que a
imagem visual utilizada nos websites de apresentação de edições
anteriores do @PCD é bastante diversificada, contudo, é comum a
utilização de um fundo simples para que a leitura dos conteúdos seja
fácil e direta. São utilizadas várias cores, não existindo nenhuma
predominante.

O tema do @PCD Lisboa 2021, 'Automação Contemporânea', é um assunto
muito atual, pois vivemos numa sociedade em que cada vez mais estamos
dependentes das 'máquinas', e por isso devemos estar informados sobre
as evoluções que têm surgido na área.

**CONCEITO**

Século XXI, um século onde as inovações tecnológicas têm vindo a marcar
uma forte e aguerrida presença.

Mesmo não sendo percecionado por muitos, não deixa de ser um facto
relevante a permanência dos algoritmos nas sociedades, nas economias e
na geopolítica. Como exemplo do anteriormente referido pode mencionar-se
a intensificação/massificação da utilização das redes sociais.

Plataformas como o Instagram, Behance, Facebook, Pinterest, entre
outras, são utilizadas diariamente por grande parte da população
mundial, contudo, a maioria dos utilizadores das mesmas não se apercebe
de que o conteúdo apresentado tem por base algoritmos. Utilizando um
post no Instagram, como exemplo, ao publicar uma fotografia no feed, a
plataforma recebe dados sobre a mesma, e transforma esses mesmos dados,
em informação (fotografia que os restantes utilizadores vão ver), sem
que seja necessário terem acesso à origem da mesma.

Assim, o presente projeto procura demonstrar o modo como os algoritmos
transformam dados em informação, utilizando os '0' como representante
dos dados recebidos, e os '1' como elemento representante da informação.
Deste modo é apresentado o 'behind the scenes' da informação recebida em
diversas plataformas, não apresentando a transformação dos dados em
informação, mas sim demonstrando a rapidez em que os algoritmos se
conseguem transformar.

Assim surge o termo 'Era do Algoritmo'.

**MOODBOARD**

![](images/image2.gif)
![](images/image3.png)
![](images/image4.gif)
![](images/image5.jpeg)

**REFERÊNCIAS**

"HOW ALGORITHMS ARE CONTROLLING YOUR LIFE", disponível em:
[https://www.vox.com/technology/2018/10/1/17882340/how-algorithms-control-your-life-hannah-fry](https://www.vox.com/technology/2018/10/1/17882340/how-algorithms-control-your-life-hannah-fry)

"MACHINES THAT LEARN ARE INVADING, AND UTTERLY TRANSFORMING, OUR
LIVES", disponível em:
[https://futurism.com/1-evergreen-machines-that-learn-are-invading-and-utterly-transforming-our-lives](https://futurism.com/1-evergreen-machines-that-learn-are-invading-and-utterly-transforming-our-lives)

![](images/image6.png)

![](images/image7.png)

[http://pcdcoimbra.dei.uc.pt/](http://pcdcoimbra.dei.uc.pt/)

![](images/image8.png)

[https://day.processing.org/pcd-ww.html](https://day.processing.org/pcd-ww.html)

![](images/image9.png)

![](images/image10.png)

![](images/image11.png)

**PROTÓTIPO 1**

**LINK PARA VISUALIZAÇÃO:
[https://editor.p5js.org/Grupo\_1/present/FrfIVdDds](https://editor.p5js.org/Grupo_1/present/FrfIVdDds)**

**LINK DO EDITOR:
[https://editor.p5js.org/Grupo\_1/sketches/FrfIVdDds](https://editor.p5js.org/Grupo_1/sketches/FrfIVdDds)**

**IMAGENS DO PROTÓTIPO:**

![](images/image12.jpeg)
![](images/image13.jpeg)
![](images/image14.jpeg)
![](images/image15.jpeg)

**PROTÓTIPO 2**

**LINK PARA VISUALIZAÇÃO:
[https://editor.p5js.org/Grupo\_1/present/lpNvLwWwl](https://editor.p5js.org/Grupo_1/present/lpNvLwWwl)**

**LINK DO EDITOR:
[https://editor.p5js.org/Grupo\_1/sketches/lpNvLwWwl](https://editor.p5js.org/Grupo_1/sketches/lpNvLwWwl)**

**IMAGENS DO PROTÓTIPO:**

![](images/image16.png)
![](images/image17.png)

**PROTÓTIPO 3**

**LINK PARA VISUALIZAÇÃO:
[https://editor.p5js.org/Grupo\_1/present/mEm6DN7fR](https://editor.p5js.org/Grupo_1/present/mEm6DN7fR)**

**LINK DO EDITOR:
[https://editor.p5js.org/Grupo\_1/sketches/mEm6DN7fR](https://editor.p5js.org/Grupo_1/sketches/mEm6DN7fR)**

**IMAGENS DO PROTÓTIPO:**

![](images/image18.png)
![](images/image19.png)

**PROJETO FINAL**

**LINK PARA VISUALIZAÇÃO:
[https://editor.p5js.org/Grupo\_1/present/\_h9AChNxs](https://editor.p5js.org/Grupo_1/present/_h9AChNxs)**

**LINK DO EDITOR:
[https://editor.p5js.org/Grupo\_1/sketches/\_h9AChNxs](https://editor.p5js.org/Grupo_1/sketches/_h9AChNxs)**

**IMAGENS DO PROTÓTIPO:**

![](images/image20.png)
![](images/image21.png)