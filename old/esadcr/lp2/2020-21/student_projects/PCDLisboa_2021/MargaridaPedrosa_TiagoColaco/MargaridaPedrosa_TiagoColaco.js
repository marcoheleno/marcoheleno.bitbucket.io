// Unidade Curricular de Laboratório de Projecto II
// Professor Marco Heleno
// Licenciatura de Design Gráfico e Multimédia - 6º Semestre / 3º Ano
// Ano Lectivo 2020-2021
// Trabalho de: 
// Margarida Pedrosa - 3180452
// Tiago Colaço      - 3190743
// PULSAR

let num_boxs, x_box, y_box, z_box, largura_box, altura_box, profundidade_box;
let espaco_maximo_composicao, espacamento_entre_boxs, ajuste, t;
let frameRateContador;
//definição de variáveis

function setup() 
{
  setAttributes('antialas', true); //melhora a qualidade final do render
  createCanvas (windowWidth, windowHeight, WEBGL); 
  
  t = 10; // tamanho
  num_boxs = t; // numero de caixas
  frameRateContador = 1;
  frameRate (frameRateContador); // velocidade dos frames
}

function draw() 
{
  //console.log (frameRate()); // consola
  
  
  orbitControl(); // rotação da visualização
  background (0); // cor do fundo
  shininess(20); // brilho do objeto
  ambientLight(0,255,255); // luz ambiente no objeto
  ambientMaterial(0,255,255); // cor do material
  
  // criação da grelha de cubos, lendo a altura e largura da tela preview
  if (width > height) 
  {
    ajuste = height/2.5;
    espaco_maximo_composicao = height - ajuste;
  }
  else 
  {
 
    ajuste = width/2.5;
    espaco_maximo_composicao = width - ajuste;
  }
  // definição do espaçamento entre cubos
  espacamento_entre_boxs = espaco_maximo_composicao/t;
  
  push();
    translate(0, 0, 0); // posição da composição
  
    rotateX (radians(45));
    rotateY (radians(0));  
    rotateZ (radians(45)); // rotação da composição
  
    for (let h=0; h<t; h++) // horizontal
    {
      for (let v=0; v<t; v++) // vertical
      {
        push();
          x_box = h * espacamento_entre_boxs;
          y_box = v * espacamento_entre_boxs; 
          z_box = 0;
          // posicao de cada box
          translate (-espaco_maximo_composicao/2 + espacamento_entre_boxs/2 + x_box, 
                     -espaco_maximo_composicao/2 + espacamento_entre_boxs/2 + y_box, z_box);
          
          /*rotateX (random(1,10)); //rotação de cada box
          rotateY (1);
          rotateZ (10);*/
          
          noStroke(); // sem linha de contorno
          specularMaterial (0, 200, 255, 15); // cor especular(relativo à refração da luz) do objeto
          largura_box = t*2; // largura, tamanho vezes 2
          altura_box  = t*2; // altura, tamanho vezes 2
          profundidade_box = random(0,espaco_maximo_composicao); // randomização do tamanho em altura dos cubos
          box (largura_box, altura_box, profundidade_box); // primitive shape box
        pop();
      }
    }
  pop();
}


function mouseReleased() //comando que interage com o público, quanto mais o user clica com o rato, maior será a velocidade da animação
{
  if (frameRate() <= 30) // se a Frame Rate for inferior a 30
  {
    frameRateContador+=0.5; // incrementa 0,5 ao Frame Rate
    frameRate (frameRateContador); // velocidade dos Frames
  }
}

