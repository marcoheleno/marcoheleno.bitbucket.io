Projeto "Automação Contemporânea" de Laboratório de Projeto II
================================================================

### por Margarida Pedrosa 3180452 e Tiago Colaço 3190743

PULSAR - PDC@Lisboa-2021
-------------------------

Foi nos desafiado, enquanto estudantes da Esad.cr no curso de Design
Multimédia, a criar uma proposta de identidade gráfica para o evento do
PCP@Lisboa-2021 enquadrado no Processing Community Day worldwide, um
evento que celebra a intersecção das várias artes, o desenvolvimento de
código e a diversidade. O tema do evento será "Automação
Contemporânea".

Esta é a nossa proposta.

**1º momento de avaliação - Conceito:**

Com base no Briefing que nos foi apresentado e, querendo explorar também
as possibilidades do 3D, pesquisámos os termos Automação e
Contemporaneidade, até para explorarmos outros significados que não os
mais diretos.
Imediatamente tentamos construir um objeto com referências naturais, que
com ação humana se iria desenvolver e automatizar, mas acabou a ser um
dilema em encontrarmos o tema certo. Acabámos a querer desenvolver algo
apelativo visualmente mas que não choque com a sua finalidade, a de
existir como fundo de um website e não ser um elemento que entre em
conflito visual com a informação que aí se encontrará disponível.
Surgiu então o Pulsar, que será visualmente uma representação de
data/conteúdo recolhido da interação humana com a máquina que age de
acordo com oscilações randomizadas de energia como que um pulsar de
inteligência artificial, que ainda não decidimos, se colocarmos numa
perspetiva fixa ortogonal e isométrica, para reforçar uma robotização do
conteúdo.
A nossa intenção é que se assemelhe ao centro operativo de uma máquina.

**Visão:**

![](media/rId21.png)

#### Moodboard:

<https://www.pinterest.pt/tiagocolacoart/processing/>

**Pesquisa:**

<https://marcoheleno.bitbucket.io/p5js/index.html>

<https://processing.org/tutorials/p3d/>

<https://behreajj.medium.com/3d-transformations-in-processing-de11acdd1fbc>

<https://stackoverflow.com/questions/35177603/r-visualize-many-3d-boxes-inside-a-big-box>

<https://www.youtube.com/watch?v=C3NN_E-CeW4>

**2º momento de avaliação - Processo:**

Após o conceito inicial ser posto em prática no p5.js e discutir formas
com o professor de visualizar a nossa ideia, apresentamos o
desenvolvimento visual do projeto. Criamos então uma rede composta por
retângulos que oscilam com um random controlado em Y. O próximo passo
consiste em adicionar interação com o utilizador do website, pelo
mousePressed. Foi explorado também, interação no sentido cromático, a
cada mouseClick a luz do objeto revela-se, estando este num ambiente
mais escuro. Esta ultima opção serviu como exploração, podendo não ser
abrangida no projeto final.

#### Desenvolvimento:

![](media/rId29.jpg)

#### Link para o P5 - Código desenvolvido

###### <https://editor.p5js.org/TiagoColaco/sketches/L7jYh-ZyL?fbclid=IwAR22GVoASSXnEsPePc30IJXxjtd0Ex1khWFLyCOsDghyKgn1r6Juk1AtjE8>

**3º momento de avaliação - Projeto Final:**

Apresentamos então, o aspeto final do Pulsar, que tal como um castelo
autónomo que processa informação, também ele se alimenta com a atividade
do user do website. Dito isto, é possível observar a interatividade
mudando a perspetiva e aumentando ou diminuindo o zoom, e no
mouseRelease, em que com cada clique do utilizador, a animação aumenta
na sua velocidade, proporcionando uma experiência imersiva.

Com a ajuda do professor Marco Heleno, conseguimos chegar ao nosso
resultado final e ultrapassar algumas dificuldades geradas no processo
criativo.

![](media/rId32.jpg)

![](media/rId33.jpg)

**Link para o P5 - Código Final**

<https://editor.p5js.org/TiagoColaco/sketches/gNEkmV4tr>

**Etapas do projeto:**

<https://editor.p5js.org/TiagoColaco/sketches/k_JxhdKpq> - Pulsar 1.1

<https://editor.p5js.org/TiagoColaco/sketches/MsFmRPUl3> - Pulsar 1.2

<https://editor.p5js.org/TiagoColaco/sketches/L1BKci44k> - Pulsar 1.3

<https://editor.p5js.org/TiagoColaco/sketches/L7jYh-ZyL> - Pulsar 2

<https://editor.p5js.org/TiagoColaco/sketches/rM5QTI28g> - Pulsar 2.1

<https://editor.p5js.org/TiagoColaco/sketches/gNEkmV4tr> - Pulsar Final

PULSAR é um projeto que nasce de 2 mentes e que intenciona contribuir
para o evento de escala global. Nós enquanto grupo acreditamos ter
construído uma ponte entre o tema proposto e o resultado final, apoiado
pelo conceito apresentado tendo em mente a finalidade pedida.

Obrigada!


###### Professor Marco Jorge Heleno

###### 3º Ano Licenciatura Design Multimédia - 2020-21

###### Politécnico de Leiria
