// Francisco Pereira, aluno nº 3180576
// João Casimiro, aluno nº 3180477
// Gonçalo Barros,  aluno nº 3180743
//
// Exercício de Avaliação: PCD@Lisboa_2021
//
// Ano Letivo: 3º Ano, 2020-2021
// 2º Semestre
// Unidade Curricular: Laboratório de Projeto I
// Licenciatura em Design Gráfico e Multimédia
// Escola Superior de Artes e Design - ESAD.CR
// Docente: Marco Heleno
// Data de avaliação: 27 de Abril
//
// ------------------------------------------------


let obj3D, a, limiteActualizacao;

function preload()
{
  // esta função faz o load do objeto 3D
  obj3D = loadModel ("student_projects/PCDLisboa_2021/FranciscoPereira_GoncaloBarros_JoaoCasimiro/torre.obj", true); 
}

function setup()
{
  createCanvas (windowWidth, windowHeight, WEBGL);
  
  //Um intervalo de valores entre 300 e 600 que de maneira aleatoria faz parar a atualização do objeto 3D
  limiteActualizacao = random (300, 600);
  //angulo inicial da rotação do objeto 3D
  a=0;
}

function draw()
{
  // 1º define a velocidade de actualização da malha
  // 2º define o intervalo de tempo que actualiza malha
  if (frameCount%3==0 && frameCount<=500)
  {
    for (let v=0; v<obj3D.vertices.length; v++) 
    {
      //faz distorcer/mover os vertices do objecto 3D nos eixos X,Y e Z
      obj3D.vertices[v].x += random (-1,1);
      obj3D.vertices[v].y += random (-1,1);
      obj3D.vertices[v].z += random (-1,1);
    }

    // "Hack" que descobrimos, ao fazer chamar o CreateCanvas ele faz atualizar o preview frame a frame
    createCanvas (windowWidth, windowHeight, WEBGL);
    prepare_p5_student_project();
  }
  
  background (0);

  // o push() guarda as definições e personalização do objeto 3D enquanto o pop recupera essas definições.
  push();
    translate (0, 0, 0);
    scale (1.5);
  
  //faz a rotação no eixo do Y
    rotateY (radians(a));
  
    stroke (255, 175);
    noFill();
    model (obj3D);
  pop();
  
  //Condição que dá inicio à rotação do objeto 3D
  if (a < 360) a += 1;
  else a = 0; 
}


