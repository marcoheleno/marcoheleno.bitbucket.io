Processing Tower - PCD@Lisboa_2021
====================================

**Pesquisa**
------------

Algoritmos e a Arquitetura

-   Com a assistência de computadores e algoritmos, os arquitetos foram
    > capazes de desenhar edifícios com formas orgânicas, livres de
    > algumas restrições, regras e régua.

-   Os algoritmos de hoje em dia tem o poder de mudar os edifícios,
    > refinar e até mesmo criar novos designs.

-   Algoritmos podem ser uma ferramenta poderosa para dar fornecer
    > exaustivamente informações para o design, construção e uso do
    > edifício.

Edifícios que usaram algoritmos para o seu desenvolvimento:

-Frank Gehry's Guggenheim Museum

![Clássicos da Arquitetura: Museu Guggenheim de Bilbao | Gehry Partners | ArchDaily Brasil](images/image6.jpg)

-Future Systems' Selfridges Department Store

![Selfridges | Future Systems - Arch2O.com](images/image10.jpg)

A questão que um artigo de pesquisa levantou foi:
[https://aeon.co/ideas/if-machines-want-to-make-art-will-humans-understand-it](https://aeon.co/ideas/if-machines-want-to-make-art-will-humans-understand-it)

Se as máquinas começarem a fazer arte, será que nós, os humanos, iremos
perceber-lá?

Como seres humanos, usamos a consciência, experiências passadas, cultura
geral, entre outras, para tentar perceber o significado de uma peça de
arte ou até perguntar a nós próprios porque se considera arte. No
entanto, se as máquinas começarem a fazer arte como iremos nós
questioná-la.

O que levanta outras sub-questões, se a máquina é criada pelo o humano,
quem é o autor da peça? Conseguimos perceber a consciência artificial?
Como a consciência artificial interpreta a arte em geral? Etc;

**Conceito**
------------

Inspirados na arquitetura dos monumentos portugueses, pretendemos
através da ferramenta p5js, criar uma construção controlada da Torre de
Belém, mais propriamente, da sua planta em 3D.

A maior parte do estilo arquitetónico apresentado nos monumentos em
Portugal enquadra-se no estilo gótico, barroco e manuelino, construções
que são criadas com grande detalhe desde as gárgulas do gótico, à
simetria do barroco.

Com o apoio da ferramenta p5js pretendemos relacionar esta arte
arquitetônica antiga criada pelo ser humano, com a arte criada por um
algoritmo. Esta ponte tem o propósito de demonstrar a capacidade
desumana de gerar arte. Ou seja, apesar da informação que é fornecida à
máquina ser originalmente humana, a máquina tem ainda a capacidade de
inovar e criar uma sua versão com base nesta informação que lhe foi
dada.

De um ponto de vista mais evolutivo, a máquina é detentora de uma
capacidade analítica e tem o poder de decidir com o conhecimento que lhe
foi transmitido, que neste caso é uma planta simplificada da Torre de
Belém no espaço tridimensional (blueprint).

Olhando para um lado mais conceptual deste projeto, juntamos uma
narrativa que remete para a inclusão de uma noção temporal na máquina.
Como se esta construísse várias versões futuras da torre em diferentes
épocas. Assim o público consegue estabelecer uma relação de um ponto de
vista artístico, arquitetónico e generativo com a criação gerada por um
algoritmo inteligente.

O trabalho reflete o conceito na medida em que este se prende na
capacidade de processamento do computador de criar arte a partir da
arquitetura alternando as suas formas. Este procedimento baseia-se na
interpretação que o humano terá depois da criação que a máquina fará.

Este projeto insere-se na parte publicitária do evento PCD@Lisboa21, e
surge como um background interativo.

Como forma de chamar a atenção do utilizador e para que este possa
apreciar a nova forma que a máquina criou, temos como intenção gerar
interatividade com o *scroll* e/ou *touch and drag.* Esta funcionalidade
permite ao utilizador uma rotação sobre a forma 3D, ou seja, sempre que
o utilizador usar o scroll para baixo a forma 3D do edifício irá rodar
para esquerda e a rotação inverte quando usado o scroll para cima.

Esta funcionalidade não está implementada no código pois este não tem
conteúdo suficiente para provocar a ação do scroll.

**StoryBoard**
--------------

Fase 1 - Começa com a torre, em blueprint![](images/image5.png)

Fase 2 - Os vértices da torre começam a mover-se dos seus pontos de origem.![](images/image8.png)

Fase 3 - A torre fica cada vez mais irreconhecível.

![](images/image9.png)

![](images/image2.png)

Fase 5 - A máquina criou outra versão da torre de Belém.

![](images/image1.png)

**MoodBoard**
-------------

[https://www.youtube.com/watch?v=aS5onWz1HjM](https://www.youtube.com/watch?v=aS5onWz1HjM)
13:30

![](images/image3.jpg)

![](images/image4.jpg)

![](images/image7.png)

**Histórico do código/iteração**
--------------------------------

1º versão - Modelo não atualiza

[https://editor.p5js.org/miguelbarros999/sketches/wjRdQbrU8](https://editor.p5js.org/miguelbarros999/sketches/wjRdQbrU8)

2º versão - Modelo já não atualiza graças a um "hack"

[https://editor.p5js.org/dizprofessor/sketches/Zkn6AJVQI](https://editor.p5js.org/dizprofessor/sketches/Zkn6AJVQI)

3º versão - Modelo da torre de belém

[https://editor.p5js.org/miguelbarros999/sketches/hCIc\_Ra6u](https://editor.p5js.org/miguelbarros999/sketches/hCIc_Ra6u)

[https://editor.p5js.org/franciscomp/sketches/l0X8x\_Ag\_](https://editor.p5js.org/franciscomp/sketches/l0X8x\_Ag\_)

Versão Final:

[https://editor.p5js.org/miguelbarros999/sketches/kDHScKNBL](https://editor.p5js.org/miguelbarros999/sketches/kDHScKNBL)

Pergunta no fórum:
[https://discourse.processing.org/t/help-with-3d-object-transfiguration-distortion-effect/28795](https://discourse.processing.org/t/help-with-3d-object-transfiguration-distortion-effect/28795)

**Memória descritiva**

Na fundação deste projeto, começamos por pensar onde e como é que os
algoritmos atuam nos dias que correm, e muito rápido, depois de alguma
pesquisa, vimos que os algoritmos estão presentes em várias máquinas de
inteligência artificial com aplicações em diferentes áreas.

Na tentativa de responder ao briefing que nos foi proposto, visto que
tínhamos que criar um banner ou um background no espaço tridimensional
da ferramenta p5js para o festival PCD@Lisboa21 (Processing Coding
Day), no decorrer do brainstorming percebemos que a arquitetura poderia
ser um bom estudo para desenvolver o conceito e o projeto em si.

Começámos por olhar para a arquitetura portuguesa, e é óbvio que a maior
parte do estilo arquitetónico apresentado nos monumentos em Portugal
enquadra-se no estilo gótico, barroco e manuelino e pensámos que poderia
ser interessante criar uma relação entre estes espaços físicos e
temporais bastante distantes.

Após alguma pesquisa sobre arquitetura criada por algoritmos e sobre a
perceção do homem em relação à arte gerada por máquinas, o nosso projeto
começa a ganhar vida em torno desta frase:

"Se as máquinas fizessem arte, será que os humanos iriam compreender?"

Por esta altura já tínhamos definido o storyboard e o moodboard visual
que iríamos tentar reproduzir no visualizador. O elemento principal do
nosso projeto é a Torre de Belém, um monumento emblemático da cidade de
Lisboa. Este objeto tridimensional criado em software é representado no
visualizador p5js como se fosse uma planta arquitetônica apenas pelas
suas arestas brancas num espaço com um fundo preto.

Com o decorrer do tempo os vértices que seguram a planta mudam a sua
posição até um valor aleatório e consequentemente o aspecto da planta
vai mudar drasticamente criando um objeto de certa forma mais abstrato.
É neste momento que o nosso projeto responde à pergunta "Se as máquinas
fizessem arte, será que os humanos iriam compreender?" A resposta nunca
é a mesma, sendo que depende sempre da avaliação do utilizador.

É a torre de Belém? É uma bota? É uma nave espacial? Afinal é o quê?

Cada um tem conhecimentos e percepções diferentes sobre a arte em
questão, e é este conflito avaliativo que nós enquanto criadores deste
código procuramos gerar, no fundo instaurar a dúvida.

Este projeto foi uma grande aprendizagem para nós criadores, pois
tivemos várias dificuldades e atrocidades que foram resolvidas e
ultrapassadas com apoio de outros programadores e detentores desta
linguagem JavaScript. Nestes momentos de estagnação no decorrer da
criação, partilhamos as nossas dúvidas no fórum do p5js e obtivemos
várias respostas que foram cruciais. Esta participação no fórum também
contribuiu para novas ideias/hacks nesta comunidade, o que é um ponto
bastante positivo presente neste projeto.

**Webgrafia**
-------------

[https://www.architectmagazine.com/aia-architect/aiadesign/aia-design-do-algorithms-make-architecture\_o](https://www.architectmagazine.com/aia-architect/aiadesign/aia-design-do-algorithms-make-architecture_o)

[https://www.architectmagazine.com/technology/can-algorithms-design-buildings\_o](https://www.architectmagazine.com/technology/can-algorithms-design-buildings_o)

[https://theconversation.com/algorithms-are-designing-better-buildings-140302](https://theconversation.com/algorithms-are-designing-better-buildings-140302)

[https://www.cmswire.com/customer-experience/ai-bias-when-algorithms-go-bad/](https://www.cmswire.com/customer-experience/ai-bias-when-algorithms-go-bad/)

[https://arcompany.co/when-algorithms-go-wrong/](https://arcompany.co/when-algorithms-go-wrong/)

[https://mx3d.com/projects/mx3d-bridge/](https://mx3d.com/projects/mx3d-bridge/)

[https://aeon.co/ideas/if-machines-want-to-make-art-will-humans-understand-it](https://aeon.co/ideas/if-machines-want-to-make-art-will-humans-understand-it)

Trabalho realizado por :

Francisco Pereira: 3180576

Gonçalo Barros: 3180743

João Casimiro: 3180477
