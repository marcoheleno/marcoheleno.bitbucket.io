

window.onload = function() 
{
  set_sections_to_window_innerHeight();
  load_and_parse_code();
}


window.addEventListener ("resize", function() 
{
  set_sections_to_window_innerHeight();
});


function set_sections_to_window_innerHeight() 
{
  document.body.style.height = window.innerHeight + "px";
  document.getElementsByTagName("section")[0].style.height = window.innerHeight-50 + "px";
  document.getElementsByTagName("section")[1].style.height = window.innerHeight-50 + "px";
}


function load_and_parse_code () 
{
  fetch("setup_draw_frames.js")

    .then(function (response) 
    {
      response.text().then(function (text_string) 
      {
        //console.log (text_string);
        
        for (let i=0; i<text_string.length; i++) 
        {
          if (text_string[i] === "/" && text_string[i+1] === "*") 
          {
            text_string = text_string.replace("/*", "");
            i-=2;
          }
          if (text_string[i] === "*" && text_string[i+1] === "/") 
          {
            text_string = text_string.replace("*/", "");
            i-=2;
          }
        }
        //console.log (text_string);

        let text_array = text_string.split(/\r?\n/);
        //console.log (text_array);

        document.getElementsByTagName("section")[0].appendChild (document.createElement("code"));

        for (let i=0; i<text_array.length; i++) 
        {
          document.getElementsByTagName("code")[0].appendChild (document.createElement("p"));
          document.getElementsByTagName("p")[i].innerHTML = text_array[i];
        }
      })
    })
    
    .catch(function (err) 
    {
      console.log("Something went wrong!", err);
    });
}


function set_p5_canvas_to (w, h) 
{
  document.getElementsByTagName("section")[1].appendChild(document.getElementById("defaultCanvas0"));

  if (w===undefined || h===undefined) 
  {
    let p5_containerSize = 
    {
        w: document.getElementsByTagName("section")[1].offsetWidth-1,
        h: document.getElementsByTagName("section")[1].offsetHeight-1
    };

    resizeCanvas (p5_containerSize.w, p5_containerSize.h);
  }
}

