

class Grid 
{
  constructor (num_colunas, num_linhas)
  {
    this.colunas = [];
    let coluna_largura = width/num_colunas;
    for (let c=0; c<num_colunas; c++) this.colunas[c] = coluna_largura;
    
    this.linhas = [];
    let linha_altura = height/num_linhas;
    for (let l=0; l<num_linhas; l++) this.linhas[l] = linha_altura;
  }
  
  
  randomizeColumns () 
  {
    let soma_larguras = 0;

    for (let c=0; c<this.colunas.length; c++) 
    {
      let sinal = int( random (1, 3) );

      if (sinal === 1) this.colunas[c] = this.colunas[c]/2;
      else 
      if (sinal === 2) this.colunas[c] = this.colunas[c]*2;

      soma_larguras += this.colunas[c];
    }

    if (soma_larguras > width) 
    {
      let diferenca_tabela_soma = soma_larguras-width;
      let corrigir_diferenca = diferenca_tabela_soma/this.colunas.length;
      for (let c=0; c<this.colunas.length; c++) this.colunas[c] -= corrigir_diferenca;
    } 

    else 
    {
      let diferenca_tabela_soma = width-soma_larguras;
      let corrigir_diferenca = diferenca_tabela_soma/this.colunas.length;
      for (let c=0; c<this.colunas.length; c++) this.colunas[c] += corrigir_diferenca;
    }
  }
  
  
  randomizeLines ()
  {
    let soma_alturas = 0;

    for (let l=0; l<this.linhas.length; l++) 
    {
      let sinal = int( random (1, 3) );

      if (sinal === 1) this.linhas[l] = this.linhas[l]/2;
      else 
      if (sinal === 2) this.linhas[l] = this.linhas[l]*2;

      soma_alturas += this.linhas[l];
    }

    if (soma_alturas > height) 
    {
      let diferenca_tabela_soma = soma_alturas-height;
      let corrigir_diferenca = diferenca_tabela_soma/this.linhas.length;
      for (let l=0; l<this.linhas.length; l++) this.linhas[l] -= corrigir_diferenca;
    } 

    else 
    {
      let diferenca_tabela_soma = height-soma_alturas;
      let corrigir_diferenca = diferenca_tabela_soma/this.linhas.length;
      for (let l=0; l<this.linhas.length; l++) this.linhas[l] += corrigir_diferenca;
    }
  }


  buildGrid () 
  {
    this.tabela = [];
    let x = 0;
    
    for (let c=0; c<this.colunas.length; c++) 
    {
      this.tabela[c] = [];
      let y = 0;
      
      for (let l=0; l<this.linhas.length; l++) 
      {
        this.tabela[c][l] = 
        {
          x:x, 
          y:y, 
          w:this.colunas[c],
          h:this.linhas[l]
        };
        
        y += this.linhas[l];
      }
      
      x += this.colunas[c];
    }
    
    return this.tabela;
  }

  
}
