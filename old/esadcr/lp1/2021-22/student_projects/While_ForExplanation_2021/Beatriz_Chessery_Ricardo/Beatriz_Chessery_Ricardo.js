/*
Grupo 2
Beatriz Dias - 3190656
Chessery Korn - 3190690
Ricardo Rodrigues - 3190773

While || For explanation - Briefing 2
Rippleception


3º ano, Design Gráfico e Multimédia
1º semestre, UC Laboratório de Projeto 
Docente Marco Heleno

ESAD.CR, 2021/2022
Data de avaliação 26/01/2022


Explicação simplificada do ciclo representado:

while (diametro_ellipse_preenchido === 0 && diametro_ellipse_não_preenchido < height || width)

  {
    diametro_ellipse_não_preenchido += valor;
    ellipse_não_preenchido (x, y, diametro_ellipse_não_preenchido, diametro_ellipse_não_preenchido);
  }
*/


//definição das variáveis utilizadas
let diametroInicialOnda, circuloInicio, circuloFim, transparenciaCirculo, transparenciaOnda,  linhasVertical, linhasHorizontal;

//definições da tipografia utilizada no cartaz, após upload no setup
function preload()
{
  tipo = loadFont ("student_projects/While_ForExplanation_2021/Beatriz_Chessery_Ricardo/Kanit-Regular.ttf");
}


//definição do tamanho, tipo de cor (HSB  trabalhar com os valores alfa que permitem opacidade)  e velocidade de atualização da tela
function setup() 
{
  createCanvas(windowWidth, windowHeight);
  colorMode (HSB,255,255,255,100);
  restart();
  frameRate(30)
}

//definição de valores iniciais e finais para um código responsivo com uma grelha perfeita, um elemento introdutório (circulo) e elementos essenciais (ondas) e suas transparências
function restart()
{
  circuloInicio = height;
  circuloFim = 0;
  transparenciaInicialCirculo = 20;
  transparenciaFinalCirculo = 0;
  transparenciaOnda = 50;
  linhasVertical = 20;
  linhasHorizontal = 20;
  transparencia1 = 500;
  transparencia2 = 500;
  transparencia3 = 500;
  transparencia4 = 500;
  transparencia5 = 500;
  tempo = 0;
  
//definição para de diferentes favores para que o ciclo termine e recomece em diversos formatos conforme planeado
  if (width>height)
    {
      diametroOnda = height/2;
    }
  else if (width<height)
    {
      diametroOnda = width/2;
    }
}

function draw() 
  {
//construção da grelha com proporções uniformes 
   let horWidth = (width / linhasVertical);
   let vertHeight = (height / linhasHorizontal);
    
  fill(0);
  stroke(10);
  rect( 0, 0, horWidth, vertHeight );
  
  for ( i = 0; i < linhasHorizontal; i++ )
  {
  for ( j = 0; j < linhasVertical; j++ )
    {
    rect( j * horWidth, i * vertHeight, horWidth, vertHeight);
    }
  }
 
// elemento introdutório (círculo) animado que vai ficando mais pequeno e com opacidade degressiva, acompanhado pelo nome do ciclo escolhido com as mesmas características de tamanho
  noStroke();
  fill(0,0,500,map(circuloInicio, height, 0, transparenciaInicialCirculo, transparenciaFinalCirculo));
  circle(width/2, height/2, circuloInicio);

  if (circuloInicio > circuloFim)
  {
  circuloInicio = circuloInicio-20;
  textWidth(29);
  strokeWeight(1)
  textFont (tipo);
  fill(102,205,170);
  textAlign (CENTER, CENTER);
  textSize(circuloInicio/4);
  text ("WHILE", width/2, height/2.05);
  }
 
    
//5 ondas com tamanho, espaçamento e transparência pré-definidas para cada uma destas surgirem e desaparecerem consoante tempo definido respetivamente
  if (circuloInicio <= circuloFim)
      {
      noFill(0);
      if (tempo>0)
        {
          stroke (0,0,transparencia1);
          circle(width/2, height/2, diametroOnda/8);
          if (tempo>200)
            {
              transparencia1 = transparencia1-10;
            }
        }
      if (tempo>50)
        {
          stroke (0,0,transparencia2);
          circle(width/2, height/2, diametroOnda/4);
           if (tempo>250)
            {
              transparencia2 = transparencia2-10;
            }
        }
      if (tempo>100)
        {
          stroke (0,0,transparencia3);
          circle(width/2, height/2, diametroOnda/2);
          
           if (tempo>350)
            {
              transparencia3 = transparencia3-10;
            }
        }
      if (tempo>200)
        {
          stroke (0,0,transparencia4);
          circle(width/2, height/2, diametroOnda); 
         
           if (tempo>500)
            {
              transparencia4 = transparencia4-10;
            }
        }
      if (tempo>300)
        {
          stroke (0,0,transparencia5);
          circle(width/2, height/2, diametroOnda*2); 
          
           if (tempo>600)
            {
              transparencia5 = transparencia5-10;
            }
        }
        
//quando a transparência da última onda (5ª) chegar a 0, o ciclo recomeça após um período de tempo
        if (transparencia5<=0 && tempo>900)
          {
            restart();
          }
        
//estalecimento do intervalo de tempo entre o surgimento de cada onda 
      tempo = tempo+4;
      }  
}

//função que permite o canvas ser responsivo
function windowResized()
{
  resizeCanvas (windowWidth, windowHeight)
}