# Rippleception

###### Beatriz Dias - 3190656

###### Chessery Korn - 3190690

###### Ricardo Rodrigues - 3190773

### Ciclo - While

Este ciclo de repetição executa uma determinada parte do código
múltiplas vezes enquanto a sua condição for verdadeira (avaliado
previamente antes da sua execução), apesar de não se conhecer a
quantidade exata de iterações. Importa que este loop tenha um final, ou
seja, que eventualmente a condição passe a ser falsa para que não seja
reproduzido infinitas vezes.

### Pesquisa

Tendo por base a pesquisa elaborada, fomos inspirados pela arte
generativa do Nakauchi Kiyoshi e do Dmitri Cherniak para o
desenvolvimento do nosso conceito.

### Conceito

Rippleception consistirá na simulação vista de cima da queda de uma gota
numa superfície líquida e consequentemente criará até a um determinado
momento uma ondulação e posteriormente o processo se inverterá, sendo
que esta ondulação reduzir-se-á gradualmente até terminar e estabilizar
novamente a superfície.

Portanto, desta maneira, iriam sugerir círculos de diâmetro cada vez
maior - por causa da queda de uma gota ativada através do pressionar da
tecla \"espaço\" - até chegarem às margens do *canvas*, assim que tal
acontecesse, outro ciclo de repetição começaria, realizando o efeito
oposto, isto é, diminuindo o tamanho gradual dos círculos até ao centro
do *canvas*. No momento em que deixarem de estar visíveis quaisquer
círculos, significa que a ondulação terminou e a superfície líquida
voltou a estabilizar. Aí cairá outra gota automaticamente, recomeçando
todo o ciclo.

Esta superfície líquida vai ser representada por uma *mesh* de estilo
retro para criar maior dinamismo ao *design*.

### Moodboard

![](README_attachments/media/rId26.jpg){width="5.833333333333333in"
height="4.124159011373578in"}

### Storyboard

![](README_attachments/media/rId30.png){width="5.833333333333333in"
height="8.248017279090114in"}

### Processo

![](README_attachments/media/rId34.jpg){width="5.833333333333333in"
height="3.28125in"}

![](README_attachments/media/rId37.jpg){width="5.833333333333333in"
height="3.28125in"}
