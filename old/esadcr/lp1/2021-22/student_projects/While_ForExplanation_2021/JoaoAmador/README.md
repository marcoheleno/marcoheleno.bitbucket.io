Exercício de avaliação : While \|\| For explanation

Por João Amador

Para este exercício escolhi desenvolver um projeto em p5.js no âmbito do
ciclo *FOR*.

O ciclo *For* funciona através da função *For* que é composta por três
elementos, Inicialização, Condição e Atualização.

Quando o ciclo *For* começa inicia uma variável, que por sua vez é
sujeita a uma condição que poderá ser verdadeira ou não, dependendo da
condição, por exemplo se a condição for \"x \< 3\" e x apresentar
valores menores que 3, então a condição é verdadeira. Isto inicia o
ciclo que cuja leitura agora passa pela Atualização, que por exemplo \"x
= x + 1\", executa um processo de acrescentar 1 valor por cada vez que a
consola ler a condição como verdadeira, quando o valor de x superar 3, o
ciclo termina, e a leitura começa de novo.

![](README_attachments/media/image1.png){width="5.052083333333333in"
height="3.2291666666666665in"}

Através desta função posso definir intervalos de valores para o programa
executar / desenhar na tela, seja o posicionamento das formas
geométricas, o espaço entre si, dimensões ou mesmo velocidade de
execução/desenho (no caso de estar a trabalhar com \"framerate()\").
Neste caso vou trabalhar com \"framerate()\", e por isso vou aplicar
condições para a primeira posição das formas e a última posição.

Para o meu projeto planeio um ciclo aleatório com algum controle
(intervalo de valores para as dimensões e espaçamentos, limite de
velocidade e cores ).

Quero simular um \"loading screen\", inspirado no jogo \"Elden Ring\".
Quero colocar um png de uma luz cintilante como figura central da tela.

A partir disto , criar um ciclo de expansão e diminuição da imagem para
simular um movimento respiratório. Para dar mais profundidade ao objeto
em questão, vou colocar um segundo png exatamente na mesma posição, mas
com dimensões diferentes da mesma luz cintilante, mas com cor preta
sólida, à medida que a luz cresce, a transparência da \"sombra\" irá
diminuir e vice-versa.

![](README_attachments/media/image2.png){width="5.904861111111111in"
height="4.547916666666667in"}

## 

## 

## ![](README_attachments/media/image3.png){width="5.904861111111111in" height="4.547916666666667in"}

## 

## 

## 

## 

## 

## 

## 

## 

## 

## ![](README_attachments/media/image4.png){width="5.904861111111111in" height="4.523611111111111in"}

## 

## 

## 

## 

## 

## 

## 

## 

## 

## 

## 

## 

## Links: 

FORNECIDOS PELO PROFESSOR:

<https://medium.com/@isohale/visualizing-algorithms-precedents-part-1-ce3f230d0329>

<https://bost.ocks.org/mike/algorithms/>

CICLO *FOR* E *WHILE*:

<https://www.youtube.com/watch?v=QdGeb0H5idM>

<https://www.youtube.com/watch?v=cnRD9o6odjk>

INSPIRAÇÕES:

<https://www.youtube.com/watch?v=LO3Awjn_gyU>

<https://www.youtube.com/watch?v=17WoOqgXsRM>

<https://happycoding.io/tutorials/p5js/animation>
