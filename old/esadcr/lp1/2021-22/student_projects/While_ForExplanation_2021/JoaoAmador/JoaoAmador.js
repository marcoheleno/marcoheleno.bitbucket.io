//Briefing WHILE CICLE 3º Momento - Laboratório de Projeto - João Amador//

//definir valor para dimensões de elipses//
let circ_size = 10;

function setup() {
  createCanvas(windowWidth, windowHeight);

  colorMode(RGB, 255, 255, 255, 100);

  frameRate(20);
}

function draw() {
  //valor de coordenada x para elipse//
  let x = 100;
  //ciclo while, com condição para crescer elipses e mudar a respetiva cor//
  while (x <= width) {
    noStroke();
    fill(random(0, 255));
    var w = 200;
    let h = height;
    ellipse(x, height / 2, circ_size, circ_size);
    x = x + 200;
  }
  //se o tamanho das elipses for maior que 100 por 100px , passam a 0//
  circ_size += 6;

  if (circ_size > 100) {
    circ_size = 0;
  }
}

function windowResized() {
  resizeCanvas(windowWidth, windowHeight);
}
