// Pedro Rosa
// Nº3190419
// Laboratório de Projeto 1
// 3ºano, 2021/2022
// 1ºsemestre
// Design Gráfico e Multimédia
// Esad.cr
// Marco Heleno
// 26 Janeiro 2022


// While || For Explanation


//Para a resolução deste exercício decidi representar o while;
//Deste modo comecei por criar as variaveis;
let x, x1, x2, y, y1, y2, lx, lx2, ly, ly2, nl, lh, ls;

function setup()
{
  createCanvas (windowWidth, windowHeight);
  frameRate(1);

  y1 = 45;
  y2 = height - 75;
  nl = 20;
  lh = 40;
 
  //No setup, coloquei o valor das variaveis que ficaram estáveis e que não se iriam alterar;
}
function draw()
{
  //No draw comecei por colocar o valor das duas variaveis que iram estar constantemente a alterar o seu valor e, de seguida, construi o background utilizando linhas criadas a partir de um While como tinhamos feito durante as aulas;
  x1 = random (40, width - 40);
  x2 = random (40, width - 40);
  background(0);
  strokeWeight(2);
  stroke(255);
  line(width, height/2, 0, height/2);
  
  ls = width/nl;

  lx = ls;
  
  lx2 = lx;

  ly = 0;
  
  ly2 = height ;



  while (lx < width)

  {

    line(lx, ly-lh/2, lx, ly+lh/2);

    lx += ls;

  }
  
    while (lx2 < width)

  {

    line(lx2, ly2-lh/2, lx2, ly2+lh/2);

    lx2 += ls;

  }
  
  //Após o background estar resolvido criei os dois rectangulos que iriam estar em constante movimento em width mas com height constante;
  
  noStroke();
  fill(255);
  rect(x1, y1, 150, 20);
  rect(x2, y2, 150, 20);
  
  //De seguida, visto que já teria os dois objetos que eram necessários, criei a representação do while presente no meu código. Com o lerp, os valores da minha ellipse estaram constantemente a alterar enquanto o mesmo nao alcançar o seu target. Neste caso, o target que foi defenido foram as variaveis x2 e y2, pertencentes ao retangulo de baixo, e o ponto de partida para a ellipse alcançar esse objeto é o retangulo de cima com as variaveis x1 e y1 ;
  
  fill(255);
  y = lerp(y1, y2, random(0.2, 0.87));
  x = lerp(x1, x2, random(0.2, 0.87));
  ellipse(x, y, 30, 30);
  
  //Deste modo, a representação do While presente neste código será que enquanto os retangulos se movem constantemente, as coordenadas da ellipse são definidas;
  
  if (y <=20)
    { 
    x2 = x;
  
    }else
    {
      if (y <= height - 80)
      {
      x1 = x;
      }
    }
  //Por fim, coloquei um if para que sempre que a ellipse se aproximar do mesmo height que as barras, estas reagirem e ir para o mesmo width que a ellipse se encontra.
}
function windowResized()

{

  resizeCanvas (windowWidth, windowHeight);

}