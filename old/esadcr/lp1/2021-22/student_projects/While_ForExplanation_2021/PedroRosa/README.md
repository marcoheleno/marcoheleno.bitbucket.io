# While explanation

Pedro Rosa nº3190419

Ciclo escolhido : While

O \"while\" é um ciclo loop que ao analisar um valor irá repeti-lo
constantemente.

Tendo isto em conta a minha ideia para este projeto será utilizar o
ciclo \"While\" para criar um loop de ellipses com posições, tamanhos e
cores random no ecrã.

StoryBoard:

![](README_attachments/media/rId20.png){width="5.833333333333333in"
height="2.99708552055993in"}

1º = seria o inicio do loop com o objeto principal.

2º = o inicio do loop.

3º = uma ideia final de como seria o loop depois de iniciado.

Moodboard

![](README_attachments/media/rId23.jpg){width="5.833333333333333in"
height="3.9326541994750657in"}

![](README_attachments/media/rId26.jpg){width="5.833333333333333in"
height="4.317041776027996in"}
