**CICLO DE REPETIÇÃO \"FOR\"**

O ciclo de repetição \"For\" foi escolhido pelo nosso grupo, devido a
ação ser aplicada ao final de toda a iteração e pela a sua utilização
ser benéfica, esta é utilizada de um ponto a outro onde se sabe o inicio
e o final, ou seja esta terá os loops contados, enquanto que o while é
um tipo de loop sem fim, ou seja tem inicio mas não tem um final, o
grupo optou por enquadrar mais no ciclo de repetição For, por esta ser
um tipo de loop com um final definido. É do interesse do grupo dar
ênfase a este tipo de ciclo de repetição através de um relatório
explicativo estipulado nesta unidade curricular.

A variável \"For\" manifesta-se por várias estruturas de repetição em
loop, quando um ou vários comandos necessitam de repetir diversas vezes
enquanto a condição é verdadeira ou até se um comando precisa de ser
solicitado a parar. Existem diversas situações em que esta estrutura
pode ser aplicada, como o loop for/in e o for/of.

O funcionamento desta linguagem de programação em javascript facilita o
desenvolvimento do programa e a sua manutenção dos códigos, pois este
disponibiliza funções que quando são interpretadas pelo navegador,
executam uma repetição do comando ou de um conjunto de indicações por
determinadas vezes. Geralmente o loop é utilizado para repetições de
estruturas, em que são adicionados ou eliminados valores de um índice.
Por isso, algumas das suas aplicações servem para a leitura de
variáveis. For, em português significa "para", determina quando uma ação
deve de ser executada a partir de uma condição inicial, até que esta
encontre outra que interrompa a ligação.
![img](README_attachments/media/image1.jpg "fig:"){width="5.833333333333333in"
height="5.876180008748906in"}

O comando for permite que um certo código seja executado um determinado
número de vezes. O loop compõe-se com três expressões de código, a
inicial, que corresponde à declaração ou à atribuição da variável do
qual o valor é utilizado para inicializar; o teste, que expressa a
constatação se a condição for verdadeira, para executar o código; o
incremento ou decremento, que altera a variável utilizada na primeira
expressão, O código dentro do corpo do loop é executado entre a
avaliação da segunda e da terceira expressão. Estas serão executadas
enquanto a condição for verdadeira. As expressões são utilizadas com um
ponto e vírgula no final, é importante garantir que o loop possa "sair"
ou que a condição acabe sendo avaliada como falsa.

A forma do comando for é a seguinte:\
`"for (``comandos`` de ``inicialização``; ``condição`` de teste; ``incremento``/``decremento``)`

`{`

`// ``comandos`` a ``serem`` ``repetidos`

`// ``comandos`` a ``serem`` ``repetidos`

`}`

`// ``comandos`` ``após`` o 'for'`

A expressão inicial, o teste e o decremento ou incremento não são
obrigatórias, significa que é possível utilizar apenas uma ou todas.
Contudo necessita atenção no código de modo a evitar que a estrutura se
torne uma condição sem saída.\
Este executa os comandos de inicialização,Testa a condição, Se a
condição for falsa então executa o comando que está logo após o bloco
subordinado ao for. Se condição for verdadeira então executa os comandos
que estão subordinados ao for, executa os comandos de
incremento/decremento e depois Volta ao passo 2.

O comando for deve ser utilizado sempre que soubermos exatamente quantas
vezes o laço deve ser repetido, o teste deva ser feito antes da execução
de um bloco de comandos ou se houver casos em que o loop não deva ser
repetido nenhuma vez.

**CONCEITO**\
O nosso conceito para este trabalho é explicar passo a passo a
funcionalidade do for como se fosse explicado a crianças, inicialmente
explicamos quando o for é utilizado, quais são as condições inerentes e
no final mostramos um caso real explicado passo a passo.

Agora que sabemos como funciona a variável For, podemos exemplificar
através de um caso prático, vamos pedir ao computador que repita 7 vezes
o código\
Imaginemos que queremos escrever 7 vezes no painel de código, neste caso
o "P5.JS" digitamos

`function setup() {`

`createCanvas``(``windowWidth``, ``windowHeight``); ``<!--``//``criação`` do ``quadro``-->`

`for (let ``i``=0;<7; ``i``++);`

`console.log(“p5.js”) <!--// ``nestas`` ``duas`` ``linha`` ``teremos`` ``três`` ``etapas`` ``feitas``, a ``primeria`` “let ``i``=0” é a ``fase`` de ``inicio``, de ``seguida`` “<7” ``testará`` a ``condição`` e ``finalmente`` “``i``++); console.log” se a ``condição`` for ``verdadeira`` ``irá`` ``executará`` ``os`` ``comandos``, que ``estão`` ``subordinados`` ``ao`` for`\--\>

\<!\--`//Este ``resultado`` agora ``irá`` se ``repetir`` 7 ``vezes``-->`

`function ``windowResized``()`

`{`

`resizeCanvas`` (``windowWidth``, ``windowHeight``);`

`} `\<!\--`// o function resized canvas ``permite-nos`` ``ajustar`` o ``quadro`` do ``resultado`` do ``código`` ``independentemente`` do ``tamanho``.``-->`

**STORYBOARD**

![](README_attachments/media/image2.png){width="5.833333333333333in"
height="4.065656167979003in"}

![](README_attachments/media/image3.png "fig:"){width="5.833333333333333in"
height="4.003521434820647in"}![](README_attachments/media/image4.png "fig:"){width="5.833333333333333in"
height="4.051467629046369in"}![](README_attachments/media/image5.png "fig:"){width="5.833333333333333in"
height="2.161457786526684in"}

**MOODBOARD**

<https://app.milanote.com/1MDSnC1nNsn72u/for-explanation?p=T0yIFwIhj4O>

**Tecnologias utilizadas**

Adobe Illustrator

Milanote

Pinterest

Typora

Google Docs

Google Slide

Google Search

P5JS
