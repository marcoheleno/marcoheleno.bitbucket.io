 /*--FOR EXPLANATION // IPLEIRIA - ESCOLA SUPERIOR DE ARTES E DESIGN - CALDAS DA RAÍNHA // 3º ANO // 2021-22 // 1ºSEMESTRE // 3ºMOMENTO DE AVALIAÇÃO // 26 DE JANEIRO 2022 // // LABORATÓRIO DE PROJETO I // DESIGN GRAFICO E MULTIMÉDIA // DOCENTE: MARCO HELENO // CLÁUDIA AGOSTINHO, CRISTIANA SANTOS, MARISA BENTO --*/




//definição do tamanho e posição dos circulos
let X, Y, R, RT, R2, T, S;
//definição do limite de ellipsses formadas (3,10)
let iterador, limite;
//reiniciar o sistema de novo
let iniciar_sistema;
//distância entre as ellipses
let DISTANCIA;
// definirção das cores
let cores;
//definição da imagem começar e continuar 
let img, continuar;
//definição da fonte para o título
let font;



  //inserir imagens(começar e continuar) e fonte
  function preload() {
  img = loadImage('student_projects/While_ForExplanation_2021/Claudia_Cristiana_Marisa/start.png');
  continuar = loadImage('student_projects/While_ForExplanation_2021/Claudia_Cristiana_Marisa/continuar.png');
  font = loadFont ("student_projects/While_ForExplanation_2021/Claudia_Cristiana_Marisa/BebasNeue-Regular.ttf");
  }

  // criar o quadro
  // função que reinicia o código
  function setup() 
  {
  frameRate(60); 
  createCanvas (windowWidth, windowHeight);
  reiniciarSistema(); 
  }

  function draw() 
  {
  //desenhar a ellipse (tamanho, posição e distância entre ellipses)
  frameRate(60); 
  background (255);
  ellipseMode(CENTER);
  //definir a posição da ellipse(largura)
  X = width/2;
  //definir a posição da ellipse(altura)  
  Y = height/2;
  //definir tamanho das imagens de começar e continuar
  RT= height/5;
  //tamanho do texto FOR
  T=width/20;
  //distâncias entre ellipse (quando são criadas)
  DISTANCIA= width/17;
  //sem preenchimento nas ellipse que são formadas aleatoriamente
  noFill();
  //tamanho do stroke da ellipses
  strokeWeight(2);
    

  //quando a largura for maior que a altura
  if(width>height)
  {
    //definir o tamanho das ellipses
    R=height/2;
    //definir o tamanho da imagem dentro do círculo
    RT=height/5;
    //definir o tamanho da fonte
    T=width/20;
    //definir o tamanho do contorno
    S=strokeWeight(2);
    
  }
  //quando a altura for maior que a largura
  else if(width<height)
  {
    //definir o tamanho das ellipses
    R=width/2;
    //definir o tamanho da imagem dentro do círculo
    RT=width/5;
    //definir o tamanho da fonte
    T=height/20;
    //definir o tamanho do contorno
    S=strokeWeight(1);
  }
  
  
  //quando o iterador é menor que o limite (ou seja <10)
  //acrescenta uma quantidade de circulos random a uma distância de width/17
  //ellipse a cinza claro
  let i=0;
  while (i<limite && iniciar_sistema===false)
  {
    //cor do stroke (cinza claro)
    stroke (240);
    ellipse (X,Y,R+(i*DISTANCIA),R);
    i++;
    
  }
  
  //quando o iterador é menor que o limite (ou seja <10)
  //vai criando o stroke nas linhas à medida que se clica no botão
  //ellipse a random cores
  
  for (let i=0; i<iterador; i++) 
  {
    stroke (cores);
    ellipse (X, Y, R+(i*DISTANCIA),R);
  }
  
  //circulo principal 
  //sem contorno
  //com preenchimento a random cores
  noStroke();
  fill(cores);
  ellipse(X,Y,R, R);
  
  //quando chega ao limite o sistema reinicia automaticamente
  //volta à imagem do play
  if (iniciar_sistema === true) 
  { 
    reiniciarSistema(); 
    imageMode(CENTER);
    image(img,X, Y, RT, RT);
    //triangle(x1, y1, x2, y2, x3, y3)
  }
  
  //se não chegar ao limite 
  //aparece a imagem continuar
  else {
  image(continuar,X, Y, RT, RT);
  }

  }

    //definir quando o circulo é presionado
    //definição da área do círculo

  function mousePressed()
  {
   distance = dist (mouseX, mouseY, X, Y);

  if (distance<=R/2 && mouseIsPressed===true)  
    {
      frameRate(60); 
      iniciar_sistema = false;
    }
  
  //definir o botão
  //quando é precionado e se o iterador for menor que o limite então adiciona mais uma ellipses, até a afirmação ser falsa
  if (distance<=R/2 && iterador < limite && iniciar_sistema === false) 
  {
    iterador++;
  }

  //quando é precionado e se o iterador for maior ou igual ao limite então reinicia o sistema
  if (distance<=R/2 && iterador >= limite ) 
  {
  iniciar_sistema = true; 
  }
  }

  //função para reiniciar o sistema
  function reiniciarSistema()
  {
  //definição da fonte, localização e tamanho do For
  textFont(font);
  textSize(T);
  textAlign(CENTER);
  text('for', width/2, height/1.1);
    
  //definição do limite das ellipses e das cores
  iterador = 0;
  limite = int(random(3, 10));
  iniciar_sistema = true;
  frameRate(1); 
  
  //cores da ellipse
  cores = color(random(241, 177, 183), random(186, 146, 147),random(243, 210, 213));
  }

  //reajuste da janela
  function windowResized() 
  {
  resizeCanvas (windowWidth, windowHeight)
  }


