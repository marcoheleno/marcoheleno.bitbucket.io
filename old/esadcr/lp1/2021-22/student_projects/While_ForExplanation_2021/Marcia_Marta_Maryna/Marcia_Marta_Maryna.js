// While or For explanation - Final
// Unidade Orgânica: Escola Superior de Artes e Design
// Curso: Lic. Design Gráfico e Multimédia
// Ano Letivo: 2021/2022
// Ano Curricular: 3º ano | 1º semestre
// Unidade Curricular: Laboratório de Projeto 1
// Docente: Marco Heleno
// Estudantes: Grupo 1 - Márcia Pinto (3190757) | Maryna Dmytruk (3190692) | Marta Santos (3180248)
// Data de Avaliação: 26 Janeiro 2022

//---------------------- VARIAVEIS --------------------
let coluna; // variavel dividir a canva por colunas para distribuir melhor cada tubo
let linha; // variavel para dividir a canva por linhas

let bolas = []; // array para guardar a posição x e y das bolas a cada iteração da função draw
let contadorBolas = 0; // variavel para contar o total de bolas geradas
let tamanhoBola = 0; //variavel para determinar o tamanho da bola
let corBola; //variavel para determinar a cor da bola
let tamanhoVerticalUsadoPelasBolas = 0; // variavel para guardar o tamanho do tubo usado pelas bolas
let posicaoXBola = 0; //variavel para a coordenada x da bol
let larguraTubo = 0; // calcular a largura do tubo
let posicaoYFinalBola = 0; //variavel para a coordenada y final da bola

//variaveis para a bola vermelha
let movimentoYAtual = 0;
let bolaVermelha;
let bolaVermelhaExiste = false;


function setup() {
  createCanvas(windowWidth, windowHeight);
  colorMode(HSB);
  
  //Texto 
  fill(0);
  textSize(12);
  textFont('Montserrat');
  text('FÓRMULA MÁGICA DO CICLO WHILE', width/2, height/2);
  
  background(234, 3, 100);//cor do fundo
  frameRate(40);
  coluna = width/10; // definir que vão ser 10 colunas
  linha = height/3; // definir  que vão ser 3 linhas

  
  //Criar o tubo
  strokeWeight(4);
  line(coluna*4 , linha , coluna*4 , height-50);
  line(coluna*5+coluna , linha , coluna*5+coluna , height-50);

  line(coluna*5+coluna-60 , linha+100 , coluna*5+coluna , linha+100);
  line(coluna*5+coluna-80 , linha+140 , coluna*5+coluna , linha+140);
  line(coluna*5+coluna-60 , linha+180 , coluna*5+coluna , linha+180);
  line(coluna*5+coluna-30 , linha+220 , coluna*5+coluna , linha+220);
  line(coluna*5+coluna-50 , linha+260 , coluna*5+coluna , linha+260);
  larguraTubo = coluna*5+coluna - coluna*4;
  noFill();
  curve(coluna*4 , height-300, coluna*4 , height-50,coluna*5+coluna , height-50, coluna*5+coluna , height - 300);
    
  
  
}



function draw() {
 
  background(234, 3, 100);
  
      //Caso a bola vermelha exista vai chamar a função para desenhar e calcular a posição dela
        if(bolaVermelhaExiste){
          DesenhaBolaVermelha();
        }
  
  // enquanto houver bolas anteriores, vai voltar a desenha-las, porque no final da função draw a bola desenhada desaparece
        for(let i = 0; i < contadorBolas; i++){
        if(bolas[i].parada){
            noStroke();
            fill(bolas[i].cor);
            ellipse(bolas[i].x, bolas[i].y ,bolas[i].diametro, bolas[i].diametro);
          }
        else{
           //Caso a bola atual já pode realizar o movimento
           if(bolas[i].iniciouMovimento === true){
              //"Apaga" do ecrã a ultima posição da bola de forma a não ter um rasto  
             bolas[i].y += 5;
              //noStroke();
              stroke(234, 3, 100);
              fill(bolas[i].cor);
              ellipse(bolas[i].x, bolas[i].y ,bolas[i].diametro, bolas[i].diametro);

              //Verifica se a bola já chegou ou ultrapassou a sua posição final no Y e coloca como parada 
              if(bolas[i].y >= bolas[i].posicaoYFinal){
                bolas[i].parada = true;
              }
              //Verifica se a bola atual já percorreu pelo menos um terço do caminho até à sua posição final para permitir a proxima bola descer até à ultima bola do array
              else if(bolas[i].y >= bolas[i].posicaoYFinal /3 && i != contadorBolas - 1){
                bolas[i + 1].iniciouMovimento = true;
            }
           } 
        }
    }
    
    //Vai gerar novas bolas enquanto não encher a altura o tubo
    if(tamanhoVerticalUsadoPelasBolas < height - linha - 50){
      
      //Obtem a cor da bola aleatoriamente
      corBola = color(232,int(random(20,80)),int(random(80,100)));

      let tentativas = 0; //contador
      tamanhoBola = int(random(25, larguraTubo / 3)); //Gera tamanho das bolas 
      
      //Enquanto o tamanho da bola mais o tamanho total das bolas anteriores ultrapassar a altura total do tubo
     
      //De modo a impedir que entre em ciclo infinito devido aos valores de diametros possiveis das bolas poder ultrapassar a altura do tubo. Esta condição while apenas executará no máximo 10 vezes para cada bola gerada
      while(tentativas <= 10 && tamanhoBola + tamanhoVerticalUsadoPelasBolas > height - linha)
      {
        tamanhoBola = int(random(40, larguraTubo/3));
        tentativas++;
      }
      
      //Obtem a posição X da bola aleatoriamente
      posicaoXBola = int(random(width/2 - width/15, width/2 + width/15));
      
      //Soma o tamanho usado pelas bolas geradas 
      tamanhoVerticalUsadoPelasBolas += tamanhoBola;
      
      let primeiraBola = false;
      
      //Caso seja a primeira bola gerada terá um limite máximo no Y definido para não ultrapassar o fundo do tubo e poder começar a movimentar. Caso contrário a posição máxima do Y será a altura da janela menos o tamanho total Y das bolas já geradas
      if(contadorBolas > 0){
          posicaoYFinalBola = height - tamanhoVerticalUsadoPelasBolas;
      }
      else
      {
         posicaoYFinalBola = height - 25 - tamanhoVerticalUsadoPelasBolas;
         primeiraBola = true;
      
      }
      
      movimentoYAtual = 50;
      
      //Cria uma bola vermelha caso não tenha sido gerada e posição final e Y da bola azul for meter que metade da altura do ecrã 
      if(posicaoYFinalBola <= height/2 && !bolaVermelhaExiste){
        bolaVermelhaExiste = true;
        bolaVermelha = {
          x: width/2,
          y: movimentoYAtual,
          cor: color(0,100,70),
          diametro: 30
        }
      }
          

      bolas[contadorBolas] = {
        x: posicaoXBola, //Posição do X
        y: movimentoYAtual, //Primeira posição do Y em que a bola será gerada
        cor: corBola, //Cor atribuida à bola
        diametro: tamanhoBola, //Diametro atribuido à bola
        parada: false, //As bolas geradas nunca começam na sua posição final logo será sempre falso
        posicaoYFinal: posicaoYFinalBola, //Posição final da bola em Y
        iniciouMovimento: primeiraBola //Se esta bola já iniciou o seu movimento 
      };
      contadorBolas++;
    }
  
   //chamar funçao para desenhar o tubo outra vez
    DesenhaTubo();
}

function DesenhaTubo(){
  //Texto 
  fill(0);
  textSize(25);
  textAlign(CENTER);
  text('FÓRMULA MÁGICA DO CICLO WHILE', width/2, 40);
  
  
  strokeWeight(5);
  stroke(0);
  line(coluna*4 , linha , coluna*4 , height-50);
  line(coluna*5+coluna , linha , coluna*5+coluna , height-50);
  
  larguraTubo = coluna*5+coluna - coluna*4;
  
  //Caso a primeira bola tenha chegado ao final preenche a parte inferior do tubo 
  if(!bolas[0].parada) {
    noFill();    
  }
  
  //Vai buscar a cor e a posição do Y da ultima bola que chegou à sua posição final/parada
  //Esta cor será a cor que representa o liquido/conteudo do tubo
  let altura = 0;
  let corUltimabola;
  for(let i = 0; i < contadorBolas; i++){
    if(bolas[i].parada){
      altura = bolas[i].y - bolas[i].diametro / 2;
      corUltimaBola = bolas[i].cor;
      
    }
  }
  
  //Caso tenha alguma bola parada vai começar a cria um rectângulo para preencher o interior do tubo
  //A altura do tubo vai ser a distância entre a altura do ecrã - 50 e a posição Y da ultima bola parada (variavel altura)
  //De forma as margens do "conteudo" do tubo não sobrepor as margens do tubo é feita a soma de 2 na posição X do conteudo e subtração de 5 na largura
  if(altura > 0){
    noStroke();
    fill(corUltimaBola);
    //Caso a altura do liquido ultrapasse a altura do tubo colocamos como quantidade máxima do liquido a altura do tubo 
    if(height - 50 - altura > height - 50 - linha){
      //Apagamos a ultima bola de forma a não ficar por cima do nível do liquido
      fill(234, 3, 100);
      let ultimaBola = bolas[contadorBolas - 1];
      ellipse(ultimaBola.x, ultimaBola.y , ultimaBola.diametro,       ultimaBola.diametro);
      
      fill(corUltimaBola);
      rect(coluna*4 + 2, linha, larguraTubo - 5, height - 50 - linha);
      
    }
    else {
      rect(coluna*4 + 2, altura, larguraTubo - 5, height - 50 - altura); 
    }
    
  }
   
  //Cria o fundo arrendondado do tubo
   strokeWeight(5);
   stroke(0);
   curve(coluna*4 , height-300, coluna*4 , height-50,coluna*5+coluna , height-50, coluna*5+coluna , height - 300);
  
   //Cria as linhas horizontais de detalhe do tubo
   line(coluna*5+coluna-60 , linha+100 , coluna*5+coluna , linha+100);
   line(coluna*5+coluna-80 , linha+140 , coluna*5+coluna , linha+140);
   line(coluna*5+coluna-60 , linha+180 , coluna*5+coluna , linha+180);
   line(coluna*5+coluna-30 , linha+220 , coluna*5+coluna , linha+220);
   line(coluna*5+coluna-50 , linha+260 , coluna*5+coluna , linha+260);

}



function DesenhaBolaVermelha(){
  
  //Se a bola ainda não chegou ao fundo continua a calcular a posição seguinte
  if(bolaVermelha.y < height - 50){
    
    //Se a bola não chegou perto do inicio do tubo continua a descer
    if(bolaVermelha.y < linha - 50){
      bolaVermelha.y += 5;
    }
    else{
      //Caso a bola já tenha chegado perto do tubo anda para o lado esquerdo até atingir a posição em X da condição
      if(bolaVermelha.x > coluna*4 - 50){
        bolaVermelha.x -= 5;
      }
      else{
        //Em ultimo movimento continua a descer até ao fundo do tubo
        if(bolaVermelha.y < height - 50){
          bolaVermelha.y += 5;
        }
      }
    }
    
    //Apenas faz o draw enquanto estiver em movimento
    noStroke();
    fill(bolaVermelha.cor);
    ellipse(bolaVermelha.x, bolaVermelha.y ,bolaVermelha.diametro, bolaVermelha.diametro);
    
  }
  
}

//Resposive window function
function windowResized() {
   resizeCanvas(windowWidth, windowHeight);
}
