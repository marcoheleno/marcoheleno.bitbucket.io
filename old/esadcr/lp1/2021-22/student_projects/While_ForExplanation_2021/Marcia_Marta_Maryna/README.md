# While \|\| For explanation

Unidade Orgânica: Escola Superior de Artes e Design

Curso: Lic. Design Gráfico e Multimédia\
Ano Letivo: 2021/2022\
Ano Curricular: 3º ano \| 1º semestre\
Unidade Curricular: Laboratório de Projeto 1\
Docente: Marco Heleno\
Estudantes: Grupo 1 - Márcia Pinto (3190757) \| Maryna Dmytruk (3190692)
\| Marta Santos (3180248)\
Data de Avaliação: 15 Dezembro 2021

## Escolha do Ciclo

~~Com base na aprendizagem em aula o grupo escolheu o ciclo for uma vez
que este ciclo permite aumentar automaticamente o valor da iteração, ao
contrario do while, e também é mais simples em casos em que é necessário
percorrer arrays ou seja é o ciclo mais versátil que o ciclo while.~~

Dado a ultima etapa e em discussão com o professor apercebemos-nos que o
conceito que estávamos a desenvolver aplicava-se melhor para o ciclo
while e não para o ciclo for, por essa razão passamos a escolher o ciclo
while.

## Pesquisa 

Na tabela seguinte identificamos as principais diferenças e utilizações
do while e for para servir de inspiração para o desenvolvimento do
conceito do exercício.

  -----------------------------------------------------------------------
  FOR                                 WHILE
  ----------------------------------- -----------------------------------
  Repete até chegar ao valor de       Repete até a condição ser falsa
  condição                            

  Comparação com o valor de condição  Satisfaz uma condição ( True \|\|
                                      False)

  Numero de iterações é conhecido     Numero de iterações não é conhecido

  Exemplo : Imprimir valores de um    Exemplo: Contar palavras de uma
  array                               frase

  for (var i = 0; i \< array; i++)    while (condição)
  -----------------------------------------------------------------------

## MoodBoard 

![](README_attachments/media/rId22.png){width="5.833333333333333in"
height="5.684332895888014in"}

## Conceito Desenvolvido

Com base nas nossas pesquisas e nas referencias visuais desenvolvemos
alguns esboços de possíveis ideias para o desafio do exercício.

![](README_attachments/media/rId26.png){width="5.833333333333333in"
height="3.651294838145232in"}

A intensão do grupo desde o inicio do exercício foi pensar numa ideia
simples e clara, que possa ser interpretada facilmente por qualquer
pessoa. A ideia passa por criar círculos com diversas cores em que
elementos que sejam da mesma cor formam conjuntos, desta forma podemos
representar um array com círculos de varias cores e que consoante o
numero de círculos , estes vão sendo agrupados por cor até não haver
mais círculos. Este será o conceito base para este exercício, contudo
esteticamente ainda vai sofrer alterações consoante o desenvolvimento da
próximas etapas.

## StoryBoard

![](README_attachments/media/rId30.png){width="5.833333333333333in"
height="3.8457425634295714in"}

![](README_attachments/media/rId33.png){width="5.833333333333333in"
height="3.8457425634295714in"}

## Tecnologias Usadas

Para este projeto utilizamos o editor de texto Atom visto que tem mais
funcionalidades que o editor do p5 como por exemplo o autocomplete.

Atom - <https://atom.io/>
