/*
   Exercício de Avaliação While || For Explanation: While Ellipses;
   Realizador por Daniele Miranda - 3190409 e Cláudia Alves - 3190416;
   3º ano letivio 2021/2022 - 5º semestre;
   UC Laboratório de Projeto 1;
   Licenciatura em Design Gráfico e Multimédia (Multimédia) ESAD.CR;
   Docente Marco Heleno;
   Avaliação final 26 de janeiro 2022;
   
   
   While - Desenhar elipses enquanto a composição movimenta-se;
 
while(composicaoEmMovimento() && posicaoDaComposicaoNoTopo()) {
	desenharElipses()
}

*/
 


    
 //Variáveis globais
 let corDaPrimeiraElipse = 255 //primeira elipse ser branca;
 let movimentoVerticalDasElipses = 150 //movimento vertical da composição;
 let contadorDeElipsesDesenhadas = 0 //última elipse ser preta;
 let numeroDeElipses = 15 //quantidade de elipses desenhadas;
 let listaDeCores = []; //lista para atribuir uma cor especifica a cada elipse;
 let minhaFonte; //fonte personalizada;


 //carregar a fonte selecionada;
function preload() {
  minhaFonte = loadFont("student_projects/While_ForExplanation_2021/Claudia_Daniele/GraphikRegular.otf");
}


function setup() {
  
  createCanvas(windowWidth, windowHeight,WEBGL); //canvas responsivo e com renderização 3D, introduz a terceira dimensão Z;
  angleMode(DEGREES); //modo de angulo em graus;
  
  
  let valorDeSubtracao = corDaPrimeiraElipse / numeroDeElipses //calculo para atribuir o valor da cor de cada elipse;
  
  let corFinal = corDaPrimeiraElipse; //atribui o valor da variável corDaPrimeiraElipse para iniciar em 255;
  
  //este laço de repetição serve para identificar o valor da cor de cada elipse que será desenhada (de branco ao preto / 255 - 0);
  for (var i = 0; i < numeroDeElipses; i++)
  {
    corFinal -= valorDeSubtracao;
    listaDeCores[i] = corFinal; //atibui uma cor a cada elipse;
  }
}

function draw()
{ 
  background(0); //background preto;
  rotateX(60); //rotação de 60 graus;
  
  
  fill(255); //da letra;
  textFont(minhaFonte); //fonte personalizada;
  textSize(26); //tamanho da fonte
  text('While', -width/2,-height/5); //texto e posicionamento;
  
  fill(0); //das elipses
  
  for (var i = 0; i < contadorDeElipsesDesenhadas; i++) //desenha as elipses uma a uma;
  {
    
    stroke(listaDeCores[i]); //Utiliza a lista para obter o valor da cor da elipse;
    
    //desenha as elipses;
    beginShape()
    for ( var j = 0; j < 360; j+=10)
    {
      var rad = i * 10
      let x = rad * cos(j)
      let y = rad * sin(j)
      let z = sin (frameCount) * 150
      movimentoVerticalDasElipses = z //movimento vertical das elipses;
      vertex(x,y,z)
    }
    endShape(CLOSE);
  }
  
  //Condições para incrementar o contador de elipses;
  if (movimentoVerticalDasElipses === 150 && contadorDeElipsesDesenhadas < numeroDeElipses) {
    contadorDeElipsesDesenhadas++
  } else if (contadorDeElipsesDesenhadas === numeroDeElipses) { //Se já foram desenhadas todas as elipses, para movimento (noLoop);
    noLoop();
  }
}
  
function windowResized()
{
  resizeCanvas(windowWidth, windowHeight);
}