## Cycles: While / For

##### Relatório de Projeto Cycles: While / For --- Laboratório de Projeto I

Cláudia Alves 3190416 \| Daniele Miranda 3190409

#### Conceito

Para a elaboração deste projeto, decidimos escolher o ciclo de repetição
For. De modo a exemplificar a forma como este ciclo funciona, resolvemos
utilizar elementos gráficos, mantendo as referências naturais utilizadas
no exercício 1. Inicialmente será apresentada uma elipse a fazer um
movimento vertical que, por cada vez que chegar ao ponto mais alto do
movimento, verá um acrescimo de uma elipse a acompanhar os seus
movimentos contínuos, num limite máximo de 10 elipses, marcando o
encerramento do ciclo.

#### Moodboard

![](README_attachments/media/rId22.png){width="5.833333333333333in"
height="1.201715879265092in"}

#### Pesquisa

\"Ciclos são utilizados em JavaScript para realizar tarefas baseadas
numa condição. Condições são normalmente categorizadas de verdadeiras ou
falsas após análise. Um ciclo irá continuar a operar até que a condição
seja definida como falsa.\"

\"O ciclo While começa por avaliar a condição. Se esta for verdadeira,
as tarefas são executadas, se a condição for falsa, a tarefa não será
executada e, após essa definição, o ciclo terminará.\"

***\"JavaScript Loops Explained: For Loop, While Loop, Do\...while Loop,
and More\"** in FreeCodeCamp, 2021*

Tecnologias: P5js

#### Storyboard

![](README_attachments/media/rId27.jpg){width="5.833333333333333in"
height="5.833333333333333in"}

![](README_attachments/media/rId30.jpg){width="5.833333333333333in"
height="5.833333333333333in"}

![](README_attachments/media/rId33.jpg){width="5.833333333333333in"
height="5.833333333333333in"}

![](README_attachments/media/rId36.jpg){width="5.833333333333333in"
height="5.833333333333333in"}

![](README_attachments/media/rId39.jpg){width="5.833333333333333in"
height="5.833333333333333in"}

![](README_attachments/media/rId42.jpg){width="5.833333333333333in"
height="5.833333333333333in"}
