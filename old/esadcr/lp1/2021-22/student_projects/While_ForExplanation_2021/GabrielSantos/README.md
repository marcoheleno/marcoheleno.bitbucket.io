**Título**

\"Mutações de Cor com Linha\"

**Autor**

Gabriel Santos \| *3190705* \| Laboratório de Projeto I

**Ciclo**

\"While\"

**Palavras Chave**

Gradiente; Cor; Liquido; Linha; Aleatório.

**Conceito**

Inspirado pelo trabalho da artista Tauba Auerbach, o projeto irá
explorar o comportamento de cores análogas, através de movimentos de
posição e mutações de forma, de maneira espontânea, tendo em
consideração o ciclo \"while\".

**MoadBoard**

![](README_attachments/media/rId20.png){width="5.833333333333333in"
height="3.6456955380577427in"}

**StoryBoard**

![](README_attachments/media/rId23.png){width="5.833333333333333in"
height="3.6458333333333335in"}
