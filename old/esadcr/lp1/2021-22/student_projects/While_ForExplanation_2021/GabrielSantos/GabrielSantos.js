// Gabriel Santos; 3190705; LPI; 2º Fase

var x, y; //variante para as posições da elipse e das linhas. 

function setup()

{

  createCanvas (windowWidth, windowHeight);

  colorMode (RGB, 255, 255, 255, 100);

  frameRate(10); //velocidade do movimento dos elementos


}



function draw()

{
  //background (255, 100, 100);
  background (255, 255, 255, 100); // A transparência permite vizualisar um arrasto dos elementos.
  line (0, height/2, width, height/2 )
  stroke (30);
  
if(frameCount>width/4) fill (0, 100, 255); //quando a elipse chegar ao ponto x, a cor altera
  if(frameCount>width/3) fill (255, 255, 0);
    if(frameCount>width/2) fill (0, 0, 0);


  
  ellipse(frameCount,height/2,40,40); // O frameCount faz a elipse mover.
  
  
  if(frameCount>width/4) {frameRate(2)} //quando o círculo chegar a este intrevalo, a velocidade das linhas passa a ser o seguinte valor
    if(frameCount>width/3) {frameRate(300)}
      if(frameCount>width/2) {frameRate(5)}

  fill(0, 0, 0);
  rect(width/2, height/2, 5,5) //estas formas geométricas têm como objetivo assinalar os diferentes intrevalos
    rect(width/3, height/2, 5,5)
      rect(width/4, height/2, 5,5)
        //rect(width/8, height/2, 30,30)




  //while (frameCount < width/2) enquanto o círculo não chegar ao meio, as linhas continuam a mexer
  
  r= random (255) //variável para a cor. 
  g= random (255);
  b= random (255);
  fill (r, g, b); //as cores variam entre o 0-255 de cada cor (vermelho, azul e verde)
  x = random(width);
  y= random (height/2);
 
    
  stroke (r, g, b);
  strokeWeight (2);
  line(x, y, 400, 200); //a linha move-se no eixo do X e Y; esta nasce do lado esquero do ecrã.A outra, do direito.
  
  stroke (r, g, b);
  strokeWeight (2);
  line(x, y, 40, 40); 
     
  strokeWeight (2);
  stroke (r,g, b);
  line(x, y, 880, 100);   
  
  stroke (0); //LINHA DO MEIO
    strokeWeight (2);
  
  
}



function windowResized()

{

  resizeCanvas (windowWidth, windowHeight);

}