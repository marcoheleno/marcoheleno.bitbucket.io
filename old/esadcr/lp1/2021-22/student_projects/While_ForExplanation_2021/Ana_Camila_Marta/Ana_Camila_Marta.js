/*
ANA INES 3190672 | CAMILA SANTOS 3190003 | MARTA VIEIRA 3190695
EXERCICIO 2 | WHILE||FOR EXPLANATION - FOR
LABORATORIO DE PROJETO 2021|2022
DESIGN GRAFICO E MULTIMEDIA | RAMO DE MULTIMEDIA
ESAD.CR
DOCENTE | MARCO HELENO
26/01/2022
*/

let i, coordX, coordY, limVert, limHoriz, frase1, frase2, frase3;

function setup() 
{
  createCanvas(windowWidth, windowHeight);
  colorMode (RGB, 255, 255, 255, 100);
  background(255, 255, 255, 100);
  rectMode (CENTER); //Para que os quadrados sejam desenhados a partir do centro
  noFill(); //Para que os quadrado seja visíveis
  textAlign (CENTER); //Alinhamento do texto
  i=60; //Primeiro quadrado a ser desenhado 
}

function draw() 
{  
  textFont ("Poppins"); //Tipografia escolhida
  
  //Valor das variaveis
  frase1='Press space to USE THE FOR!'; //Frase para interagir
  frase2= 'for (i=60; i<=limHoriz || i<=limVert; i+=60)'; //Frase para fim do for
  frase3= 'Click or press ENTER to restart!'; //Frase que indica como reiniciar
  coordX=width/2; //Coordenada do meio da tela
  coordY=height/2; //Coordenada do meio da tela
  limVert=height-200; //Limite da iteracao em Y
  limHoriz=width-200;  //Limite da iteracao em X
  
  //Texto do FOR
  textSize (20); //Tamanho do Texto
  stroke (0); //Cor do Texto
  strokeWeight (1); //
  text (frase2, coordX, height/8);
  
  //Texto para interagir
  textSize (30);
  stroke (0); //Cor do Texto
  strokeWeight (1); //Grossura do texto
  text (frase1, coordX, height/20); //Coordenada do texto
  
  //Indicacao do limite do FOR
  stroke (255,0,0,100); //Limite
  strokeWeight (10);
  rect (coordX, coordY, limVert-30, limVert-30); //Quadrado de limite (vermelho)  
  
  //Retangulos para iteracao
  if (i<=limVert-30 && i<=limHoriz-30)
    {
    noFill (); //Preenchimento dos retangulos 
    stroke (0); //Cor da borda dos retangulos
    strokeWeight (map (i, 0, height, 1, 10)); //Mapeamento para engrossar o stroke
    rect (coordX,coordY,i,i); //Aumenta o tamanho variando o tamanho de i
    }
  
   else 
  {
    fill (0);
  }
  
    //Texto para reiniciar
    textSize (30);
    stroke (0);
    strokeWeight (1);
    text (frase3, coordX, height-40); 
}

function keyPressed() //Ao usar espaço, há iteracao
{
 if (i<=limVert && i<=limHoriz) i+=60; 
  
  //se for o ultimo, fill preto
  else 
  {
    fill (0);
    textSize (30);
    stroke (0);
    strokeWeight (1);
    text (frase3, coordX, height-40);
  }
  
  if (keyCode === ENTER) //Reinicia o ciclo ao apertar ENTER
    {
      background(255, 100);
      i=60;
      noFill (); 
    }
}

function mousePressed ()
{
    textSize (30);
    stroke (0);
    strokeWeight (1);
    text (frase3, coordX, height-40);
  
  if (i<=limVert && i<=limHoriz) 
    {
    i+=60;     
    }

    if (mouseButton === LEFT && i>=limVert) //Reinicia o ciclo ao clicar
     {
       i=60;       
       background(255, 100);
      }
    
}


function windowResized ()
{
  resizeCanvas (windowWidth, windowHeight);
}