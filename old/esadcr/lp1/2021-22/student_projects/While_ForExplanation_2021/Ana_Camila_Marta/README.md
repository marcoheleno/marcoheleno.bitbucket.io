# RELATÓRIO - WHILE \|\| FOR explanation

> Ana Inês Manuel \| 3190672, Camila Santos \| 3190003, Marta Vieira \|
> 3190695
>
> Laboratório de Projeto I, 1º Semestre, 3º Ano
>
> ESAD.CR \| IPLEIRIA \| 2021/2022

## Introdução

Partindo da junção dos elementos do grupo de trabalho iniciámos com a
análise de ambos os briefings. Inicialmente deparámo-nos com algumas
dúvidas, mas depois de algumas reuniões e discussão entre nós
conseguimos chegar a um consenso sobre o que pretendemos fazer.

No projeto While \|\| For explanation é proposto que seja desenvolvido
um projeto em código de ,modo a que seja possível explicar o
funcionamento de um dos dois ciclos de repetição. Nós escolhemos
trabalhar com o ciclo For.

## Proposta 

#### Conceito

Após a realização de alguma pesquisa e recolha de referencias foi-nos
possível chegar a um consenso sobre que conceito/temas utilizar em ambos
os projetos, tais como os elementos e animações que melhor se enquadram
para o resultado que pretendemos, sendo que, com a realização de
diversas experiências certas ideias e elementos vão sendo alterados com
o objetivo de melhorar.

Para o projeto de apresentação e explicação de ciclos decidimos escolher
o ciclo FOR e optámos pelo tema \"Batimento Cardíaco\", sendo que o
objetivo é utilizar os elementos escolhidos possam demonstrar não o
símbolo de batimento cardíaco , mas sim o som que é reproduzido como
ondas de som, como por exemplo utilizando uma interação mousePress ou
keyPressed e quando se pressiona em cima do elemento ele fazer um pico
utilizando alteração da escala. Relativamente a como explicar o ciclo
pensámos representar como se fosse um storyboard, ou seja, uma animação
frame a frame de modo a que demonstrasse as diversas fases da repetição.

#### Pesquisa

A partir da receção de ambos os briefings começámos por realizar
pesquisa sobre Design Generativo e a recolher referencias partindo do
princípio que o objetivo de ambos os projetos é desenvolver um sistema
de desenho automático e de certo modo aleatório e explicar o ciclo de
repetição.

Depois da pesquisa inicial passámos para a recolha de referências
visuais que nos ajudaram a decidir o que pretendemos realizar em cada um
dos projetos e desenvolvemos o moodboard a partir da recolha.

##### .Referências

<https://creative-coding.decontextualize.com/>

<https://creative-coding.decontextualize.com/transformations-and-functions/>

<https://editor.p5js.org/camilaS/collections/WT5LYWk5H>

<https://genekogan.com/code/p5js-transformations/>

<https://learn.digitalharbor.org/courses/creative-programming/lessons/p5-js-transformations-rotate/>

##### .Moodboard

![](README_attachments/media/image1.jpg){width="5.833333333333333in"
height="4.129057305336833in"}

![](README_attachments/media/image2.jpg){width="5.833333333333333in"
height="4.124159011373578in"}

#### Esboços Iniciais:

#### ![](README_attachments/media/image3.jpg "fig:"){width="5.833333333333333in" height="8.241050962379703in"}

##### . FOR StoryBoard![](README_attachments/media/image4.jpg "fig:"){width="5.833333333333333in" height="8.241050962379703in"}

![](README_attachments/media/image5.jpg){width="5.833333333333333in"
height="8.241050962379703in"}

### Processo de Trabalho

#### . Experiências

Após a realização de storyboard, iniciámos com a realização de várias
experiências no P5, sendo que deparámo-nos com diversas dificuldades
pois algumas vezes estava o resultado que queríamos mas o projeto em si
não estava a transmitir o que era suposto relativamente á demonstração
do ciclo e não algo que realizasse um ciclo.

###### Coleção de experiências P5:

<https://editor.p5js.org/camilaS/collections/0UehUhvfP>

Nesta experiencia realizamos círculos a serem desenhados de modo a irem
aumentando a escala até á construção de um círculo maior, sendo que vão
aparecendo e quando se faz mouseIsPressed o stroke da linha aumenta
criando assim a interação.

![image-20211214160317581](README_attachments/media/image6.png){width="4.777777777777778in"
height="4.577870734908137in"}

image-20211214160317581

![](README_attachments/media/image7.png){width="2.888888888888889in"
height="2.328472222222222in"}![image-20211207155358951](README_attachments/media/image8.png){width="2.9711603237095363in"
height="2.388888888888889in"}

image-20211207155358951

image-20211207155439694

Ex. 1 -<https://editor.p5js.org/camilaS/full/PzXF4Nc2Y>

Algumas das experências que realizamos para chegar a um bom resultado
utilização ociclo For para a fazer, mas não é pretendido que se utilize
o ciclo For para demonstrar o mesmo, então para resolver essa questão
optámos por utilizar uma função *if*, realizando assim o mesmo resultado
mas desenvolvido de forma diferente.

![](README_attachments/media/image9.png){width="3.75in"
height="3.8402777777777777in"}Para exemplificar demonstramos o código
com o ciclo For e renovado com a função *if* :

image-20211214161044055

![](README_attachments/media/image10.png){width="3.310416666666667in"
height="2.1944444444444446in"}![](README_attachments/media/image11.png){width="3.35in"
height="2.2291666666666665in"}![](README_attachments/media/image12.png){width="3.30625in"
height="2.2083333333333335in"}![](README_attachments/media/image13.png){width="3.347916666666667in"
height="2.2222222222222223in"}

image-20211214190642208

image-20211214190731792

image-20211214190807347

image-20211214190836037

Ex. 2 - <https://editor.p5js.org/camilaS/sketches/5kqFr5tqv>

Nesta experiência conseguimos que realize o mesmo que a anterior mas
utilizando a função *if*, sendo que o círculo vai-se completando com
outros círculos que vão aparecendo e com a intereção de mouseIsPressed
cria novos círculos azuis tentando desta forma demonstrar o ciclo, pois
os círculos vão sendo desenhados e não colocados propositadamente.

#### 

#### 

#### 

#### Tecnologias Usadas

Typora

Word

P5\*js

Adobe Illustrator

Notebloc (App digitalizador)
