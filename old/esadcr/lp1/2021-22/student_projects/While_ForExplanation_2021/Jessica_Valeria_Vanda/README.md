# \"Sequences\"

###### Jéssica Duarte 3190679, Valéria Beskorovaynyy 3190663, Vanda Bica 3190668

Em resposta ao Briefing 2, escolhemos o conceito "for". Este consiste em
três expressões diferentes entre parênteses, todas opcionais. Essas
expressões são usadas para controlar o número de execuções de loop. A
primeira expressão é a instrução usada para definir o estado inicial do
loop. A segunda expressão é a condição que desejamos verificar antes de
cada loop. Se esta expressão retornar falso, o loop será encerrado. A
terceira expressão é executada no final de cada loop.

Portanto, para continuar com a coerência entre os dois cartazes, vamos
recorrer à mesma paleta de cores (preto e branco, ou possíveis variações
dessas mesmas cores a nível de transparência.) O nosso cartaz conterá os
três elementos essenciais para a demonstração/explicação do conceito:
Parênteses; expressão; ponto e vírgula;

Para a interação, temos em mente criar uma interação entre o rato e a
composição, dando ao utilizador a liberdade de explorar a mesma.

**Sketch**

![Изображение выглядит как квадрат Автоматически созданное
описание](README_attachments/media/image1.png){width="3.7236843832021in"
height="4.118182414698163in"}

**StoryBoard**

![](README_attachments/media/image2.png){width="5.540629921259843in"
height="3.4605260279965004in"}

**MoodBoard**

![moodboard2](README_attachments/media/image3.jpg){width="4.365954724409449in"
height="3.9270833333333335in"}

**Link para o Código:**

<https://editor.p5js.org/vlrab/sketches/GzOgNYBgN>
