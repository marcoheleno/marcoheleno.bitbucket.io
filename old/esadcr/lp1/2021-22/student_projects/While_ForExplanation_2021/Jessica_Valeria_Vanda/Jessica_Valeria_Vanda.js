//Jéssica Duarte 3190679, Valéria Beskorovaynyy 3190663, Vanda Bica 3190668

//Exercício de avaliação: FOR explanation

//Licenciatura em Design Gráfico e Multimédia (Multimédia) @ESAD.cr
//3ºano | 2º semestre 
//UC Laboratório de Projeto I | 2021/2022
//Docente: Marco Heleno
//26 de janeiro de 2022

let font;
let caracter = ';;;;;;;;;;;;;;;;;;;;;;;;;';
let distancia = 360; // angulo do circulo
let raio1, raio2, raio3; // nome dos circulos
let opacidade1, opacidade2, opacidade3; //opacidade de cada círculo
let contador; 
let vel;
let guarda_frame;

function preload()
{
  font = loadFont ("student_projects/While_ForExplanation_2021/Jessica_Valeria_Vanda/Lato-Thin.ttf"); // para os elementos gráficos
  font_titulo = loadFont ("student_projects/While_ForExplanation_2021/Jessica_Valeria_Vanda/Lato-Regular-1.ttf"); //para o titulo
}

function setup() 
{
  createCanvas(windowWidth, windowHeight);
  colorMode (RGB, 255, 255, 255, 100);
  
  //definição do clique para a criação do ciclo
  opacidade1 = 30;
  opacidade2 = 30;
  opacidade3 = 100;
  
  contador = 3;
  vel = 1;
  
  guarda_frame = 0;
  
}


function draw() 
{
  if (frameCount <= guarda_frame) background(0);
  else background(255); //frame preto, para recomeçar o ciclo
 
  //Título  
  textAlign(CENTER); 
  textSize (width/30);
  textFont (font_titulo);
  textStyle (NORMAL);
  fill(0); 
  text("- FOR EXPLANATION -", width/2, height/1.08);
  
  //Chavetas
  //"min" = função dá o menor de dois números, garantindo que as chavetas não sejam cortadas
  textSize (min(width,height));
  fill ("black"); 
  textFont (font);
  
  //Chavetas do lado esquerdo
  textAlign (LEFT, CENTER);
  text('{', 0, height/3);
  
  //Chavetas do lado direto
  textAlign (RIGHT, CENTER);
  text('}', width, height/3);
  

  
  //"min" = função dá o menor de dois números, garantindo que o circulo não seja cortado
  raio1 = min(width,height) / 2.5; 
  raio2 = min(width,height) / 4.5;
  raio3 = min(width,height) / 15;
  
  
  // fonte dinamica
  noFill ();
  noStroke();
  textSize(raio1/4.5);
  textFont (font);
  
  

  //circulos onde o caracter vai inserir
  //os circulos estão sem stroke para não se notarem
  //raio*2 porque o dobro do raio é igual ao diametro, por sua vez, este é igual ao tamanho do circulo
  circle(width/2,height/2, raio1*2); 
  circle(width/2,height/2, raio2*2);
  circle(width/2,height/2, raio3*2);
  
  //Espaçamento entre cada caracter
  let anguloEntreCaracter = radians(distancia) / caracter.length; 
  //radians = calcula o angulo
  
  //Caracter a ser inserido
  push();
  translate(width/2, height/2);// circulo centrado
  
  vel++;
  
  for (let i=0; i<caracter.length; i++) 
  {   
    push();
    rotate(vel+i * anguloEntreCaracter); //formação do circulo e rotação do proprio circulo
    translate(0,-raio1); //translação para o circulo
    noStroke ();
    fill(0, opacidade1);
    text(caracter[i], 0,0);        
    pop();
  }
    for (let i=0; i<caracter.length; i++) 
    { 
      push();
      rotate(vel+i * anguloEntreCaracter); //formação do circulo e rotação do proprio circulo
      translate(0,-raio2);  // translação para o circulo
      noStroke ();
      fill(0, opacidade2);
      text(caracter[i], 0,0);                
      pop();
    }
  
    for (let i=0; i<caracter.length; i++) 
    {   
      push();
      rotate(vel+i * anguloEntreCaracter); //formação do circulo e rotação do proprio circulo
      translate(0,-raio3); // translação para o circulo
      noStroke ();
      fill(0, opacidade3);
      text(caracter[i], 0,0);                
      pop();
    }
}


function mouseReleased () 
{
  if (contador === 3) 
  {
    //console.log("2");
    contador = 2;
    opacidade2 = 100;
  }
  else
  if (contador === 2) 
  {
    //console.log("3");
    contador = 1;
    opacidade1 = 100;
  }
   else
  if (contador === 1) 
  {
    //console.log("restart");
    contador = 3;
    opacidade1 = 30;
    opacidade2 = 30;
    opacidade3 = 100;
    guarda_frame = frameCount+40;
  } 
}

function windowResized() 
{
  resizeCanvas (windowWidth, windowHeight);
}