// Random Grids - Final
// Unidade Orgânica: Escola Superior de Artes e Design
// Curso: Lic. Design Gráfico e Multimédia
// Ano Letivo: 2021/2022
// Ano Curricular: 3º ano | 1º semestre
// Unidade Curricular: Laboratório de Projeto 1
// Docente: Marco Heleno
// Estudantes: Grupo 1 - Márcia Pinto (3190757) | Maryna Dmytruk (3190692) | Marta Santos (3180248)
// Data de Avaliação: 26 Janeiro 2022


//---------------------- VARIAVEIS --------------------
let coordenadaXPontosGlobais = []; //array para guardar as coordenadas X dos pontos
let coordenadaYPontosGlobais = []; //array para guardar as coordenadas Y dos pontos
let tamanhoXQuadrado = 0; // variavel para saber a largura da celula da grelha
let tamanhoYQuadrado = 0; // variavel para saber a altura da celula da grelha
let numeroCelulasAleatorias = 8; // Numero maximo de pontos aleatorios gerados
let pontosUsados = []; //array para guardar os pontos usados da mesma figura
function setup() {
  // Criar a folha A4 - codigo do professor
  let folha = new printCanvas();
  folha.createPaperCanvas ("A4", "PORTRAIT", P2D);
  // (A1 a A7), (PORTRAIT ou LANDSCAPE), não mexer
  folha.defineHiResCanvas(300);
  noLoop();
  colorMode (RGB, 255, 255, 255, 100); // Definir o modo da cor para termos o efeito de transparencia nas figuras
}


function draw() {

  let grelha = new Grid (10, 10);   // numero de linhas e colunas da grelha - codigo do professor
  let tabela = grelha.buildGrid(); // criar a grelha - codigo do professor
  background(255);

  coordenadaXPontosGlobais = []; //array para guardar as coordenadas X dos pontos
  coordenadaYPontosGlobais = []; //array para guardar as coordenadas Y dos pontos
  
   tamanhoXQuadrado = tabela[0][0].w; // saber a largura da celula
   tamanhoYQuadrado = tabela[0][0].h; // saber a altura da celula
   //console.log(tamanhoXQuadrado, tamanhoYQuadrado); // verificar se esta a obter o tamanho

  // variavies temporarias para auxilixar a obter as coordenadas dos pontos aleatorios gerados
   let c = 0; // comprimento (largura) da celula
   let l = 0; // largura (altura) da celula
   let pC = 0;
   let pL = 0;

// ciclo para obter as coordenadas de cada ponto aleatorio gerado
   for (let i=0; i<numeroCelulasAleatorias; i++) { // enquanto houver pontos aleatorios
     while (pC === c) c = int (random (0, tabela.length) ); // gerar uma celula aleatoria entre 0 à largura da grelha, para definer a celula onde o ponto vai ficar, caso esse valor seja igual ao anterior gera um novo valor
     while (pL === l) l = int (random (0, tabela.length) ); // o mesmo que o de cima mas para a altura da grelha

     // Coordenadas dos pontos aleatorios gerados
     let x = l * tamanhoXQuadrado + tamanhoXQuadrado / 2; // multiplicação do largura da celula pelo valor acima gerado (celula em que o ponto vai ficar) e soma metade da celula (para dar o ponto do centro)
     let y = c * tamanhoYQuadrado + tamanhoYQuadrado / 2; // o mesmo que o de cima mas para a altura

     // guardar a celula em que o ponto está, para o proximo valor gerado não ficar na mesma celula
     pC = c;
     pL = l;
     // guardar no array as coordenadas dos pontos aleatorios gerados (x,y)
     coordenadaXPontosGlobais.push(x);
     coordenadaYPontosGlobais.push(y);

   }

   //console.log(coordenadaXPontosGlobais, coordenadaYPontosGlobais) // verificar se esta a obter
   // ------------------- CRIAR AS FORMAS -----
   for (let i=0; i<numeroCelulasAleatorias; i++) {

      let composicao = createGraphics (width, height); // criar o elemento grafico - codigo do professor
      let nladosForma = int (random (5, 8)); // numero de lados que as figuras podem ter


      composicao.fill(random(255), random(255), random(255), 50); // cor random das figuras
      composicao.noStroke(); // tirar a grossora da linha das figuras
      composicao.beginShape(); // Criar a figuras - inicio

      pontosUsados = [];
      for(let j = 0; j < nladosForma; j++) { // ciclo para ir buscar pontos para as figuras, consoante o numero de lados gerado acima

            let rnd = int (random (0, numeroCelulasAleatorias)); // buscar um valor aleatorio do numero maximo de pontos aleatorios gerados, para a figura ter formas aleatorias
            while(pontosUsados.find(p => p.x === int(coordenadaXPontosGlobais[rnd]) && p.y === int(coordenadaYPontosGlobais[rnd])) !== undefined){ // caso não encontre as coordenados do ponto no array volta a ir buscar outros 
              rnd = int (random (0, numeroCelulasAleatorias));
            }
            composicao.vertex(int(coordenadaXPontosGlobais[rnd]),int(coordenadaYPontosGlobais[rnd])); // x e y de cada ponto, obtido apartir do array das coordenadas acima
            pontosUsados.push({x: int(coordenadaXPontosGlobais[rnd]), y: int(coordenadaYPontosGlobais[rnd])});

            stroke('#191919'); //cor dos pontos
            strokeWeight(6); // tamanho dos pontos
            point(int(coordenadaXPontosGlobais[rnd]), int(coordenadaYPontosGlobais[rnd])); //criar os pontos
            noStroke();
      }

      composicao.endShape(CLOSE);  // Criar a figuras - final
      image(composicao, 0, 0, width, height); // mostrar o resultado na canva

   }


}

// função para fazer print - codigo do professor
function keyPressed()
{
  if (key == "l") redraw();

  if (key == "s") saveCanvas ("print", "png");
}
