# Random Grids

Unidade Orgânica: Escola Superior de Artes e Design

Curso: Lic. Design Gráfico e Multimédia\
Ano Letivo: 2021/2022\
Ano Curricular: 3º ano \| 1º semestre\
Unidade Curricular: Laboratório de Projeto 1\
Docente: Marco Heleno\
Estudantes: Grupo 1 - Márcia Pinto (3190757) \| Maryna Dmytruk (3190692)
\| Marta Santos (3180248)\
Data de Avaliação: 15 Dezembro 2021

## Pesquisa e MoodBoard 

![](README_attachments/media/rId20.png){width="5.833333333333333in"
height="3.6068339895013124in"}

Para desenvolver este exercício o grupo realizou uma pesquisa intensiva
por artistas e trabalhos que pudessem inspirar-nos a criar. Foram os
seguintes artistas que inspiraram para desenvolver o conceito:

Sam Tsao (<https://www.instagram.com/sam___tsao/>)

Okazz (<https://www.instagram.com/okazzsp/>)

## Conceito Desenvolvido

Com base nos propostos do briefing e com alguma pesquisa o grupo
fundamentou uma ideia acessível aos nossos conhecimentos e que vai dar
resultados muito interessantes. A ideia consiste em criar figuras a
partir da ligação entre pontos, esses pontos terão posições aleatórias o
que dará as formas dinâmicas como esboçadas a baixo. As figuras também
terão o preenchimento com canal alfa ou seja com transparência, para
quando houver sobreposição das formas o seu resultado dar uma cor
diferente das demais.

Esta ideia foi inspirada nos artistas do movimento artístico
abstracionismo geométrico Wassily Kandinsky, Kazimir Malevich e Piet
Mondrian, em que apartir da observação podemos ver que a característica
mais importante desta arte é a liberdade, tanto para o artista quanto
para o público, que tem liberdade para apreciar e interpretar a obra,
sem compromissos com a realidade. Podemos dizer que a nossa ideia para
este briefing passa por criar uma composição abstrata, onde exploramos a
diversidades das cores e das formas dinâmicas que vão ser organizadas de
maneira a criar uma composição geométrica expressiva.

![](README_attachments/media/rId26.png){width="5.833333333333333in"
height="3.6068339895013124in"}

![](README_attachments/media/rId29.png){width="5.833333333333333in"
height="3.6068339895013124in"}

É um conceito à primeira vista que parece simples contudo pode ser muito
explorado como por exemplo ter um formulário com várias paleta de cores
onde a pessoa pode escolher e assim criar um background dinâmico á sua
medida, onde poderá aplicar em diversos materiais, como por exemplo para
criar cartazes ou padrões para marcas. Na imagem seguinte temos exemplos
de dois cartazes onde aplicamos este conceito para testar a sua
utilidade.

![](README_attachments/media/rId32.png){width="5.833333333333333in"
height="4.47544728783902in"}

## Tecnologias Usadas

Para este projeto utilizamos o editor de texto Atom visto que tem mais
funcionalidades que o editor do p5.js como por exemplo o autocomplete.

Atom - <https://atom.io/>
