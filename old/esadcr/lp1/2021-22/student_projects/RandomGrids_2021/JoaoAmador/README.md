RANDOM GRIDS - \"CARTAZES\"

Por João Amador

Através de algoritmos podemos conceder vida a produtos artísticos ou de
design, com parâmetros que nós definimos.

Se o sistema algorítmico tiver um nível alto de autonomia, maior será a
aleatoriedade na sua criação de objetos de arte.

Este tipo de produção não é um movimento artístico, mas um método, a
arte generativa não quebra as correntes de arte atuais nem passadas. A
temática que quero evidenciar nos meus cartazes segue uma determinada
lógica, eu quero que assente no Abstracionismo Geométrico, cujo
movimento engloba vários sub-estilos, como o Suprematismo,
Neoplasticismo, Construtivismo
etc...![](README_attachments/media/image1.png){width="5.905555555555556in"
height="6.529861111111111in"}

Portanto quero empregar o uso de formas geométricas bidimensionais como
elementos ilustrativos integrados na hierarquia de informação a ser
apresentada na tela (cartazes).

![](README_attachments/media/image2.png){width="5.864583333333333in"
height="7.072916666666667in"}

> Suprematismo

![](README_attachments/media/image3.png){width="5.864583333333333in"
height="5.875in"}

> Neoplasticismo

![](README_attachments/media/image4.png){width="5.552083333333333in"
height="6.25in"}

> Construtivismo

![](README_attachments/media/image5.png){width="5.875in"
height="5.854166666666667in"}

> Construtivismo Neoconcreto

As temáticas a serem aplicadas vão remeter para os sete pecados mortais
e emoções humanas. Para cada cartaz quero titular apenas com um
substantivo.

O projeto será realizado no editor de java script P5.js onde irei criar
uma grelha dinâmica na qual irei definir as caraterísticas a serem
alteradas de forma autónoma.

![](README_attachments/media/image6.png){width="1.5625in"
height="1.5729166666666667in"}

A fim de conseguir gerar cartazes capazes de suportar esta meta, vou
criar uma biblioteca de formas geométricas, algumas mais simples
(primitivas), como a elipse, o círculo, quadrado\... e outras mais
complexas (mas não demasiado) como estrelas, hexágonos, corações,
através de montagens com encapsulamento do código.

Pretendo ir mudando as formas a serem mostradas, provavelmente através
de condições (por exemplo se a estrela estiver visível, o círculo fica
invisível). Para facilitar vou ter em conta um certo paralelismo de
interpretação entre as formas, assim os cartazes terão mais contraste
visual/informativo.

Quanto ao uso de texto, pretendo aplicar condições para obrigar a cada
*frame* só haver uma palavra visível, independentemente da sua posição
ou dimensão. As palavras a selecionar serão simples e com paralelismo
também.

Utilizando as mesmas figuras posso obter resultados bem diferentes,
claro que sempre a considerar que as posições terão sempre que se
adaptar automática ao tamanho da tela:

A fim de conseguir gerar cartazes capazes de suportar esta meta, vou
criar uma biblioteca de formas geométricas, algumas mais simples
(primitivas), como a elipse, o círculo, quadrado\... e outras mais
complexas (mas não demasiado) como estrelas, hexágonos, corações,
através de montagens com encapsulamento do código.

Pretendo ir mudando as formas a serem mostradas, provavelmente através
de condições (por exemplo se a estrela estiver visível, o círculo fica
invisível). Para facilitar vou ter em conta um certo paralelismo de
interpretação entre as formas, assim os cartazes terão mais contraste
visual/informativo.

Quanto ao uso de texto, pretendo aplicar condições para obrigar a cada
*frame* só haver uma palavra visível, independentemente da sua posição
ou dimensão. As palavras a selecionar serão simples e com paralelismo
também.

Utilizando as mesmas figuras posso obter resultados bem diferentes,
claro que sempre a considerar que as posições terão sempre que se
adaptar automática ao tamanho da tela:

![](README_attachments/media/image7.png){width="5.905555555555556in"
height="8.338888888888889in"}

![](README_attachments/media/image8.png){width="5.905555555555556in"
height="8.352777777777778in"}

![](README_attachments/media/image9.png){width="5.905555555555556in"
height="8.338888888888889in"}

![](README_attachments/media/image10.png){width="5.905555555555556in"
height="8.352777777777778in"}

![](README_attachments/media/image11.png){width="5.905555555555556in"
height="8.338888888888889in"}

![](README_attachments/media/image12.png){width="5.905555555555556in"
height="8.352777777777778in"}

Apresento alguns exemplos de cartazes muito simples, mas com contraste.
Sempre com os elementos embutidos na grelha.

![](README_attachments/media/image13.png){width="5.905555555555556in"
height="8.343055555555555in"}

Inspirado no Neoplasticismo, construção de uma imagem através de
\"puzzle\"

![](README_attachments/media/image14.png){width="5.905555555555556in"
height="8.329166666666667in"}

Inspirado no Construtivismo Neoconcreto, uso do preto e branco, e
\"vazios/preenchimentos\"

![](README_attachments/media/image15.png){width="5.905555555555556in"
height="8.343055555555555in"}

> O tipo de exemplo que acho mais plausível alcançar.

![](README_attachments/media/image16.png){width="5.905555555555556in"
height="8.329166666666667in"}

Com o mesmo tipo de estilo que o anterior mas com outra disposição.

Será complicado atingir resultados tão exatos, mas com o controlo das
cores disponíveis para serem randomizadas, dimensões e quantidade de
elementos por linhas e colunas acredito ser possível atingir expressões
interessantes, este relatório será atualizado com os resultados obtidos
ao longo do desenvolvimento do projeto.

Links:

FORNECIDOS PELO PROFESSOR:

<https://futurism.com/1-evergreen-machines-that-learn-are-invading-and-utterly-transforming-our-lives>

<https://www.vox.com/technology/2018/10/1/17882340/how-algorithms-control-your-life-hannah-fry>

<https://uxdesign.cc/how-ai-will-impact-your-routine-as-a-designer-2773a4b1728c>

<https://uxdesign.cc/ai-and-design-ai-is-your-creative-partner-cb035b8ef107>

<https://www.forbes.com/sites/bernardmarr/2018/09/02/what-is-industry-4-0-heres-a-super-easy-explanation-for-anyone/?sh=efd72839788a>

ARTE GENERATIVA:

<https://artegenerativasc.wordpress.com/2009/12/17/arte-generativa-nao-e-um-movimento-ou-ideologia-e-um-metodo/>

<https://www.hisour.com/pt/generative-art-21197/>

ABSTRACIONISMO GEOMÉTRICO:

<https://pt.wikipedia.org/wiki/Abstraccionismo_geom%C3%A9trico>

https://www.pinterest.co.uk/naracvc/abstracionismo-geom%C3%A9trico\_/
