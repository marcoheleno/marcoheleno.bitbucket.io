//Briefing Random Grids - Laboratório de Projeto - João Amador//

//Definir dimensão da tela, sistema de cores e impedir de o editor de continuar a executar o código em ciclo//

function setup() {
  let folha = new printCanvas();
  folha.createPaperCanvas("A2", "PORTRAIT", P2D);
  // (A1 a A7), (PORTRAIT ou LANDSCAPE), não mexer
  colorMode(RGB, 255, 255, 255, 100);

  folha.defineHiResCanvas(300);

  noLoop();
}

//Definir a grelha, indicar variáveis, cor de fundo//

function draw() {
  let grelha = new Grid(10, 10);
  let tabela = grelha.buildGrid();
  let pontos = [];
  let x,
    y,
    i = 0;

  background(0, 175, 90);

  //Condição para manter a grelha na tela, ao longo da altura e largura//

  for (let c = 0; c < tabela.length; c++) {
    for (let l = 0; l < tabela[c].length; l++) {
      x = random(tabela[c][l].x, tabela[c][l].x + tabela[c][l].w);
      y = random(tabela[c][l].y, tabela[c][l].y + tabela[c][l].h);

      pontos[i] = { x: x, y: y };
      i++;

      //Polígonos azuis//

      let composicao = int(random(2, 500));

      let forma = formaCelula(composicao, composicao);

      image(
        forma,
        tabela[c][l].x + tabela[c][l].w / 4,
        tabela[c][l].y + tabela[c][l].h / 4,
        composicao,
        composicao
      );
      rotate(radians(random(90, 360)));
    }
  }
  // Linhas vermelhas //
  let index;
  let numObjectos = tabela.length / 4;
  let numPontos = int(random(40, 20));
  strokeWeight(2);

  for (let o = 0; o < numObjectos; o++) {
    stroke(255, 60, 0);

    noFill();
    beginShape();
    for (let p = 0; p < numPontos; p++) {
      index = int(random(pontos.length));
      vertex(pontos[index].x, pontos[index].y);
    }
    endShape(CLOSE);
  }
}

// Parâmetros dos polígonos azuis //
function formaCelula(largura, altura) {
  let composicao = createGraphics(largura, altura);

  composicao.noFill();
  composicao.strokeWeight(44);
  for (let t = largura; t > 0; t -= 20) {
    composicao.stroke(
      0,
      map(t, largura, 0, 0, 100),
      map(t, largura, 0, 0, 255)
    );
    composicao.line(
      random(composicao.width / 20),
      random(composicao.height / 40),
      t,
      t
    );
  }

  //Retornar à variável, composicao//

  return composicao;
}

// Atalhos para redesenhar e captura de ecrã, para futura impressão do cartaz//

function keyPressed() {
  if (key == "l") redraw();

  if (key == "s") saveCanvas("print", "png");
}
