# The Xperience

###### Beatriz Dias - 3190656

###### Chessery Korn - 3190690

###### Ricardo Rodrigues - 3190773

### Pesquisa

Começámos com um brainstorming para apurar temas de interesse comum para
desenvolvermos um design generativo. Rapidamente percebemos que a música
era uma forte opção pelas suas vastas possibilidades. Entre um cartaz de
design generativo de um festival e um referente à capa de um álbum,
escolhemos a segunda.

*Num passo intermédio e mais à base de curiosidade, explorámos o código
de possíveis cores a utilizar, gostando das seguintes opções:*

Seguidamente averiguámos que artistas gostávamos em comum e selecionámos
5. Alt-j, The xx, Frank Ocean, Aphex Twin, The Weeknd e Tyler the
Creator

*Pesquisa de arte generativa por vários artistas.*

### Conceito

Devido à dificuldade em escolher um artista, decidimos elaborar um
código em JavaScript que tomasse essa decisão por nós, sendo que
ficaríamos igualmente satisfeitos com qualquer um que fosse. O código
escolheu os The xx, o que nos levou a fa

Dos três álbuns da banda (*xx* - 2009, *Coexist* - 2012, *I See You* -
2017) decidimos focarmo-nos apenas no primeiro que lançaram, o *xx*.

Concluímos que seria melhor se se selecionasse apenas uma faixa das 11
e, deste modo, recriar um template Spotify no telemóvel, cPara - 2009 -
track 2 - VCR

Interpretação gráfica com um vinil, letras do nome da música (VCR),
preenchimento cartaz com \"x\"

Pressionar a tecla espaço para \"play\" \"pause\" da faixa e assim
imprimir uma versão do poster.

Fase da impressão com o icon do \"triângulo\" para \"play\" (quer dizer
que a faixa está em pausa)

Barra de progressão da música animada em loop.\
Tempo da duração da faixa.

Escrito numa faixa a girar à volta

X no meio da bolacha, segundo momento de \"x\" à volta do vinil

fundo preto, \"x\" brancos

11 cores (1 cada faixa)

Só a faixa VCR por escrito, dando perceção de movimento ao vinil

Tipografia - Ailerons

Aparecimento e desaparecimento gradual dos \"x\"

### Moodboard

![](README_attachments/media/rId25.jpg){width="5.833333333333333in"
height="4.122129265091863in"}

### Protótipo

![](README_attachments/media/rId29.png){width="5.833333333333333in"
height="8.248017279090114in"}

![](README_attachments/media/rId32.png){width="5.833333333333333in"
height="8.248017279090114in"}

### Processo

![](README_attachments/media/rId36.jpg){width="5.833333333333333in"
height="3.28125in"}
