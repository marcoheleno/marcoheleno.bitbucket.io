/*
Grupo 2
Beatriz Dias - 3190656
Chessery Korn - 3190690
Ricardo Rodrigues - 3190773

Random Grids - Briefing 1
The Xperiencel


3º ano, Design Gráfico e Multimédia
1º semestre, UC Laboratório de Projeto 1
Docente Marco Heleno

ESAD.CR, 2021/2022
Data de avaliação 26/01/2022
*/



//definições da tipografia utilizada no cartaz, após upload no setup
function preload()
{
  tipo = loadFont ("student_projects/RandomGrids_2021/Beatriz_Chessery_Ricardo/Kanit-Bold.ttf");
}


//definição do tamanho, tipo de cor e resolução do canvas
function setup() 
{
  let folha = new printCanvas();
  folha.createPaperCanvas ("A2", "PORTRAIT", P2D)
  // (A1 a A7), (PORTRAIT ou LANDSCAPE)
  colorMode(RGB, 255, 255, 255, 100);
  
  folha.defineHiResCanvas(300);
  
  noLoop();
}



//permite a visualização por camadas da grelha, vinil e título, respetivamente
function draw() 
{
  desenhaGrelha();
  composicaoCentral();
  titulo();
}


//desenha uma grelha com 40 células de tamanhos (c e l) e (pC e pL) de proporção exata com rotações aleatórias (push) que permite inserir os "x"
function desenhaGrelha() 
{
  background (0);
  
//valores para executar uma grelha visível
  let grelha = new Grid (15, 15);
  let tabela = grelha.buildGrid();
  
//definição das células, utilizando como referência o comprimento do canvas, que suportam os "x"
  for (let c=0; c<tabela.length; c++) 
  {
    for (let l=0; l<tabela[c].length; l++) 
    {
      let fundo = formaCelulaFundo (tabela[c][l].w, tabela[c][l].h);
      image (fundo, tabela[c][l].x, tabela[c][l].y, tabela[c][l].w, tabela[c][l].h);
    }
  }
  
//desenvolvimento das 40 células com as suas variáveis
  let numeroCelulasAleatorias = 40;
  let c = 0;
  let l = 0;
  let pC = 0;
  let pL = 0;
  
  
//inserção dos "x" (cruz perfeita/não esticado) das células com tamanho, posicionamento e rotação aleatória
  for (let i=0; i<numeroCelulasAleatorias; i++)   
  {
    while (pC === c) c = int (random (0, tabela.length) );
    while (pL === l) l = int (random (0, tabela[c].length) );
    
    
    let forma = formaCelula (tabela[c][l].w, tabela[c][l].h);
    let cruzPerfeita = random(width/40, width/5);
    
    push();
      translate (tabela[c][l].x, tabela[c][l].y);
      rotate ( radians( random(0, 360) ) );
      image (forma, 0, 0, cruzPerfeita, cruzPerfeita);
    pop();
    
    pC = c;
    pL = l;
  }
}

//diferentes "x" brancos com tipografia pré-definida diferentemente inseridos  nas células a cada "refresh", podendo estar sobrepostos, no entanto sempre distinguíveis entre si por causa do seu contorno
function formaCelula (largura, altura)
{ 
  let composicao = createGraphics (largura*2, altura*2);
  
  composicao.strokeWeight(2);
  composicao.stroke(0); 
  composicao.fill (255);
  composicao.textSize (altura*2);
  composicao.textFont(tipo);
  composicao.textAlign (CENTER, CENTER);
  composicao.text ("X",composicao.width/2, composicao.height/4);
  
  return composicao;
}

//função para ser possível a visibilidade subtil da grelha 
function formaCelulaFundo (largura, altura)
{ 
  let composicao = createGraphics (largura, altura);
  
  composicao.strokeWeight(1);
  composicao.stroke(80); 
  composicao.noFill();
  composicao.rect (0, 0, composicao.width, composicao.height);
  
  return composicao;
}

//título com o nome do projeto "The Xperience" com contorno para se distinguir sobre os "x"   
function titulo()
{
  stroke(0);
  strokeWeight(11);
  textAlign (CENTER, CENTER);
  textSize(width/24);
  text ("PLAYING FROM", width/2, height/24);
  textSize (width/15);
  text ("THE XPERIENCE", width/2, height/13);
}

//elaboração de um disco vinil com 11 faixas representativas do álbum escolhido e seleção de 11 cores
function composicaoCentral() 
{ 
  let numCirculos = 11;
  let tamanhoComposicao = width/1.5;
  let diferenca = tamanhoComposicao/numCirculos;
  
  let cores = [];
  cores[0] = color(106,90,205);
  cores[1] = color(255,165,0);
  cores[2] = color(255,99,71);
  cores[3] = color(178,34,34);
  cores[4] = color(102,205,170);
  cores[5] = color(221,160,221);
  cores[6] = color(186,85,211);
  cores[7] = color(218,112,214);
  cores[8] = color(123,104,238);
  cores[9] = color(240,230,140);
  cores[10] = color(250,128,114);
  
  noStroke();
  let corEscolhida = 0;
  let corAnterior = 0;
  
//cada uma das faixas representadas por círculos tem uma cor diferente da faixa anterior e irrepetível. estas são também aleatoriamente atribuída do conjunto de cores pré-definido
  for (let i=numCirculos; i>0; i--)
  {
    do
    {
      corEscolhida = int(random(0, cores.length));
    } 
    while (cores[corEscolhida] === 0)

//diferentes e aleatórias larguras para cada faixa, num determinado intervalo de tamanhos possíveis 
    fill (cores[corEscolhida]);
    circle(width/2, height/2, diferenca*i+random(-diferenca/2.5, diferenca/2.5));
    
    cores[corEscolhida] = 0;
  }
}

//função que ao carregar na tecla "l" redesenha um cartaz e ao carregar "s" exporta o cartaz em formato png
function keyPressed() 
{
  if (key == "l") redraw();
  
  if (key == "s") saveCanvas ("print", "png");
}