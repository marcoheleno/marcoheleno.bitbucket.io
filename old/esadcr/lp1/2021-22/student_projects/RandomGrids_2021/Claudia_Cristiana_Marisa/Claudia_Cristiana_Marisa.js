/*--RANDOM GRIDS // IPLEIRIA - ESCOLA SUPERIOR DE ARTES E DESIGN - CALDAS DA RAÍNHA // 3º ANO // 2021-22 // 1ºSEMESTRE // 3ºMOMENTO DE AVALIAÇÃO // 26 DE JANEIRO 2022 // LABORATÓRIO DE PROJETO I // DESIGN GRAFICO E MULTIMÉDIA // DOCENTE: MARCO HELENO // CLÁUDIA AGOSTINHO, CRISTIANA SANTOS, MARISA BENTO --*/

//definir as fontes (fonte datas e fonte do nome da artista)
let fonte, fontedatas;
//definir textura
let textura = [];
//definir cores de fundo
let cores, paleta, paleta_anterior, cors_paleta_selcionada;
//definir datas e espaçamento entre as datas
let datas, text_size, num_lines, line_num, datas_index, font;

function preload()
{
  //importar texturas de tecido
  let textura1 = loadImage('student_projects/RandomGrids_2021/Claudia_Cristiana_Marisa/texture.jpg');
  let textura2 = loadImage('student_projects/RandomGrids_2021/Claudia_Cristiana_Marisa/textura2.jpeg');
  let textura3 = loadImage('student_projects/RandomGrids_2021/Claudia_Cristiana_Marisa/textura4.jpg');
  textura = [textura1, textura2, textura3];
  
  ////importar fontes para a assinatura e para as datas
  fonte = loadFont ("student_projects/RandomGrids_2021/Claudia_Cristiana_Marisa/Signatra.ttf");
  fontedatas = loadFont ("student_projects/RandomGrids_2021/Claudia_Cristiana_Marisa/Roboto-Black.ttf");
  
}

function setup() 
{ 
   //DEFINIR TAMANHO DA FOLHA 
  let folha = new printCanvas();
  folha.createPaperCanvas ("A2", "PORTRAIT", P2D);
  // (A1 a A7), (PORTRAIT ou LANDSCAPE), não mexer

  folha.defineHiResCanvas(300);
  
  //PALLETE DE CORES 1
  //rosa|amarelo|azul
  rosa = color(212, 144, 143);
  amarelo = color(237, 213, 150);
  azul = color(39, 86, 98);
  
  //PALLETE DE CORES 2
  //azulescuro|azulclaro|amarelo
  azulescuro = color(70,120, 128);
  azulclaro = color(204, 218, 212);
  amarelo_ = color(210, 171, 78);
  
  //PALLETE DE CORES 3
  //azul|laranja|rosa
  azul_ = color(55,78,119);
  laranja = color(200, 114, 81);
  creme = color(200);

 //Array das cores do tint
  cores = 
  [
    [rosa, amarelo, azul],
    [azulescuro, azulclaro, amarelo_],
    [azul_, laranja, creme],
  ];

  noLoop();
  //definição da palete anterior para não haver repetição
  paleta_anterior = 0;
}


  //Array para definir as datas
function draw() 
{
 
  datas = [];

  datas[0] = "1951";

  datas[1] = "1980";

  datas[2] = "1930";

  datas[3] = "1981";
  
  datas[4] = "1941";

  datas[5] = "1977";

  datas[6] = "1949";

  datas[7] = "1959";
  
  datas[8] = "1985";

  //começa a linha no número 1
  line_num = 1;
  //definição do tint na grelha
  do
  {
    paleta = int(random(0, cores.length));
  }
  
  while (paleta === paleta_anterior);
    
  //definição da grelha
  paleta_anterior = paleta;
  let grelha = new Grid (20, 20);
  grelha.randomizeColumns();
  let tabela = grelha.buildGrid();
  for (let c=0; c<tabela.length; c++) 
  {
    for (let l=0; l<tabela[c].length; l++) 
    {
      let forma = formaCelula (tabela[c][l].w, tabela[c][l].h);
      image (forma, tabela[c][l].x-1, tabela[c][l].y-1, tabela[c][l].w+2, tabela[c][l].h+2);
  }
}
  
  
  
  //definição da altura da linha das datas
  let alturalinha = (height-width/8)/datas.length;
  
  //vai adicionando as datas de forma random até chegar ao final
  for (let i=0; i<datas.length; i++)
  {
  //DEFINIÇÃO DAS DATAS
  //definição das fontes, tamanho, posição e cor
  noStroke();
  fill(255);
  textAlign (CENTER, CENTER);
  dataT = height/30;
  textSize(dataT);
  //definição das fonte, da posição e do tamanho da datas
  textFont(fontedatas);
  text (datas[i], random( textWidth(datas[i])/2, width-textWidth(datas[i])/2 ), alturalinha*i+dataT);
  }

  //DEFINIÇÃO DA ASSINATURA
  //definição da fonte, tamanho, e posição
  textFont(fonte);
  textSize(width/13);
  //texto visível no ecrã 
  text("Anni Albers", width/1.2, height/1.05);
  }


function formaCelula (largura, altura)
{
  //definição das cores na grelha
  let composicao = createGraphics (largura, altura);
  cors_paleta_selcionada = int(random(0, cores[paleta].length));
  composicao.tint(cores[paleta][cors_paleta_selcionada] );
  //definição da textura na grelha
  let randtext = random(textura) 
  composicao.image(randtext, 0, 0, largura, altura);
  return composicao;
  
}

  

function keyPressed() 
{
  //quando clica no l ou L é redesenhado
  if (key === "l" || key === "L") redraw();
  //quando clica no s ou S é guardado um ficheiro em png
  if (key === "s" || key === "S") saveCanvas ("print", "png");
}

