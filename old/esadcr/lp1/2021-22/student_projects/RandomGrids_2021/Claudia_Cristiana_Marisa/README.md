\<!\--RANDOM GRIDS // IPLEIRIA - ESCOLA SUPERIOR DE ARTES E DESIGN -
CALDAS DA RAÍNHA // 3º ANO // 2021-22 // 1ºSEMESTRE // LABORATÓRIO DE
PROJETO I // DESIGN GRÁFICO E MULTIMÉDIA // DOCENTE: MARCO HELENO //
CLÁUDIA AGOSTINHO, CRISTIANA SANTOS, MARISA BENTO \--\>

# RANDOM GRIDS

#### PESQUISA

A exploração do Design Generativo, tem como principal objetivo explorar
o processo de algoritmo dos **Teares Jacquard**, o tear de Jacquard foi
uma invenção revolucionária para os designers têxteis, mas o dispositivo
também inspira o funcionamento dos primeiros computadores modernos. do
mundo inventado por Joseph Marie Jacquard no início do século 19.

Esta invenção era um acessório que ficava no topo de tear. Com cartões
compostos por furos que giravam à volta do dispositivo. Cada furo
corresponde a um gancho, que servia como comando para levantar e abaixar
o gancho. A posição do mesmo dita o padrão dos fios levantados e
abaixados, permitindo que os têxteis repetissem padrões complexos com
velocidade e precisão.

\[![img](README_attachments/media/image1.jpg "fig:"){width="5.833333333333333in"
height="4.374998906386701in"}

Esta Imagem Retrata o primeiro computador de cartões perfurados de
Herman Hollerith, inventado no ano de 1880, inspirado no tear Jacquard.
Este é o antepassado do computador que hoje conhecemos.

#### **ANNI ALBERS**

![](README_attachments/media/image2.jpg){width="3.0555555555555554in"
height="3.875in"}

**Anni Albers** foi uma artista têxtil, pintora, designer alemã e
professora da escola de arte alemã Bauhaus. O seu trabalho era
reconhecido pela combinação do antigo ofício da tecelagem manual com o
design da arte moderna. Esta foi considerada como uma das mais
importantes **pioneiras da arte têxtil**, apesar de na altura não ter
tido tanto reconhecimento como artista inovadora Bauhaus, por isso, as
suas obras só ficaram reconhecidas mais tarde.

As obras de Albers deste período caracterizam-se por uma redução da
paleta de cores e uma ênfase na estrutura de padrões e tecidos, sendo as
interseções vertical e horizontal da teia e da trama enfatizadas por
desenhos geométricos rigorosos.

![Captura de ecrã 2021-11-09, às
21.40.07](README_attachments/media/image3.png){width="4.9930063429571305in"
height="2.9650339020122485in"}

Captura de ecrã 2021-11-09, às 21.40.07

#### DESIGN GENERATIVO

Design generativo é um método cuja finalização é gerado através de
regras ou algoritmos, usando geralmente por programas de computador.

![Captura de ecrã 2021-11-09, às
21.38.39](README_attachments/media/image4.png){width="4.237761373578302in"
height="4.909090113735783in"}

Captura de ecrã 2021-11-09, às 21.38.39

#### CONCEITO

Neste projeto vamos explorar o **design generativo** que homenageia o
processo algorítmico dos teares de jacquard do início do século 19. De
acordo com o tema escolhido pelo grupo, decidimos dar contexto a este
projeto iniciando por relatar a invenção textil, inovadora naquela
época.

**Teares Jacquard**, é um aparelho que conseguiu inovar a indústria da
moda, na execução de têxteis, em que executava produtos com maior
rapidez e eficácia. Contudo decidimos basear na arte têxtil de Anni
Albers, embora tenha sido a primeira mulher na época a ter formação
nessa área, pois este curso era limitado apenas para homens, ainda
assim, após várias tentativas conseguiu entrar. As suas obras
suscitaram-nos interesse devido as formas geométricas ( quadrados e
linhas sobrepostas) com interseções verticais e horizontais da teia e da
trama que a artista utiliza normalmente na sua arte e também pela sua
paleta cromática que torna o design mais simples e funcional visto que
uiliza poucas cores (*less is more*). Contudo na parte da composição
interativa, será realizada com frame rate () de 1fps, Idealizamos em
efetuar ligeiras rotações e posições das figuras geométricas (quadrados
e linhas).

#### MOODBOARD

![Captura de ecrã 2021-11-09, às
21.37.35](README_attachments/media/image5.png){width="5.833333333333333in"
height="2.836563867016623in"}

Captura de ecrã 2021-11-09, às 21.37.35

![Captura de ecrã 2021-11-09, às
21.38.14](README_attachments/media/image6.png){width="5.833333333333333in"
height="2.9069761592300964in"}

Captura de ecrã 2021-11-09, às 21.38.14

#### PALETA CROMÁTICA

![img](README_attachments/media/image7.png){width="5.833333333333333in"
height="3.6713538932633423in"}

img

![img](README_attachments/media/image8.png){width="5.833333333333333in"
height="3.6458333333333335in"}

img

![img](README_attachments/media/image9.png){width="5.833333333333333in"
height="3.656770559930009in"}

img

#### TECNOLOGIAS UTILIZADAS

-   Coolors.co

-   Milanote

-   Pinterest

-   Typora

-   Google Docs

-   Google Slide

-   Google Search

-   P5JS

-   Adobe Illustrator
