function setup() 
{
  let folha = new printCanvas();
  folha.createPaperCanvas ("A7", "PORTRAIT", P2D);
  // (A1 a A7), (PORTRAIT ou LANDSCAPE), não mexer
  
  colorMode (RGB, 255, 255, 255, 100);

  //folha.defineHiResCanvas(300);

  noLoop();
}



function draw() 
{
  let grelha = new Grid (75, 75);
  
  //let equilatero = int(random(2, 20));
  //let grelha = new Grid (equilatero, equilatero);
  
  //let grelha = new Grid ( int(random(1, 50)), int(random(1, 50)) );
  
  //grelha.randomizeColumns();
  //grelha.randomizeLines();
  
  let tabela = grelha.buildGrid();
  
  background (0);
  
  /*
  for (let c=0; c<tabela.length; c++) 
  {
    for (let l=0; l<tabela[c].length; l++) 
    {
      for (let i=0; i<45; i++) 
      {
        let forma = formaCelula_1 (i, tabela[c][l].w/4, tabela[c][l].h/4, tabela[c][l].w, tabela[c][l].h);
        image (forma, tabela[c][l].x, tabela[c][l].y, tabela[c][l].w, tabela[c][l].h);
      }
    }
  }
  */
  
  
  let c = 0;
  let l = 0;
  let pC = 0;
  let pL = 0;
  
  for (let i=0; i<tabela.length; i++)   
  {
    while (pC === c) c = int (random (0, tabela.length) );
    while (pL === l) l = int (random (0, tabela[c].length) );
    
    let tamanho = random(10, 150);
    let forma;
    let escolheForma = int( random(1, 3) );
    
    if (escolheForma === 1) forma = formaCelula_1 (tamanho/4, tamanho/4, tamanho, tamanho);
    else
    if (escolheForma === 2) forma = formaCelula_2 (tamanho/2, tamanho/2, tamanho, tamanho);
    
    push();
      translate (tabela[c][l].x, tabela[c][l].y);
      image (forma, 0, 0, tamanho, tamanho);
    pop();
    
    pC = c;
    pL = l;
  }
  
}



function formaCelula_1 (x, y, largura, altura)
{
  let composicao = createGraphics (largura, altura);
  
  composicao.rectMode (CENTER);
  composicao.strokeWeight (map(largura, 10, 150, 0.1, 1));
  composicao.noFill();
  
  let anguloInicial = int(random(0, 45));
  let anguloFinal = anguloInicial + 45;
  
  for (let i=anguloInicial; i<anguloFinal; i++) 
  {
    composicao.stroke (130, 140, 200, map(i, 0, 45, 0, 100));

    if (i %2 == 0)
    {
      composicao.push();
        composicao.translate (largura/2, altura/2);
          composicao.rotate (radians (i));
          composicao.rect (0, 0, x/2, y/2);
      composicao.pop();
    }

    else
    {
      composicao.push ();
        composicao.translate (largura/2, altura/2);
          composicao.rotate (radians (i*-2));
          composicao.rect (0,0,x*1.5 ,y*1.5);
      composicao.pop();
    }

    composicao.push();
      composicao.translate (largura/2, altura/2);
        composicao.rotate (radians (i*-1));
        composicao.rect(0, 0, composicao.width/1.5, composicao.height/1.5);
    composicao.pop();
  }
  
  return composicao;
}



function formaCelula_2 (x, y, largura, altura)
{
  let composicao = createGraphics (largura, altura);
  
  ellipseMode (RADIUS);
  composicao.strokeWeight (0);
  composicao.noStroke();
  //composicao.nofill();
  
  let anguloInicial = int(random(0, 45));
  let anguloFinal = anguloInicial + 45;
  
  for (let i=0; i<anguloFinal; i++)
  {
    composicao.stroke (122, 33, 33, map(i, 0, 45, 0, 100));
    composicao.fill(255, 255, 255, random (5, 20));
    if (i %2 == 0)
    {
      composicao.push();
        composicao.translate (largura/2, altura/2);
          composicao.rotate (radians (i));
          composicao.ellipse (0, 0, x, y);
      composicao.pop();
    }
/*
    else
    {
      composicao.push ();
        composicao.translate (largura/2, altura/2);
          composicao.rotate (radians (i*2));
          composicao.ellipse (0,0,x+i,y);
      composicao.pop();
    }
/*
    composicao.push();
      composicao.translate (largura/2, altura/2);
        composicao.rotate (radians (i*-1));
        composicao.ellipse(0, 0, composicao.width/1.5, composicao.height/1.5);
    composicao.pop();
    */
  }
  
  return composicao;
}



function keyPressed() 
{
  if (key == "l") redraw();
  
  if (key == "s") saveCanvas ("print", "png");
}


