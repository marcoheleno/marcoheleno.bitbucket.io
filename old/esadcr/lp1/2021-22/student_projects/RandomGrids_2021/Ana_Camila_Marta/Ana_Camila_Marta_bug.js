/*
ANA INES 3190672 | CAMILA SANTOS 3190003 | MARTA VIEIRA 3190695
EXERCICIO 1 | RANDOM GRIDS
LABORATORIO DE PROJETO 2021|2022
DESIGN GRAFICO E MULTIMEDIA | RAMO DE MULTIMEDIA
ESAD.CR
DOCENTE | MARCO HELENO
26/01/2022
*/

function setup() 
{
  let folha = new printCanvas();
  folha.createPaperCanvas ("A7", "PORTRAIT", P2D); //Tamanho da tela do projeto 
  colorMode (RGB, 255, 255, 255, 100);
  noLoop();
}

function draw() 
{
  let grelha = new Grid (120, 120); //Numero de elementos dentro da grelha
  let tabela = grelha.buildGrid(); //Construcao da grelha
  
  background (0);
  
  for (let c=0; c<tabela.length; c++) //Retorna o fundo da tabela
  {
    for (let l=0; l<tabela[c].length; l++) 
    {
      let fundo = fundoCelula (tabela[c][l].w, tabela[c][l].h);
      image (fundo, tabela[c][l].x, tabela[c][l].y, tabela[c][l].w, tabela[c][l].h);
    }
  }
  
  let c = 0;
  let l = 0;
  let pC = 0;
  let pL = 0;
  
  for (let i=0; i<tabela.length; i++)   //Altura e largura da grelha
  {
    while (pC === c) c = int (random (0,tabela.length) );
    while (pL === l) l = int (random (0,tabela[c].length) );
    
    let tamanho = random(10,300); //Tamanho das formas
    let forma;
    let escolheForma = int(random(1,3)); //Escolha de uma forma por célula da grelha
    
    if (escolheForma === 1) forma = Forma_Flor (tamanho/4, tamanho/4, tamanho, tamanho); //Escolha da forma Flor
    else
    if (escolheForma === 2) forma = Forma_Circulos (tamanho/4, tamanho/4, tamanho, tamanho); //Escolha da forma Circulo
    
    push(); //Rotacao da forma Flor para as diferentes celulas da grelha
      translate (tabela[c][l].x, tabela[c][l].y);
      image (forma, 0, 0, tamanho, tamanho);
    pop();
    
    pC = c;
    pL = l;
  }
  
}

function fundoCelula (largura, altura) 
{
  let composicao = createGraphics (largura, altura);
  
  composicao.strokeWeight(1);
  composicao.stroke(40); //Quando sobrepostos, distinguir-se os x's
  composicao.noFill();
  composicao.rect (0,0,composicao.width,composicao.height);
  
  return composicao;
}

function Forma_Flor (coordX,coordY, larguraFlor, alturaFlor) //Funcao da Flor, coordX = coordenada em X, coordY = coordenada em Y
{
  let composicao = createGraphics (larguraFlor, alturaFlor); //Desenha buffer de gráficos fora da tela
  
  composicao.rectMode (CENTER); //Para o retangulo ser feito apartir de seu Centro
  composicao.strokeWeight (map(larguraFlor, 10, 150, 0.1, 1)); //Efeito de arrasto
  composicao.noFill(); //Sem preenchimento para visualizacao do arrasto
  
  let anguloInicial = int(random(0,45)); //Angulo inicial da rotacao da Flor
  let anguloFinal = anguloInicial+45;  //Angulo final da rotacao da Flor
  
  for (let i=anguloInicial; i<anguloFinal; i++) 
  {
    composicao.stroke (130,140,200,map(i,0,45,0,100)); //Efeito de arrasto

    if (i %2 == 0) //Desenha o Retangulo mais interior da Flor
    {
      composicao.push(); //Salva as modificacoes feitas
        composicao.translate (larguraFlor/2,alturaFlor/2); //Translacao do Retangulo
          composicao.rotate (radians (i)); //Rotacao do Retangulo
          composicao.rect (0,0,coordX/2,coordY/2); //Retangulo
      composicao.pop(); //Reinicia as modificacoes feitas
    }

    else //Desenha o Retangulo exterior da Flor
    {
      composicao.push ();
        composicao.translate (larguraFlor/2,alturaFlor/2);
          composicao.rotate (radians(i*-2));
          composicao.rect (0,0,coordX*1.5,coordY*1.5);
      composicao.pop();
    }
    
    //Desenha o retangulo intermediario da Flor
    composicao.push(); 
      composicao.translate (larguraFlor/2, alturaFlor/2);
        composicao.rotate (radians(i*-1));
        composicao.rect(0, 0, composicao.width/1.5, composicao.height/1.5);
    composicao.pop();
  }
  
  return composicao; //Desenha a Flor
}

function Forma_Circulos (coordX, coordY, larguraCirculos, alturaCirculos) //Funcao dos Circulos, coordX = coordenada em X, coordY = coordenada em Y
{
  let composicao = createGraphics (larguraCirculos, alturaCirculos); //Desenha buffer de gráficos fora da tela
  
  composicao.noStroke(); //Sem contornos
  
  let relacao = abs(composicao.width/composicao.height); //Valor absoluto da relacao entre a altura e largura dos circulos para encaixar na grelha
  let anguloInicial = int(random(0, relacao*20)); //Angulo inicial de rotacao
  let anguloFinal = anguloInicial + relacao*20; //Angulo final de rotacao
  
  for (let i=0;i<anguloFinal;i++)
  {
    composicao.fill(255, 255, 255, map(i,0,40,0,80)); //Efeito de arrasto
    
    if (i %2 == 0)
    {
      composicao.push();
        composicao.translate (larguraCirculos/2, alturaCirculos/2);
          composicao.rotate (radians(i));
          composicao.ellipse (0, 0, coordX, coordY);
      composicao.pop();
    }

    else //Responsavel pelo efeito de arrasto ("blur")
    {
      composicao.push ();
        composicao.translate (larguraCirculos/2, alturaCirculos/2);
          composicao.rotate (radians(i*2));
          composicao.ellipse (0,0,coordX+i,coordY);
      composicao.pop();
    }
  }
  return composicao;
}

function keyPressed() 
{
  if (key == "l") redraw();
  
  if (key == "s") saveCanvas ("print", "png");
}