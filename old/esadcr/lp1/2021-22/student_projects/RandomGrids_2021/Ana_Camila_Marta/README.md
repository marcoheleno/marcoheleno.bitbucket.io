# RELATÓRIO - RANDOM GRIDS

> **Ana Inês Manuel \| 3190672, Camila Santos \| 3190003, Marta Vieira
> \| 3190695**
>
> Laboratório de Projeto I, 1º Semestre, 3º Ano
>
> ESAD.CR \| IPLEIRIA \| 2021/2022

## Introdução

Partindo da junção dos elementos do grupo de trabalho iniciámos com a
análise de ambos os briefings. Inicialmente deparámo-nos com algumas
dúvidas, mas depois de algumas reuniões e discussão entre nós
conseguimos chegar a um consenso sobre o que pretendemos fazer.

Para o projeto Random Grids é pretendido que seja desenvolvimento um
sistema de desenho automático a partir de código com o objetivo de
desenvolvimento de cartazes baseados na repetição de elementos visuais
com aleatoriedade controlada utilizando por exemplo o tamanho, a
posição, a cor ou a forma.

Após a realização de alguma pesquisa e recolha de referências foi-nos
possível chegar a um consenso em relação a que conceito/temas
gostariamos de abordar em ambos os projetos, tais como os elementos e
animações que melhor se enquadram para o resultado que pretendemos, no
entanto, com a realização de diversas experiências certas ideias e
elementos vão sendo alterados com o objetivo de melhorar o resultado
final.

## Proposta

#### Conceito

Depois para a execução de algumas experiências realizadas inicialmente
no programa P5\*js optámos por escolher \"Tempo\" como conceito e ponto
de partida, sendo que o principal objetivo é que esteticamente o cartaz
consiga representar a passagem do tempo utilizando os diversos elementos
para a sua construção, baseando-nos principalmente no movimento dos
ponteiros do relógio tendo em mente que o propósito do projeto é tentar
que o cartaz, a ser gerado mais tarde, transmita e demonstre de certa
forma a ideia a ser abordada.

#### Pesquisa

A partir da receção de ambos os briefings começámos por realizar
pesquisa sobre Design Generativo e a recolher referencias partindo do
princípio que o objetivo de ambos os projetos é desenvolver um sistema
de desenho automático e de certo modo aleatório e explicar o ciclo de
repetição.

Depois da pesquisa inicial passámos para a recolha de referências
visuais que nos ajudaram a decidir o que pretendemos realizar em cada um
dos projetos e desenvolvemos o moodboard a partir da recolha.

##### .Referências

<https://creative-coding.decontextualize.com/>

<https://creative-coding.decontextualize.com/transformations-and-functions/>

<https://editor.p5js.org/camilaS/collections/WT5LYWk5H>

<https://genekogan.com/code/p5js-transformations/>

<https://learn.digitalharbor.org/courses/creative-programming/lessons/p5-js-transformations-rotate/>

###### tutoriais:

<https://happycoding.io/tutorials/p5js/>

##### ![](README_attachments/media/image1.jpg){width="5.833333333333333in" height="4.124159011373578in"}.Moodboard

![](README_attachments/media/image2.jpg){width="5.833333333333333in"
height="4.129057305336833in"}

#### Esboços Iniciais:

#### ![](README_attachments/media/image3.jpg "fig:"){width="5.833333333333333in" height="8.241050962379703in"}

### Processo de Trabalho

Após a ideia estar estruturada e sabermos o que queremos executar.
Começamos por fazer a criação de cada um dos elementos no P5. Em
ficheiros separados realizamos os elementos e também a estrutura da
grelha para ser possível definir o local onde cada um deles aparece.

###### Coleção de experiências:

<https://editor.p5js.org/camilaS/collections/WT5LYWk5H>

Ao colocar um dos elementos na grelha deparamo-nos com um problema em
que a grelha não estava a aceitar o elemento e assim dificultou o
desenvolver do projeto. Mas após mais algumas experiências, alterações
no código e conversa com o docente conseguimos chegar a um resultado que
nos agradou, pensando sempre que as variáveis tinham de estar mais
organizadas mas não criar confusão caso fosse necessário disponibilizar
o código a outra pessoa.

![](README_attachments/media/image4.png){width="5.027777777777778in"
height="4.103472222222222in"}Inicialmente colocámos o elemento a
aparecer várias vezes e com duas cores diferentes, mas mais tarde
optámos por colocar círculos de modo a criar um cartaz mais dinamico.

image-20211214193709991

Ex.1 - <https://editor.p5js.org/camilaS/sketches/KriYDC7A0>

![image-20211214192724111](README_attachments/media/image5.png){width="5.833333333333333in"
height="4.805181539807524in"}

image-20211214192724111

Ex.2 - <https://editor.p5js.org/camilaS/sketches/D8NRhcp4D>

Depois de termos o resultado mais perto do pretendido realizamos
alterações na parte do design do futuro cartaz, sendo que ao utilizarmos
a grelha alguns dos elementos apresentaram no degradê em forma de
quadrado monstrando o seu limite.

Neste outro exemplo o conceito é o mesmo mas alteramos o elemento dos
círculos para tentar criar uma noção de movimento e não só de círculos
que aparecem. Realizamos desta forma depois de termos feito experiências
com mais formas a aparecerem e com diferentes escalas mas criava muita
confusão e então optámos por ter só dois elementos diferenciados.

![image-20211214194303388](README_attachments/media/image6.png){width="5.833333333333333in"
height="4.212962598425197in"}

image-20211214194303388

Ex.3 - <https://editor.p5js.org/camilaS/sketches/i-epv1cb>\_

Neste caso alteramos a cor de fundo e colocamos preenchimento nos
círculos, o que acontece é que com a Random Grid os elementos vão
aparecendo em diferentes zonas da tela criando assim em cada frame um
cartaz diferente. Este foi o resultado que nos agradou mais até agora
por ser mais simples e não tão confuso.

![image-20211214193002062](README_attachments/media/image7.png){width="5.436077209098863in"
height="4.2in"}

image-20211214193002062

![image-20211214193050246](README_attachments/media/image8.png){width="5.466666666666667in"
height="3.783066491688539in"}

image-20211214193050246

Ex.4 - <https://editor.p5js.org/camilaS/sketches/wBz8SCrl9>

Tecnologias Usadas

Typora

Word

P5\*js

Adobe Illustrator

Notebloc (App digitalizador)
