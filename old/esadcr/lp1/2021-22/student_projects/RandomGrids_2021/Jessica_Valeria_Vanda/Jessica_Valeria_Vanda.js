//Jéssica Duarte 3190679, Valéria Beskorovaynyy 3190663, Vanda Bica 3190668

//Exercício de avaliação: Random Grids

//Licenciatura em Design Gráfico e Multimédia (Multimédia) @ESAD.cr
//3ºano | 2º semestre 
//UC Laboratório de Projeto I | 2021/2022
//Docente: Marco Heleno
//26 de janeiro de 2022


let imagensImportadas;
let relacao, larguraImagemDesejada, alturaImagemDesejada;
let font;
let grelha;

//Definição das imagens e fonte
function preload()
{
  font = loadFont ("student_projects/RandomGrids_2021/Jessica_Valeria_Vanda/Montserrat-Regular.ttf");
  
  imagensImportadas = [];
  imagensImportadas[0] = loadImage ("student_projects/RandomGrids_2021/Jessica_Valeria_Vanda/01-Reduce.png");
  imagensImportadas[1] = loadImage ("student_projects/RandomGrids_2021/Jessica_Valeria_Vanda/02-Organize.png");
  imagensImportadas[2] = loadImage ("student_projects/RandomGrids_2021/Jessica_Valeria_Vanda/03-Time.png");
  imagensImportadas[3] = loadImage ("student_projects/RandomGrids_2021/Jessica_Valeria_Vanda/04-Learn.png");
  imagensImportadas[4] = loadImage ("student_projects/RandomGrids_2021/Jessica_Valeria_Vanda/05-Differences.png");
  imagensImportadas[5] = loadImage ("student_projects/RandomGrids_2021/Jessica_Valeria_Vanda/06-Context.png");
  imagensImportadas[6] = loadImage ("student_projects/RandomGrids_2021/Jessica_Valeria_Vanda/07-Emotion.png");
  imagensImportadas[7] = loadImage ("student_projects/RandomGrids_2021/Jessica_Valeria_Vanda/08-Trust.png");
  imagensImportadas[8] = loadImage ("student_projects/RandomGrids_2021/Jessica_Valeria_Vanda/09-Failure.png");
  imagensImportadas[9] = loadImage ("student_projects/RandomGrids_2021/Jessica_Valeria_Vanda/10-TheOne.png");

}

function setup() 
{
  let folha = new printCanvas();
  folha.createPaperCanvas ("A4", "PORTRAIT", P2D);
  // (A1 a A7), (PORTRAIT ou LANDSCAPE)

  noLoop();
}


function draw() 
{
  background (0);
  
  //Grelha
  let ia = int(random(0, imagensImportadas.length)); //ia = indice aleatório
  
  //cartaz 1 e 10 só com uma coluna, o resto dos cartazes com 5
  if (ia===0 || ia===9)
  {
    grelha = new Grid (1, 1);
  }
  else
  {
    grelha = new Grid (5, 5);
  }
  
  let tabela = grelha.buildGrid();
  
  let numeroCelulasAleatorias = int(random(6, 12));
  let c = 0;
  let l = 0;
  let pC = 0;
  let pL = 0;
  
  
  if (ia!=0 && ia!=9) 
  {
  
  for (let i=0; i<numeroCelulasAleatorias; i++)   
  {
    while (pC === c) c = int (random (0, tabela.length) );
    while (pL === l) l = int (random (0, tabela[c].length) );
    
    if (tabela[c][l].h > tabela[c][l].w) 
    {
      relacao = imagensImportadas[ia].width / imagensImportadas[ia].height;
      larguraImagemDesejada = tabela[c][l].w;
      alturaImagemDesejada = larguraImagemDesejada * relacao;
    }
    
    else 
    {
      relacao = imagensImportadas[ia].width / imagensImportadas[ia].height;
      alturaImagemDesejada = tabela[c][l].h; 
      larguraImagemDesejada = alturaImagemDesejada * relacao; 
    }
    
    push();
      translate (tabela[c][l].x, tabela[c][l].y);
      image (imagensImportadas[ia], 0, 0, larguraImagemDesejada, alturaImagemDesejada);
    pop();
    
    pC = c;
    pL = l;
  }
    }
  
  //Para não haver sobreposição das imagens
  if (ia===0 || ia===9)
  {
  for (let c=0; c<tabela.length; c++) 
  {
    for (let l=0; l<tabela[c].length; l++) 
    {
      if (tabela[c][l].h > tabela[c][l].w) 
      {
        relacao = imagensImportadas[ia].width / imagensImportadas[ia].height;
        larguraImagemDesejada = tabela[c][l].w;
        alturaImagemDesejada = larguraImagemDesejada * relacao;
      }
      else 
      {
        relacao = imagensImportadas[ia].width / imagensImportadas[ia].height;
        alturaImagemDesejada = tabela[c][l].h; 
        larguraImagemDesejada = alturaImagemDesejada * relacao; 
      }

      
      push();
        translate (tabela[c][l].x, tabela[c][l].y);
        image (imagensImportadas[ia], 0, 0, larguraImagemDesejada, alturaImagemDesejada);
      pop();
    }
  }
  }

  
  
  //Retângulo para fundo do texto
  fill(0); 
  rect (0, height/1.25, width);
  
  //Definição do texto
  textAlign(LEFT); 
  textSize (width/40);
  textFont (font);
  textStyle (NORMAL);
  fill(255);
  
  
  // Título associado às imagens
  if (ia === 0) text ("Law 01 | Reduce", width/15, height /1.15);
  else
  if (ia === 1) text ("Law 02 | Organize", width/15, height/ 1.15); 
  else
  if (ia === 2) text ("Law 03 | Time", width/15, height / 1.15); 
  else
  if (ia === 3) text ("Law 04 | Learn", width/15, height / 1.15);
  else
  if (ia === 4) text ("Law 05 | Differences", width/15, height /1.15);
  else
  if (ia === 5) text ("Law 06 | Context", width/15, height /1.15);
  else
  if (ia === 6) text ("Law 07 | Emotion", width/15, height /1.15);
  else
  if (ia === 7) text ("Law 08 | Trust", width/15, height /1.15);
  else
  if (ia === 8) text ("Law 09 | Failure", width/15, height /1.15);
  else
  if (ia === 9) text ("Law 10 | The One", width/15, height /1.15);
  
  
  // Texto associado às imagens
  textSize (width/50);
  
  if (ia === 0) text ("The simplest way to achieve simplicity is through thoughtful reduction.", width/15, height/1.1);
  else
  if (ia === 1) text ("Organization makes a systeem of many appear fewer.", width/15, height/1.1); 
  else
  if (ia === 2) text ("Savings in time feel like simplicity.", width/15, height/1.1); 
  else
  if (ia === 3) text ("Knowledge makes everything simpler.", width/15, height/1.1);
  else
  if (ia === 4) text ("Simplicity and complexity need each other.", width/15, height/1.1);
  else
  if (ia === 5) text ("What lies in the periphery of simplicity is defintely not peripheral.", width/15, height/1.1);
  else
  if (ia === 6) text ("More emotions are better than less.", width/15, height/1.1);
  else
  if (ia === 7) text ("In simplicity we trust.", width/15, height/1.1);
  else
  if (ia === 8) text ("Some things can never be made simple.", width/15, height/1.1);
  else
  if (ia === 9) text ("Simplicity is about subtracting the obvious, and adding the meaningful.", width/15, height/1.1);
}

//Tirar print e mudança de cartaz
function keyPressed() 
{
  if (key == "l") redraw();
  
  if (key == "s") saveCanvas ("print", "png");
}


function mousePressed () 
{
  redraw();
}
