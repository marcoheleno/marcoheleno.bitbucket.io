# \"Savings in time feel like simplicity\"

###### Jéssica Duarte 3190679, Valéria Beskorovaynyy 3190663, Vanda Bica 3190668

O nosso trabalho resume-se pela frase \"less is more\", refletindo o
minimalismo, seja em termos de grafismos, quer também de cores que vamos
usar, tendo em conta que será essencialmente preto e branco, ou
possíveis variações dessas mesmas cores a nível de transparência. Vamos
trabalhar um resultado gráfico que não seja demasiado composto, ou seja,
sem sobreposições de vários elementos distintos, uma vez que o grupo não
se identifica muito com esse estilo, como já referido.

Como podemos observar no Moodboard, as nossas inspirações assentam muito
sobre o rasto (passagem do tempo - um elemento gráfico específico, seja
elipse e/ou linha, deixam uma marca por onde passam - assumindo o
caminho percorrido) e também pela deformação do mesmo.

![](README_attachments/media/image1.jpg){width="5.833333333333333in"
height="5.246960848643919in"}

**Protótipo dos cartazes**

![](README_attachments/media/image2.png){width="1.5717257217847769in"
height="2.2241885389326335in"} ![Изображение выглядит как текст
Автоматически созданное
описание](README_attachments/media/image3.png){width="1.5696423884514437in"
height="2.221240157480315in"} ![Изображение выглядит как ночное небо
Автоматически созданное
описание](README_attachments/media/image4.png){width="1.5738090551181103in"
height="2.2271358267716534in"}
![](README_attachments/media/image5.png){width="1.572415791776028in"
height="2.2251640419947507in"}

![](README_attachments/media/image6.png){width="1.5824650043744533in"
height="2.2393853893263342in"} ![Изображение выглядит как текст
Автоматически созданное
описание](README_attachments/media/image7.png){width="1.5823217410323709in"
height="2.23918416447944in"} ![Изображение выглядит как наружный объект
Автоматически созданное
описание](README_attachments/media/image8.png){width="1.584394138232721in"
height="2.24211832895888in"} ![Изображение выглядит как силуэт, ночное
небо Автоматически созданное
описание](README_attachments/media/image9.png){width="1.5797222222222222in"
height="2.235507436570429in"}

![Изображение выглядит как текст Автоматически созданное
описание](README_attachments/media/image10.png){width="1.5746041119860017in"
height="2.2282611548556432in"} ![Изображение выглядит как текст
Автоматически созданное
описание](README_attachments/media/image11.png){width="1.5708333333333333in"
height="2.222924321959755in"}

**Link para o primeiro código:**

<https://editor.p5js.org/vlrab/sketches/r0p6xGJ7P>

**Referências:**

<http://lawsofsimplicity.com/los/law-3-time.html>

<https://www.fidelizarte.pt/blog/less-is-more-o-design-minimalista/>
