// Pedro Rosa
// Nº3190419
// Laboratório de Projeto 1
// 3ºano, 2021/2022
// 1ºsemestre
// Design Gráfico e Multimédia
// Esad.cr
// Marco Heleno
// 26 Janeiro 2022


//Random Grids

//Primeiro comecei por criar a variavel para que houvesse colunas diferentes dentro da grelha
let celulaAleatoria=0;

//código do professor
function setup() 
{
  let folha = new printCanvas();
  folha.createPaperCanvas ("A2", "PORTRAIT", P2D);
  // (A1 a A7), (PORTRAIT ou LANDSCAPE), não mexer

  folha.defineHiResCanvas(300);

  noLoop();
}


function draw() 
{
  //alterei os valores da grelha para o que me pareceu mais indicado para a minha ideia
  let grelha = new Grid (8, 10);
  
  let equilatero = int(random(0, 3));
  //let grelha = new Grid (equilatero, equilatero);
  
  //let grelha = new Grid ( int(random(1, 50)), int(random(1, 50)) );
  
  //grelha.randomizeColumns();
  //grelha.randomizeLines();
  
  let tabela = grelha.buildGrid();

  

  background (0);
  
  for (let c=0; c<tabela.length; c++) 
  {
    for (let l=0; l<tabela[c].length; l++) 
    {
      let forma = formaCelula (tabela[c][l].w, tabela[c][l].h);
      image (forma, tabela[c][l].x, tabela[c][l].y, tabela[c][l].w, tabela[c][l].h);
    }
  }
}


function formaCelula (largura, altura)
{ 
  let composicao = createGraphics (largura, altura);

  //ao chegar à forma da celula comecei por dar um falor a variavel que criei no inicio do código e coloquei o angleMode para defenir os angulos para graus;
  celulaAleatoria = int(random(0,3));
  
  angleMode(DEGREES);
  
  //Para que o rotate fosse diferente em cada coluna só a variavel e o rotate não chegariam, por isso, decidi utilizar o if para que caso a celulaAleatoria fosse 0 o rotate seria de 90º, se a variavel tivesse um int de 1 faria um rotate de 270. Isto acontece apenas nas colunas da grelha que compartilham este mesmo int.
  if(celulaAleatoria==0)
  {
    composicao.rotate(90);
  }
  
  if(celulaAleatoria==1)
  {
   composicao.rotate(270);
  }
  
  //Por fim comecei a criar os objetos que iriam criar a minha composição
  composicao.background (0);
  
  composicao.strokeWeight(2);
  composicao.stroke(255);
  composicao.noFill ();  

  composicao.ellipse(1, 4, 10, 10, 0, composicao.height);
  composicao.ellipse(1, 4, 24, 24, 0, composicao.height);
  composicao.ellipse(1, 4, 38, 38, 0, composicao.height);
  composicao.ellipse(1, 4, 52, 52, 0, composicao.height);
  composicao.ellipse(1, 4, 66, 66, 0, composicao.height);
  composicao.ellipse(1, 4, 80, 80, 0, composicao.height);
  composicao.ellipse(1, 4, 94, 94, 0, composicao.height);
  composicao.ellipse(1, 4, 108, 108, 0, composicao.height);
  composicao.ellipse(1, 4, 122, 122, 0, composicao.height);
  composicao.ellipse(1, 4, 136, 136, 0, composicao.height);
  composicao.ellipse(1, 4, 150, 150, 0, composicao.height);
  composicao.ellipse(1, 4, 164, 164, 0, composicao.height);
  composicao.ellipse(1, 4, 178, 178, 0, composicao.height);
  composicao.ellipse(1, 4, 192, 192, 0, composicao.height);
  composicao.ellipse(1, 4, 206, 206, 0, composicao.height);
  composicao.ellipse(1, 4, 220, 220, 0, composicao.height);
  composicao.ellipse(1, 4, 234, 234, 0, composicao.height);
  composicao.ellipse(1, 4, 248, 248, 0, composicao.height);
  
  composicao.noStroke();
  composicao.fill(0);
  composicao.ellipse(150, 168, 318, 318, 0, composicao.height);
  
  composicao.strokeWeight(3);
  composicao.stroke(255);
  composicao.noFill ();  
  composicao.ellipse(150, 168, 10,10, 0, composicao.height);
  composicao.ellipse(150, 168, 24, 24, 0, composicao.height); 
  composicao.ellipse(150, 168, 38, 38, 0, composicao.height);
  composicao.ellipse(150, 168, 52, 52, 0, composicao.height);
  composicao.ellipse(150, 168, 66, 66, 0, composicao.height);
  composicao.ellipse(150, 168, 80, 80, 0, composicao.height);
  composicao.ellipse(150, 168, 94, 94, 0, composicao.height);
  composicao.ellipse(150, 168, 108, 108, 0, composicao.height);
  composicao.ellipse(150, 168, 122, 122, 0, composicao.height);
  composicao.ellipse(150, 168, 136, 136, 0, composicao.height);
  composicao.ellipse(150, 168, 150, 150, 0, composicao.height);
  composicao.ellipse(150, 168, 164, 164, 0, composicao.height); 
  composicao.ellipse(150, 168, 178, 178, 0, composicao.height);
  composicao.ellipse(150, 168, 192, 192, 0, composicao.height);
  composicao.ellipse(150, 168, 206, 206, 0, composicao.height);
  composicao.ellipse(150, 168, 220, 220, 0, composicao.height);
  composicao.ellipse(150, 168, 234, 234, 0, composicao.height);
  composicao.ellipse(150, 168, 248, 248, 0, composicao.height);
  composicao.ellipse(150, 168, 262, 262, 0, composicao.height); 
  composicao.ellipse(150, 168, 276, 276, 0, composicao.height);
  composicao.ellipse(150, 168, 290, 290, 0, composicao.height);
  composicao.ellipse(150, 168, 304, 304, 0, composicao.height);
  composicao.ellipse(150, 168, 318, 318, 0, composicao.height);
  
  
  return composicao;
}


function keyPressed() 
{
  if (key == "l") redraw();
  
  if (key == "s") saveCanvas ("print", "png");
}

