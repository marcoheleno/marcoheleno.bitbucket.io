# Random Grids

Pedro Rosa nº3190419

A ideia seria em fundo preto e em cada 1 frame houvesse 5 rects com
fades, como se fossem bolas de fogo, a mudar a sua posição.

MoodBoard:

![](README_attachments/media/rId20.jpg){width="5.833333333333333in"
height="2.2934470691163606in"}

![](README_attachments/media/rId23.jpg){width="5.833333333333333in"
height="2.3451673228346457in"}

![](README_attachments/media/rId26.png){width="5.833333333333333in"
height="3.28125in"}

![](README_attachments/media/rId29.jpg){width="5.833333333333333in"
height="4.384367891513561in"}
