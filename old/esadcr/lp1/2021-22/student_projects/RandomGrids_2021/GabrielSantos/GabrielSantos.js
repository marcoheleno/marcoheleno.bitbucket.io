// Gabriel Santos; 3190705; LPI; 2º Fase

function setup() 
{
  let folha = new printCanvas();
  folha.createPaperCanvas ("A2", "PORTRAIT", P2D);
  // (A1 a A7), (PORTRAIT ou LANDSCAPE), não mexer

  folha.defineHiResCanvas(300);

  noLoop();
}


function draw() 
{
  let grelha = new Grid (10, 10); // Distânciamento entre os módulos das grelhas.
  
  
  let tabela = grelha.buildGrid();

  

  background (0);
  
  for (let c=0; c<tabela.length; c++) 
  {
    for (let l=0; l<tabela[c].length; l++) 
    {
      //let composicaoPerfeita = int(random(20, 900)); //tamanho e densidade dos elementos geométricos; 
      let composicaoPerfeita = int(random(width/4, height/3)); //tamanho e densidade dos elementos geométricos; 
      
      let forma = formaCelula (composicaoPerfeita, composicaoPerfeita);
      
      imageMode (CENTER);
      image (forma, 
             tabela[c][l].x+tabela[c][l].w/2, 
             tabela[c][l].y+tabela[c][l].h/2, 
             composicaoPerfeita, composicaoPerfeita);
    }
  }
  
}


function formaCelula (largura, altura)
{ 
  let composicao = createGraphics (largura, altura);
  
  composicao.noFill ();
  composicao.strokeWeight(1);

  for (let t=largura; t>0; t-=10) // distânciamento entre as circunferências
  {
composicao.stroke(0, map(t, largura, 0, 0, 255), map(t, largura, 0, 255, 0), map(t, largura, 0, 0, 255));//cor
    composicao.ellipse (composicao.width/2, composicao.height/2, t, t);
  } // cor da circunferência
  
  
  
  
  return composicao;
}


function keyPressed() 
{
  if (key == "l") redraw();
  
  if (key == "s") saveCanvas ("print", "png");
}

function windowResized()
{
  resizeCanvas (windowWidth, windowHeight);
}