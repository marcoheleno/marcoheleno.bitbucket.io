**Título**

\"Olaria\"

**Autor**

Gabriel Santos \| *3190705* \| Laboratório de Projeto I

**Palavras Chave**

Espiral; galáxia; Oleiras de Barro; Op Art; Escala; Aleatoriedade;
Coordenação; Movimento; Rasto.

**Conceito**

Tendo por base a composição do universo, e o "ecossistema de galáxias, o
movimento das oleiras de barro será replicado, através de
circunferências dentro de circunferências, com mutações de escala, com
um movimento coordenado e consistente, criando um efeito de ilusão
ótica.

**MoadBoard**

![](README_attachments/media/rId20.png){width="5.833333333333333in"
height="3.6456955380577427in"}

**StoryBoard**

![](README_attachments/media/rId23.png){width="5.833333333333333in"
height="3.6458333333333335in"}
