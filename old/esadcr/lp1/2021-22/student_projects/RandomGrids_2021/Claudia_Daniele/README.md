# O Código da Natureza

##### Relatório do Projeto Random Grids --- Laboratório de Projeto I

Cláudia Alves 3190416 \| Daniele Miranda 3190409

Uma linguagem de programação é uma ferramenta criada pelo Homem, que
utiliza algoritmos como passos na resolução de problemas. Através de
algoritmos e matemática, o ser humano esforçou-se por encontrar padrões
e tentar entender tudo o que o rodeia, percebendo que estes conceitos
matemáticos que a natureza cria naturalmente, passando a redundância,
são de certa forma, como se fossem os passos tomados pelo meio na
resolução dos seus problemas de criação. Partindo destes princípios,
decidimos procurar entender de que forma a matemática se manifesta na
natureza.

O termo *fractal* (do Latim *fractus,* que significa "quebrado") foi
cunhado pelo matemático Benoit Mandelbrot em 1975. No seu trabalho
seminal "A Geometria Fractal da Natureza", este define um fractal como
"uma forma geométrica acidentada ou fragmentada que pode ser dividida em
partes, cada uma das quais é (pelo menos aproximadamente) uma cópia em
tamanho reduzido do todo". O fractal é uma figura da geometria não
classificada, muito encontrada na natureza, para o qual as definições
tradicionais da geometria Euclidiana falham. Além de ser encontrado na
natureza (como nos flocos de neve, raios, fetos, couve romanesca, etc),
também foi aplicado em ciência, tecnologia e em arte gerada pelo
computador. O fractal é o código da natureza.

Com este projeto tomamos inspiração nesta forma geométrica e nas formas
orgânicas presentes tanto na natureza como no universo e exploramos a
simbiose entre natureza e máquina.

#### Moodboard: 

![](README_attachments/media/rId21.jpg){width="5.833333333333333in"
height="2.3363713910761157in"}

![](README_attachments/media/rId24.jpg){width="5.833333333333333in"
height="2.357638888888889in"}

![](README_attachments/media/rId27.jpg){width="5.833333333333333in"
height="2.3728291776027994in"}

#### Tecnologia: Pj5s

#### O Código da Natureza:

-   **fractal** \| *adj. 2 g.* \| \*n. m.\*\*

**frac·tal\***\* \|àct\|\
(latim \*fractus)\
*adjectivo de dois géneros*

\\1. \[Matemática\] Diz-se de objecto matemático cuja forma é irregular
e fragmentada.

*nome masculino*

\\2. \[Matemática\] Conjunto geométrico ou objecto natural cujas partes
têm a mesma estrutura (irregular e fragmentada) que o todo, mas a
escalas diferentes.

***\"fractal\"**, in Dicionário Priberam da Língua Portuguesa \[em
linha\], 2008-2021.*

-   **Sequência de Fibonacci**

Na matemática, a **sucessão de Fibonacci** (ou **sequência de
Fibonacci**), é uma sequência de números inteiros, começando normalmente
por 0 e 1, na qual cada termo subsequente corresponde à soma dos dois
anteriores.

***\"Sequência de Fibonacci\"**, in Wikipédia, 2021.*

-   **Algoritmo**

Em matemática e ciência da computação, um **algoritmo** é uma sequência
finita de ações executáveis que visam obter uma solução para um
determinado tipo de problema. Segundo Dasgupta, Papadimitriou e
Vazirani; \"Algoritmos são procedimentos precisos, não ambíguos,
padronizados, eficientes e corretos.\"

**\"Algoritmo\"**, *in Wikipédia, 2021.*
