/*
   Exercício de Avaliação Random Grids: A natureza do Código;
   Realizador por Daniele Miranda - 3190409 e Cláudia Alves - 3190416;
   3º ano letivio 2021/2022 - 5º semestre;
   UC Laboratório de Projeto 1;
   Licenciatura em Design Gráfico e Multimédia (Multimédia) ESAD.CR;
   Docente Marco Heleno;
   Avaliação final 26 de janeiro 2022;
*/




// Variaveis globais
let r, g, b, a;	//cores e alfa; 


function setup()
{ 
  createCanvas(windowWidth, windowHeight);

  let folha = new printCanvas();
  folha.createPaperCanvas ("A4", "PORTRAIT", P2D);
  // (A1 a A7), (PORTRAIT ou LANDSCAPE), não mexer
  folha.defineHiResCanvas(300);

 
  colorMode(RGB, 100); //modo de cor e alfa;
  

  //build(); //função para desenhar a grelha;
  noLoop();
}


function draw()
{ 
  
  background(0); // cor do background;
    
  r=random(255); //desenhar as elipses com uma cor aleatória;
  g=random(255);  
  b=random(255);
  
  let grelha = new Grid (4, 6);  //linhas e colunas da grelha;
  let tabela = grelha.buildGrid();

  for (let c=0; c<tabela.length; c++) //percorre a grelha para desenhar a composição;
  {
    for (let l=0; l<tabela[c].length; l++) //percorre a grelha para desenhar a composição;
    {
      let percentualReducao = [0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7];  //declaração dos valores para redução da composição dentro da célula;
      let reduzirEm = percentualReducao[int(random(0, percentualReducao.length))]; //definição aleatória do valor de redução da célula;
      let larguraMax = tabela[c][l].w - (tabela[c][l].w * reduzirEm);  //aplicação do percentual de redução na largura das elipses;
      let alturaMax = tabela[c][l].h - (tabela[c][l].h * reduzirEm);  //aplicação do percentual de redução na altura das elipses;
      for (let i = 0; i < 20; i++) //desenho da composição de 20 elipses;
      {  
        let forma = formaCelula (tabela[c][l].w, tabela[c][l].h, larguraMax, alturaMax);  //criação da composição com as 20 elipses em diferentes tamanhos;
        image (forma, tabela[c][l].x, tabela[c][l].y, tabela[c][l].w, tabela[c][l].h);  //desenha imagem no canvas a partir da composição criada;
      }
    }
  }
}


function formaCelula (larguraMaxComposicao, alturaMaxComposicao, maxLarguraElipse, maxAlturaElipse) //função para formar as células;
{ 
  
  let composicao = createGraphics(larguraMaxComposicao, alturaMaxComposicao); //cria um objeto de composição, "folha em branco";

  
  //variáveis;
  a=random(30,70);  //variação do alfa das elipses, mantém o alfa baixo criando alguma transparência;
  let larguraElipse=random(1,maxLarguraElipse);  //variação da largura das elipses;
  let alturaElipse=random(1,maxAlturaElipse);  //variação da altura das elipses;

  //desenhar as elipses na composição;  
  composicao.noFill();  //sem preenchimento;
  composicao.strokeWeight(random(1,5));  //variação da espessura do stroke;
  composicao.stroke(r, g, b, a);  //cores e alfa;
  composicao.ellipse(composicao.width/2,composicao.height/2,larguraElipse,alturaElipse);  //desenhar as elipses no centro da composição e tamanho da altura e largura;
  
  
  return composicao;
}


function keyPressed() 
{
  if (key == "l") { 
    build(); //redesenhar a grelha
  }
  
  if (key == "s") saveCanvas ("print", "png");
}