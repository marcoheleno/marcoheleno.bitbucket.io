

document.addEventListener("DOMContentLoaded", function() 
{
  get_editor_reference_from_url();
});


function get_editor_reference_from_url() 
{
  const url_query = window.location.search;
  let editor_reference = "1f89caf1-21ef-4bbd-925c-999ee52ae614";

  if (url_query != "") 
  {
    const url_param = new URLSearchParams (url_query);
    editor_reference = url_param.get('ref');
  }

  load_editor_with_reference_in_iframe (editor_reference);
}


function load_editor_with_reference_in_iframe (editor_reference) 
{
  const url = "https://create.arduino.cc/editor/dizprofessor/" + editor_reference + "/preview?embed";
  document.getElementsByTagName("iframe")[0].setAttribute("src", url);
}


function load_url_with_editor_reference (editor_reference) 
{
  const url = window.location.origin + window.location.pathname + "?ref=" + editor_reference;
  location.assign(url);
}

